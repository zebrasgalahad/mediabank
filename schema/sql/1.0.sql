ALTER TABLE `true_romance_asset_index` 
CHANGE COLUMN `asset_type` `asset_type` VARCHAR(45) NOT NULL ,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`object_id`, `asset_type`);
;
