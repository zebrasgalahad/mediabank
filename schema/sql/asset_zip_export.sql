CREATE TABLE IF NOT EXISTS `asset_zip_export` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `CreationDate` DATETIME NULL,
    `StartDate` DATETIME NULL,
    `EndDate` DATETIME NULL,
    `Locale` TEXT NULL,
    `UserId` INT NULL,
    `CatalogId` INT NULL,
    `JobStatus` TEXT NULL,
    `exportType` TEXT NULL,
    PRIMARY KEY (`id`)
);

