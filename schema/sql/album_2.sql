UPDATE `true_romance_album`
SET `Locale` = 'de_DE';

UPDATE `true_romance_album_asset`
SET `Locale` = 'de_DE';

UPDATE `true_romance_album_user`
SET `Locale` = 'de_DE';