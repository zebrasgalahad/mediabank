ALTER TABLE `true_romance_album`
ADD `Locale` TEXT NULL;

ALTER TABLE `true_romance_album_asset`
ADD `Locale` TEXT NULL;

ALTER TABLE `true_romance_album_user`
ADD `Locale` TEXT NULL;