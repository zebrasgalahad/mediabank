CREATE TABLE IF NOT EXISTS `tasks` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `CreationDate` DATETIME NULL,
    `StartDate` DATETIME NULL,
    `EndDate` DATETIME NULL,
    `UserId` INT NULL,
    `JobStatus` TEXT NULL,
    `SerializedData` LONGBLOB NULL,
    `ErrorMessage` TEXT NULL,
    `type` TEXT NULL,
    PRIMARY KEY (`id`)
);

