<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */
declare(strict_types = 1);

namespace UserProfileBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CustomerProfile extends AbstractType {

    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('change_customerCompany', TextType::class, [
                    'label' => 'change_customer.Company',
                    'disabled' => true
                ])
                ->add('change_customerSalutation', ChoiceType::class, [
                    'label' => 'change_customer.Salutation',
                    'choices' => [
                        'Mr.' => '0',
                        'Mrs.' => '1'
                    ],
                    'choices_as_values' => true,
                    'multiple' => false,
                    'expanded' => true,
                    'placeholder' => false,
                    'required' => false,
                    'data' => $options["data"]["salutationValue"]
                ])
                ->add('change_customerTitle', TextType::class, [
                    'label' => 'change_customer.Title',
                    'required' => false
                ])
                ->add('change_customerFirstname', TextType::class, [
                    'label' => 'change_customer.Firstname',
                    'disabled' => true
                ])
                ->add('change_customerLastname', TextType::class, [
                    'label' => 'change_customer.Lastname',
                    'disabled' => true
                ])
                ->add('change_customerStreet', TextType::class, [
                    'label' => 'change_customer.Street',
                    'required' => false
                ])
                ->add('change_customerCity', TextType::class, [
                    'label' => 'change_customer.City',
                    'required' => false
                ])
                ->add('change_customerZip', TextType::class, [
                    'label' => 'change_customer.Zip',
                    'required' => false
                ])
                ->add('change_customerCountry', ChoiceType::class, [
                    'label' => 'change_customer.Country',
                    'choices' => [
                        'Germany' => 'DE',
                        'Austria' => 'AT',
                        'Switzerland' => 'CH'
                    ],
                    'required' => false,
                    'placeholder' => false,
                    'data' => $options["data"]["countryCode"]
                ])
                ->add('change_customerPhone', TextType::class, [
                    'label' => 'change_customer.Phone',
                    'required' => false
                ])
                ->add('change_customerEmail', EmailType::class, [
                    'label' => 'change_customer.Email',
                    'required' => false
                ])
                ->add('_abort_data_change', ButtonType::class, [
                    'label' => 'customer.abort.data.change'
                ])
                ->add('_submit', SubmitType::class, [
                    'label' => 'customer.datachange'
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix() {
        // we need to set this to an empty string as we want _username as input name
        // instead of login_form[_username] to work with the form authenticator out
        // of the box
        return '';
    }

    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver) {
        
    }
}
