<?php

namespace UserProfileBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class UserProfileBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/userprofile/js/pimcore/startup.js'
        ];
    }
}
