<?php

namespace UserProfileBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends FrontendController
{
    /**
     * @Route("/user_profile")
     */
    public function indexAction(Request $request)
    {
        return new Response('Hello world from user_profile');
    }
}
