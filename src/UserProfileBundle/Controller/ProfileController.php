<?php

namespace UserProfileBundle\Controller;

use UserProfileBundle\Form\UserProfilePasswordChangeType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use UserProfileBundle\Form\CustomerProfile;
use UserAuthenticationBundle\Library\Security\UserToken as UserTokenUtility;
use TrueRomanceBundle\Controller\PermissionController;

/**
 * Description of Profile
 *
 * @author lukadrezga
 */
class ProfileController extends PermissionController {
    
    /**
     *
     * @var Config
     */
    protected $config;
    
    /**
     *
     * @var UserTokenUtility
     */
    protected $token;
    
    /**
     *
     * @var \UserAuthenticationBundle\Library\Security\Authenticator
     */
    protected $userAuth;
    
    /**
     *
     * @var Pimcore\Translation\Translator
     */
    protected $translator;    
    
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
        parent::__construct($container);
        
        $this->config = \Pimcore\Config::getWebsiteConfig()->configuration;
        
        $this->token = $container->get("userauthentication.user.token");
        
        $this->userAuth = $container->get("userauthentication.user.auth");
        
        $this->translator = $container->get("translator");
    }

    /**
    * Index page for account - it is restricted to ROLE_USER via security annotation
    *
    * @return RedirectResponse
    */
    public function indexAction(Request $request) {
        if (!$this->isGranted('ROLE_USER')) {
            return $this->getLocaleRootRedirect();
        }
        
        $userProfileIndexPage = $this->config->getUser_Profile_Page()->getFullPath();
        
        $customer = $this->getUser();

        $salutationText = $customer->getSalutation() === "0" ? $this->translator->trans("Mr.") : $this->translator->trans("Mrs.");
        
        $changeDataUserButtonText = $this->translator->trans("change.user.data.button.text");
        
        $countryCode = $customer->getCountryCode();
        
        $userCountry = $this->translator->trans($countryCode);
        
        $userCompany = $customer->getCompany();

        $formData = [
            'salutation' => $salutationText,
            'salutationValue' => $customer->getSalutation(),
            'title' => $customer->getTitle(),
            'lastName' => $customer->getLastName(),
            'firstName' => $customer->getFirstName(),
            'street' => $customer->getStreet(),
            'city' => $customer->getCity(),
            'zip' => $customer->getZip(),
            'country' => $userCountry,
            'countryCode' => $countryCode,
            'phone' => $customer->getPhone(),
            'email' => $customer->getEmail(),
        ]; 

        $form = $this->createForm(CustomerProfile::class, $formData, []);
        $this->view->form = $form->createView();

        $this->view->customer = $customer;
        $this->view->customerData = $formData;
        $this->view->changeUserDataButtonText = $changeDataUserButtonText;

        if ($request->getMethod() === Request::METHOD_POST) {

            $user = $this->getUser();

            $customerSalutation = $request->get("change_customerSalutation");
            $cutomerTitle = $request->get("change_customerTitle");
            $customerStreet = $request->get("change_customerStreet");
            $customerCity = $request->get("change_customerCity");
            $customerZip = $request->get("change_customerZip");
            $customerPhone = $request->get("change_customerPhone");
            $customerEmail = $request->get("change_customerEmail");
            $customerCountryCode = $request->get("change_customerCountry");

            $customerData = [
                "salutation" => $customerSalutation,
                "title" => $cutomerTitle,
                "street" => $customerStreet,
                "city" => $customerCity,
                "zip" => $customerZip,
                "countryCode" => $customerCountryCode,
                "phone" => $customerPhone,
                "email" => $customerEmail
            ];

            foreach ($customerData as $key => $value) {
                    $setter = 'set' . ucfirst($key);

                    $user->$setter($value);
            }

            $errors = [];

            try {
                $user->save();
            } catch (\Exception $ex) {
                $errors[] = $this->translator->trans("user.data.change.error");
            }

            $this->view->errors = $errors;

            return new RedirectResponse($userProfileIndexPage);
        }

        $passwordChanged = $request->get("change-password-success");

        $this->view->passwordChanged = $passwordChanged;
        $this->view->hideNav = true;
        $this->view->hideBreadcrumb = true;
    }

    public function changePasswordAction(Request $request) {
        if ($this->container->get("userauthentication.user.auth")->isAllowed() === false) {
            return $this->getLocaleRootRedirect();
        }

        $session = $request->getSession();

        $form = $this->createForm(UserProfilePasswordChangeType::class, $formData, []);

        $this->view->form = $form->createView();
        
        $userProfileIndexPage = $this->config->getUser_Profile_Page()->getFullPath();
        
        $this->view->userProfileIndexPage = $userProfileIndexPage;

        if ($request->getMethod() === Request::METHOD_POST) {
            $encoder = $this->container->get("cmf.security.user_password_encoder_factory");

            $userEncoder = $encoder->getEncoder($this->getUser());
            /* @var $userEncoder \Pimcore\Security\Encoder\PasswordFieldEncoder */

            $user = $this->getUser();

            $currentPassword = $user->getPassword();

            $currentGivenPassword = $request->get("_currentPassword");

            $isCurrentPasswordValid = $userEncoder->isPasswordValid($currentPassword, $currentGivenPassword, $user->getUsername());

            $errors = [];

            if ($isCurrentPasswordValid === false) {
                $errors[] = $this->translator->trans("current.password.must.match.error");
            }

            $password1 = $request->get("_password1");
            $password2 = $request->get("_password2");

            if ($password1 !== $password2) {
                $errors[] = $this->translator->trans("password.change.must.match.error");
            }

            $valid = preg_match('/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])((.){8,32})$/', $password1);

            if ($valid === 1 && $isCurrentPasswordValid === true) {
                if ($password1 === $password2) {
                    $hashedPassword = $userEncoder->encodePassword($password1, $user->getUsername());

                    $user->setPassword($hashedPassword);

                    $user->save();

                    return new RedirectResponse($userProfileIndexPage . "?change-password-success=1");
                }
            }
            if ($valid === 0 || $isCurrentPasswordValid === false) {
                $errors[] = $this->translator->trans("password.change.not.valid");
            }

            $this->view->errors = $errors;
        }
    }

    public function paymentDataAction(Request $request) {
        //insert code for user payment data here
    }

    public function invoiceDataAction(Request $request) {
        //insert code for user invoice data here
    }
    
    /**
     * 
     * @param \Pimcore\Model\DataObject\Customer $customer
     * @param string $email
     * @return string
     */
    private function updateCustomerUniqueId(\Pimcore\Model\DataObject\Customer $customer, $email) {
      
        $customerId = "{$email}";
        
        return $customerId;
    }
    
}
