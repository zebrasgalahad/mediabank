pimcore.registerNS("pimcore.plugin.UserProfileBundle");

pimcore.plugin.UserProfileBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.UserProfileBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("UserProfileBundle ready!");
    }
});

var UserProfileBundlePlugin = new pimcore.plugin.UserProfileBundle();
