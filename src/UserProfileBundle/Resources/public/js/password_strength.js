$(document).ready(function () {
    var password1 = $(".change-password-1 input#_password1");
    var result = $(".container .login-block #result");

    password1.on("input", function () {
        console.log("The text has been changed.");
        result.html(checkStrength(password1.val()));
    });

    // TODO: messages need to be translatable
    function checkStrength(password) {
        var strength = 0;
        if (password.length < 8) {
            result.removeClass();
            result.addClass('short');
            return 'Too short';
        }
        if (password.length > 8)
            strength += 1;
        // If password contains both lower and uppercase characters, increase strength value.
        if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))
            strength += 1;
        // If it has numbers and characters, increase strength value.
        if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))
            strength += 1;
        // If it has one special character, increase strength value.
        if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;
        // If it has two special characters, increase strength value.
        if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,%,&,@,#,$,^,*,?,_,~])/))
            strength += 1;
        // Calculated strength value, we can return messages
        if (strength < 2) {
            result.removeClass();
            result.addClass('weak');
            return 'Weak';
        } else if (strength == 2) {
            result.removeClass();
            result.addClass('good');
            return 'Good';
        } else {
            result.removeClass();
            result.addClass('strong');
            return 'Strong';
        }
    }
});

