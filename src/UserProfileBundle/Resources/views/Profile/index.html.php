<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<?php echo $this->inc("/includes/header");?>

<?php if ($this->passwordChanged !== NULL && $this->passwordChanged === "1"): ?>
    <div class="alert alert-success password-successfully-changed">
        <?php echo $this->translate("user.profile-change-password-successful-message"); ?>
    </div>
<?php endif; ?>

<div class="page-header">
    <h1><?php echo $this->translate("account.welcome", [$customer->getFirstname(), $customer->getLastname()]) ?></h1>
</div>

<div class="text-block">
    <div class="container">
        <div class="profile">
            <?php if (count($this->errors) > 0): ?>
                <?php foreach ($this->errors as $errorMessage) : ?>
                    <div class="alert alert-danger"><?php echo $errorMessage; ?></div>
                <?php endforeach; ?>
            <?php endif; ?> 
            <div class="customer-name">
                <p><?php echo trim($customerData["salutation"]) . " " . trim($customerData["title"]) . " " . trim($customerData["firstName"]) . " " . trim($customerData["lastName"]); ?></p>
            </div>
            <div class="customer-street">
                <p><?php echo trim($customerData["street"]); ?></p>
            </div>
            <div class="customer-city">
                <p><?php echo trim($customerData["zip"]) . " " . trim($customerData["city"]); ?></p>
            </div>
            <div class="customer-land">
                <p><?php echo trim($customerData["country"]); ?></p>
            </div>
            <div class="customer-phone">
                <p><?php echo trim($customerData["phone"]); ?></p>
            </div>
            <div class="customer-email">
                <p><?php echo trim($customerData["email"]); ?></p>
            </div>
            <div class="data-change-block">
                <input type="button" value="<?php echo $changeUserDataButtonText; ?>">
            </div>
        </div>
    </div>
</div>

<div class="text-block-data-change" hidden="">
    <div class="container-data-change">
        <div class="profile-data-change">
            <?php if (count($this->errors) > 0): ?>
                <?php foreach ($this->errors as $errorMessage) : ?>
                    <div class="alert alert-danger"><?php echo $errorMessage; ?></div>
                <?php endforeach; ?>
            <?php endif; ?>  
            <?php echo $this->form()->start($form); ?>
            <?php echo $this->form()->row($form['change_customerSalutation'], array("attr" => array("value" => $customerData["salutationValue"]))); ?>
            <?php echo $this->form()->row($form['change_customerTitle'], array("attr" => array("value" => $customerData["title"]))); ?>
            <?php echo $this->form()->row($form['change_customerFirstname'], array("attr" => array("value" => $customerData["firstName"]))); ?>
            <?php echo $this->form()->row($form['change_customerLastname'], array("attr" => array("value" => $customerData["lastName"]))); ?>
            <?php echo $this->form()->row($form['change_customerStreet'], array("attr" => array("value" => $customerData["street"]))); ?>
            <?php echo $this->form()->row($form['change_customerCity'], array("attr" => array("value" => $customerData["city"]))); ?>
            <?php echo $this->form()->row($form['change_customerZip'], array("attr" => array("value" => $customerData["zip"]))); ?>
            <?php echo $this->form()->row($form['change_customerCountry']); ?>
            <?php echo $this->form()->row($form['change_customerPhone'], array("attr" => array("value" => $customerData["phone"]))); ?>
            <?php echo $this->form()->row($form['change_customerEmail'], array("attr" => array("value" => $customerData["email"]))); ?>
            <div class="submit-block text-left">
                <?php
                echo $this->form()->widget($form['_submit'],
                        [
                    'attr' => [
                        'class' => 'btn btn-primary btn-block button grey'
                    ]
                ]);
                ?>
            </div>
            <div class="abort-data-change text-right">
                <?php
                echo $this->form()->widget($form['_abort_data_change'],
                        [
                    'attr' => [
                        'class' => 'btn btn-primary btn-block button red'
                    ]
                ]);
                ?>
            </div>
                <?php echo $this->form()->end($form); ?>
        </div>
    </div>
</div>

<?php echo $this->inc("/includes/footer");?>

<script> 
$(document).ready(function() {
    
var dataChangeButton = $(".data-change-block input");
var dataChangeBlock = $(".text-block-data-change");
var userInformationBlock = $(".text-block");
var abortDataChangeButton = $(".abort-data-change button");

dataChangeButton.on("click", function(){
        dataChangeBlock.show();
        userInformationBlock.hide();
    });
    
abortDataChangeButton.on("click", function() {
    dataChangeBlock.hide();
    userInformationBlock.show();
    });
});
</script>