<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
?>

<?php echo $this->inc("/includes/header");?>

<div class="container">
    <div class="login-block">
        <img src="/../static/img/logo.png" alt="StanleyBlack&Decker Logo">
        
        <?php if($success): ?>
            <div class="alert alert-success">
                <?php echo $this->translate("login.reset-password-success-message"); ?>
            </div>
        <?php else: ?>
            <?php if(count($this->errors) > 0): ?>
                <?php foreach($this->errors as $errorMessage) : ?>
                    <div class="alert alert-danger"><?php echo $errorMessage; ?></div>
                <?php endforeach; ?>
            <?php endif; ?>
                    
            <?php echo $this->form()->start($form); ?>
            <div class="current-password">
                  <?php echo $this->form()->row($form['_currentPassword']); ?>      
            </div>
            <div class="password-hint">
                <h3><?php echo $this->translate("user.password.change.hint.title"); ?></h3>
                <p><?php echo "1. " . $this->translate("user.password.change.hint_1"); ?></p>
                <p><?php echo "2. " . $this->translate("user.password.change.hint_2"); ?></p>
            </div>
            <div class="password-change-recommendation">
                <h3><?php echo $this->translate("user.password.change.recommendation.title"); ?></h3>
                <p><?php echo "1. " . $this->translate("user.password.change.recommendation_1"); ?></p>
                <p><?php echo "2. " . $this->translate("user.password.change.recommendation_2"); ?></p>   
            </div>
            <div class="change-password-1">
                  <?php echo $this->form()->row($form['_password1']); ?>      
            </div>
            <span id="result"></span>
            <div class="change-password-2">
                  <?php echo $this->form()->row($form['_password2']); ?>      
            </div>
            <div class="submit-block text-right">
                <?php echo $this->form()->widget($form['_submit'], [
                    'attr' => [
                        'class' => 'btn btn-primary btn-block button red'
                    ]
                ]); ?>
            </div>
            <?php echo $this->form()->end($form); ?>
        <?php endif; ?>
    </div>
</div>

<div class="row justify-content-center mt-4">
    <div class="col-4 text-center">

        <p><a href="<?php echo $userProfileIndexPage; ?>">&larr; <?php echo $this->translate("back.to.user.profile.message"); ?></a></p>

    </div>
</div>

<?php echo $this->inc("/includes/footer"); ?>