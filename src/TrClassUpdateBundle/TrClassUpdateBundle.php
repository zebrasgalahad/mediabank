<?php

namespace TrClassUpdateBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class TrClassUpdateBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/trclassupdate/js/pimcore/startup.js'
        ];
    }
    
    public function getContainerExtension()
    {
        return new DependencyInjection\TrClassUpdateExtension();
    }    
    
}
