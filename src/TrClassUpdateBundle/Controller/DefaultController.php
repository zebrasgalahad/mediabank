<?php

namespace TrClassUpdateBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends FrontendController
{

    /**
     * @Route("/tr_class_update")
     */
    public function indexAction(Request $request)
    {
        return new Response('Forbidden', Response::HTTP_FORBIDDEN);
    }
}
