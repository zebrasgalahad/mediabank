<?php

namespace TrClassUpdateBundle\Library;

use TrClassUpdateBundle\Library\Config;
use Pimcore\Model\DataObject\ClassDefinition\Listing as ClassDefinitionListing;
use Pimcore\Model\DataObject\ClassDefinition\Service as ClassDefinitionService;
use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;
use TrClassUpdateBundle\Library\Helper;

class Export
{
    protected $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function export()
    {
        CliHelper::section("CUSTOM LAYOUTS EXPORT");
        $this->exportCustomLayouts();

        CliHelper::section("CLASSES EXPORT");
        $this->exportClasses();

        CliHelper::section("OBJECTBRICKS EXPORT");
        $this->exportObjectBricks();

        CliHelper::section("FIELDCOLLECTIONS EXPORT");
        $this->exportFieldcollections();
    }

    public function exportCustomLayouts($schemaBasePath = null, $returnOnly = false)
    {
        if ($schemaBasePath === null) {
            $schemaBasePath = Helper::getSchemaPath(Config::SCHEMA_TYPE_CUSTOM_LAYOUTS);
        }

        $customLayoutListing = new \Pimcore\Model\DataObject\ClassDefinition\CustomLayout\Listing();

        $customLayouts = $customLayoutListing->load();

        $return = [];
        foreach ($customLayouts as $customLayout) {
            $filename = "custom_definition_{$customLayout->id}.json";

            $identifier = "{$customLayout->name}({$customLayout->id})";

            unset($customLayout->creationDate);
            unset($customLayout->modificationDate);
            unset($customLayout->userOwner);
            unset($customLayout->userModification);
            unset($customLayout->fieldDefinitions);

            $json = json_encode($customLayout, JSON_PRETTY_PRINT);

            $return["custom_definition_{$customLayout->id}"] = json_decode($json, true);

            $targetFilepath = $schemaBasePath . "/" . $filename;

            if ($returnOnly === false) {
                file_put_contents($targetFilepath, $json);

                CliHelper::success("Custom layout '{$identifier}' was exported to {$targetFilepath}.");
            }
        }

        return $return;
    }

    public function exportClasses($schemaBasePath = null, $returnOnly = false)
    {
        if ($schemaBasePath === null) {
            $schemaBasePath = Helper::getSchemaPath(Config::SCHEMA_TYPE_CLASSES);
        }

        $classesList = new ClassDefinitionListing;
        $classesList->setOrderKey('name');
        $classesList->setOrder('asc');

        $classes = $classesList->load();

        $return = [];
        foreach ($classes as $class) {
            /* @var $class Pimcore\Model\DataObject\ClassDefinition */
            $name = $class->getName();

            $filename = "class_" . $name . ".json";

            $json = ClassDefinitionService::generateClassDefinitionJson($class);

            $return[$name] = json_decode($json, true);

            $targetFilepath = $schemaBasePath . "/" . $filename;

            if ($returnOnly === false) {
                file_put_contents($targetFilepath, $json);

                CliHelper::success("Class '{$name}' was exported to {$targetFilepath}.");
            }
        }

        return $return;
    }

    public function exportObjectBricks($schemaBasePath = null, $returnOnly = false)
    {
        $type = Config::SCHEMA_TYPE_OBJECTBRICKS;

        if ($schemaBasePath === null) {
            $schemaBasePath = Helper::getSchemaPath($type);
        }

        $objectBrickListing = new \Pimcore\Model\DataObject\Objectbrick\Definition\Listing();

        $objectBricks = $objectBrickListing->load();

        $return = [];
        foreach ($objectBricks as $objectBrick) {
            /* @var $class Pimcore\Model\DataObject\ClassDefinition */
            $key = $objectBrick->getKey();

            $filename = "$type_" . $key . ".json";

            $json = ClassDefinitionService::generateObjectBrickJson($objectBrick);

            $return[$key] = json_decode($json, true);

            $targetFilepath = $schemaBasePath . "/" . $filename;
            if ($returnOnly === false) {
                file_put_contents($targetFilepath, $json);

                CliHelper::success("Objectbrick '{$key}' was exported to {$targetFilepath}.");
            }
        }

        return $return;
    }

    public function exportFieldcollections($schemaBasePath = null, $returnOnly = false)
    {
        $type = Config::SCHEMA_TYPE_FIELDCOLLECTIONS;

        if ($schemaBasePath === null) {
            $schemaBasePath = Helper::getSchemaPath($type);
        }

        $fieldCollectionListing = new \Pimcore\Model\DataObject\Fieldcollection\Definition\Listing();

        $fieldCollections = $fieldCollectionListing->load();

        $return = [];
        foreach ($fieldCollections as $fieldCollection) {
            /* @var $class Pimcore\Model\DataObject\ClassDefinition */
            $key = $fieldCollection->getKey();

            $filename = "$type_" . $key . ".json";

            $json = ClassDefinitionService::generateFieldCollectionJson($fieldCollection);

            $return[$key] = json_decode($json, true);

            $targetFilepath = $schemaBasePath . "/" . $filename;
            if ($returnOnly === false) {
                file_put_contents($targetFilepath, $json);

                CliHelper::success("Fieldcollection '{$key}' was exported to {$targetFilepath}.");
            }
        }

        return $return;
    }
}
