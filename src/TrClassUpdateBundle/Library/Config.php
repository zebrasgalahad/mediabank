<?php

namespace TrClassUpdateBundle\Library;

class Config
{
    private $updateUserId = 3;
    private $dumpKeepDays = 10;
    const SCHEMA_TYPE_CLASSES = 'classes';
    const SCHEMA_TYPE_OBJECTBRICKS = 'objectbricks';
    const SCHEMA_TYPE_FIELDCOLLECTIONS = 'fieldcollections';
    const SCHEMA_TYPE_CUSTOM_LAYOUTS = 'customlayouts';
    const FILE_TYPE_CLASS = 'class';
    const FILE_TYPE_OBJECTBRICK = 'objectbrick';
    const FILE_TYPE_FIELDCOLLECTION = 'fieldcollection';
    const FILE_TYPE_CUSTOM_LAYOUT = 'customlayouts';
    const SCHEMA_TMP_DIRECTORY = 'tmp';

    public function __construct()
    {
        if ($tcubUdpateUserId = \Pimcore\Model\WebsiteSetting::getByName("tcub_update_user_id")) {
            $this->updateUserId = $tcubUdpateUserId->getData();
        }

        if ($tcubDumpKeepDays = \Pimcore\Model\WebsiteSetting::getByName("tcub_dump_keep_days")) {
            $this->dumpKeepDays = $tcubDumpKeepDays->getData();
        }
    }

    public function getUpdateUserId(): int
    {
        return $this->updateUserId;
    }

    public function getDumpKeepDays(): int
    {
        return $this->dumpKeepDays;
    }
}
