<?php

namespace TrClassUpdateBundle\Library;

class Helper
{

    public static function normalizeFilename($filename)
    {
        $filename = preg_replace('/[^a-zA-Z0-9]+/', '', $filename);
        $filename = preg_replace("/^[0-9]+/", "", $filename);        
        
        return $filename;
    }


    public static function getFilesFromDirectory($path): array
    {
        return preg_grep('/^([^.])/', scandir($path));
    }
    
    public static function getSchemaPath($type): string
    {
        $path = PIMCORE_PROJECT_ROOT . "/schema/$type";

        if (is_dir($path) === false) {
            throw new \Exception("$path is not a directory or has the wrong type. Allowed: classes, bricks, fieldcollections, customlayouts");
        }

        return $path;
    }

    public static function getTempPath(): string
    {
        $path = PIMCORE_PROJECT_ROOT . "/schema/tmp";

        if (is_dir($path) === false) {
            throw new \Exception("$path is not a directory and is required.");
        }

        return $path;
    }

    public static function getLogPath(): string
    {
        $path = PIMCORE_PROJECT_ROOT . "/schema/logs";

        if (is_dir($path) === false) {
            throw new \Exception("$path is not a directory and is required.");
        }

        return $path;
    }    
    
    public static function getBackupPath(): string
    {
        $path = PIMCORE_PROJECT_ROOT . "/schema/backups";

        if (is_dir($path) === false) {
            throw new \Exception("$path is not a directory and is required.");
        }

        return $path;
    }    
    
    public static function getClassesPath(): string
    {
        $path = PIMCORE_PROJECT_ROOT . "/var/classes";

        return $path;
    }        
    
    public static function getSchemaTypeTempPath($type): string
    {
        $path = PIMCORE_PROJECT_ROOT . "/schema/tmp/$type";

        if (is_dir($path) === false) {
            mkdir($path, 0775, true);
        }

        return $path;
    }

    public static function clearTempDirectory()
    {
        self::deleteDirectory(self::getTempPath());
    }

    public static function deleteDirectory($path)
    {
        $i = new \DirectoryIterator($path);

        foreach ($i as $f) {
            if (strpos($f, ".gitkeep") !== false) {
                continue;
            }
            
            if ($f->isFile()) {
                unlink($f->getRealPath());
            } else if (!$f->isDot() && $f->isDir()) {
                self::_deleteDirectory($f->getRealPath());
            }
        }
    }

    private static function _deleteDirectory($path)
    {
        $i = new \DirectoryIterator($path);
        
        foreach ($i as $f) {
            if (strpos($f, ".gitkeep") !== false) {
                continue;
            }

            if ($f->isFile()) {
                unlink($f->getRealPath());
            } else if (!$f->isDot() && $f->isDir()) {
                self::_deleteDirectory($f->getRealPath());
            }
        }

        rmdir($path);        
    }
}
