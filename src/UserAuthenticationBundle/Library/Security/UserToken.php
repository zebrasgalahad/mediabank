<?php

namespace UserAuthenticationBundle\Library\Security;

use \Pimcore\Model\DataObject\Customer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Handles token für password forgotten action
 *
 * @author mariobaer
 */
class UserToken {

    const DEFAULT_TOKEN_LIFETIME = 14;
    
    /**
     *
     * @var Container 
     */
    private $container;
    
    public function __construct(Container $container) {
        $this->container = $container;
    }
    
    /**
     * 
     * @param Customer $customer
     * @return \Pimcore\Model\DataObject\UserToken
     */
    public function create(Customer $customer) {
        $hash = password_hash($customer->getEmail(), PASSWORD_BCRYPT);
        
        $parentFolderId = $customer->getId(); 
        
        $tokenInstance = new \Pimcore\Model\DataObject\UserToken();
        
        $tokenInstance->setGenerated_Token($hash);
        
        $tokenInstance->setParentId($parentFolderId);
        
        $tokenInstance->setKey($customer->getEmail() . "_" . time());
        
        $tokenInstance->setGenerated_Time(new \DateTime());
        
        $tokenInstance->setCustomer($customer);
        
        $tokenInstance->setPublished(true);
        
        $tokenInstance->save();

        return $tokenInstance;
    }

    /**
     * 
     * @param string $token
     * @return \Pimcore\Model\DataObject\UserToken
     */
    public function getTokenInstanceByToken($token) {
        $tokenListing = \Pimcore\Model\DataObject\UserToken::getByGenerated_Token($token);
        
        return $tokenListing->current();
    }
    
    /**
     * 
     * @param type $token
     * @return type
     */
    public function getCustomerByToken($token) {
        $tokenInstance = $this->getTokenInstanceByToken($token);
        
        return $tokenInstance->getCustomer();
    }

    /**
     * 
     * @param string $token
     * @return bool
     */
    public function tokenIsValid(string $token) : bool {
        if($this->getTokenInstanceByToken($token) instanceof \Pimcore\Model\DataObject\UserToken) {
            $tokenInstance = $this->getTokenInstanceByToken($token);
            
            if ($tokenInstance->getPublished() === false) {
                return false;
            }
            
            $tokenLifetime = $this->getTokenLifetime();
            
            $tokenTimestamp = $tokenInstance->getGenerated_Time()->getTimestamp();
            
            $difference = time() - $tokenTimestamp;

            if($difference <= $tokenLifetime) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $token
     */
    public function unvalidateToken($token) {
        $tokenInstance = $this->getTokenInstanceByToken($token);
        
        $tokenInstance->setPublished(false);
        
        $tokenInstance->save();
    }
    
    private function getTokenLifetime() {
        $tokenLifetime = $this->container->get("trueromance.object.config")->get("Password_Forgotten_Token_Lifetime");
        
        if(!$tokenLifetime) {
            $tokenLifetime = $this->DEFAULT_TOKEN_LIFETIME;
        }

        return $tokenLifetime * ((60 * 60) * 24);
    }
    
}
