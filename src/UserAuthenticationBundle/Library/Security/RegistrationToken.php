<?php

namespace UserAuthenticationBundle\Library\Security;

use \Pimcore\Model\DataObject\Customer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

/**
 * Handles token für password forgotten action
 *
 * @author mariobaer
 */
class RegistrationToken {

    const DEFAULT_TOKEN_LIFETIME = 14;
    
    /**
     *
     * @var Container 
     */
    private $container;
    
    public function __construct(Container $container) {
        $this->container = $container;
    }
    
    /**
     * 
     * @return \Pimcore\Model\DataObject\RegistrationToken
     */
    public function create($email, $customerId) {
        $hash = password_hash($email, PASSWORD_BCRYPT);
        
        $parentFolderId = $customerId;
        
        $tokenInstance = new \Pimcore\Model\DataObject\RegistrationToken();
        
        $tokenInstance->setGenerated_Token($hash);
        
        $tokenInstance->setParentId($parentFolderId);
        
        $tokenInstance->setKey($email . "_" . time());
        
        $tokenInstance->setGenerated_Time(new \DateTime());
        
        $tokenInstance->setEmail($email);
        
        $tokenInstance->setPublished(true);
        
        $tokenInstance->save();

        return $tokenInstance;
    }

    /**
     * 
     * @param string $token
     * @return \Pimcore\Model\DataObject\RegistrationToken
     */
    public function getTokenInstanceByToken($token) {

        $tokenListing = \Pimcore\Model\DataObject\RegistrationToken::getByGenerated_Token($token)->current();

        return $tokenListing;
    }

    /**
     * 
     * @param string $token
     * @return bool
     */
    public function tokenIsValid(string $token) : bool {
        if($this->getTokenInstanceByToken($token) instanceof \Pimcore\Model\DataObject\RegistrationToken) {
            $tokenInstance = $this->getTokenInstanceByToken($token);
            
            if ($tokenInstance->getPublished() === false) {
                return false;
            }
            
            $tokenLifetime = $this->getTokenLifetime();
            
            $tokenTimestamp = $tokenInstance->getGenerated_Time()->getTimestamp();
            
            $difference = time() - $tokenTimestamp;

            if($difference <= $tokenLifetime) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param string $token
     */
    public function unvalidateToken($token) {
        $tokenInstance = $this->getTokenInstanceByToken($token);
        
        $tokenInstance->setPublished(false);
        
        $tokenInstance->save();
    }
    
    private function getTokenLifetime() {
        $tokenLifetime = $this->container->get("trueromance.object.config")->get("Password_Forgotten_Token_Lifetime");
        
        if(!$tokenLifetime) {
            $tokenLifetime = $this->DEFAULT_TOKEN_LIFETIME;
        }

        return $tokenLifetime * ((60 * 60) * 24);
    }
    
}
