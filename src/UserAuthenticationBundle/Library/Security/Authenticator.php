<?php

namespace UserAuthenticationBundle\Library\Security;

use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;

class Authenticator extends AbstractGuardAuthenticator
{
    /**
     *
     * @var AuthenticationUtils
     */
    private $authenticationUtils;

    /**
     *
     * @var Container
     */
    private $container;

    /**
     *
     * @var \TrueRomanceBundle\Library\Object\Config
     */
    protected $config;

    /**
     *
     * @var UserToken
     */
    protected $token;

    /**
     *
     * @var Authenticator
     */
    protected $userAuth;

    /**
     *
     * @var Pimcore\Translation\Translator
     */
    protected $translator;

    /**
     * @var EncoderFactoryInterface
     */
    protected $encoderFactory;

    /**
     *
     * @param AuthenticationUtils $authenticationUtils
     * @param Container $container
     */
    public function __construct(AuthenticationUtils $authenticationUtils, Container $container, EncoderFactoryInterface $encoderFactory)
    {
        $this->container = $container;

        $this->authenticationUtils = $authenticationUtils;

        $this->config = \Pimcore\Config::getWebsiteConfig()->configuration;

        $this->token = $container->get("userauthentication.user.token");

        $this->userAuth = $container->get("userauthentication.user.auth");

        $this->translator = $container->get("translator");

        $this->encoderFactory = $encoderFactory;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning false will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        return ($request->get("_username") !== null && $request->get("_password")) || $request->get("_tok");
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        if($request->get("_username") && $request->get("_password")) {
            return array(
                'user' => $request->get("_username"),
                'password' => $request->get("_password"),
            );
        } else {
            return array(
                'token' => $request->get("_tok"),
            );
        }
    }

    public function getUser($credentials, UserProviderInterface $userProvider) {
        $userListingEmail = \Pimcore\Model\DataObject\Customer::getByEmail($credentials["user"]);

        if ($userListingEmail->current() !== null && $userListingEmail->current() !== false) {
            return $userListingEmail->current();
        }

        throw new AuthenticationException($this->translator->trans('user.login.bad.credentials'));
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        if($credentials["token"])  {
            return true;
        }

        if($user->getActive() !== true) {
            return false;
        }

        if($user->getPassword() === "" || $user->getPassword() === null) {
            return $user->getZip() === $credentials["password"];
        }

        if($user->getPassword() !== "") {
            $userEncoder = $this->encoderFactory->getEncoder($user);
            /* @var $userEncoder \Pimcore\Security\Encoder\PasswordFieldEncoder */

            return $userEncoder->isPasswordValid($user->getPassword(), $credentials["password"], $user->getUsername());
        }

        return false;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
        $targetLink = $this->config->getLogin_Success_Page()->getFullPath();

        $response = new \Symfony\Component\HttpFoundation\RedirectResponse($targetLink);

        return $response;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        if(!$request->get("_tok")) {
            $request->attributes->set(Security::AUTHENTICATION_ERROR, $this->translator->trans('user.login.bad.credentials'));
        }
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = array(
            // you might translate this message
            'message' => 'Authentication Required'
        );

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
