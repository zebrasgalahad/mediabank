<?php

namespace UserAuthenticationBundle\Library\Security;

use \Pimcore\Model\DataObject\Customer;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Security\Core\Security;

class User {

    /**
     *
     * @var Container 
     */
    private $container;

    /**
     *
     * @var Customer 
     */
    private $user;

    /**
     * 
     * @param Container $container
     * @param Security $security
     */
    public function __construct(Container $container, Security $security) {
        $this->container = $container;

        $this->initUser();
    }

    private function initUser() {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }

        if (!\is_object($user = $token->getUser())) {
            return;
        }

        $this->user = $user;
    }

    /**
     * 
     * @return boolean
     */
    public function isAllowed() {
        return $this->user !== null;
    }

}
