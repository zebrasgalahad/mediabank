<?php

namespace UserAuthenticationBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class UserAuthenticationBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/userauthentication/js/pimcore/startup.js'
        ];
    }
}
