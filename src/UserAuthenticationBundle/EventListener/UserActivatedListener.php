<?php

namespace UserAuthenticationBundle\EventListener;

use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject\Customer;

class UserActivatedListener {

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    private $container;

    /**
     *
     * @var \Pimcore\Config 
     */
    private $config;

    /**
     *
     * @var \Pimcore\Translation\Translator 
     */
    private $translator;

    /**
     *
     * @var \UserAuthenticationBundle\Model\Mailer
     */
    private $mailer;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\RequestStack 
     */
    private $request;

    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;

        $this->config = \Pimcore\Config::getWebsiteConfig()->configuration;

        $this->translator = $container->get("translator");

        $this->mailer = $container->get("userauthentication.mailer");

        $this->request = $container->get("request_stack")->getCurrentRequest();
    }

    /**
     * @param DataObjectEvent $event
     */
    public function onPreUpdate(DataObjectEvent $event)
    {
        $object = $event->getObject();
        
        if ($object instanceof Customer) {
            if ($this->request && $this->request->getRequestUri() === "/admin/object/save?task=publish") {
                $data = json_decode($this->request->get("data"));
                
                if ($this->shouldSendActivationMail($object->getId(), $data) === true) {
                    $this->mailer->sendUserActivatedMail($object);
                }
            }
        }
    }
    
    /**
     * 
     * @param string $customerId
     * @param array $postData
     * @return boolean
     */
    private function shouldSendActivationMail($customerId, $postData)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();
        
        $query = "SELECT active FROM {$this->getCustomerTable()} WHERE oo_id = ?";
        
        $isActive = $db->fetchOne($query, [$customerId]);
        
        if ($isActive === "0" && (bool) $postData->active === true) {
            return true;
        }
        
        return false;
    }
    
     /**
     * 
     * @return string
     */
    private function getCustomerTable()
    {
        $customerObject = new \Pimcore\Model\DataObject\Customer();

        $class = $customerObject->getClass()->getId();

        $customerTable = "object_query_{$class}";

        return $customerTable;
    }

}
