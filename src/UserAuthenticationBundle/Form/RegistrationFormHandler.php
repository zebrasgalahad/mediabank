<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace UserAuthenticationBundle\Form;

use CustomerManagementFrameworkBundle\Model\CustomerInterface;
use Symfony\Component\Form\Form;

class RegistrationFormHandler
{
    protected function getFormDataMapping(): array {
        return [
            "customerFirstname" => [
                "object" => "no",
                "field" => "firstname",
                "placeholder" => "customer.Firstname"
            ],
            "customerLastname" => [
                "object" => "no",
                "field" => "lastname",
                "placeholder" => "customer.Lastname"
            ],
            "customerEmail" => [
                "object" => "no",
                "field" => "email",
                "placeholder" => "customer.Email"
            ],
            "customerCompany" => [
                "object" => "no",
                "field" => "company",
                "placeholder" => "customer.Company"
            ],
            "customerCustomerNumber" => [
                "object" => "no",
                "field" => "customerNumber",
                "placeholder" => "customer.CustomerNumber"
            ],
            "customerDepartment" => [
                "object" => "no",
                "field" => "department",
                "placeholder" => "customer.Department",
            ],
        ];
    }

    protected function getCustomerMapping(): array
    {
        $mapping = $this->getFormDataMapping();
        $mapping["customerPassword"] = [
            "object" => "no",
            "field" => "password"
        ];

        return $mapping;
    }

    /**
     * Builds initial form data
     *
     * @param CustomerInterface $customer
     *
     * @return array
     */
    public function buildFormData(CustomerInterface $customer): array
    {
        $formData = [];
        foreach ($this->getFormDataMapping() as $formField => $customerProperty) {
            $getter = 'get' . ucfirst($customerProperty["field"]);
            
            $value = $customer->$getter();
            
            if (!$value) {
                continue;
            }

            $formData[$formField] = $value;
        }
        
        return $formData;
    }

    /**
     * Maps form values to customer
     *
     * @param CustomerInterface $customer
     * @param Form $form
     */
    public function updateCustomerFromForm(CustomerInterface $customer, Form $form)
    {
        if (!$form->isSubmitted() || !$form->isValid()) {
            throw new \RuntimeException('Form must be submitted and valid to apply form data');
        }
        
        $formData = $form->getData();
        
        foreach ($this->getCustomerMapping() as $formField => $customerProperty) {
            $value = $formData[$formField] ?? null;
            
            $setter = 'set' . ucfirst($customerProperty["field"]);

            if (!$value) {
                continue;
            }

            $customer->$setter($value);
        }
    }
    
    /**
     * 
     * @param CustomerInterface $customer
     * @return array
     */
    public function getDepartmentOptions(CustomerInterface $customer)
    {
        $options = \TrueRomanceBundle\Library\Tools\ObjectOption::getAttributeInstanceOptions($customer, "department", false);
        
        $finalData = [];
        
        foreach ($options as $option) {
            $finalData[$option["key"]] = $option["value"];
        }
        
        return $finalData;
    }
}