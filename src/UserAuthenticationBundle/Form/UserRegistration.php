<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

declare(strict_types=1);

namespace UserAuthenticationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UserRegistration extends AbstractType
{
    /**
     * @inheritDoc
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('customerFirstname', TextType::class, [
                    'label' => 'customer.Firstname',
                    "required" => true
                ])
                ->add('customerLastname', TextType::class, [
                    'label' => 'customer.Lastname',
                    "required" => true
                ])
                ->add('customerEmail', EmailType::class, [
                    'label' => 'customer.Email',
                    "required" => true
                ])
                ->add('customerPassword', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'invalid_message' => $options["data"]["translator"]->trans('password.must.match'),
                    'options' => ['attr' => ['class' => 'password-field']],
                    'required' => true,
                    'first_options'  => [
                        'label' => 'customer.Password',
                        'constraints' => [
                        new NotBlank([
                            'message' =>  $options["data"]["translator"]->trans('password.required.message'),
                                ]),
                        new Regex([
                            'pattern' => '/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])((.){8,32})$/',
                            'message' => $options["data"]["translator"]->trans("password.pattern.does.not.match"),
                                ]),
                        ]
                    ],
                    'second_options' => [
                        'label' => 'customer.Password2',
                        'constraints' => [
                        new NotBlank([
                            'message' =>  $options["data"]["translator"]->trans('password.required.message'),
                                ]),
                        new Regex([
                            'pattern' => '/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])((.){8,32})$/',
                            'message' => $options["data"]["translator"]->trans("password.pattern.does.not.match"),
                                ]),
                        ]
                    ],
                ])
                ->add('customerCompany', TextType::class, [
                    'label' => 'customer.Company',
                    "required" => true
                ])
                ->add('customerCustomerNumber', TextType::class, [
                    'label' => 'customer.CustomerNumber',
                    "required" => false
                ])
                ->add('customerDepartment', ChoiceType::class, [
                    'label' => 'customer.Department',
                    'placeholder' => 'customer.department.choose.option',
                    "required" => false,
                    'choices' => $options["data"]["department_options"],
                    'attr' => ['class' => 'select2']
                ])
                ->add('_check', ButtonType::class, [
                    'label' => 'to.terms.of.use'
                ])
                ->add('_submit', SubmitType::class, [
                    'label' => 'customer.register'
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getBlockPrefix()
    {
        // we need to set this to an empty string as we want _username as input name
        // instead of login_form[_username] to work with the form authenticator out
        // of the box
        return '';
    }
    /**
     * @inheritDoc
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
