<?php

namespace UserAuthenticationBundle\Model;

/**
 * Creates HTML for email body
 *
 * @author lukadrezga
 */
class EmailBodyHtml {
    
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    private $container;
    
    /**
     *
     * @var \Pimcore\Translation\Translator 
     */
    private $translator;
    
    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;
        
        $this->translator = $container->get("translator");
    }
   
    /**
     * 
     * @param \Pimcore\Mail $mail
     * @param array $data
     * @return string
     */
    public function createEmailBodyHtml(\Pimcore\Mail $mail, $data = [])
    {
        $params = [];
        
        $bodyHtml = $mail->getBodyHtmlRendered();
        
        preg_match_all('/\{\{.*?\}\}/m', $bodyHtml, $matches, PREG_SET_ORDER, 0);
        
        foreach ($matches as $match) {
            $matchType = str_replace(["{{", "}}"], "", $match[0]);

            $explodedMatch = explode(".", $matchType);

            $getter = "get" . ucfirst($explodedMatch[1]);
            
            $attribute = $explodedMatch[1];
            
            if (method_exists($data[$explodedMatch[0]], $getter) === true) {
                $params[$match[0]] = $data[$explodedMatch[0]]->$getter();
            } else if (property_exists($data[$explodedMatch[0]], $attribute) === true) {
                $params[$match[0]] = $data[$explodedMatch[0]]->$attribute;
            } 
        }
        
        foreach ($params as $replacement => $param) {
            $bodyHtml = str_replace($replacement, $param, $bodyHtml);
        }
        
        return $bodyHtml;
    }
}
