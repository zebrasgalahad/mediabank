<?php

namespace UserAuthenticationBundle\Model;

use Symfony\Component\HttpFoundation\Request;
use TrueRomanceBundle\Library\Tools\Server;

/**
 * Used to send emails by different jobs (user registration, password forgot, password successfully set)
 *
 * @author lukadrezga
 */
class Mailer {
    
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    private $container;
    
    /**
     *
     * @var \Pimcore\Config 
     */
    private $config;
    
    /**
     *
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;
    
    /**
     *
     * @var \Pimcore\Translation\Translator 
     */
    private $translator;
    
    /**
     *
     * @var \UserAuthenticationBundle\Model\EmailBodyHtml
     */
    private $bodyHtml;
    
    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;
        
        $this->config = \Pimcore\Config::getWebsiteConfig()->configuration;

        $this->translator = $container->get("translator");
        
        $this->request = $container->get("request_stack")->getCurrentRequest();
        
        $this->bodyHtml = $container->get("userauthentication.body.html");
        
    }

    /**
     * 
     * @param \Pimcore\Model\DataObject\UserToken $userToken
     * @param string $requestUri
     */
    public function sendInitialPasswordMail(\Pimcore\Model\DataObject\UserToken $userToken, $requestUri)
    {
        $customer = $userToken->getCustomer();

        $customerEmail = $customer->getEmail();

        $url = $requestUri . "?_tok=" . urlencode($userToken->getGenerated_Token());
        
        $mail = new \Pimcore\Mail("Stanley Black & Decker – Mediathek | Relaunch");
        
        $mail->addTo($customerEmail);
        
        $key = "de_inital_password_email_template";
        
        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);
        
        $mail->setDocument($documentObject);
        
        $config = new \stdClass();
        
        $config->domain = "pim.sbdinc.de";
        
        $config->token = $url;
        
        $params = [
            "customer" => $customer,
            "config" => $config
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);
        
        $mail->setBodyHtml($bodyHtml);
        
        $mail->send();
    }
    
    /**
     * 
     * @param \Pimcore\Model\DataObject\UserToken $userToken
     * @param string $requestUri
     */
    public function sendChangePasswordMail(\Pimcore\Model\DataObject\UserToken $userToken, $requestUri)
    {
        $customer = $userToken->getCustomer();

        $customerEmail = $customer->getEmail();

        $url = $requestUri . "?_tok=" . urlencode($userToken->getGenerated_Token());
      
        $language = $this->getLanguage();
        
        $mail = new \Pimcore\Mail($this->translator->trans("password.forgotten.mail.subject"));
        
        $mail->addTo($customerEmail);
        
        $key = "{$language}_change_password_email_template";
        
        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);
        
        $mail->setDocument($documentObject);
        
        $config = new \stdClass();
        
        $config->domain = $this->getDomain();
        
        $config->token = $url;
        
        $params = [
            "customer" => $customer,
            "config" => $config
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);
        
        $mail->setBodyHtml($bodyHtml);
        
        $mail->send();
    }

    /**
     * 
     * @param Customer $customer
     */
    public function sendPasswordSuccessfullyChangedMail(\Pimcore\Model\DataObject\Customer $customer)
    {

        $customerEmail = $customer->getEmail();

        $language = $this->getLanguage();
        
        $mail = new \Pimcore\Mail($this->translator->trans("password.successfully.set.mail.subject"));
        
        $mail->addTo($customerEmail);
        
        $key = "{$language}_password_change_successful_email_template";
        
        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);
        
        $mail->setDocument($documentObject);
        
        $loginPage = $this->config->getLogin_Page()->getFullPath();
        
        $domain = $this->getDomain();
        
        $fullLoginUrlPath = $domain . $loginPage;
        
        $config = new \stdClass();
        
        $config->domain = $this->getDomain();
        
        $config->login = $fullLoginUrlPath;
        
        $params = [
            "customer" => $customer,
            "config" => $config,
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);
        
        $mail->setBodyHtml($bodyHtml);
        
        $mail->send();
    }

    /**
     * 
     * @param \Pimcore\Model\DataObject\RegistrationToken $registrationToken
     * @param string $requestUri
     * @param \Pimcore\Model\DataObject\Customer $customer
     */
    public function sendUserRegistrationMail(\Pimcore\Model\DataObject\RegistrationToken $registrationToken, $requestUri, \Pimcore\Model\DataObject\Customer $customer)
    {
        $customerEmail = $customer->getEmail();
        
        $language = $this->getLanguage();
        
        $url = $requestUri . "?_tok=" . urlencode($registrationToken->getGenerated_Token());
        
        $mail = new \Pimcore\Mail($this->translator->trans("user.registration.mail.subject"));
        
        $mail->addTo($customerEmail);
        
        $key = "{$language}_user_registration_email_template";
        
        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);
        
        $mail->setDocument($documentObject);
        
        $bcc = $this->container->get("trueromance.object.config")->get("INVITATION_PERMISSION_NOTICE_MAILS", $this->request->getLocale());
        
        if(trim($bcc) !== "") {
            $explodedMails = explode(",", $bcc);
            
            $mail->setBcc($explodedMails);
        }

        $config = new \stdClass();
        
        $config->domain = $this->getDomain();
        
        $config->email = $customerEmail;
        
        $config->token = $url;
        
        $params = [
            "customer" => $customer,
            "config" => $config,
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);
        
        $mail->setBodyHtml($bodyHtml);
        
        $mail->send();
    }
    
    public function sendUserActivatedMail(\Pimcore\Model\DataObject\Customer $customer)
    {
        $customerEmail = $customer->getEmail();
        
        $language = $customer->getRegistered_Language();
        
        $explodedLocale = explode("_", $language);
        
        if (count($explodedLocale) > 1) {
            $language = $explodedLocale[0];
        }
        
        if (!$language) {
            throw new \Exception("Keine Sprache (Registered_Language) in Customer Objekt gesetzt!");
        }
        
        $mail = new \Pimcore\Mail($this->translator->trans("user.activated.mail.subject", [], null, "de_DE"));

        $mail->addTo($customerEmail);

        $key = "{$language}_user_activated_mail";

        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);

        $mail->setDocument($documentObject);

        $loginPage = $this->config->getLogin_Page()->getFullPath();

        $domain = \TrueRomanceBundle\Library\Tools\Server::getBaseUrl();

        $fullLoginUrlPath = $domain . $loginPage;

        $config = new \stdClass();

        $config->domain = $domain;

        $config->login = $fullLoginUrlPath;

        $params = [
            "customer" => $customer,
            "config" => $config,
        ];

        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);

        $mail->setBodyHtml($bodyHtml);

        $mail->send();
    }
    
    public function sendUserInvitationEmail(\Pimcore\Model\DataObject\RegistrationToken $registrationToken, Request $request)
    {
        $mail = new \Pimcore\Mail($this->translator->trans("user.invitation.mail.subject"));
        
        $customerEmail = $registrationToken->getEmail();
        
        $mail->addTo($customerEmail);
        
        $language = $this->getLanguage();

        $key = "{$language}_user_invite_email_template";

        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);

        $mail->setDocument($documentObject);
        
        $requestUri = $request->getUri();

        $message = $request->get("message");

        $userRegisterPage = $this->config->getUser_Registration_Page()->getFullPath();

        $explodedUri[] = explode("/", $requestUri);

        $uri = $explodedUri[0][0] . "//" . $explodedUri[0][2] . $userRegisterPage . "?email=" . $customerEmail;

        $url = $uri . "&_token=" . urlencode($registrationToken->getGenerated_Token());
        
        $config = new \stdClass();
        
        $config->message = $message;
        
        $config->url = $url;
        
        $params = [
            "config" => $config,
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);

        $mail->setBodyHtml($bodyHtml);

        $mail->send();
    }
    
    public function sendExternalUserAlbumShare($emailAdress, $albumId)
    {
        $mail = new \Pimcore\Mail($this->translator->trans("user.album.share.subject"));
        
        $mail->addTo($emailAdress);
        
        $language = $this->getLanguage();

        $key = "{$language}_user_share_external_email_template";

        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);
        
        $mail->setDocument($documentObject);
        
        $config = new \stdClass();
        
        $config->url = Server::getBaseUrl($this->container->get("trueromance.object.config")->get("ALBUMS_EXTERNAL_ALBUM_PAGE")->getFullpath()) . "?id=" . $albumId;
        
        $config->email = $emailAdress;
        
        $params = [
            "config" => $config,
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);

        $mail->setBodyHtml($bodyHtml);
        
        $mail->send();
    }    

    public function sendInternalUserAlbumShare($userId, $albumId)
    {
        $user = \Pimcore\Model\DataObject\Customer::getById($userId);
        
        $album = $this->container->get("trueromance.album.service")->getAlbumOnly($albumId);
        
        $mail = new \Pimcore\Mail($this->translator->trans("user.album.share.subject"));
        
        $mail->addTo($user->getEmail());
        
        $language = $this->getLanguage();

        $key = "{$language}_user_share_internal_email_template";

        $documentObject = $this->getEmailTemplateFromWebsiteSettings($key);
        
        $mail->setDocument($documentObject);
        
        $config = new \stdClass();
        
        $config->album = $album["name"];
        
        $params = [
            "config" => $config,
            "user" => $user
        ];
        
        $bodyHtml = $this->bodyHtml->createEmailBodyHtml($mail, $params);

        $mail->setBodyHtml($bodyHtml);
        
        $mail->send();
    }        
    
    /**
     * 
     * @return string
     */
    private function getLocale() {
        return $this->request->getLocale();
    }
    
    /**
     * 
     * @return string
     */
    public function getLanguage() {
        return explode("_", $this->getLocale())[0];
    }
    
    /**
     * 
     * @return string
     */
    private function getDomain() {
        return \TrueRomanceBundle\Library\Tools\Server::getBaseUrl();
    }
    
    /**
     * 
     * @param string $key
     * @return \Pimcore\Model\Document
     */
    private function getEmailTemplateFromWebsiteSettings($key) : \Pimcore\Model\Document {
        $websiteConfiguration = \Pimcore\Config::getWebsiteConfig();
        
        return $websiteConfiguration->$key;
    }
    
}
