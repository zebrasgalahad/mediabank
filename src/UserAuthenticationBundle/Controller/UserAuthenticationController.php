<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace UserAuthenticationBundle\Controller;

use UserAuthenticationBundle\Form\LoginFormType;
use UserAuthenticationBundle\Form\PasswordChangeType;
use UserAuthenticationBundle\Form\RegistrationFormHandler;
use CustomerManagementFrameworkBundle\CustomerProvider\CustomerProviderInterface;
use CustomerManagementFrameworkBundle\Model\CustomerInterface;
use CustomerManagementFrameworkBundle\Security\Authentication\LoginManagerInterface;
use CustomerManagementFrameworkBundle\Security\OAuth\OAuthRegistrationHandler;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use Pimcore\Bundle\EcommerceFrameworkBundle\IEnvironment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use UserAuthenticationBundle\Library\Security\UserToken as UserTokenUtility;
use UserAuthenticationBundle\Library\Security\RegistrationToken as RegistrationTokenUtility;
use UserAuthenticationBundle\Form\UserRegistration;
use TrueRomanceBundle\Controller\BaseController;
use Pimcore\Model\DataObject;

/**
 * Class UserAuthenticationController
 *
 * Controller that handles all account functionality, including register, login and connect to SSO profiles
 */
class UserAuthenticationController extends BaseController {

    /**
     *
     * @var \TrueRomanceBundle\Library\Object\Config 
     */
    protected $config;

    /**
     *
     * @var UserTokenUtility 
     */
    protected $token;

    /**
     *
     * @var RegistrationTokenUtility 
     */
    protected $regtoken;

    /**
     *
     * @var \UserAuthenticationBundle\Library\Security\Authenticator
     */
    protected $userAuth;

    /**
     *
     * @var Pimcore\Translation\Translator 
     */
    protected $translator;

    /**
     *
     * @var \UserAuthenticationBundle\Model\Mailer
     */
    private $mailer;

    /**
     *
     * @var string
     */
    private $errorCode;

    /**
     *
     * @var string
     */
    private $userInviteText;

    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->config = \Pimcore\Config::getWebsiteConfig()->configuration;

        $this->token = $container->get("userauthentication.user.token");

        $this->userAuth = $container->get("userauthentication.user.auth");

        $this->regtoken = $container->get("userauthentication.registration.token");

        $this->mailer = $container->get("userauthentication.mailer");

        $this->translator = $container->get("translator");
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }
    
    public function defaultAction(Request $request)
    {
        
    }

    /**
     * @param AuthenticationUtils $authenticationUtils
     * @param OAuthRegistrationHandler $oAuthHandler
     * @param UserInterface|null $user
     * @return RedirectResponse
     */
    public function loginAction(
    AuthenticationUtils $authenticationUtils, OAuthRegistrationHandler $oAuthHandler, UserInterface $user = null, Request $request
    )
    {
        //redirect user to user profile page if logged in
        if ($this->container->get("trueromance.user.permissions")->isLoggedIn() === true 
                && $this->container->get("trueromance.user.permissions")->isAllowed("view_mediathek") === false) {
            $mediathekPage = $this->config->getMISC_INDEX_PAGE()->getFullPath();
            
            return $this->redirect($mediathekPage);
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        $formData = [
            '_username' => $lastUsername
        ];

        $loginLink = $this->config->getLogin_Page()->getFullPath();

        $passwordForgottenLink = $this->config->getPassword_Forgotten_Page()->getFullPath();

        $userRegistrationLink = $this->config->getUser_Registration_Page()->getFullPath();

        $form = $this->createForm(LoginFormType::class, $formData, [
            'action' => $loginLink,
        ]);

        $this->view->form = $form->createView();
        $this->view->passwordForgotLink = $passwordForgottenLink;
        $this->view->userRegistrationLink = $userRegistrationLink;
        $this->view->error = $error;
        $this->view->hideNav = true;
        $this->view->hideBreadcrumb = true;
    }

    /**
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function passwordForgotAction(Request $request)
    {
        $loginPage = $this->config->getLogin_Page()->getFullPath();

        $passwordForgotPage = $this->config->getPassword_Forgotten_Page()->getFullPath();

        if ($request->get("_tok")) {
            $targetLink = $this->config->getPassword_Initial_Set_Page()->getFullPath();

            $response = new RedirectResponse($targetLink . "?_t=" . $request->get("_tok"));

            return $response;
        }

        if ($request->getMethod() === 'POST' && $userIdentification = $request->get("_email")) {
            $valid = filter_var($userIdentification, FILTER_VALIDATE_EMAIL);

            //save email in session for later use
            $request->getSession()->set("email", $userIdentification);

            //if the email schema is not valid, throw error
            if (!$valid) {
                return new RedirectResponse($request->getRequestUri() . "?error_code=4");
            }

            $user = null;

            $userListingEmail = \Pimcore\Model\DataObject\Customer::getByEmail($userIdentification);

            if ($userListingEmail->current() !== null && $userListingEmail->current() !== false) {
                $user = $userListingEmail->current();
            }

            if ($user) {
                $tokenInstance = UserTokenUtility::create($user);

                $this->mailer->sendChangePasswordMail($tokenInstance, $request->getUri());

                return new RedirectResponse($request->getRequestUri() . "?success=1");
            }
        }

        if ($request->get("success")) {
            $this->view->success = 1;
        }

        if ($request->get("error_code")) {
            $this->view->error = $this->getErrorMessageByErrorCode($request->get("error_code"), $request->getSession()->get("email"));
        }

        $this->view->loginPage = $loginPage;
        $this->view->passwordForgotPage = $passwordForgotPage;
        $this->view->email = $request->getSession()->get("email");
    }

    /**
     * 
     * @param Request $request
     * @return RedirectResponse
     */
    public function passwordSetAction(Request $request)
    {
        $form = $this->createForm(PasswordChangeType::class, $formData, []);

        $this->view->form = $form->createView();

        $passwordInitialSetPage = $this->config->getPassword_Initial_Set_Page()->getFullPath();

        if ($request->getMethod() === Request::METHOD_POST) {
            if ($this->isTokenValid($request->get("_t")) === false) {
                return new RedirectResponse($passwordInitialSetPage . "?error_code={$this->getErrorCode()}");
            }

            $encoder = $this->container->get("cmf.security.user_password_encoder_factory");

            $user = $this->token->getCustomerByToken($request->get("_t"));

            $userEncoder = $encoder->getEncoder($user);
            /* @var $userEncoder \Pimcore\Security\Encoder\PasswordFieldEncoder */

            $password1 = $request->get("_password1");
            $password2 = $request->get("_password2");

            $errors = [];
            if ($password1 !== $password2) {
                $errors[] = $this->translator->trans("password.change.must.match.error");
            }

            $valid = preg_match('/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])((.){8,32})$/', $password1);

            if ($valid === 1) {
                if ($password1 === $password2) {
                    $hashedPassword = $userEncoder->encodePassword($password1, $user->getUsername());

                    $user->setPassword($hashedPassword);

                    $user->save();

                    $this->mailer->sendPasswordSuccessfullyChangedMail($user);

                    $this->token->unvalidateToken($request->get("_t"));

                    $this->view->success = true;

                    return new RedirectResponse($passwordInitialSetPage . "?success=1");
                }
            }
            if ($valid === 0) {
                $errors[] = $this->translator->trans("password.change.not.valid");
            }

            $this->view->errors = $errors;
        }

        $this->view->errorMessage = $this->getErrorMessageByErrorCode($request->get("error_code"));
        if ($request->get("success")) {
            $this->view->success = 1;
        }

        $this->view->loginPage = $this->config->getLogin_Page()->getFullPath();
    }

    /**
     * If registration is called with a registration key, the key will be used to look for an existing OAuth token in
     * the session. This OAuth token will be used to fetch user info which can be used to pre-populate the form and to
     * link a SSO identity to the created customer object.
     *
     * This could be further separated into services, but was kept as single method for demonstration purposes as the
     * registration process is different on every project.
     *
     * @param Request $request
     * @param CustomerProviderInterface $customerProvider
     * @param LoginManagerInterface $loginManager
     * @param OAuthRegistrationHandler $oAuthHandler
     * @param RegistrationFormHandler $registrationFormHandler
     * @param string|null $registrationKey
     * @param UserInterface|null $user
     *
     * @return Response|null
     */
    public function registerAction(
    Request $request, CustomerProviderInterface $customerProvider, OAuthRegistrationHandler $oAuthHandler, LoginManagerInterface $loginManager,
            RegistrationFormHandler $registrationFormHandler, UserInterface $user = null, SessionInterface $session, IEnvironment $environment)
    {
        $successParam = $request->get("user-registration");

        $userProfilePage = $this->config->getUser_Profile_Page()->getFullPath();

        $loginPage = $this->config->getLogin_Page()->getFullPath();

        $userRegisterPage = $this->config->getUser_Registration_Page()->getFullPath();

        if ($this->isSuccessRegistrationPage($successParam) === false) {

            $token = $request->get("_token");
            
            if (!$token) {
                return new RedirectResponse($loginPage);
            }

            $registrationToken = RegistrationTokenUtility::getTokenInstanceByToken($token);
            
            if(!$registrationToken) {
                return new Response("Invaild or already used token!", 403);
            }
            
            $email = $registrationToken->getEmail();
            
            $customer = \Pimcore\Model\DataObject\Customer::getById($registrationToken->getParentId());

            //redirect user to user profile page if logged in
            if ($user && ($user->getId() === $customer->getId()) && $this->isGranted('ROLE_USER')) {
                return new RedirectResponse($userProfilePage);
            }

            if ($this->isRegTokenValid($token) === false) {
                return new RedirectResponse($userRegisterPage . "?_token=[]&registration-confirmed=0&error_code={$this->getErrorCode()}");
            }

            if ($email != $request->get("email")) {
                $this->setErrorCode("4");
                return new RedirectResponse($userRegisterPage . "?_token=[]&registration-confirmed=0&error_code={$this->getErrorCode()}");
            }

            if ($this->isCustomerRegistrationValid($customer) === false) {
                return new RedirectResponse($loginPage . "?_token=[]&registration-confirmed=0&error_code={$this->getErrorCode()}");
            }

            $registrationKey = $request->get('registrationKey');

            /** @var OAuthToken $oAuthToken */
            $oAuthToken = null;

            /** @var UserResponseInterface $oAuthUserInfo */
            $oAuthUserInfo = null;

            // load previously stored token from the session and try to load user profile
            // from provider
            if (null !== $registrationKey) {
                $oAuthToken = $oAuthHandler->loadToken($registrationKey);
                $oAuthUserInfo = $oAuthHandler->loadUserInformation($oAuthToken);
            }

            if (null !== $oAuthUserInfo) {
                // try to load a customer with the given identity from our storage. if this succeeds, we can't register
                // the customer and should either log in the existing identity or show an error. for simplicity, we just
                // throw an exception here.
                // this shouldn't happen as the login would log in the user if found
                if ($oAuthHandler->getCustomerFromUserResponse($oAuthUserInfo)) {
                    throw new \RuntimeException('Customer is already registered');
                }
            }

            // the registration form handler is just a utility class to map pimcore object data to form
            // and vice versa.
            $formData = $registrationFormHandler->buildFormData($customer);
            $hidePassword = false;

            if (null !== $oAuthToken) {
                $hidePassword = true;
            }
            
            $departmentOptions =  $registrationFormHandler->getDepartmentOptions($customer);

            $formData["translator"] = $this->translator;
            
            $formData["department_options"] = $departmentOptions;
            
            $this->view->registrationTerms = $this->container->get("trueromance.object.config")->get("INVITATION_REGISTER_TERMS", $request->getLocale());
            
            // build the registration form and pre-fill it with customer data
            $form = $this->createForm(UserRegistration::class, $formData, ['attr' => ['class' => 'invite', 'id' => 'invite']]);

            $form->handleRequest($request);

            $errors = [];
            if ($form->isSubmitted() && $form->isValid()) {
                $registrationFormHandler->updateCustomerFromForm($customer, $form);

                $encoder = $this->container->get("cmf.security.user_password_encoder_factory");

                $userEncoder = $encoder->getEncoder($customer);
                /* @var $userEncoder \Pimcore\Security\Encoder\PasswordFieldEncoder */

                $password = $form->getData()["customerPassword"];

                $hashedPassword = $userEncoder->encodePassword($password, $customer->getUsername());

                $customer->setPassword($hashedPassword);

                $customer->setRegistered_Language($this->mailer->getLanguage());

                $customer->setTerms_Accepted(true);

                try {
                    $customer->save();

                    // add SSO identity from OAuth data
                    if (null !== $oAuthUserInfo) {
                        $oAuthHandler->connectSsoIdentity($customer, $oAuthUserInfo);
                    }

                    //check if special redirect is necessary
                    if ($session->get("referrer")) {
                        $response = $this->redirect($session->get("referrer"));
                        $session->remove("referrer");
                    } else {
                        $response = $userRegisterPage . "?user-registration=1";
                    }

                    $this->mailer->sendUserRegistrationMail($registrationToken, $request->getUri(), $customer);
                    
                    $this->regtoken->unvalidateToken($token);

                    return new RedirectResponse($response);
                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                }
            }

            if (!$form->isValid()) {
                foreach ($form->getErrors() as $error) {
                    $errors[] = $error->getMessage();
                }
            }

            // re-save user info to session as we need it in subsequent requests (e.g. after form errors) or
            // when form is rendered for the first time
            if (null !== $registrationKey && null !== $oAuthToken) {
                $oAuthHandler->saveToken($registrationKey, $oAuthToken);
            }

            $this->view->customer = $customer;
            $this->view->form = $form->createView();

            $this->view->errors = $errors;
            $this->view->hidePassword = $hidePassword;
            $this->view->hideNav = true;
            $this->view->hideBreadcrumb = true;
            $this->view->errorMessage = $this->getErrorMessageByErrorCode($request->get("error_code"));
            $this->view->email = $request->get("email");
        }

        $this->view->userRegistered = $successParam;
        $this->view->loginPage = $loginPage;
        $this->view->userRegistrationConfirmed = $request->get("registration-confirmed");
    }

    /**
     * @Route("/{_locale}/ajax/user-authentication/user-invite")
     */
    public function userInviteAction(Request $request)
    {
        $emails = $request->get("emails");

        if (!$emails) {
            return new \Symfony\Component\HttpFoundation\JsonResponse(["error" => 'Parameter emails not given!'], 400);
        }

        if ($this->isValid($emails) === false) {
            $this->userInviteText = $this->translator->trans("user.invite.error.user.exists");
            return new \Symfony\Component\HttpFoundation\JsonResponse(["result" => $this->userInviteText, "emails" => $this->checkUserForMultipleEmails($emails)],
                    500);
        }

        foreach ($emails as $email) {

            /* @var $cutomer Pimcore\Model\DataObject\Customer */
            $customer = new DataObject\Customer();

            $customer->setEmail($email);

            $items = new DataObject\Fieldcollection();
            $item = new DataObject\Fieldcollection\Data\UserPermission();
            $item->setView_mediathek(true);
            $items->add($item);
            $customer->setUserPermission($items);

            $itemsCatalogs = new DataObject\Fieldcollection();
            $itemCatalog = new DataObject\Fieldcollection\Data\UserCatalogs();
            
            $userGroups = [];

            if ($request->get("userGroup")) {
                foreach ($request->get("userGroup") as $userGroup) {
                    $userGroupInstance = DataObject\UserGroup::getByName($userGroup)->current();
                    $userGroups[] = $userGroupInstance;
                }
            }

            if ($request->get("catalog")) {
                foreach ($request->get("catalog") as $catalog) {
                    $catalogObject = DataObject\Catalog::getById($catalog);
                    $catalogs[] = $catalogObject;
                }
            }

            if ($request->get("country")) {
                foreach ($request->get("country") as $country) {
                    $countries[] = $country;
                }
            }

            $customer->setUserGroup($userGroups);
            $itemCatalog->setCatalogs($catalogs);
            $customer->setCountries($countries);

            $itemsCatalogs->add($itemCatalog);
            $customer->setUserCatalogs($itemsCatalogs);

            $customer->setPublished(true);

            $customer->save();

            $customerId = $customer->getId();

            $registrationToken = RegistrationTokenUtility::create($email, $customerId);

            $this->mailer->sendUserInvitationEmail($registrationToken, $request);
        }

        $this->userInviteText = $this->translator->trans("user.invite.sent.mail");

        return new \Symfony\Component\HttpFoundation\JsonResponse(["result" => $this->userInviteText, "emails" => $emails], 200);
    }

    /**
     * 
     * @param string $email
     * @return boolean
     */
    private function userExists($email)
    {
        $customer = \Pimcore\Model\DataObject\Customer::getByEmail($email)->current();

        if ($customer) {
            return true;
        }

        return false;
    }

    private function isValid($emails)
    {
        $result = $this->checkUserForMultipleEmails($emails);

        if (empty($result)) {
            return true;
        }

        return false;
    }

    private function checkUserForMultipleEmails($emails)
    {
        $result = [];

        foreach ($emails as $email) {
            if ($this->userExists($email) === true) {
                $result[] = $email;
            }
        }

        return $result;
    }

    private function isSuccessRegistrationPage($param)
    {
        if ($param && $param === "1") {
            return true;
        }

        return false;
    }

    /**
     * Special route for connecting to social profiles that saves referrer in session for later
     * redirect to that referrer
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param $service
     * @return Response
     * @Route("/auth/oauth/referrerLogin/{service}", name="app_auth_oauth_login_referrer")
     */
    public function connectAction(Request $request, SessionInterface $session, $service)
    {
        // we overwrite this route to store user's referrer in the session
        $session->set('referrer', $request->headers->get('referer'));
        return $this->forward('HWIOAuthBundle:Connect:redirectToService', array('service' => $service));
    }

    public function sendMailAction()
    {
        
    }

    private function sendUserInvitationMail(\Pimcore\Model\DataObject\RegistrationToken $registrationToken, $request)
    {
        $requestUri = $request->getUri();

        $message = $request->get("message");

        $customerEmail = $registrationToken->getEmail();

        $userRegisterPage = $this->config->getUser_Registration_Page()->getFullPath();

        $explodedUri[] = explode("/", $requestUri);

        $uri = $explodedUri[0][0] . "//" . $explodedUri[0][2] . $userRegisterPage . "?email=" . $customerEmail;

        $url = $uri . "&_token=" . urlencode($registrationToken->getGenerated_Token());

        $mail = \Pimcore\Tool::getMail([$customerEmail], $this->translator->trans("user.invitation.mail.subject"));
        $mail->setIgnoreDebugMode(true);
        $mail->setBody($this->renderView(
                        '@UserAuthenticationBundle/Resources/views/mails/user-invitation.html.php',
                        ['url' => $url, 'message' => $message, 'translator' => $this->translator]
                ), 'text/html');
        $mail->send();
    }

    /**
     * 
     * @param string $token
     * @return boolean
     */
    private function isTokenValid($token): bool
    {
        $tokenIsValid = $this->token->tokenIsValid($token);

        if ($tokenIsValid === false) {
            $this->setErrorCode("1");
            return false;
        }

        return true;
    }

    /**
     * 
     * @param string $token
     * @return boolean
     */
    private function isRegTokenValid($token): bool
    {
        $tokenIsValid = $this->regtoken->tokenIsValid($token);

        if ($tokenIsValid === false) {
            $this->setErrorCode("1");
            return false;
        }

        return true;
    }

    /**
     * 
     * @param \Pimcore\Model\DataObject\Customer $customer
     * @param string $token
     * @return bool
     */
    private function isCustomerRegistrationValid(\Pimcore\Model\DataObject\Customer $customer): bool
    {
        if (!$customer) {
            $this->setErrorCode("2");
            return false;
        }

        if ($customer->getActive() === true) {
            $this->setErrorCode("3");
            return false;
        }

        return true;
    }

    /**
     * 
     * @param string $errorCode
     * @return string
     */
    private function getErrorMessageByErrorCode($errorCode, $data = null)
    {
        $errorMessages = [
            "1" => $this->translator->trans("token.is.invalid", [$data]),
            "2" => $this->translator->trans("customer.does.not.exist", [$data]),
            "3" => $this->translator->trans("customer.is.active", [$data]),
            "4" => $this->translator->trans("email.not.valid", [$data])
        ];

        return $errorMessages[$errorCode];
    }

    /**
     * 
     * @param string $email
     */
    private function getCustomerFromDatabaseByEmail($email)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $query = "SELECT * FROM {$this->getCustomerTable()} WHERE email = '$email' LIMIT 1";

        return $db->fetchRow($query);
    }

    /**
     * 
     * @return string
     */
    private function getCustomerTable()
    {
        $customerObject = new \Pimcore\Model\DataObject\Customer();

        $class = $customerObject->getClass()->getId();

        $customerTable = "object_query_{$class}";

        return $customerTable;
    }

}
