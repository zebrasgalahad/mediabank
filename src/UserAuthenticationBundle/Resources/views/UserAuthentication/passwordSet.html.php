<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('@UserAuthenticationBundle/Resources/layouts/login.html.php');
?>

<div class="login-container <?php echo $this->error ? "has-error" : ""; ?>">
    <div class="block">
        <img src="/../static/img/logo_big.png" alt="StanleyBlack&Decker Logo">
        <div class="inner">      
            <?php if($success): ?>
                <h2 class="title"><?php echo $this->translate("login.reset-password-success-title"); ?></h2>
                <p><?php echo $this->translate("login.reset-password-success-message"); ?><br><br></p>
                <a href="<?php echo $loginPage; ?>" class="button"><?php echo $this->translate("to.login.message"); ?></a>
            <?php elseif($errorMessage !== NULL): ?>
                <div class="alert alert-danger">
                    <?php echo $errorMessage; ?>
                </div>
            <?php else: ?>
                         
                <?php echo $this->form()->start($form); ?>
                <?php echo $this->form()->row($form['_password1']); ?>
                <?php echo $this->form()->row($form['_password2']); ?>
                <?php if($this->errors && count($this->errors) > 0): ?>
                    <?php foreach($this->errors as $errorMessage) : ?>
                        <div class="alert alert-danger"><?php echo $errorMessage; ?></div>
                    <?php endforeach; ?>
                <?php endif; ?>
                <a href="<?php echo $loginPage; ?>" class="back-link"><?php echo $this->translate("back.to.login.message"); ?></a>
                <div class="submit-block text-right">
                    <?php echo $this->form()->widget($form['_submit'], [
                        'attr' => [
                            'class' => 'button'
                        ]
                    ]); ?>
                </div>
                <?php echo $this->form()->end($form); ?>
            <?php endif; ?>
        </div>
    </div>
</div>