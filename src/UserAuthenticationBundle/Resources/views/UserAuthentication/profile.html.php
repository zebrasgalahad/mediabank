<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */
$this->extend('@UserAuthenticationBundle/Resources/layouts/login.html.php');
?>
<div class="page-header">
        <h1><?php echo $this->translate("account.welcome", [$customer->getFirstname(), $customer->getLastname()]) ?></h1>
</div>

<div class="text-block">
    <div class="container">
        <div class="profile">
            <?php if(count($this->errors) > 0): ?>
                    <?php foreach($this->errors as $errorMessage) : ?>
                        <div class="alert alert-danger"><?php echo $errorMessage; ?></div>
                    <?php endforeach; ?>
                <?php endif; ?>     
                <?php echo $this->form()->start($form); ?>
                <h3 class="group-title"><?php echo $this->translate("profile.customer.data.title"); ?></h3>
                <?php echo $this->form()->row($form['customerFirstname']); ?>
                <?php echo $this->form()->row($form['customerLastname']); ?>
                <h3 class="group-title"><?php echo $this->translate("profile.customer.address.label"); ?></h3>
                <?php echo $this->form()->row($form['customerStreet']); ?>
                <?php echo $this->form()->row($form['customerCity']); ?>
                <?php echo $this->form()->row($form['customerZip']); ?>
                <h3 class="group-title"><?php echo $this->translate("profile.customer.contact.label"); ?></h3>
                <?php echo $this->form()->row($form['customerPhone']); ?>
                <?php echo $this->form()->row($form['customerEmail']); ?>
                <div class="submit-block text-right">
                    <?php echo $this->form()->widget($form['_submit'], [
                        'attr' => [
                            'class' => 'btn btn-primary btn-block button red'
                        ]
                    ]); ?>
                </div>
            <?php echo $this->form()->end($form); ?>
        </div>
    </div>
</div>