<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$form = $this->form;

$this->extend('@UserAuthenticationBundle/Resources/layouts/login.html.php');
?>

<div class="login-container <?php echo $this->error ? "has-error" : ""; ?>">
    <div class="block">
        <img src="/../static/img/logo_big.png" alt="StanleyBlack&Decker Logo">
        <div class="inner">
            <?php echo $this->form()->start($form); ?>
            <?php echo $this->form()->row($form['_username']) ?>
            <?php echo $this->form()->row($form['_password']) ?>
            <?php if ($this->error): ?>
                <div class="error"><?php echo $this->error; ?></div>
            <?php endif; ?>
            <p class="password-forgot">
                <a href="<?php echo $passwordForgotLink; ?>"><?php echo $this->translate("general.login.ln.forgotpassword"); ?></a>
            </p>
            <?php
            echo $this->form()->widget($form['_submit'],
                    [
                'attr' => [
                    'class' => 'button'
                ]
            ])
            ?>
<?php echo $this->form()->end($form); ?>
        </div>
    </div>