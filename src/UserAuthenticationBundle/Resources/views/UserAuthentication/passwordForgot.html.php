<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$this->extend('@UserAuthenticationBundle/Resources/layouts/login.html.php');
?>

<div class="login-container <?php echo $this->error ? "has-error" : ""; ?>">
    <div class="block">
        <img src="/../static/img/logo_big.png" alt="StanleyBlack&Decker Logo">
        <div class="inner">
            <?php if($success): ?>
                <h2 class="title"><?php echo $this->translate("login.reset-password-success-request-title"); ?></h2>
                <p><?php echo $this->translate("login.reset-password-success-request", [$email]); ?><br><br></p>
                <a href="<?php echo $loginPage; ?>" class="back-link"><?php echo $this->translate("back.to.login.message"); ?></a>
            <?php else: ?>
                <form name="password-forgot" method="post" action="<?php echo $passwordForgotPage; ?>">
                    <p class="hint"><?php echo $this->translate("user.password.forgot.hint"); ?></p>
                    <div class="form-group">
                        <label class="required" for="_email"><?php echo $this->translate("user.email"); ?></label>
                        <input id="_email" class="form-control" name="_email" required="required">
                    </div>
                    <?php if($error): ?>
                        <div class="error"><?php echo $error; ?></div>
                    <?php endif; ?>
                    <a href="<?php echo $loginPage; ?>" class="back-link"><?php echo $this->translate("back.to.login.message"); ?></a>
                    <div class="submit-block text-right">
                        <button id="_submit" class="button" type="submit" name="_submit"><?php echo $this->translate("login.password-reset"); ?></button>
                    </div>
                </form>
            <?php endif; ?>
        </div>
    </div>
</div>