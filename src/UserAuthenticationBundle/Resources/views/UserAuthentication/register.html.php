<?php
/**
 * @var \Pimcore\Templating\PhpEngine $this
 * @var \Pimcore\Templating\PhpEngine $view
 * @var \Pimcore\Templating\GlobalVariables $app
 */

$form = $this->form;

$this->extend('@UserAuthenticationBundle/Resources/layouts/login.html.php');
?>

<div class="login-container register <?php echo $this->error ? "has-error" : ""; ?>">
    <div class="block">
        <img src="/../static/img/logo_big.png" alt="StanleyBlack&Decker Logo">
        
        <div class="inner">
            <?php if ($userRegistered !== NULL && $userRegistered === "1"): ?>
            <div class="successful-registration">
                <p><?php echo $this->translate("user.registered.frontend.message"); ?> </p></br>
               <a href="<?php echo $loginPage; ?>" class="back-link"><?php echo $this->translate("back.to.login.message"); ?></a>
            </div>
            <?php elseif ($userRegistrationConfirmed !== NULL && $userRegistrationConfirmed === "1"): ?>
            <div class="user-registration-confirmed">
               <p><?php echo $this->translate("user.registration.email.confirmed.frontend.message"); ?></p>
            </div>
            <?php elseif ($userRegistrationConfirmed !== NULL && $userRegistrationConfirmed === "0"): ?>
            <div class="user-confirmation-not-confirmed">
               <p><?php echo $this->translate("user.registration.email.not.confirmed.frontend.message"); ?></p>
               <h4><?php echo $errorMessage; ?></h4>
            </div>
            <?php else: ?>
                <?php if ($this->errors) { ?>
                    <?php foreach($this->errors as $error) { ?>
                        <div class="alert alert-danger"><?php echo $error ?></div>
                    <?php } ?>
                <?php } ?>
                
                <?php echo $this->form()->start($form); ?>
                    <div class="register-data">
                        <p><?php echo $this->translate("user.registration.start.message"); ?></p>
                        <?php echo $this->form()->row($form['customerFirstname']) ?>
                        <?php echo $this->form()->row($form['customerLastname']) ?>
                        <?php echo $this->form()->row($form['customerEmail'],
                                [
                                'attr' => [
                                    'value' => $email,
                                    'readonly' => "readonly"
                                    ]
                                ]) ?>
                        <?php if (!$this->hidePassword) { ?>
                            <?php echo $this->form()->row($form['customerPassword']) ?>
                        <?php } ?>
                        <br>
                        <?php echo $this->form()->row($form['customerCompany']) ?>
                        <?php echo $this->form()->row($form['customerCustomerNumber']) ?>
                        <?php echo $this->form()->row($form['customerDepartment']) ?>
                        <div style="clear: both;" ></div>
                        <p><?php echo $this->translate("user.registration.disclaimer.message"); ?></p>
                        <div><?php
                            echo $this->form()->row($form['_check'],
                                    [
                                'attr' => [
                                    'class' => 'button show-terms-of-use',
                                ]
                            ])
                        ?>
                        </div>
                    </div>
                    <div class="terms-of-use">
                        <p class="back-to-form">&laquo; <?php echo $this->translate("user.registration.back-to-form"); ?></p>
                        <p><?php echo $this->translate("user.registration.disclaimer.message"); ?></p>
                        <textarea  readonly="readonly"><?php echo $registrationTerms; ?></textarea>
                        <?php
                            echo $this->form()->widget($form['_submit'],
                                    [
                                'attr' => [
                                    'class' => 'button'
                                ]
                            ])
                        ?>
                        <p><?php echo $this->translate("user.registration.disclaimer.accept"); ?></p> 
                    </div>
                <?php echo $this->form()->end($form, ['render_rest' => true]); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
