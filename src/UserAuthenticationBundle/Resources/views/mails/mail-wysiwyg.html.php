<div class="mail-title">
    <?php echo $this->wysiwyg("title", ["width" => 400]) ?>
</div>

<div class="mail-body">
    <?php echo $this->wysiwyg("body", ["width" => 400]) ?>
</div>

