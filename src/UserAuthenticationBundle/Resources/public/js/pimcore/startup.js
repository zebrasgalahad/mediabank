pimcore.registerNS("pimcore.plugin.UserAuthenticationBundle");

pimcore.plugin.UserAuthenticationBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.UserAuthenticationBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("UserAuthenticationBundle ready!");
    }
});

var UserAuthenticationBundlePlugin = new pimcore.plugin.UserAuthenticationBundle();
