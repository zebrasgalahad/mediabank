<?php
/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use TrueRomanceBundle\Controller\PermissionController;

class DefaultController extends PermissionController
{
    /**
     * @param Request $request
     */
    public function defaultAction(Request $request)
    {
    }
}
