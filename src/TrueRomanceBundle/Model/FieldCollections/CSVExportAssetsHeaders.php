<?php

namespace TrueRomanceBundle\Model\FieldCollections;

use Pimcore\Model\DataObject\Fieldcollection\Data\CSVExportAssetsHeaders as PimcoreCSVExportAssetsHeaders;

class CSVExportAssetsHeaders extends PimcoreCSVExportAssetsHeaders
{
    const HEROSHOT_BILD_1 = 'Bild 1 Heroshot 45° (WEB)';
    const HEROSHOT_BILD_2 = 'Bild 2 Heroshot Frontal (WEB1)';
    const CONTENT_BILD_3 = 'Bild 3 Inhaltsbild (KIT)';
    const APPLICATION_BILD_4 = 'Bild 4 Anwendung (ANW)';
    const APPLICATION_BILD_5 = 'Bild 5 Anwendung (ANW1)';
    const APPLICATION_BILD_6 = 'Bild 6 Anwendung (ANW2)';
    const APPLICATION_BILD_7 = 'Bild 7 Anwendung (ANW3)';
    const APPLICATION_BILD_8 = 'Bild 8 Anwendung (ANW4)';
    const APPLICATION_BILD_9 = 'Bild 9 Anwendung (ANW5)';
    const APPLICATION_BILD_10 = 'Bild 10 Anwendung (ANW6)';
    const APPLICATION_BILD_11 = 'Bild 11 Anwendung (ANW7)';
    const PRODUCT_OTHER_BILD_10 = 'Produktzeichnung (WEB3)';
    const PRODUCT_OTHER_BILD_12 = 'Bild 12 Others (OTH)';
    const PRODUCT_OTHER_BILD_13 = 'Bild 13 Others (OTH1)';
    const PRODUCT_OTHER_BILD_14 = 'Bild 14 Others (OTH2)';
    const PRODUCT_OTHER_BILD_15 = 'Bild 15 Others (OTH3)';
    const HEROSHOT_BILD_16 = 'Bild 16 Heroshot Frontal EPS (WEB2)';
    const VIDEO = 'Video';
    const CATALOG_ASSET_VIDEO = 'Catalog Asset Video';
    const PRODUCT_DATA_SHEET_INFO_1 = 'Produktinfoblatt - PDF (INFO)';
    const PRODUCT_DATA_SHEET_INFO_2 = 'Produktinfoblatt - PPT (INFO1)';
    const PRODUCT_DATA_SHEET_INFO_3 = 'Produktinfoblatt - DOC (INFO2)';
    const AWARD_1_LOW = 'Auszeichnung 1 - JPG (AWARD_LOW)';
    const AWARD_1_HIGH = 'Auszeichnung 1 - EPS (AWARD_HIGH)';
    const AWARD_2_LOW = 'Auszeichnung 2 - JPG (AWARD1_LOW)';
    const AWARD_2_HIGH = 'Auszeichnung 2 - EPS (AWARD1_HIGH)';
    const AWARD_3_HIGH = 'Auszeichnung 3 - EPS (AWARD2_HIGH)';
    const CATALOG_ASSET_LOGO = 'Catalog Asset Logo';
    const CATALOG_ASSET_ICON = 'Catalog Asset Icon';
    const PRESS_TEXT_1 = 'Pressetext 1 - DOC (PRESS)';
    const PRESS_TEXT_2 = 'Pressetext 2 - DOC (PRESS1)';
    const PRESS_TEXT_3 = 'Pressetext 3 - DOC (PRESS2)';
    const CATALOG_ASSET_CERTIFICATE = 'Catalog Asset Certificate';
    const CATALOG_ASSET_DOWNLOAD = 'Catalog Asset Download';
    const CATALOG_ASSET_PRICELIST_DOWNLOAD = 'Catalog Asset Pricelist Download';

    public function getHEROSHOT_BILD_1()
    {
        $data = parent::getHEROSHOT_BILD_1();

        return !empty($data) ? $data : self::HEROSHOT_BILD_1;
    }

    public function getHEROSHOT_BILD_2()
    {
        $data = parent::getHEROSHOT_BILD_2();

        return !empty($data) ? $data : self::HEROSHOT_BILD_2;
    }

    public function getCONTENT_BILD_3()
    {
        $data = parent::getCONTENT_BILD_3();

        return !empty($data) ? $data : self::CONTENT_BILD_3;
    }

    public function getAPPLICATION_BILD_4()
    {
        $data = parent::getAPPLICATION_BILD_4();

        return !empty($data) ? $data : self::APPLICATION_BILD_4;
    }

    public function getAPPLICATION_BILD_5()
    {
        $data = parent::getAPPLICATION_BILD_5();

        return !empty($data) ? $data : self::APPLICATION_BILD_5;
    }

    public function getAPPLICATION_BILD_6()
    {
        $data = parent::getAPPLICATION_BILD_6();

        return !empty($data) ? $data : self::APPLICATION_BILD_6;
    }

    public function getAPPLICATION_BILD_7()
    {
        $data = parent::getAPPLICATION_BILD_7();

        return !empty($data) ? $data : self::APPLICATION_BILD_7;
    }

    public function getAPPLICATION_BILD_8()
    {
        $data = parent::getAPPLICATION_BILD_8();

        return !empty($data) ? $data : self::APPLICATION_BILD_8;
    }

    public function getAPPLICATION_BILD_9()
    {
        $data = parent::getAPPLICATION_BILD_9();

        return !empty($data) ? $data : self::APPLICATION_BILD_9;
    }

    public function getAPPLICATION_BILD_10()
    {
        $data = parent::getAPPLICATION_BILD_10();

        return !empty($data) ? $data : self::APPLICATION_BILD_10;
    }

    public function getAPPLICATION_BILD_11()
    {
        $data = parent::getAPPLICATION_BILD_11();

        return !empty($data) ? $data : self::APPLICATION_BILD_11;
    }

    public function getPRODUCT_OTHER_BILD_10()
    {
        $data = parent::getPRODUCT_OTHER_BILD_10();

        return !empty($data) ? $data : self::PRODUCT_OTHER_BILD_10;
    }

    public function getPRODUCT_OTHER_BILD_12()
    {
        $data = parent::getPRODUCT_OTHER_BILD_12();

        return !empty($data) ? $data : self::PRODUCT_OTHER_BILD_12;
    }

    public function getPRODUCT_OTHER_BILD_13()
    {
        $data = parent::getPRODUCT_OTHER_BILD_13();

        return !empty($data) ? $data : self::PRODUCT_OTHER_BILD_13;
    }

    public function getPRODUCT_OTHER_BILD_14()
    {
        $data = parent::getPRODUCT_OTHER_BILD_14();

        return !empty($data) ? $data : self::PRODUCT_OTHER_BILD_14;
    }

    public function getPRODUCT_OTHER_BILD_15()
    {
        $data = parent::getPRODUCT_OTHER_BILD_15();

        return !empty($data) ? $data : self::PRODUCT_OTHER_BILD_15;
    }

    public function getHEROSHOT_BILD_16()
    {
        $data = parent::getHEROSHOT_BILD_16();

        return !empty($data) ? $data : self::HEROSHOT_BILD_16;
    }

    public function getPRODUCT_DATA_SHEET_INFO_1()
    {
        $data = parent::getPRODUCT_DATA_SHEET_INFO_1();

        return !empty($data) ? $data : self::PRODUCT_DATA_SHEET_INFO_1;
    }

    public function getPRODUCT_DATA_SHEET_INFO_2()
    {
        $data = parent::getPRODUCT_DATA_SHEET_INFO_2();

        return !empty($data) ? $data : self::PRODUCT_DATA_SHEET_INFO_2;
    }

    public function getPRODUCT_DATA_SHEET_INFO_3()
    {
        $data = parent::getPRODUCT_DATA_SHEET_INFO_3();

        return !empty($data) ? $data : self::PRODUCT_DATA_SHEET_INFO_3;
    }

    public function getVIDEO()
    {
        $data = parent::getVIDEO();

        return !empty($data) ? $data : self::VIDEO;
    }

    public function getCATALOG_ASSET_VIDEO()
    {
        $data = parent::getCATALOG_ASSET_VIDEO();

        return !empty($data) ? $data : self::CATALOG_ASSET_VIDEO;
    }

    public function getAWARD_1_LOW()
    {
        $data = parent::getAWARD_1_LOW();

        return !empty($data) ? $data : self::AWARD_1_LOW;
    }

    public function getAWARD_1_HIGH()
    {
        $data = parent::getAWARD_1_HIGH();

        return !empty($data) ? $data : self::AWARD_1_HIGH;
    }

    public function getAWARD_2_LOW()
    {
        $data = parent::getAWARD_2_LOW();

        return !empty($data) ? $data : self::AWARD_2_LOW;
    }

    public function getAWARD_2_HIGH()
    {
        $data = parent::getAWARD_2_HIGH();

        return !empty($data) ? $data : self::AWARD_2_HIGH;
    }

    public function getAWARD_3_HIGH()
    {
        $data = parent::getAWARD_3_HIGH();

        return !empty($data) ? $data : self::AWARD_3_HIGH;
    }

    public function getCATALOG_ASSET_LOGO()
    {
        $data = parent::getCATALOG_ASSET_LOGO();

        return !empty($data) ? $data : self::CATALOG_ASSET_LOGO;
    }

    public function getCATALOG_ASSET_ICON()
    {
        $data = parent::getCATALOG_ASSET_ICON();

        return !empty($data) ? $data : self::CATALOG_ASSET_ICON;
    }

    public function getPRESS_TEXT_1()
    {
        $data = parent::getPRESS_TEXT_1();

        return !empty($data) ? $data : self::PRESS_TEXT_1;
    }

    public function getPRESS_TEXT_2()
    {
        $data = parent::getPRESS_TEXT_2();

        return !empty($data) ? $data : self::PRESS_TEXT_2;
    }

    public function getPRESS_TEXT_3()
    {
        $data = parent::getPRESS_TEXT_3();

        return !empty($data) ? $data : self::PRESS_TEXT_3;
    }

    public function getCATALOG_ASSET_CERTIFICATE()
    {
        $data = parent::getCATALOG_ASSET_CERTIFICATE();

        return !empty($data) ? $data : self::CATALOG_ASSET_CERTIFICATE;
    }

    public function getCATALOG_ASSET_DOWNLOAD()
    {
        $data = parent::getCATALOG_ASSET_DOWNLOAD();

        return !empty($data) ? $data : self::CATALOG_ASSET_DOWNLOAD;
    }

    public function getCATALOG_ASSET_PRICELIST_DOWNLOAD()
    {
        $data = parent::getCATALOG_ASSET_PRICELIST_DOWNLOAD();

        return !empty($data) ? $data : self::CATALOG_ASSET_PRICELIST_DOWNLOAD;
    }
}
