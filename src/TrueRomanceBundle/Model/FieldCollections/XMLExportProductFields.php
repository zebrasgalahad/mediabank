<?php

namespace TrueRomanceBundle\Model\FieldCollections;

use Pimcore\Model\DataObject\Fieldcollection\Data\XMLExportProductFields as PimcoreXMLExportProductFields;

class XMLExportProductFields extends PimcoreXMLExportProductFields
{
    const DETAILS = 'DETAILS';
    const DESCRIPTION_SHORT = 'DESCRIPTION_SHORT';
    const DESCRIPTION_LONG = 'DESCRIPTION_LONG';
    const EAN = 'EAN';
    const SUPPLIER_ALT = 'SUPPLIER_ALT';
    const MANUFACTURER = 'MANUFACTURER';
    const MANUFACTURER_NAME = 'MANUFACTURER_NAME';
    const MANUFACTURER_TYPE_DESCR = 'MANUFACTURER_TYPE_DESCR';
    const SPECIAL_TREATMENT_CLASS = 'SPECIAL_TREATMENT_CLASS';
    const STATUS = 'STATUS';
    const FEATURES = 'FEATURES';
    const REFERENCE_FEATURE_SYSTEM_NAME = 'REFERENCE_FEATURE_SYSTEM_NAME';
    const REFERENCE_FEATURE_GROUP_ID = 'REFERENCE_FEATURE_GROUP_ID';
    const REFERENCE_FEATURE_GROUP_NAME = 'REFERENCE_FEATURE_GROUP_NAME';
    const FEATURE = 'FEATURE';
    const FNAME = 'FNAME';
    const FVALUE = 'FVALUE';
    const FUNIT = 'FUNIT';
    const FDESCR = 'FDESCR';
    const ORDER_DETAILS = 'ORDER_DETAILS';
    const ORDER_UNIT = 'ORDER_UNIT';
    const CONTENT_UNIT = 'CONTENT_UNIT';
    const NO_CU_PER_OU = 'NO_CU_PER_OU';
    const QUANTITY_MIN = 'QUANTITY_MIN';
    const QUANTITY_INTERVAL = 'QUANTITY_INTERVAL';
    const PRICE_QUANTITY = 'PRICE_QUANTITY';
    const PRICE_DETAILS = 'PRICE_DETAILS';
    const DATETIME = 'DATETIME';
    const DATE = 'DATE';
    const PRICE = 'PRICE';
    const PRICE_AMOUNT = 'PRICE_AMOUNT';
    const PRICE_CURRENCY = 'PRICE_CURRENCY';
    const TAX = 'TAX';
    const LOWER_BOUND = 'LOWER_BOUND';
    const MIME_INFO = 'MIME_INFO';
    const MIME = 'MIME';
    const MIME_SOURCE = 'MIME_SOURCE';
    const MIME_DESCR = 'MIME_DESCR';
    const MIME_TYPE = 'MIME_TYPE';
    const MIME_PURPOSE = 'MIME_PURPOSE';
    const USER_DEFINED_EXTENSIONS = 'USER_DEFINED_EXTENSIONS';
    const MANUFACTURER_ACROYNM = 'UDX.EDXF.MANUFACTURER_ACROYNM';
    const LANGTEXT = 'UDX.EDXF.LANGTEXT';
    const LIEFERUMFANG = 'UDX.EDXF.LIEFERUMFANG';
    const TECHNISCHE_DATEN = 'UDX.EDXF.TECHNISCHE_DATEN';
    const GARANTIEBEDINGUNGEN = 'UDX.EDXF.GARANTIEBEDINGUNGEN';
    const ANWENDUNGSBEISPIELE = 'UDX.EDXF.ANWENDUNGSBEISPIELE';
    const DISCOUNT_GROUP_MANUFACTURER = 'UDX.EDXF.DISCOUNT_GROUP_MANUFACTURER';
    const PACKING_UNITS = 'UDX.EDXF.PACKING_UNITS';
    const PACKING_UNIT = 'UDX.EDXF.PACKING_UNIT';
    const PACKING_UNIT_QUANTITY_MIN = 'UDX.EDXF.PACKING_UNIT_QUANTITY_MIN';
    const PACKING_UNIT_QUANTITY_MAX = 'UDX.EDXF.PACKING_UNIT_QUANTITY_MAX';
    const PACKING_UNIT_LENGTH = 'UDX.EDXF.PACKING_UNIT_LENGTH';
    const PACKING_UNIT_LENGTH_UNIT = 'UDX.EDXF.PACKING_UNIT_LENGTH_UNIT';
    const PACKING_UNIT_WIDTH = 'UDX.EDXF.PACKING_UNIT_WIDTH';
    const PACKING_UNIT_WIDTH_UNIT = 'UDX.EDXF.PACKING_UNIT_WIDTH_UNIT';
    const PACKING_UNIT_DEPTH = 'UDX.EDXF.PACKING_UNIT_DEPTH';
    const PACKING_UNIT_DEPTH_UNIT = 'UDX.EDXF.PACKING_UNIT_DEPTH_UNIT';
    const PACKING_UNIT_WEIGHT = 'UDX.EDXF.PACKING_UNIT_WEIGHT';
    const PACKING_UNIT_WEIGHT_UNIT = 'UDX.EDXF.PACKING_UNIT_WEIGHT_UNIT';
    const WEIGHT = 'UDX.EDXF.WEIGHT';
    const REACH = 'UDX.EDXF.REACH';
    const REACH_INFO = 'UDX.EDXF.REACH_INFO';
    const LOGISTICS_DETAILS = 'LOGISTICS_DETAILS';
    const CUSTOMS_TARIFF_NUMBER = 'CUSTOMS_TARIFF_NUMBER';
    const CUSTOMS_NUMBER = 'CUSTOMS_NUMBER';
    const COUNTRY_OF_ORIGIN = 'COUNTRY_OF_ORIGIN';

    /**
     * @inheritDoc
     */
    public function getDETAILS()
    {
        $data = parent::getDETAILS();

        return !empty($data) ? $data : self::DETAILS;
    }

    /**
     * @inheritDoc
     */
    public function getDESCRIPTION_SHORT()
    {
        $data = parent::getDESCRIPTION_SHORT();

        return !empty($data) ? $data : self::DESCRIPTION_SHORT;
    }

    /**
     * @inheritDoc
     */
    public function getDESCRIPTION_LONG()
    {
        $data = parent::getDESCRIPTION_LONG();

        return !empty($data) ? $data : self::DESCRIPTION_LONG;
    }

    /**
     * @inheritDoc
     */
    public function getEAN()
    {
        $data = parent::getEAN();

        return !empty($data) ? $data : self::EAN;
    }

    /**
     * @inheritDoc
     */
    public function getSUPPLIER_ALT()
    {
        $data = parent::getSUPPLIER_ALT();

        return !empty($data) ? $data : self::SUPPLIER_ALT;
    }

    /**
     * @inheritDoc
     */
    public function getMANUFACTURER()
    {
        $data = parent::getMANUFACTURER();

        return !empty($data) ? $data : self::MANUFACTURER;
    }

    /**
     * @inheritDoc
     */
    public function getMANUFACTURER_NAME()
    {
        $data = parent::getMANUFACTURER_NAME();

        return !empty($data) ? $data : self::MANUFACTURER_NAME;
    }

    /**
     * @inheritDoc
     */
    public function getMANUFACTURER_TYPE_DESCR()
    {
        $data = parent::getMANUFACTURER_TYPE_DESCR();

        return !empty($data) ? $data : self::MANUFACTURER_TYPE_DESCR;
    }

    /**
     * @inheritDoc
     */
    public function getSPECIAL_TREATMENT_CLASS()
    {
        $data = parent::getSPECIAL_TREATMENT_CLASS();

        return !empty($data) ? $data : self::SPECIAL_TREATMENT_CLASS;
    }

    /**
     * @inheritDoc
     */
    public function getSTATUS()
    {
        $data = parent::getSTATUS();

        return !empty($data) ? $data : self::STATUS;
    }

    /**
     * @inheritDoc
     */
    public function getFEATURES()
    {
        $data = parent::getFEATURES();

        return !empty($data) ? $data : self::FEATURES;
    }

    /**
     * @inheritDoc
     */
    public function getREFERENCE_FEATURE_SYSTEM_NAME()
    {
        $data = parent::getREFERENCE_FEATURE_SYSTEM_NAME();

        return !empty($data) ? $data : self::REFERENCE_FEATURE_SYSTEM_NAME;
    }

    /**
     * @inheritDoc
     */
    public function getREFERENCE_FEATURE_GROUP_ID()
    {
        $data = parent::getREFERENCE_FEATURE_GROUP_ID();

        return !empty($data) ? $data : self::REFERENCE_FEATURE_GROUP_ID;
    }

    /**
     * @inheritDoc
     */
    public function getREFERENCE_FEATURE_GROUP_NAME()
    {
        $data = parent::getREFERENCE_FEATURE_GROUP_NAME();

        return !empty($data) ? $data : self::REFERENCE_FEATURE_GROUP_NAME;
    }

    /**
     * @inheritDoc
     */
    public function getFEATURE()
    {
        $data = parent::getFEATURE();

        return !empty($data) ? $data : self::FEATURE;
    }
    /**
     * @inheritDoc
     */
    public function getFNAME()
    {
        $data = parent::getFNAME();

        return !empty($data) ? $data : self::FNAME;
    }
    /**
     * @inheritDoc
     */
    public function getFVALUE()
    {
        $data = parent::getFVALUE();

        return !empty($data) ? $data : self::FVALUE;
    }
    /**
     * @inheritDoc
     */
    public function getFUNIT()
    {
        $data = parent::getFUNIT();

        return !empty($data) ? $data : self::FUNIT;
    }
    /**
     * @inheritDoc
     */
    public function getFDESCR()
    {
        $data = parent::getFDESCR();

        return !empty($data) ? $data : self::FDESCR;
    }
    /**
     * @inheritDoc
     */
    public function getORDER_DETAILS()
    {
        $data = parent::getORDER_DETAILS();

        return !empty($data) ? $data : self::ORDER_DETAILS;
    }
    /**
     * @inheritDoc
     */
    public function getORDER_UNIT()
    {
        $data = parent::getORDER_UNIT();

        return !empty($data) ? $data : self::ORDER_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getCONTENT_UNIT()
    {
        $data = parent::getCONTENT_UNIT();

        return !empty($data) ? $data : self::CONTENT_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getNO_CU_PER_OU()
    {
        $data = parent::getNO_CU_PER_OU();

        return !empty($data) ? $data : self::NO_CU_PER_OU;
    }
    /**
     * @inheritDoc
     */
    public function getQUANTITY_MIN()
    {
        $data = parent::getQUANTITY_MIN();

        return !empty($data) ? $data : self::QUANTITY_MIN;
    }
    /**
     * @inheritDoc
     */
    public function getQUANTITY_INTERVAL()
    {
        $data = parent::getQUANTITY_INTERVAL();

        return !empty($data) ? $data : self::QUANTITY_INTERVAL;
    }
    /**
     * @inheritDoc
     */
    public function getPRICE_QUANTITY()
    {
        $data = parent::getPRICE_QUANTITY();

        return !empty($data) ? $data : self::PRICE_QUANTITY;
    }
    /**
     * @inheritDoc
     */
    public function getPRICE_DETAILS()
    {
        $data = parent::getPRICE_DETAILS();

        return !empty($data) ? $data : self::PRICE_DETAILS;
    }
    /**
     * @inheritDoc
     */
    public function getDATETIME()
    {
        $data = parent::getDATETIME();

        return !empty($data) ? $data : self::DATETIME;
    }
    /**
     * @inheritDoc
     */
    public function getDATE()
    {
        $data = parent::getDATE();

        return !empty($data) ? $data : self::DATE;
    }
    /**
     * @inheritDoc
     */
    public function getPRICE()
    {
        $data = parent::getPRICE();

        return !empty($data) ? $data : self::PRICE;
    }
    /**
     * @inheritDoc
     */
    public function getPRICE_AMOUNT()
    {
        $data = parent::getPRICE_AMOUNT();

        return !empty($data) ? $data : self::PRICE_AMOUNT;
    }
    /**
     * @inheritDoc
     */
    public function getPRICE_CURRENCY()
    {
        $data = parent::getPRICE_CURRENCY();

        return !empty($data) ? $data : self::PRICE_CURRENCY;
    }
    /**
     * @inheritDoc
     */
    public function getTAX()
    {
        $data = parent::getTAX();

        return !empty($data) ? $data : self::TAX;
    }
    /**
     * @inheritDoc
     */
    public function getLOWER_BOUND()
    {
        $data = parent::getLOWER_BOUND();

        return !empty($data) ? $data : self::LOWER_BOUND;
    }

    /**
     * @inheritDoc
     */
    public function getMIME_INFO()
    {
        $data = parent::getMIME_INFO();

        return !empty($data) ? $data : self::MIME_INFO;
    }

    /**
     * @inheritDoc
     */
    public function getMIME()
    {
        $data = parent::getMIME();

        return !empty($data) ? $data : self::MIME;
    }
    
    /**
     * @inheritDoc
     */
    public function getMIME_SOURCE()
    {
        $data = parent::getMIME_SOURCE();

        return !empty($data) ? $data : self::MIME_SOURCE;
    }

    /**
     * @inheritDoc
     */
    public function getMIME_DESCR()
    {
        $data = parent::getMIME_DESCR();

        return !empty($data) ? $data : self::MIME_DESCR;
    }
    /**
     * @inheritDoc
     */
    public function getMIME_TYPE()
    {
        $data = parent::getMIME_TYPE();

        return !empty($data) ? $data : self::MIME_TYPE;
    }
    /**
     * @inheritDoc
     */
    public function getMIME_PURPOSE()
    {
        $data = parent::getMIME_PURPOSE();

        return !empty($data) ? $data : self::MIME_PURPOSE;
    }
    /**
     * @inheritDoc
     */
    public function getUSER_DEFINED_EXTENSIONS()
    {
        $data = parent::getUSER_DEFINED_EXTENSIONS();

        return !empty($data) ? $data : self::USER_DEFINED_EXTENSIONS;
    }
    /**
     * @inheritDoc
     */
    public function getMANUFACTURER_ACROYNM()
    {
        $data = parent::getMANUFACTURER_ACROYNM();

        return !empty($data) ? $data : self::MANUFACTURER_ACROYNM;
    }
    /**
     * @inheritDoc
     */
    public function getLANGTEXT()
    {
        $data = parent::getLANGTEXT();

        return !empty($data) ? $data : self::LANGTEXT;
    }
    /**
     * @inheritDoc
     */
    public function getLIEFERUMFANG()
    {
        $data = parent::getLIEFERUMFANG();

        return !empty($data) ? $data : self::LIEFERUMFANG;
    }
    /**
     * @inheritDoc
     */
    public function getTECHNISCHE_DATEN()
    {
        $data = parent::getTECHNISCHE_DATEN();

        return !empty($data) ? $data : self::TECHNISCHE_DATEN;
    }
    /**
     * @inheritDoc
     */
    public function getGARANTIEBEDINGUNGEN()
    {
        $data = parent::getGARANTIEBEDINGUNGEN();

        return !empty($data) ? $data : self::GARANTIEBEDINGUNGEN;
    }
    /**
     * @inheritDoc
     */
    public function getANWENDUNGSBEISPIELE()
    {
        $data = parent::getANWENDUNGSBEISPIELE();

        return !empty($data) ? $data : self::ANWENDUNGSBEISPIELE;
    }
    /**
     * @inheritDoc
     */
    public function getDISCOUNT_GROUP_MANUFACTURER()
    {
        $data = parent::getDISCOUNT_GROUP_MANUFACTURER();

        return !empty($data) ? $data : self::DISCOUNT_GROUP_MANUFACTURER;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNITS()
    {
        $data = parent::getPACKING_UNITS();

        return !empty($data) ? $data : self::PACKING_UNITS;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT()
    {
        $data = parent::getPACKING_UNIT();

        return !empty($data) ? $data : self::PACKING_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_QUANTITY_MIN()
    {
        $data = parent::getPACKING_UNIT_QUANTITY_MIN();

        return !empty($data) ? $data : self::PACKING_UNIT_QUANTITY_MIN;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_QUANTITY_MAX()
    {
        $data = parent::getPACKING_UNIT_QUANTITY_MAX();

        return !empty($data) ? $data : self::PACKING_UNIT_QUANTITY_MAX;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_LENGTH()
    {
        $data = parent::getPACKING_UNIT_LENGTH();

        return !empty($data) ? $data : self::PACKING_UNIT_LENGTH;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_LENGTH_UNIT()
    {
        $data = parent::getPACKING_UNIT_LENGTH_UNIT();

        return !empty($data) ? $data : self::PACKING_UNIT_LENGTH_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_WIDTH()
    {
        $data = parent::getPACKING_UNIT_WIDTH();

        return !empty($data) ? $data : self::PACKING_UNIT_WIDTH;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_WIDTH_UNIT()
    {
        $data = parent::getPACKING_UNIT_WIDTH_UNIT();

        return !empty($data) ? $data : self::PACKING_UNIT_WIDTH_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_DEPTH()
    {
        $data = parent::getPACKING_UNIT_DEPTH();

        return !empty($data) ? $data : self::PACKING_UNIT_DEPTH;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_DEPTH_UNIT()
    {
        $data = parent::getPACKING_UNIT_DEPTH_UNIT();

        return !empty($data) ? $data : self::PACKING_UNIT_DEPTH_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_WEIGHT()
    {
        $data = parent::getPACKING_UNIT_WEIGHT();

        return !empty($data) ? $data : self::PACKING_UNIT_WEIGHT;
    }
    /**
     * @inheritDoc
     */
    public function getPACKING_UNIT_WEIGHT_UNIT()
    {
        $data = parent::getPACKING_UNIT_WEIGHT_UNIT();

        return !empty($data) ? $data : self::PACKING_UNIT_WEIGHT_UNIT;
    }
    /**
     * @inheritDoc
     */
    public function getWEIGHT()
    {
        $data = parent::getWEIGHT();

        return !empty($data) ? $data : self::WEIGHT;
    }
    /**
     * @inheritDoc
     */
    public function getREACH()
    {
        $data = parent::getREACH();

        return !empty($data) ? $data : self::REACH;
    }
    /**
     * @inheritDoc
     */
    public function getREACH_INFO()
    {
        $data = parent::getREACH_INFO();

        return !empty($data) ? $data : self::REACH_INFO;
    }
    /**
     * @inheritDoc
     */
    public function getLOGISTICS_DETAILS()
    {
        $data = parent::getLOGISTICS_DETAILS();

        return !empty($data) ? $data : self::LOGISTICS_DETAILS;
    }
    /**
     * @inheritDoc
     */
    public function getCUSTOMS_TARIFF_NUMBER()
    {
        $data = parent::getCUSTOMS_TARIFF_NUMBER();

        return !empty($data) ? $data : self::CUSTOMS_TARIFF_NUMBER;
    }
    /**
     * @inheritDoc
     */
    public function getCUSTOMS_NUMBER()
    {
        $data = parent::getCUSTOMS_NUMBER();

        return !empty($data) ? $data : self::CUSTOMS_NUMBER;
    }
    /**
     * @inheritDoc
     */
    public function getCOUNTRY_OF_ORIGIN()
    {
        $data = parent::getCOUNTRY_OF_ORIGIN();

        return !empty($data) ? $data : self::COUNTRY_OF_ORIGIN;
    }
}
