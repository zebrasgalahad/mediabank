<?php

namespace TrueRomanceBundle\Model\FieldCollections;

use Pimcore\Model\DataObject\Fieldcollection\Data\XMLExportSupplierFields as PimcoreXMLExportSupplierFields;

class XMLExportSupplierFields extends PimcoreXMLExportSupplierFields
{
    const SUPPLIER = 'SUPPLIER';
    const SUPPLIER_ID = 'SUPPLIER_ID';
    const SUPPLIER_NAME = 'NAME';
    const SUPPLIER_ADDRESS = 'ADDRESS';
    const SUPPLIER_STREET = 'STREET';
    const SUPPLIER_ZIP = 'ZIP';
    const SUPPLIER_CITY = 'CITY';
    const SUPPLIER_STATE = 'STATE';
    const SUPPLIER_COUNTRY = 'COUNTRY';
    const SUPPLIER_PHONE = 'PHONE';
    const SUPPLIER_FAX = 'FAX';
    const SUPPLIER_EMAIL = 'EMAIL';
    const SUPPLIER_URL = 'URL';
    const SUPPLIER_REMARKS = 'ADDRESS_REMARKS';

    /**
     * @inheritDoc
     */
    public function getSUPPLIER()
    {
        $data = parent::getSUPPLIER();

        return !empty($data) ? $data : self::SUPPLIER;
    }

    /**
     * @inheritDoc
     */
    public function getSUPPLIER_ID()
    {
        $data = parent::getSUPPLIER_ID();

        return !empty($data) ? $data : self::SUPPLIER_ID;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_NAME()
    {
        $data = parent::getSUPPLIER_NAME();

        return !empty($data) ? $data : self::SUPPLIER_NAME;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_ADDRESS()
    {
        $data = parent::getSUPPLIER_ADDRESS();

        return !empty($data) ? $data : self::SUPPLIER_ADDRESS;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_STREET()
    {
        $data = parent::getSUPPLIER_STREET();

        return !empty($data) ? $data : self::SUPPLIER_STREET;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_ZIP()
    {
        $data = parent::getSUPPLIER_ZIP();

        return !empty($data) ? $data : self::SUPPLIER_ZIP;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_CITY()
    {
        $data = parent::getSUPPLIER_CITY();

        return !empty($data) ? $data : self::SUPPLIER_CITY;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_STATE()
    {
        $data = parent::getSUPPLIER_STATE();

        return !empty($data) ? $data : self::SUPPLIER_STATE;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_COUNTRY()
    {
        $data = parent::getSUPPLIER_COUNTRY();

        return !empty($data) ? $data : self::SUPPLIER_COUNTRY;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_PHONE()
    {
        $data = parent::getSUPPLIER_PHONE();

        return !empty($data) ? $data : self::SUPPLIER_PHONE;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_FAX()
    {
        $data = parent::getSUPPLIER_FAX();

        return !empty($data) ? $data : self::SUPPLIER_FAX;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_EMAIL()
    {
        $data = parent::getSUPPLIER_EMAIL();

        return !empty($data) ? $data : self::SUPPLIER_EMAIL;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_URL()
    {
        $data = parent::getSUPPLIER_URL();

        return !empty($data) ? $data : self::SUPPLIER_URL;
    }
    /**
     * @inheritDoc
     */
    public function getSUPPLIER_ADDRESS_REMARKS()
    {
        $data = parent::getSUPPLIER_ADDRESS_REMARKS();

        return !empty($data) ? $data : self::SUPPLIER_REMARKS;
    }

}
