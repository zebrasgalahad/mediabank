<?php

namespace TrueRomanceBundle\Model\FieldCollections;

use Pimcore\Model\DataObject\Fieldcollection\Data\XMLExportCatalogGroupFields as PimcoreXMLExportCatalogGroupFields;

class XMLExportCatalogGroupFields extends PimcoreXMLExportCatalogGroupFields
{
    const CATALOG_GROUP_SYSTEM = 'CATALOG_GROUP_SYSTEM';
    const CATALOG_STRUCTURE = 'CATALOG_STRUCTURE';
    const CATALOG_GROUP_ID = 'CATALOG_GROUP_ID';
    const CATALOG_GROUP_NAME = 'CATALOG_GROUP_NAME';
    const CATALOG_PARENT_ID = 'CATALOG_PARENT_ID';
    const CATALOG_GROUP_ORDER = 'CATALOG_GROUP_ORDER';
    const PRODUCT_TO_CATALOGGROUP_MAP = 'PRODUCT_TO_CATALOGGROUP_MAP';
    const PROD_ID = 'PROD_ID';
    const MAP_CATALOG_GROUP_ID = 'MAP_CATALOG_GROUP_ID';
    const PRODUCT_TO_CATALOGGROUP_MAP_GROUP_ID = 'CATALOG_GROUP_ID';

    /**
     * @inheritDoc
     */
    public function getCATALOG_GROUP_SYSTEM()
    {
        $data = parent::getCATALOG_GROUP_SYSTEM();

        return !empty($data) ? $data : self::CATALOG_GROUP_SYSTEM;
    }

    /**
     * @inheritDoc
     */
    public function getCATALOG_STRUCTURE()
    {
        $data = parent::getCATALOG_STRUCTURE();

        return !empty($data) ? $data : self::CATALOG_STRUCTURE;
    }
    /**
     * @inheritDoc
     */
    public function getCATALOG_GROUP_ID()
    {
        $data = parent::getCATALOG_GROUP_ID();

        return !empty($data) ? $data : self::CATALOG_GROUP_ID;
    }
    /**
     * @inheritDoc
     */
    public function getCATALOG_GROUP_NAME()
    {
        $data = parent::getCATALOG_GROUP_NAME();

        return !empty($data) ? $data : self::CATALOG_GROUP_NAME;
    }
    /**
     * @inheritDoc
     */
    public function getCATALOG_PARENT_ID()
    {
        $data = parent::getCATALOG_PARENT_ID();

        return !empty($data) ? $data : self::CATALOG_PARENT_ID;
    }
    /**
     * @inheritDoc
     */
    public function getCATALOG_GROUP_ORDER()
    {
        $data = parent::getCATALOG_GROUP_ORDER();

        return !empty($data) ? $data : self::CATALOG_GROUP_ORDER;
    }

    /**
     * @inheritDoc
     */
    public function getPRODUCT_TO_CATALOGGROUP_MAP()
    {
        $data = parent::getPRODUCT_TO_CATALOGGROUP_MAP();

        return !empty($data) ? $data : self::PRODUCT_TO_CATALOGGROUP_MAP;
    }

    /**
     * @inheritDoc
     */
    public function getPRODUCT_TO_CATALOGGROUP_MAP_GROUP_ID()
    {
        $data = parent::getPRODUCT_TO_CATALOGGROUP_MAP_GROUP_ID();

        return !empty($data) ? $data : self::PRODUCT_TO_CATALOGGROUP_MAP_GROUP_ID;
    }

    /**
     * @inheritDoc
     */
    public function getPROD_ID()
    {
        $data = parent::getPROD_ID();

        return !empty($data) ? $data : self::PROD_ID;
    }
    /**
     * @inheritDoc
     */
    public function getMAP_CATALOG_GROUP_ID()
    {
        $data = parent::getMAP_CATALOG_GROUP_ID();

        return !empty($data) ? $data : self::MAP_CATALOG_GROUP_ID;
    }
}
