<?php

namespace TrueRomanceBundle\Model\FieldCollections;

use Pimcore\Model\DataObject\Fieldcollection\Data\XMLExportBuyerFields as PimcoreXMLExportBuyerFields;

class XMLExportBuyerFields extends PimcoreXMLExportBuyerFields
{
    const BUYER = 'BUYER';
    const BUYER_ID = 'BUYER_ID';
    const BUYER_NAME = 'BUYER_NAME';
    const ADDRESS = 'ADDRESS';
    const STREET = 'STREET';
    const ZIP = 'ZIP';
    const CITY = 'CITY';
    const COUNTRY = 'COUNTRY';

    /**
     * @inheritDoc
     */
    public function getBUYER_ID_SUPPLIER_SPECIFIC()
    {
        $data = parent::getBUYER_ID_SUPPLIER_SPECIFIC();

        return !empty($data) ? $data : self::BUYER_ID;
    }

    /**
     * @inheritDoc
     */
    public function getBUYER()
    {
        $data = parent::getBUYER();

        return !empty($data) ? $data : self::BUYER;
    }

    /**
     * @inheritDoc
     */
    public function getBUYER_NAME()
    {
        $data = parent::getBUYER_NAME();

        return !empty($data) ? $data : self::BUYER_NAME;
    }
    /**
     * @inheritDoc
     */
    public function getBUYER_ADDRESS()
    {
        $data = parent::getBUYER_ADDRESS();

        return !empty($data) ? $data : self::ADDRESS;
    }
    /**
     * @inheritDoc
     */
    public function getBUYER_STREET()
    {
        $data = parent::getBUYER_STREET();

        return !empty($data) ? $data : self::STREET;
    }
    /**
     * @inheritDoc
     */
    public function getBUYER_ZIP()
    {
        $data = parent::getBUYER_ZIP();

        return !empty($data) ? $data : self::ZIP;
    }
    /**
     * @inheritDoc
     */
    public function getBUYER_CITY()
    {
        $data = parent::getBUYER_CITY();

        return !empty($data) ? $data : self::CITY;
    }
    /**
     * @inheritDoc
     */
    public function getBUYER_COUNTRY()
    {
        $data = parent::getBUYER_COUNTRY();

        return !empty($data) ? $data : self::COUNTRY;
    }
}
