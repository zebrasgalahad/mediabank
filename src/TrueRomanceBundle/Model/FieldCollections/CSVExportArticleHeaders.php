<?php

namespace TrueRomanceBundle\Model\FieldCollections;

use Pimcore\Model\DataObject\Fieldcollection\Data\CSVExportArticleHeaders as PimcoreCSVExportArticleHeaders;

class CSVExportArticleHeaders extends PimcoreCSVExportArticleHeaders
{
    const PIM_ID = 'Pim-ID';
    const SUPPLIER_PID = 'Artikelnummer';
    const MANUFACTURER_TYPE_DESCR = 'Type';
    const MANUFACTURER_NAME = 'Hersteller';
    const BRAND = 'Marke';
    const DESCRIPTION_SHORT = 'Artikeltext 1';
    const DESCRIPTION_SHORT2 = 'Artikeltext 2';
    const DESCRIPTION_LONG = 'Kurzbeschreibung';
    const USP_LIST = 'Produktstärken';
    const SCOPE_OF_DELIVERY_LIST = 'Lieferumfang';
    const TEC_SPEC_LIST = 'Technische Daten';
    const WARRANTIES_LIST = 'Grantiebedingungen';
    const APPLICATION_LIST = 'Anwendungsbeispiele';
    const EAN = 'GTIN/EAN';
    const DISCOUNT_GROUP_MANUFACTURER = 'Rabattgruppe';
    const PRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT = 'UVP o. Mwst.';
    const CUSTOMER_SPECIAL_PRICE_AMOUNT = 'Aktions-UVP o. Mwst.';
    const PRODUCT_PRICE_NRP_PRICE_AMOUNT_ = 'Händler-Nettopreis ohne MWST';
    const PRODUCT_PRICE_NRP_PRICE_CURRENCY = 'Währung';
    const PRODUCT_PRICE_NRP_TAX = 'Umsatzsteuer';
    const PRICE_NRP_PRICE_AMOUNT = 'Listenpreis 1';
    const PRICE_NRP_QUANTITY = 'Listenpreismenge 1';
    const LIST_PRICE2_AMOUNT = 'Listenpreis 2';
    const LIST_PRICE2_QUANTITY = 'Listenpreismenge 2';
    const LIST_PRICE3_AMOUNT = 'Listenpreis 3';
    const LIST_PRICE3_QUANTITY = 'Listenpreismenge 3';
    const LIST_PRICE4_AMOUNT = 'Listenpreis 4';
    const LIST_PRICE4_QUANTITY = 'Listenpreismenge 4';
    const LIST_PRICE5_AMOUNT = 'Listenpreis 5';
    const LIST_PRICE5_QUANTITY = 'Listenpreismenge 5';
    const LIST_PRICE6_AMOUNT = 'Listenpreis 6';
    const LIST_PRICE6_QUANTITY = 'Listenpreismenge 6';
    const PRICE_QUANTITY = 'Preisbasis';
    const ORDER_UNIT = 'Bestelleinheit';
    const NO_CU_PER_OU = 'Inhalt pro Bestelleinheit';
    const CONTENT_UNIT = 'Inhaltseinheit';
    const QUANTITY_MIN = 'Mindestbestellmenge';
    const QUANTITY_MIN_UNIT = 'Mengeneinheit';
    const PACKING_UNITS_LENGTH = 'Länge';
    const PACKING_UNITS_WIDTH = 'Breite';
    const PACKING_UNITS_DEPTH = 'Höhe';
    const PACKING_UNITS_LENGTH_UNIT = 'Einheit';
    const PACKING_UNITS_WEIGHT = 'Gewicht';
    const PACKING_UNITS_WEIGHT_UNIT = 'Einheit';
    const PRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER = 'Zolltarifnummer';
    const PRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN = 'Ursprungsland';
    const REFERENCE_FEATURE_GROUP_ID = 'proficl@ss 5.0 ID';
    const REFERENCE_FEATURE_GROUP_NAME = 'proficl@ss 5.0 Name';
    const CATALOG_STRUCTURE_LEVEL = 'Katalogstruktur LV';
    const PRODUCT_STATUS_NEW_PRODUCT = 'Neuprodukt';
    const NEW_FROM = 'Einführungsdatum';
    const DISCONTINUED_PRODUCT = 'Auslaufartikel';
    const DISCONTINUED_FROM = 'Auslaufdatum';
    const FOLLOWUP_ARTICLE = 'Empfohlener Ersatzartikel';
    const ARTICLE_STATUS = 'Artikelstatus';
    const SPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE = 'Gefahrstoffe';
    const MASTER_SERVICE = 'Meisterservice';

    public function getPIM_ID()
    {
        $data = parent::getPIM_ID();

        return !empty($data) ? $data : self::PIM_ID;
    }

    public function getSUPPLIER_PID()
    {
        $data = parent::getSUPPLIER_PID();

        return !empty($data) ? $data : self::SUPPLIER_PID;
    }

    public function getMANUFACTURER_TYPE_DESCR()
    {
        $data = parent::getMANUFACTURER_TYPE_DESCR();

        return !empty($data) ? $data : self::MANUFACTURER_TYPE_DESCR;
    }

    public function getMANUFACTURER_NAME()
    {
        $data = parent::getMANUFACTURER_NAME();

        return !empty($data) ? $data : self::MANUFACTURER_NAME;
    }

    public function getBRAND()
    {
        $data = parent::getBRAND();

        return !empty($data) ? $data : self::BRAND;
    }

    public function getDESCRIPTION_SHORT()
    {
        $data = parent::getDESCRIPTION_SHORT();

        return !empty($data) ? $data : self::DESCRIPTION_SHORT;
    }

    public function getDESCRIPTION_SHORT2()
    {
        $data = parent::getDESCRIPTION_SHORT2();

        return !empty($data) ? $data : self::DESCRIPTION_SHORT2;
    }

    public function getDESCRIPTION_LONG()
    {
        $data = parent::getDESCRIPTION_LONG();

        return !empty($data) ? $data : self::DESCRIPTION_LONG;
    }

    public function getUSP_LIST()
    {
        $data = parent::getUSP_LIST();

        return !empty($data) ? $data : self::USP_LIST;
    }

    public function getSCOPE_OF_DELIVERY_LIST()
    {
        $data = parent::getSCOPE_OF_DELIVERY_LIST();

        return !empty($data) ? $data : self::SCOPE_OF_DELIVERY_LIST;
    }

    public function getTec_specs_list()
    {
        $data = parent::getTec_specs_list();

        return !empty($data) ? $data : self::TEC_SPEC_LIST;
    }

    public function getWARRANTIES_LIST()
    {
        $data = parent::getWARRANTIES_LIST();

        return !empty($data) ? $data : self::WARRANTIES_LIST;
    }

    public function getAPPLICATION_LIST()
    {
        $data = parent::getAPPLICATION_LIST();

        return !empty($data) ? $data : self::APPLICATION_LIST;
    }

    public function getEAN()
    {
        $data = parent::getEAN();

        return !empty($data) ? $data : self::EAN;
    }

    public function getDISCOUNT_GROUP_MANUFACTURER()
    {
        $data = parent::getDISCOUNT_GROUP_MANUFACTURER();

        return !empty($data) ? $data : self::DISCOUNT_GROUP_MANUFACTURER;
    }

    public function getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT()
    {
        $data = parent::getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT();

        return !empty($data) ? $data : self::PRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT;
    }

    public function getCUSTOMER_SPECIAL_PRICE_AMOUNT()
    {
        $data = parent::getCUSTOMER_SPECIAL_PRICE_AMOUNT();

        return !empty($data) ? $data : self::CUSTOMER_SPECIAL_PRICE_AMOUNT;
    }

    public function getPRODUCT_PRICE_NRP_PRICE_AMOUNT_()
    {
        $data = parent::getPRODUCT_PRICE_NRP_PRICE_AMOUNT_();

        return !empty($data) ? $data : self::PRODUCT_PRICE_NRP_PRICE_AMOUNT_;
    }

    public function getPRODUCT_PRICE_NRP_PRICE_CURRENCY()
    {
        $data = parent::getPRODUCT_PRICE_NRP_PRICE_CURRENCY();

        return !empty($data) ? $data : self::PRODUCT_PRICE_NRP_PRICE_CURRENCY;
    }

    public function getPRODUCT_PRICE_NRP_TAX()
    {
        $data = parent::getPRODUCT_PRICE_NRP_TAX();

        return !empty($data) ? $data : self::PRODUCT_PRICE_NRP_TAX;
    }

    public function getPRICE_NRP_PRICE_AMOUNT()
    {
        $data = parent::getPRICE_NRP_PRICE_AMOUNT();

        return !empty($data) ? $data : self::PRICE_NRP_PRICE_AMOUNT;
    }

    public function getPRICE_NRP_QUANTITY()
    {
        $data = parent::getPRICE_NRP_QUANTITY();

        return !empty($data) ? $data : self::PRICE_NRP_QUANTITY;
    }

    public function getLIST_PRICE2_AMOUNT()
    {
        $data = parent::getLIST_PRICE2_AMOUNT();

        return !empty($data) ? $data : self::LIST_PRICE2_AMOUNT;
    }

    public function getLIST_PRICE2_QUANTITY()
    {
        $data = parent::getLIST_PRICE2_QUANTITY();

        return !empty($data) ? $data : self::LIST_PRICE2_QUANTITY;
    }

    public function getLIST_PRICE3_AMOUNT()
    {
        $data = parent::getLIST_PRICE3_AMOUNT();

        return !empty($data) ? $data : self::LIST_PRICE3_AMOUNT;
    }

    public function getLIST_PRICE3_QUANTITY()
    {
        $data = parent::getLIST_PRICE3_QUANTITY();

        return !empty($data) ? $data : self::LIST_PRICE3_QUANTITY;
    }

    public function getLIST_PRICE4_AMOUNT()
    {
        $data = parent::getLIST_PRICE4_AMOUNT();

        return !empty($data) ? $data : self::LIST_PRICE4_AMOUNT;
    }

    public function getLIST_PRICE4_QUANTITY()
    {
        $data = parent::getLIST_PRICE4_QUANTITY();

        return !empty($data) ? $data : self::LIST_PRICE4_QUANTITY;
    }

    public function getLIST_PRICE5_AMOUNT()
    {
        $data = parent::getLIST_PRICE5_AMOUNT();

        return !empty($data) ? $data : self::LIST_PRICE5_AMOUNT;
    }

    public function getLIST_PRICE5_QUANTITY()
    {
        $data = parent::getLIST_PRICE5_QUANTITY();

        return !empty($data) ? $data : self::LIST_PRICE5_QUANTITY;
    }

    public function getLIST_PRICE6_AMOUNT()
    {
        $data = parent::getLIST_PRICE6_AMOUNT();

        return !empty($data) ? $data : self::LIST_PRICE6_AMOUNT;
    }

    public function getLIST_PRICE6_QUANTITY()
    {
        $data = parent::getLIST_PRICE6_QUANTITY();

        return !empty($data) ? $data : self::LIST_PRICE6_QUANTITY;
    }

    public function getPRICE_QUANTITY()
    {
        $data = parent::getPRICE_QUANTITY();

        return !empty($data) ? $data : self::PRICE_QUANTITY;
    }

    public function getORDER_UNIT()
    {
        $data = parent::getORDER_UNIT();

        return !empty($data) ? $data : self::ORDER_UNIT;
    }

    public function getNO_CU_PER_OU()
    {
        $data = parent::getNO_CU_PER_OU();

        return !empty($data) ? $data : self::NO_CU_PER_OU;
    }

    public function getCONTENT_UNIT()
    {
        $data = parent::getCONTENT_UNIT();

        return !empty($data) ? $data : self::CONTENT_UNIT;
    }

    public function getQUANTITY_MIN()
    {
        $data = parent::getQUANTITY_MIN();

        return !empty($data) ? $data : self::QUANTITY_MIN;
    }

    public function getQUANTITY_MIN_UNIT()
    {
        $data = parent::getQUANTITY_MIN_UNIT();

        return !empty($data) ? $data : self::QUANTITY_MIN_UNIT;
    }

    public function getPACKING_UNITS_LENGTH()
    {
        $data = parent::getPACKING_UNITS_LENGTH();

        return !empty($data) ? $data : self::PACKING_UNITS_LENGTH;
    }

    public function getPACKING_UNITS_WIDTH()
    {
        $data = parent::getPACKING_UNITS_WIDTH();

        return !empty($data) ? $data : self::PACKING_UNITS_WIDTH;
    }

    public function getPACKING_UNITS_DEPTH()
    {
        $data = parent::getPACKING_UNITS_DEPTH();

        return !empty($data) ? $data : self::PACKING_UNITS_DEPTH;
    }

    public function getPACKING_UNITS_LENGTH_UNIT()
    {
        $data = parent::getPACKING_UNITS_LENGTH_UNIT();

        return !empty($data) ? $data : self::PACKING_UNITS_LENGTH_UNIT;
    }

    public function getPACKING_UNITS_WEIGHT()
    {
        $data = parent::getPACKING_UNITS_WEIGHT();

        return !empty($data) ? $data : self::PACKING_UNITS_WEIGHT;
    }

    public function getPACKING_UNITS_WEIGHT_UNIT()
    {
        $data = parent::getPACKING_UNITS_WEIGHT_UNIT();

        return !empty($data) ? $data : self::PACKING_UNITS_WEIGHT_UNIT;
    }

    public function getPRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER()
    {
        $data = parent::getPRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER();

        return !empty($data) ? $data : self::PRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER;
    }

    public function getPRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN()
    {
        $data = parent::getPRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN();

        return !empty($data) ? $data : self::PRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN;
    }

    public function getREFERENCE_FEATURE_GROUP_ID()
    {
        $data = parent::getREFERENCE_FEATURE_GROUP_ID();

        return !empty($data) ? $data : self::REFERENCE_FEATURE_GROUP_ID;
    }

    public function getREFERENCE_FEATURE_GROUP_NAME()
    {
        $data = parent::getREFERENCE_FEATURE_GROUP_NAME();

        return !empty($data) ? $data : self::REFERENCE_FEATURE_GROUP_NAME;
    }

    public function getCATALOG_STRUCTURE_LEVEL()
    {
        $data = parent::getCATALOG_STRUCTURE_LEVEL();

        return !empty($data) ? $data : self::CATALOG_STRUCTURE_LEVEL;
    }

    public function getPRODUCT_STATUS_NEW_PRODUCT()
    {
        $data = parent::getPRODUCT_STATUS_NEW_PRODUCT();

        return !empty($data) ? $data : self::PRODUCT_STATUS_NEW_PRODUCT;
    }

    public function getNEW_FROM()
    {
        $data = parent::getNEW_FROM();

        return !empty($data) ? $data : self::NEW_FROM;
    }

    public function getDISCONTINUED_PRODUCT()
    {
        $data = parent::getDISCONTINUED_PRODUCT();

        return !empty($data) ? $data : self::DISCONTINUED_PRODUCT;
    }

    public function getDISCONTINUED_FROM()
    {
        $data = parent::getDISCONTINUED_FROM();

        return !empty($data) ? $data : self::DISCONTINUED_FROM;
    }

    public function getFOLLOWUP_ARTICLE()
    {
        $data = parent::getFOLLOWUP_ARTICLE();

        return !empty($data) ? $data : self::FOLLOWUP_ARTICLE;
    }

    public function getARTICLE_STATUS()
    {
        $data = parent::getARTICLE_STATUS();

        return !empty($data) ? $data : self::ARTICLE_STATUS;
    }

    public function getSPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE()
    {
        $data = parent::getSPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE();

        return !empty($data) ? $data : self::SPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE;
    }

    public function getMASTER_SERVICE()
    {
        $data = parent::getMASTER_SERVICE();

        return !empty($data) ? $data : self::MASTER_SERVICE;
    }

}
