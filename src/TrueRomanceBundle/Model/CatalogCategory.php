<?php

namespace TrueRomanceBundle\Model;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\CatalogCategory as PimcoreCatalogCategory;

class CatalogCategory extends PimcoreCatalogCategory
{
    /**
     * Get all sub categories recursivly
     *
     * @return DataObject\CatalogCategory[] $subCategories
     */
    public function getSubCateogries()
    {
        $subCategories = [];

        $children = $this->getChildren([DataObject::OBJECT_TYPE_OBJECT]);

        foreach ($children as $child) {
            if ($child instanceof PimcoreCatalogCategory) {
                $subCategories[] = $child;
            }
        }

        return $subCategories;
    }
}
