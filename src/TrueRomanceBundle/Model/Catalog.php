<?php

namespace TrueRomanceBundle\Model;

use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Catalog as PimcoreCatalog;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Database\AbstractDatabase;

class Catalog extends PimcoreCatalog
{
    /**
     * Get all catalog categories recursivly
     *
     * @param integer $limit
     * @param integer $offset
     * @param boolean $getObjects
     * @return void
     */
    public function getCategories(int $limit = 0, int $offset = 0, bool $getObjects = true)
    {
        $cateogryList = new DataObject\CatalogCategory\Listing();
        $quotedPath = $cateogryList->quote($this->getRealFullPath());
        $quotedWildcardPath = $cateogryList->quote(str_replace('//', '/', $this->getRealFullPath() . '/') . '%');
        $condition = '(o_published = 1 AND (o_path = ' . $quotedPath . ' OR o_path LIKE ' . $quotedWildcardPath . '))';
        $cateogryList->setLimit($limit);
        $cateogryList->setOffset($offset);

        $cateogryList->setCondition($condition);

        if (!$getObjects) {
            return $cateogryList;
        } else {
            return $cateogryList->getObjects();
        }
    }

    /**
     * Get catalog articles
     *
     * @param integer $limit
     * @param integer $offset
     * @param boolean $getObjects
     * @return DataObject\Article\Listing|DataObject\Article[]
     */
    public function getCatalogArticles(int $limit = 0, int $offset = 0, bool $getObjects = true)
    {
        $articleList = new DataObject\Article\Listing();
        $quotedPath = $articleList->quote($this->getRealFullPath());
        $quotedWildcardPath = $articleList->quote(str_replace('//', '/', $this->getRealFullPath() . '/') . '%');

        $categoryClassId = \Pimcore\Model\DataObject\CatalogCategory::classId();

        $condition = "
            o_published = 1 AND
            data_export_share = 'true' AND
            o_parentId IN (
                SELECT o_id FROM object_$categoryClassId
                WHERE o_published = 1 AND
                (o_path = $quotedPath OR o_path LIKE $quotedWildcardPath)
            )
        ";

        $articleList->setCondition($condition);
        $articleList->setLimit($limit);
        $articleList->setOffset($offset);

        if (!$getObjects) {
            return $articleList;
        } else {
            return $articleList->getObjects();
        }
    }

    /**
     * Retrieves all image assets linked to a catalog via a field collection.
     *
     * @param int $limit
     * @param int $offset
     * @param bool $getObjects
     * @return Asset\Listing
     */
    public function getCatalogImageAssets(int $limit = 0, int $offset = 0, bool $getObjects = true, $locale = 'de_DE')
    {
        $assetsList = new Asset\Listing();
        $db = AbstractDatabase::get();

        $articleAssetsClassId = DataObject\AssetImage::classId();
        $quotedPath = $assetsList->quote($this->getRealFullPath());
        $quotedWildcardPath = $assetsList->quote(str_replace('//', '/', $this->getRealFullPath() . '/') . '%');
        $articleClassId = DataObject\Article::classId();
        $categoryClassId = DataObject\CatalogCategory::classId();

        $query = "SELECT ooo_id FROM object_localized_query_{$articleAssetsClassId}_{$locale}
                WHERE (
                item_bmecat=1 AND
                ooo_id IN (
                SELECT object_{$articleAssetsClassId}.o_id as o_id FROM `object_{$articleAssetsClassId}` WHERE ((
                    o_published = 1 AND
                    o_parentId IN (
                        SELECT o_id FROM object_{$articleClassId}
                        WHERE o_published = 1 AND
                        o_parentId IN (
                            SELECT o_id FROM object_{$categoryClassId}
                            WHERE o_published = 1 AND
                            (o_path = {$quotedPath} OR o_path LIKE {$quotedWildcardPath})
                        )
                    )
                 AND object_{$articleAssetsClassId}.o_type IN ('object','folder')) AND object_{$articleAssetsClassId}.o_published = 1)
                )
            )";

        $assetIds = [];

        $articleAssetIds = $db->fetchAll($query);

        $listOfIds = [];

        foreach ($articleAssetIds as $articleAssetId) {
            $listOfIds[] = $articleAssetId['ooo_id'];
        }

        $subQuery = '';

        if (!empty($listOfIds)) {
            $subQuery = 'ooo_id NOT IN (' . implode(",", $listOfIds) . ') AND';
        }

        $deQuery = "SELECT ooo_id FROM object_localized_query_{$articleAssetsClassId}_de_DE
                WHERE (
                item_bmecat=1 AND
                $subQuery
                ooo_id IN (
                SELECT object_{$articleAssetsClassId}.o_id as o_id FROM `object_{$articleAssetsClassId}` WHERE ((
                    o_published = 1 AND
                    o_parentId IN (
                        SELECT o_id FROM object_{$articleClassId}
                        WHERE o_published = 1 AND
                        o_parentId IN (
                            SELECT o_id FROM object_{$categoryClassId}
                            WHERE o_published = 1 AND
                            (o_path = {$quotedPath} OR o_path LIKE {$quotedWildcardPath})
                        )
                    )
                 AND object_{$articleAssetsClassId}.o_type IN ('object','folder')) AND object_{$articleAssetsClassId}.o_published = 1)
                )
            )";

        $additionalIds = $db->fetchAll($deQuery);

        $totalIds = array_merge($articleAssetIds, $additionalIds);

        foreach ($totalIds as $articleAssetId) {
            $assetImage = DataObject\AssetImage::getById($articleAssetId['ooo_id']);

            $imageAsset = $assetImage->getAsset_relation($locale);

            // get asset for fallback locale (de_DE) if not available for $locale
            if (!$imageAsset && $locale != "de_DE") {
                $imageAsset = $assetImage->getAsset_relation('de_DE');
            }

            if ($imageAsset) {
                $assetIds[] = $imageAsset->getId();
            }
        }

        if (empty($assetIds)) {
            return [];
        }

        $implodedAssetIds = implode(",", $assetIds);

        $condition = "id IN(" . $implodedAssetIds . ")";

        $assetsList->setCondition($condition);
        $assetsList->setLimit($limit);
        $assetsList->setOffset($offset);

        if (!$getObjects) {
            return $assetsList;
        } else {
            return $assetsList->getAssets();
        }
    }

    /**
     * Get csv catalog articles
     *
     * @param integer $limit
     * @param integer $offset
     * @param boolean $getObjects
     * @return DataObject\Article\Listing|DataObject\Article[]
     */
    public function getCsvCatalogArticles(int $limit = 0, int $offset = 0, bool $getObjects = true)
    {
        $articleList = new DataObject\Article\Listing();
        $articleList->setUnpublished(true);
        $quotedPath = $articleList->quote($this->getRealFullPath());
        $quotedWildcardPath = $articleList->quote(str_replace('//', '/', $this->getRealFullPath() . '/') . '%');

        $categoryClassId = \Pimcore\Model\DataObject\CatalogCategory::classId();

        $condition = "
            data_export_share = 'true' AND
            o_parentId IN (
                SELECT o_id FROM object_$categoryClassId
                WHERE o_published = 1 AND
                (o_path = $quotedPath OR o_path LIKE $quotedWildcardPath)
            )
        ";

        $articleList->setCondition($condition);
        $articleList->setLimit($limit);
        $articleList->setOffset($offset);

        if (!$getObjects) {
            return $articleList;
        } else {
            return $articleList->getObjects();
        }
    }

    /**
     * Retrieves all image assets linked to a catalog via a field collection for csv
     *
     * @param int $limit
     * @param int $offset
     * @param bool $getObjects
     * @return Asset\Listing
     */
    public function getCsvCatalogImageAssets(int $limit = 0, int $offset = 0, bool $getObjects = true)
    {
        $assetsList = new Asset\Listing();

        $articleAssetsClassId = DataObject\ArticleAssets::classId();
        $quotedPath = $assetsList->quote($this->getRealFullPath());
        $collectionName = 'AssetImage';
        $quotedWildcardPath = $assetsList->quote(str_replace('//', '/', $this->getRealFullPath() . '/') . '%');
        $articleClassId = DataObject\Article::classId();
        $categoryClassId = DataObject\CatalogCategory::classId();

        $condition = "id IN (SELECT asset_relation FROM object_collection_{$collectionName}_{$articleAssetsClassId}
            WHERE o_id IN (
                SELECT ooo_id FROM object_collection_{$collectionName}_localized_{$articleAssetsClassId}
                WHERE (
                item_bmecat=1 AND
                ooo_id IN (
                SELECT object_{$articleAssetsClassId}.o_id as o_id FROM `object_{$articleAssetsClassId}` WHERE ((
                    o_published = 1 AND
                    o_parentId IN (
                        SELECT o_id FROM object_{$articleClassId}
                        WHERE o_published = 1 AND
                        o_parentId IN (
                            SELECT o_id FROM object_{$categoryClassId}
                            WHERE o_published = 1 AND
                            (o_path = {$quotedPath} OR o_path LIKE {$quotedWildcardPath})
                        )
                    )
                 AND object_{$articleAssetsClassId}.o_type IN ('object','folder')) AND object_{$articleAssetsClassId}.o_published = 1)
                )
                )
            ))";

        $assetsList->setCondition($condition);
        $assetsList->setLimit($limit);
        $assetsList->setOffset($offset);

        if (!$getObjects) {
            return $assetsList;
        } else {
            return $assetsList->getAssets();
        }
    }

    /**
     * Retrieves all file assets linked to a catalog via a field collection.
     *
     * @param int $limit
     * @param int $offset
     * @param bool $getObjects
     * @return Asset\Listing
     */
    public function getCatalogFileAssets(int $limit = 0, int $offset = 0, bool $getObjects = true, $locale = 'de_DE')
    {
        $assetsList = new Asset\Listing();
        $db = AbstractDatabase::get();

        $articleAssetsClassId = DataObject\AssetFile::classId();
        $quotedPath = $assetsList->quote($this->getRealFullPath());
        $quotedWildcardPath = $assetsList->quote(str_replace('//', '/', $this->getRealFullPath() . '/') . '%');
        $articleClassId = DataObject\Article::classId();
        $categoryClassId = DataObject\CatalogCategory::classId();

        $query = "SELECT ooo_id FROM object_localized_query_{$articleAssetsClassId}_{$locale}
                WHERE (
                item_bmecat=1 AND
                ooo_id IN (
                SELECT object_{$articleAssetsClassId}.o_id as o_id FROM `object_{$articleAssetsClassId}` WHERE ((
                    o_published = 1 AND
                    o_parentId IN (
                        SELECT o_id FROM object_{$articleClassId}
                        WHERE o_published = 1 AND
                        o_parentId IN (
                            SELECT o_id FROM object_{$categoryClassId}
                            WHERE o_published = 1 AND
                            (o_path = {$quotedPath} OR o_path LIKE {$quotedWildcardPath})
                        )
                    )
                 AND object_{$articleAssetsClassId}.o_type IN ('object','folder')) AND object_{$articleAssetsClassId}.o_published = 1)
                )
            )";

        $assetIds = [];

        $articleAssetIds = $db->fetchAll($query);

        $listOfIds = [];

        foreach ($articleAssetIds as $articleAssetId) {
            $listOfIds[] = $articleAssetId['ooo_id'];
        }

        $subQuery = '';

        if (!empty($listOfIds)) {
            $subQuery = 'ooo_id NOT IN (' . implode(",", $listOfIds) . ') AND';
        }

        $deQuery = "SELECT ooo_id FROM object_localized_query_{$articleAssetsClassId}_de_DE
                WHERE (
                item_bmecat=1 AND
                $subQuery
                ooo_id IN (
                SELECT object_{$articleAssetsClassId}.o_id as o_id FROM `object_{$articleAssetsClassId}` WHERE ((
                    o_published = 1 AND
                    o_parentId IN (
                        SELECT o_id FROM object_{$articleClassId}
                        WHERE o_published = 1 AND
                        o_parentId IN (
                            SELECT o_id FROM object_{$categoryClassId}
                            WHERE o_published = 1 AND
                            (o_path = {$quotedPath} OR o_path LIKE {$quotedWildcardPath})
                        )
                    )
                 AND object_{$articleAssetsClassId}.o_type IN ('object','folder')) AND object_{$articleAssetsClassId}.o_published = 1)
                )
            )";

        $additionalIds = $db->fetchAll($deQuery);

        $totalIds = array_merge($articleAssetIds, $additionalIds);

        foreach ($totalIds as $articleAssetId) {
            $assetImage = DataObject\AssetFile::getById($articleAssetId['ooo_id']);

            $imageAsset = $assetImage->getAsset_relation($locale);

            // get asset for fallback locale (de_DE) if not available for $locale
            if (!$imageAsset && $locale != "de_DE") {
                $imageAsset = $assetImage->getAsset_relation('de_DE');
            }

            if ($imageAsset) {
                $assetIds[] = $imageAsset->getId();
            }
        }

        if (empty($assetIds)) {
            return [];
        }

        $implodedAssetIds = implode(",", $assetIds);

        $condition = "id IN(" . $implodedAssetIds . ")";

        $assetsList->setCondition($condition);
        $assetsList->setLimit($limit);
        $assetsList->setOffset($offset);

        if (!$getObjects) {
            return $assetsList;
        } else {
            return $assetsList->getAssets();
        }
    }
}
