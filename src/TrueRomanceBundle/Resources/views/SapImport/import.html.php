<?php
$this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('sap.import.title') ?></h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">

    </div>
    <form class="form" method="POST" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('sap.import.label.file_upload') ?></span>
                    </div>
                    <input name="file" id="file" type="file" class="form-control" accept="text/*" required=""> 
                </div>
            </div>
            <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">
            
            <div class="form-group col-sm-12">
                <?php if ($errors && is_array($errors) && count($errors) > 0) : ?>
                    <?php foreach ($errors as $error) : ?>
                        <div><p style="color: red;"><?= $error ?></p></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>    
            
            <div class="form-group col-sm-12">
                <?php if ($success === true && ($errors && is_array($errors) && count($errors) > 0) === false) : ?>
                        <div><p style="color: green;"><?= $this->translateAdmin('sap.import.task.created') ?></p></div>
                <?php endif; ?>
            </div>      
            
            <div class="form-group col-sm-12">
                <div><p><?= $this->translateAdmin('sap.import.help_text') ?></p></div>
            </div>
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('sap.import.button.import') ?></button>
        </div>
    </form>
</div>
