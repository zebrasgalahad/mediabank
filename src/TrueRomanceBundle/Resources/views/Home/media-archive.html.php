<?php 

$result = $this->result;
/* @var $result \TrueRomanceBundle\Library\Index\Service\Result */

?>

<div class="container">
    <div class="title-panel">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <p><?php echo $this->translate("media.dataexports.title"); ?> <span class="count">(<?php echo count($result->getCollections()); ?>)</span></p>
            </div>
        </div>
    </div>
    <div class="media-block">
        <div class="row">
            <?php foreach ($result->getCollections() as $collection): ?>
                <?php 
                    $fileType = unserialize($collection["additional_data"])["file_type"];
                    $collectionType = unserialize($collection["additional_data"])["collection_type"];
                ?>
                <div class="media file visible <?php echo $fileType;?> <?php echo $collectionType; ?>" data-asset-id="<?php echo $collection["object_id"]; ?>">
                    <?php if($collection["name"]): ?>
                        <p class="overlay-title <?php echo "yellow"; ?>" title="<?php echo $collection["name"]; ?>"><?php echo $collection["name"]; ?></p>
                    <?php endif; ?>
                    <div class="title">
                        <p><?php echo $collection["description_short"]; ?></p>
                        <div class="add"></div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>