<a class="my-collection-inactive" href="" data-toggle="modal" data-target=".collection.add"><?php echo $this->translate("site.link.title.my-collections"); ?> (<span class="count"></span>)</a>

<div class="collection add modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"></button>
            <div class="modal-header">
                <p class="title"><?php echo $this->translate("collection.modal.add.title"); ?></p>
            </div>
            <div class="modal-body">
                <div class="collection-download">Auswahl download</div>
                <div class="collection-to-album">Zum Album hinzufügen</div>
                <div class="collection-share">Auswahl teilen</div>
                <div class="collection-delete">Auswahl löschen</div>
                <div class="collection-elements">
                    
                    
                    
                    
                </div>
            </div>
        </div>
    </div>
</div>

