<?php
$result = $this->result;
/* @var $result \TrueRomanceBundle\Library\Index\Service\Result */

$this->extend('@TrueRomanceBundle/Resources/layouts/home.html.php');
?>

<div class="container top" >
    <h1 class="title"><?php echo $this->document->getTitle(); ?></h1>
</div>

<div class="container overlay asset-detail-full"></div>

<div class="container overlay collection"></div>

<div class="container overlay albums"></div>

<div class="container overlay content-page"></div>

<?php require_once 'Inc/horizontal_catalog_bar.html.php'; ?>

<?php require_once 'Inc/horizontal_search_bar.html.php'; ?>

<div class="container overlay media"></div>

<div class="container content media">
    <?php if ($this->isInitialRequest === true) : ?>
        <?php require_once 'Inc/horizontal_catalog_teaser_list.html.php'; ?>
        
        <?php require_once 'media-archive.html.php'; ?>
        
        <?php require_once 'Inc/horizontal_albums_teaser_list.html.php'; ?>
    <?php endif; ?>
    
    <?php if ($this->isFilteredByCatalog === true && $this->isFiltered === false) : ?>
        <?php require_once 'Inc/horizontal_newest_teaser_list.html.php'; ?>
    
        <?php require_once 'Inc/horizontal_collection_teaser_list.html.php'; ?>
    <?php endif; ?>
    <?php require_once 'Inc/horizontal_asset_list.html.php'; ?>
</div>

<div class="container overlay dashboard">
    <?php require_once 'Inc/horizontal_media_dashboard.html.php'; ?>
</div>