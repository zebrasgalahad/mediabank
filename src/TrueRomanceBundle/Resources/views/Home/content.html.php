<div class="container content media">
    <div class="content-text-wrapper brick-container">
        <div class="content-text">
            <?= $this->areablock('content', [
                    'allowed' => [
                        'faq',
                        'wysiwyg'
                    ]
            ]); ?>
        </div>
    </div>
</div>

