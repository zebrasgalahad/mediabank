<?php 
/* @var $result \TrueRomanceBundle\Library\Index\Service\Result */
?>

<div class="media-filter" filter-id="0" style="display: none;">
    <div class="toggle"><?php echo $this->translate("filter.toggle.title"); ?></div>
    <div class="dropdown">
        <div class="facets" >
            <?php foreach ($result->getFacets() as $facetId => $facetValues): ?>
                <?php 
                    if(count($facetValues) === 0) {
                        continue;
                    }
                ?>    
                <div class="filter-group">
                    <div class="title"><?php echo $facetValues[0]["facet_title"]; ?></div>
                    <div class="filter-group-dropdown">
                        <?php foreach($facetValues as $facetValue): ?>
                            <div class="filter" filter-id="<?php echo $facetValue["facet_value"]; ?>">
                                <input name="<?php echo $facetValue["facet_id"]?>[]" value="<?php echo $facetValue["facet_value"]; ?>" type="hidden"/>
                                <label><?php echo $facetValue["facet_value_display"]; ?></label>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>        
            <?php endforeach; ?>
        </div>    
        <div class="filter-group category">
            <div class="title"><?php echo $this->translate("facet.title.product.catalog"); ?></div>
            <div class="filter-group-dropdown">
                <div class="categories">
                </div>
            </div>
        </div>
        <div class="filter-reset">
            <button class="button secondary"><?php echo $this->translate("filter.reset.all"); ?></button>
        </div>
    </div>
</div>