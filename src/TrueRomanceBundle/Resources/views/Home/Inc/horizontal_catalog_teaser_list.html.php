<div class="container horizontal-catalog-teaser-list">
    <div class="title-panel">
        <p><?php echo $this->translate("site.archive.title"); ?> <span class="count">(<?php echo count($result->getCatalogs()); ?>)</span></p>
    </div>
    <div class="archives">
        <div class="row">
            <?php foreach ($result->getCatalogs() as $catalog) : ?>
                <div class="col-xs-12 col-sm-6 col-md-3">
                    <div class="archive">
                        <a href="<?php echo $catalog["link"]; ?>" title="<?php echo $catalog["title"]; ?>" data-catalog="<?php echo $catalog["object_id"]; ?>">
                            <img src="<?php echo $catalog["preview_web_path"]; ?>"/>
                        </a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>  