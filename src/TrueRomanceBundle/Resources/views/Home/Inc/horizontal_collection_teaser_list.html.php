<div class="container horizontal-collection-teaser-list" style="display: none;">
    <div class="title-panel">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <p><?php echo $this->translate("media.dataexports.title"); ?> <span class="count">(<?php echo count($result->getCollections()); ?>)</span></p>
            </div>
            <div class="col-sm-12 col-md-3">
                <a href="#" class="all collections-teaser"><?php echo $this->translate("global.all"); ?></a>
            </div>
        </div>
    </div>
    <div class="media-block">
        <div class="row">
        </div>
    </div>   
</div>

