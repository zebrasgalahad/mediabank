<div class="container horizontal-newest-teaser-list" style="display: none;">
    <div class="title-panel">
        <div class="row">
            <div class="col-sm-12 col-md-9">
                <p><span class="yellow"><?php echo $this->translate("media.new.title"); ?></p>
            </div>
            <div class="col-sm-12 col-md-3">
                <a href="#" class="all newest-assets-teaser"><?php echo $this->translate("global.all"); ?></a>
            </div>
        </div>
    </div>
    <div class="media-block mosaic">
    </div>
</div>  