<?php 

$result = $this->result;
/* @var $result \TrueRomanceBundle\Library\Index\Service\Result */

?>

<div class="container horizontal-albums-teaser-list">
    <div class="title-panel">
        <p><?php echo $this->translate("site.albums.title"); ?> <span class="count">(<?php echo count($result->getAlbums()); ?>)</span></p>
    </div>
    <div class="albums">
        <div class="media-block mosaic">
            <?php foreach ($result->getAlbums() as $id => $album) : ?>
                <div class="album homepage-list media" data-album-id="<?php echo $id; ?>">
                    <div class="image">
                        <img src="<?php echo $album["preview_web_path"]; ?>" style="width: <?php echo $album["preview_web_width"]; ?>px; height: <?php echo $album["preview_web_height"]; ?>px"/>
                    </div>
                    <div class="title">
                        <p><?php echo $album["album_name"]; ?> <?php echo $album["creation_date"]; ?></p>
                        <a href="" class="share"></a>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
