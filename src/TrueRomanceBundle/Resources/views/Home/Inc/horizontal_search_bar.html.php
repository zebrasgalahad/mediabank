<?php /* @var $result \TrueRomanceBundle\Library\Index\Service\Result */ ?>

<div class="container horizontal-search-bar">
    <div class="search-container">
        <form class="search-form" action="<?php echo $result->getViewHelper()->createLink(); ?>">
            <?php if ($this->isFilteredByCatalog === true) : ?>
                <?php require_once 'vertical_facets_list.html.php'; ?>
            <?php endif; ?>
            <select name="top-facets" class="top-facets tr-select">
                <?php foreach($result->getMainFacets() as $mainFacet): ?>                
                    <option value="<?php echo $mainFacet["facet_id"] . ":" . $mainFacet["facet_value"];?>"
                            <?php echo $result->getViewHelper()->isActiveFilterValue($mainFacet["facet_id"], $mainFacet["facet_value"]) === true ? ' selected ' : '';  ?>
                            
                            ><?php echo $mainFacet["facet_value_display"]; ?></option>
                <?php endforeach; ?>
            </select>
            
            <div class="search">
                <input class="desktop-only" type="text" value="<?php echo $result->viewHelper->getCurrentRequestParamIfSet("search"); ?>" name="search" placeholder="<?php echo $this->translate("site.search.input.placeholder"); ?>"/>
                <input class="mobile-only" type="text" value="<?php echo $result->viewHelper->getCurrentRequestParamIfSet("search"); ?>" name="search" placeholder="<?php echo $this->translate("site.search.input.placeholder.short"); ?>"/>
                <button type="submit"></button>
            </div>
            
            <?php echo $result->getViewHelper()->getCurrentParamsAsHiddenInputFields(["search"]); ?>
        </form>
    </div>
</div>