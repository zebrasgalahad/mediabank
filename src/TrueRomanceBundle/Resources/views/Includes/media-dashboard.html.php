<div>
    <div class="media-dashboard visible">
        <p id="added-message"><?php echo $this->translate("media.dashboard.added.message"); ?></p>
        <div class="toggle"></div>
        <div class="container">
            <div class="buttons">
                <a href="#" class="button download"><?php echo $this->translate("media.dashboard.download"); ?></a>
                <a href="#" class="button add-to-album"><?php echo $this->translate("media.dashboard.add.to.album"); ?></a>
                <a href="#" class="button share"><?php echo $this->translate("media.dashboard.share"); ?></a>
                <a href="#" class="button selection-reset"><?php echo $this->translate("media.dashboard.selection.reset"); ?></a>
            </div>
            <div class="items">
                <?php for($i = 1; $i < 5; $i++): ?>
                    <div class="item">
                        <div class="remove"></div>
                        <a href="#">
                            <img src="/../static/img/dummy_media_<?php echo $i; ?>.jpg"/>
                        </a>
                    </div>
                <?php endfor; ?>
            </div>
        </div>
    </div>
</div>