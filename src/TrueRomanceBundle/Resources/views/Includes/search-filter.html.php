<div class="container">
    <div class="search-container">
        <form class="search-form" action="">
            <div class="media-filter" filter-id="0">
                <div class="toggle"><?php echo $this->translate("filter.toggle.title"); ?></div>
                <div class="dropdown">
                    <div class="filter-group" filter-id="<?php echo 1000000; // Dummy ?>">
                        <div class="title"><?php echo $this->translate("media.new.title"); ?></div>
                        <div class="filter-group-dropdown">
                            <?php for($i = 0; $i < 5; $i++): // Dummy ?>
                                <div class="filter" filter-id="<?php echo 1+$i; // Dummy ?>">
                                    <input type="hidden"/>
                                    <label><?php echo $this->translate("Lorem Ipsum"); ?></label>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="filter-group" filter-id="<?php echo 2000000; // Dummy ?>">
                        <div class="title"><?php echo $this->translate("media.new.title"); ?></div>
                        <div class="filter-group-dropdown">
                            <?php for($i = 0; $i < 5; $i++): // Dummy ?>
                                <div class="filter-sub-group" filter-id="<?php echo 200+$i; // Dummy ?>">
                                    <div class="title"><?php echo $this->translate("media.new.title"); ?></div>
                                    <div class="filter-sub-group-dropdown">
                                        <?php for($j = 0; $j < 5; $j++): // Dummy ?>
                                            <div class="filter" filter-id="<?php echo 200+$i+$j*10; // Dummy ?>">
                                                <input type="hidden" name="dummy-<?php echo 200+$i+($j+1)*10; // Dummy ?>"/>
                                                <label><?php echo $this->translate("Lorem Ipsum"); ?></label>
                                            </div>
                                        <?php endfor; ?>
                                    </div>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                    <div class="filter-group" filter-id="<?php echo 3000000; // Dummy ?>">
                        <div class="title"><?php echo $this->translate("Orientierung"); ?></div>
                        <div class="filter-group-dropdown">
                            <div class="filter orientation horizontal" filter-id="100000">
                                <input type="hidden">
                                <label><?php echo $this->translate("filter.orientation.horizontal"); ?><span class="icon"></span></label>
                            </div>
                            <div class="filter orientation vertical" filter-id="100001">
                                <input type="hidden">
                                <label><?php echo $this->translate("filter.orientation.vertical"); ?><span class="icon"></span></label>
                            </div>
                            <div class="filter orientation square" filter-id="100002">
                                <input type="hidden">
                                <label><?php echo $this->translate("filter.orientation.square"); ?><span class="icon"></span></label>
                            </div>
                            <div class="filter orientation panorama" filter-id="100003">
                                <input type="hidden">
                                <label><?php echo $this->translate("filter.orientation.panorama"); ?><span class="icon"></span></label>
                            </div>
                        </div>
                    </div>
                    <div class="filter-reset">
                        <button class="button secondary"><?php echo $this->translate("filter.reset.all"); ?></button>
                    </div>
                </div>
            </div>
            
            <select name="category-select">
                <option value="">Alle Kategorien</option>
                <option value="Neuheiten">Neuheiten</option>
                <option value="Bilder">Bilder</option>
                <option value="Logos" selected>Logos</option>
                <option value="Datenexporte">Datenexporte</option>
                <option value="Verkaufsunterlagen">Verkaufsunterlagen</option>
                <option value="Videos">Videos</option>
            </select>
            
            <div class="search <?php echo $_GET["search"] ? "active" : ""; ?>">
                <input name="search" type="text" value="<?php echo $_GET["search"]; ?>" placeholder="<?php echo $this->translate("search.placeholder"); ?>"/>
                <button type="submit"></button>
            </div>
            
            <?php echo $this->template("@ClickLayoutBundle/Resources/views/Includes/add-notification.html.php"); ?>
        </form>
    </div>
</div>