<?php
    $this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('csv.catalog.title') ?></h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">

    </div>
    <form class="form" action="<?= $this->path('admin-features-csv-export') ?>" method="POST">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.features.export.catalog_name') ?></span>
                    </div>
                    <select name="catalog" id="catalog" class="form-control">
                        <?php foreach ($catalogsSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.features.export.language') ?></span>
                    </div>
                    <select name="language" id="language" class="form-control">
                        <?php foreach ($languagesSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.features.export.bmecat') ?></span>
                    </div>
                    <select name="BMECat" id="BMECat" class="form-control">
                        <?php foreach ($BMECatSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('csv.catalog.export') ?></button>
        </div>
    </form>
</div>
