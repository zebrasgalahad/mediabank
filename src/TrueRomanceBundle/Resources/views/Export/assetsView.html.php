<?php
    $this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('admin.assets.zip.title') ?></h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">
        <?php if (!empty($message)) : ?>
            <span style="color: #298e4b;">
                <?= $message ?>
            </span>
        <?php endif; ?>
    </div>

    <form class="form" action="<?= $this->path('admin-assets-zip-view') ?>" method="POST">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('admin.assets.zip.catalog_name') ?></span>
                    </div>
                    <select name="catalog" id="catalog" class="form-control">
                        <?php foreach ($catalogsSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.language') ?></span>
                    </div>
                    <select name="language" id="language" class="form-control">
                        <?php foreach ($languagesSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('admin.assets.zip.asset_type') ?></span>
                    </div>
                    <select name="zip_mode" id="zip_mode" class="form-control">
                        <?php foreach ($zipSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('admin.assets.zip.button') ?></button>
        </div>
    </form>
</div>
