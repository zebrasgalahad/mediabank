<?php
    $this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('csv.catalog.title') ?></h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">

    </div>
    <form class="form" action="<?= $this->path('admin-catalog-csv-export') ?>" method="POST">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.catalog.export.catalog_name') ?></span>
                    </div>
                    <select name="catalog" id="catalog" class="form-control">
                        <?php foreach ($catalogsSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.catalog.export.language') ?></span>
                    </div>
                    <select name="language" id="language" class="form-control">
                        <?php foreach ($languagesSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.catalog.export.text_mode') ?></span>
                    </div>
                    <select name="textmode" id="textmode" class="form-control">
                        <?php foreach ($textmodeSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('csv.catalog.export.header_config') ?></span>
                    </div>
                    <select name="header_config" id="header_config" class="form-control">
                        <?php foreach ($configurationsSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('csv.catalog.export') ?></button>
        </div>
    </form>
</div>
