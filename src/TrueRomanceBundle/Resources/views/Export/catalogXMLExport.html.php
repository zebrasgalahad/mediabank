<?php
    $this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('xml.catalog.title') ?></h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">

    </div>
    <form class="form" action="<?= $this->path('admin-catalog-xml-export') ?>" method="POST">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.catalog_name') ?></span>
                    </div>
                    <select name="catalog[]" id="catalog" class="form-control" multiple required>
                        <?php foreach ($catalogsSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.language') ?></span>
                    </div>
                    <select name="language" id="language" class="form-control">
                        <?php foreach ($languagesSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.bmecat') ?></span>
                    </div>
                    <select name="BMECat" id="BMECat" class="form-control">
                        <?php foreach ($BMECatSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.text_mode') ?></span>
                    </div>
                    <select name="textmode" id="textmode" class="form-control">
                        <?php foreach ($textmodeSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.supplier') ?></span>
                    </div>
                    <select name="supplier" id="supplier" class="form-control">
                        <?php foreach ($suppliersSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.buyer') ?></span>
                    </div>
                    <select name="buyer" id="buyer" class="form-control">
                        <?php foreach ($buyersSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.tag_config') ?></span>
                    </div>
                    <select name="configuration" id="configuration" class="form-control">
                        <?php foreach ($configurationsSelectData as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('xml.catalog.export') ?></button>
        </div>
    </form>
</div>
