<?php
$result = $this->result;
/* @var $result \TrueRomanceBundle\Library\Index\Service\Result */

$this->extend('@TrueRomanceBundle/Resources/layouts/album.html.php');
?>

<div class="container top" >
    <h1 class="title"><?php echo $this->document->getTitle(); ?></h1>
</div>

<div class="container overlay collection"></div>

<div class="container overlay albums"></div>

<div class="container overlay media"></div>

<div class="container content media">

</div>

<div class="container overlay dashboard">
    
</div>