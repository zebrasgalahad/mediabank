<?php echo $this->inc("/includes/header");?>

<div class="container">
    <h1 class="title"><?php echo $this->document->getTitle(); ?></h1>
</div>

<?php echo $this->inc("/includes/search");?>

<div class="container">
    <div class="content">
        <?php 
            $this->wysiwyg("specialContent", [
                "height" => 200
            ]);
        ?>
    </div>
</div>

<?php echo $this->inc("/includes/footer"); ?>