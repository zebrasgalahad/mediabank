<?php echo $this->inc("/includes/header");?>

<div class="container">
    <ul class="media-navigation">
        <?php for($i = 0; $i < 9; $i++): //Dummy ?>
            <li style="width: calc(100%/9);"><a <?php echo $i==2 ? "class='active'" : ""; ?> href="">Marke <?php echo $i+1; ?><span class="count"><?php echo rand(1, 30000); ?> <?php echo $this->translate("media.objects"); ?></span></a></li>
        <?php endfor; ?>
    </ul>
</div>

<?php echo $this->inc("/includes/search-filter");?>

<?php echo $this->template("@ClickLayoutBundle/Resources/views/Includes/product-detail.html.php"); ?>

<div class="container">
    <div class="loading" for="media-block">
        <p class="text"><?php echo $this->translate("media.loading"); ?></p>
    </div>

    <div class="media-block mosaic">
        <?php for($i = 0; $i < 50; $i++): //Dummy ?>
            <div class="media">
                <a href="">
                    <img src="/../static/img/dummy_media_<?php echo rand(1, 4); ?>.jpg"/>
                </a>
                <div class="info">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                    <p class="ean"><?php echo $this->translate("EAN"); ?> 123456789</p>
                </div>
                <div class="title">
                    <p>Media #<?php echo $i+1; ?></p>
                    <div class="add"></div>
                </div>
            </div>
        <?php endfor; ?>
    </div>
</div>

<?php echo $this->inc("/includes/media-dashboard");?>

<?php echo $this->inc("/includes/footer"); ?>