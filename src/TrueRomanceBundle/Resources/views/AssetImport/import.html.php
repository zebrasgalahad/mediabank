<?php
$this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('asset.import.title') ?></h1>
</div>

<?php if ($success && $success === true): ?>
    <div class="page-sub-title">
        <h3><?= $this->translateAdmin("asset.import.job.started") ?></h3>
    </div>
<?php else: ?>
<div class="panel panel-default">
    <div class="panel-heading">

    </div>
    <form class="form" method="POST" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('asset.import.label.file_upload') ?></span>
                    </div>
                    <input name="file" id="file" type="file" class="form-control" accept="text/*" required="">
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('xml.catalog.export.language') ?></span>
                    </div>
                    <select name="locale" id="language" class="form-control" style="width: 150px;">
                        <?php foreach ($validLocales as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">
            <div class="form-group col-sm-12">
                <?php if ($errors && is_array($errors) && count($errors) > 0) : ?>
                    <?php foreach ($errors as $error) : ?>
                        <div><p style="color: red;"><?= $error ?></p></div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('asset.import.button.import') ?></button>
        </div>
    </form>
</div>
<?php endif; ?>
