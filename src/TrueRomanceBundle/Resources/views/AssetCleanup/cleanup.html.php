<?php
$this->extend('TrueRomanceBundle:Export:index.html.php');
?>
<div class="page-header">
    <h1><?= $this->translateAdmin('asset.cleanup.title') ?></h1>
</div>
<div class="panel panel-default">
    <div class="panel-heading">

    </div>
    <form class="form" method="POST" enctype="multipart/form-data">
        <div class="panel-body">
            <div class="form-group col-sm-12">
                <?php if ($success === true && ($errors && is_array($errors) && count($errors) > 0) === false) : ?>
                    <div class="form-group col-sm-12">
                        <div><p style="color: green;"><?= $this->translateAdmin('asset.cleanup.task.created') ?></p></div>
                    </div>
                <?php endif; ?>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('asset.cleanup.label.file_upload') ?></span>
                    </div>
                    <input name="file" id="file" type="file" class="form-control" accept="text/*" required="">
                </div>
            </div>
            <input type="hidden" name="csrfToken" value="<?= $this->csrfToken ?>">

            <?php if ($errors && is_array($errors) && count($errors) > 0) : ?>
                <div class="form-group col-sm-12">
                    <?php foreach ($errors as $error) : ?>
                        <div><p style="color: red;"><?= $error ?></p></div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <div class="form-group col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <span><?= $this->translateAdmin('asset.cleanup.export.language') ?></span>
                    </div>
                    <select name="locale" id="language" class="form-control" style="width: 150px;">
                        <?php foreach ($validLocales as $key => $label) : ?>
                            <option value="<?= $key ?>"><?= $label ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
            <div class="form-group col-sm-12">
                <div><p><?= $this->translateAdmin('asset.cleanup.help_text') ?></p></div>
                <div>
                    <a class="example-file" href="/static/resources/sample_files/Asset_Cleanup.csv" download=""><?= $this->translateAdmin('asset.cleanup.download-sample-file') ?></a>
                </div>
            </div>
        </div>
        <div class="panel-footer text-center">
            <button type="submit" class="btn btn-default btn-sm"><?= $this->translateAdmin('asset.cleanup.button.import') ?></button>
        </div>
    </form>
</div>
