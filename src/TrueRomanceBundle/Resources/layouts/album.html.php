<?php
    use Pimcore\Model\Document;
?>

<!DOCTYPE html>
<html lang="<?php echo $locale; ?>">  
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
        <?php
            $document = Document::getByPath($_SERVER["REQUEST_URI"]);
            
            if(!$document) {
                $document = Document::getById(1);
            }

            if($document->getTitle()) {
                // use the manually set title if available
                $this->headTitle()->set($document->getTitle());
            }
    
            if($document->getDescription()) {
                // use the manually set description if available
                $this->headMeta()->appendName('description', $document->getDescription());
            }
     
            $this->headTitle()->append("Stanley Black & Decker");
            $this->headTitle()->setSeparator(" : ");
    
            echo $this->headTitle();
            echo $this->headMeta();
            echo $this->placeholder('canonical');

            $this->headLink()->appendStylesheet('/static/bootstrap/css/bootstrap.css');
            $this->headLink()->appendStylesheet('/static/css/global.css');
            $this->headLink()->appendStylesheet('/static/css/main.css');
            $this->headLink()->appendStylesheet('/static/css/slider.css');
            $this->headLink()->appendStylesheet('/static/css/teaser.css');
            $this->headLink()->appendStylesheet('/static/css/buttons.css');
            $this->headLink()->appendStylesheet('/static/css/header.css');
            $this->headLink()->appendStylesheet('/static/css/error.css');
            $this->headLink()->appendStylesheet('/static/css/profile-dashboard.css');
            $this->headLink()->appendStylesheet('/static/css/footer.css');
            $this->headLink()->appendStylesheet('/static/css/login.css');
            $this->headLink()->appendStylesheet('/static/css/navbar.css');
            $this->headLink()->appendStylesheet('/static/css/push-menu.css');
            $this->headLink()->appendStylesheet('/static/lib/swiper/swiper.min.css');
            $this->headLink()->appendStylesheet('/static/css/search.css');
            $this->headLink()->appendStylesheet('/static/css/tr-select.css');
            $this->headLink()->appendStylesheet('/static/css/archives.css');
            $this->headLink()->appendStylesheet('/static/css/albums.css');
            $this->headLink()->appendStylesheet('/static/css/login.css');
            $this->headLink()->appendStylesheet('/static/css/register.css');
            $this->headLink()->appendStylesheet('/static/css/media.css');
            $this->headLink()->appendStylesheet('/static/css/media-navigation.css');
            $this->headLink()->appendStylesheet('/static/css/media-filter.css');
            $this->headLink()->appendStylesheet('/static/css/media-dashboard.css');
            $this->headLink()->appendStylesheet('/static/css/modal-override.css');
            $this->headLink()->appendStylesheet('/static/css/notifications.css');
            $this->headLink()->appendStylesheet('/static/css/user-invite.css');
            $this->headLink()->appendStylesheet('/static/css/loading-overlay.css');
            $this->headLink()->appendStylesheet('/static/css/product-detail.css');
            $this->headLink()->appendStylesheet('/static/css/richtext.min.css');
            $this->headLink()->appendStylesheet('/static/css/richtext_override.css');
            $this->headLink()->appendStylesheet('/static/lib/video-js/video-js.min.css', "screen");
            $this->headLink()->appendStylesheet('/static/lib/magnific/magnific.css', "screen");
            $this->headLink()->appendStylesheet('/static/lib/responsiveslides/responsiveslides.css');
            $this->headLink()->appendStylesheet('/static/lib/jquery-mosaic/jquery.mosaic.min.css');
            $this->headLink()->appendStylesheet('/static/lib/select2/css/select2.min.css');
            $this->headLink()->appendStylesheet('/static/css/jquery-confirm.css');
            $this->headLink()->appendStylesheet('/static/css/media-collection.css');
            $this->headLink()->appendStylesheet('/static/css/media-collection-download.css');
            $this->headLink()->appendStylesheet('/static/css/media-collection-albums.css');
            $this->headLink()->appendStylesheet('/static/css/select2-override.css');
            $this->headLink()->appendStylesheet('/static/css/jconfirm-override.css');
            
            if($this->editmode) {
                $this->headLink()->appendStylesheet('/static/css/editmode.css', "screen");
            }
            
            echo $this->headLink();
        ?>
    </head> 
    <body>
        <div class="push-wrapper">
                <div class="push-overlay"></div>
                <div class="loading-overlay">
                    <div class="inner">
                        <p><?php echo $this->translate("loading.overlay.text"); ?></p>
                        <img src="/static/img/loading.gif"/>
                    </div>
                </div>
                <header>
                    <div class="container">
                        <div class="logo">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <a class="navbar-brand" href="<?php echo $indexPageLink; ?>">
                                    <img src="/../static/img/logo.png" alt="StanleyBlack&Decker Logo">
                                </a>
                            </div>
                        </div>
                        <div class="header-actions">
                            <div class="profile-dashboard">
                                <a href="<?php echo $loginLink; ?>" >Login</a>
                            </div>
                        </div>
                    </div>
                </header>
            <main>
                <div class="container media-collection-container"></div>
                <div class="container album-list-container"></div>
                <?php $this->slots()->output('_content') ?>
            </main>
            <footer>   
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <ul>
                                <li><a href="">Impressum</a></li>
                                <li><a href="">Nutzungsbedingungen</a></li>
                                <li><a href="">Allgemeine Verkaufsbedingungen</a></li>
                            </ul>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <p><b>Haben Sie Fragen, Anregungen oder Kritik?</b></p>
                            <p>Senden Sie eine E-Mail mit Betreff "Mediathek" an:</p>
                            <p><a href="mailto:verkaufde.sbd@sbdinc.com">verkaufde.sbd@sbdinc.com</a></p>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
        
        <?php
            $this->headScript()->offsetSetFile(100, '/static/js/jquery-1.11.0.min.js');
            $this->headScript()->offsetSetFile(200, '/static/js/jquery-ui.min.js');
            $this->headScript()->offsetSetFile(250, '/static/js/history.js');
            $this->headScript()->offsetSetFile(275, '/static/js/jquery-confirm.js');
            $this->headScript()->offsetSetFile(260, '/static/js/jquery.loading.overlay.js');
            $this->headScript()->offsetSetFile(300, '/static/bootstrap/js/bootstrap.js');
            $this->headScript()->offsetSetFile(350, '/static/js/store.everything.min.js');
            $this->headScript()->offsetSetFile(400, '/static/lib/magnific/magnific.js');
            $this->headScript()->offsetSetFile(500, '/static/lib/video-js/video.js');
            $this->headScript()->offsetSetFile(600, '/static/lib/responsiveslides/responsiveslides.min.js');
            $this->headScript()->offsetSetFile(700, '/static/lib/equalize/equalize.js');
            $this->headScript()->offsetSetFile(800, '/static/lib/swiper/swiper.min.js');
            $this->headScript()->offsetSetFile(900, '/static/lib/bootstrap-select/bootstrap-select.js');
            $this->headScript()->offsetSetFile(1000, '/static/lib/js-cookie/js-cookie.js');
            $this->headScript()->offsetSetFile(1400, '/static/lib/jquery-mosaic/jquery.mosaic.min.js');
            $this->headScript()->offsetSetFile(1410, '/static/lib/select2/js/select2.full.min.js');
            $this->headScript()->offsetSetFile(1500, '/static/js/profile-dashboard.js');
            $this->headScript()->offsetSetFile(1600, '/static/js/srcset-polyfill.min.js');
            $this->headScript()->offsetSetFile(1700, '/static/js/header.js');
            $this->headScript()->offsetSetFile(1800, '/static/js/teaser.js');
            $this->headScript()->offsetSetFile(1900, '/static/js/slider.js');
            $this->headScript()->offsetSetFile(2000, '/static/js/search.js');
            $this->headScript()->offsetSetFile(2100, '/static/js/gallery-full.js');
            $this->headScript()->offsetSetFile(2200, '/static/js/gallery-products.js');
            $this->headScript()->offsetSetFile(2300, '/static/js/navbar.js');
            $this->headScript()->offsetSetFile(2400, '/static/js/cms-modules.js');
            $this->headScript()->offsetSetFile(2700, '/static/js/language-switch.js');
            $this->headScript()->offsetSetFile(2800, '/static/js/jquery_template.js');
            $this->headScript()->offsetSetFile(2900, '/static/js/url_parameter.js');
            $this->headScript()->offsetSetFile(3000, '/static/js/scrolling_paging_search.js');
            $this->headScript()->offsetSetFile(3100, '/static/js/scrolling_paging_product.js');
            $this->headScript()->offsetSetFile(3200, '/static/js/jquery_template.js');
            $this->headScript()->offsetSetFile(3300, '/static/js/tr-select.js');
            $this->headScript()->offsetSetFile(3400, '/static/js/media-filter.js');
            $this->headScript()->offsetSetFile(3550, '/static/js/media-collection.js');
            $this->headScript()->offsetSetFile(3560, '/static/js/album-list.js');
            $this->headScript()->offsetSetFile(3600, '/static/js/user-registration.js');
            $this->headScript()->offsetSetFile(3900, '/static/js/jquery.richtext_custom.js');
            $this->headScript()->offsetSetFile(4000, '/static/js/translation.js');
            $this->headScript()->offsetSetFile(4100, '/'. $locale .'/javascript/frontend');
            $this->headScript()->offsetSetFile(4110, '/static/js/static/helper.js');
            $this->headScript()->offsetSetFile(4150, '/static/js/static/request.js');
            $this->headScript()->offsetSetFile(4200, '/static/js/jquery.tmpl.min.js');
            $this->headScript()->offsetSetFile(4300, '/static/js/class/assetDetail.js');
            $this->headScript()->offsetSetFile(4700, '/static/js/static/user-permissions.js');
            $this->headScript()->offsetSetFile(4800, '/static/js/album.js');
            
            echo $this->headScript();
        ?>
        
        <script type="text/javascript">
            // main menu
            $(".navbar-wrapper ul.nav>li>ul").each(function () {
                var li = $(this).parent();
                var a = $("a.main", li);
        
                $(this).addClass("dropdown-menu");
                li.addClass("dropdown");
                a.addClass("dropdown-toggle");
                li.on("mouseenter", function () {
                    $("ul", $(this)).show();
                });
                li.on("mouseleave", function () {
                    $("ul", $(this)).hide();
                });
            });
        
            // side menu
            $(".bs-sidenav ul").each(function () {
                $(this).addClass("nav");
            });
        
            // gallery carousel: do not auto-start
            $('.gallery').carousel('pause');
        
            // tabbed slider text
            var clickEvent = false;
            $('.tabbed-slider').on('click', '.nav a', function() {
                clickEvent = true;
                $('.nav li').removeClass('active');
                $(this).parent().addClass('active');
            }).on('slid.bs.carousel', function(e) {
                if(!clickEvent) {
                    var count = $('.nav').children().length -1;
                    var current = $('.nav li.active');
                    current.removeClass('active').next().addClass('active');
                    var id = parseInt(current.data('slide-to'));
                    if(count == id) {
                        $('.nav li').first().addClass('active');
                    }
                }
                clickEvent = false;
            });
        
            $("#portalHeader img, #portalHeader .item, #portalHeader").height($(window).height());
        
            <?php if(!$this->editmode) { ?>
        
                // center the caption on the portal page
                $("#portalHeader .carousel-caption").css("bottom", Math.round(($(window).height() - $("#portalHeader .carousel-caption").height())/3) + "px");
        
                $(document).ready(function() {
        
                    // lightbox (magnific)
                    $('a.thumbnail').magnificPopup({
                        type:'image',
                        gallery: {
                            enabled: true
                        }
                    });
        
                    $(".image-hotspot").tooltip();
                    $(".image-marker").tooltip();
                });
        
            <?php } ?> 
                var sbd = sbd ? sbd : {};

                sbd.custom = <?php echo TrueRomanceBundle\Library\Frontend\Javascript\Data::get(); ?>;
        </script>
    </body>
</html>
                
                
