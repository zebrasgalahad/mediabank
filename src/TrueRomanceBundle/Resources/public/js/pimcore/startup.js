pimcore.registerNS("pimcore.plugin.TrueRomanceBundle");

pimcore.plugin.TrueRomanceBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.TrueRomanceBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        var user = pimcore.globalmanager.get('user');

        var toolbar = pimcore.globalmanager.get("layout_toolbar");

        if (user.isAllowed('export')) {
            this.exportNavEl = Ext.get('pimcore_menu_search').insertSibling(
                    '<li id="pimcore_menu_export" data-menu-tooltip="Export" class="pimcore_menu_item export_tab">Export</li>', 'after'
                    );

            this.exportMenu = new Ext.menu.Menu({
                items: [{
                        text: "Catalog export XML",
                        iconCls: "pimcore_icon_printcontainer",
                        handler: function () {
                            var xmlPanel = "xml_export";
                            pimcore.globalmanager.add(xmlPanel, new pimcore.tool.genericiframewindow(xmlPanel, "/admin/export/xml/catalog", "pimcore_icon_printcontainer", "Catalog export"));
                        }
                    }, {
                        text: "Catalog export CSV",
                        iconCls: "pimcore_icon_structuredTable",
                        handler: function () {
                            var csvPanel = "csv_export";
                            pimcore.globalmanager.add(csvPanel, new pimcore.tool.genericiframewindow(csvPanel, "/admin/export/csv/catalog", "pimcore_icon_structuredTable", "Catalog export"));
                        }
                    }, {
                        text: "Export assets as Zip",
                        iconCls: "pimcore_icon_download_sidebar",
                        handler: function () {
                            var assetsPanel = "assets_zip";
                            pimcore.globalmanager.add(assetsPanel, new pimcore.tool.genericiframewindow(assetsPanel, "/admin/export/zip/assets", "pimcore_icon_download_sidebar", "Export assets"));
                        }
                    }, {
                        text: "Feature Export CSV",
                        iconCls: "pimcore_icon_download_sidebar",
                        handler: function () {
                            var assetsPanel = "features_csv";
                            pimcore.globalmanager.add(assetsPanel, new pimcore.tool.genericiframewindow(assetsPanel, "/admin/export/csv/features", "pimcore_icon_download_sidebar", "Export features"));
                        }
                    }, ],
                cls: "pimcore_navigation_flyout"
            });

            pimcore.layout.toolbar.prototype.exportMenu = this.exportMenu;

            this.exportNavEl.on("mousedown", toolbar.showSubMenu.bind(toolbar.exportMenu));

            pimcore.plugin.broker.fireEvent("mdsMenuReady", toolbar.exportMenu);
        }

        if (user.isAllowed('import')) {
            this.tasksNavEl = Ext.get('pimcore_menu_export').insertSibling(
                    '<li id="pimcore_menu_tasks" data-menu-tooltip="Tasks" class="pimcore_menu_item tasks_tab">Tasks</li>', 'after'
                    );

            this.tasksMenu = new Ext.menu.Menu({
                items: [{
                        text: "SAP Import",
                        iconCls: "pimcore_icon_overlay_upload",
                        handler: function () {
                            var sapImportPanel = "tasks_sap_import";
                            pimcore.globalmanager.add(sapImportPanel, new pimcore.tool.genericiframewindow(sapImportPanel, "/admin/tasks/import/sap", "pimcore_icon_overlay_upload", "SAP Import"));
                        }
                    }, {
                        text: "Asset Import",
                        iconCls: "pimcore_icon_asset",
                        handler: function () {
                            var assetImportPanel = "tasks_asset_import";
                            pimcore.globalmanager.add(assetImportPanel, new pimcore.tool.genericiframewindow(assetImportPanel, "/admin/tasks/import/asset", "pimcore_icon_asset", "Asset Import"));
                        }
                    }, {
                        text: "Change Price Dates",
                        iconCls: "pimcore_icon_data_group_date",
                        handler: function () {
                            var changePriceDatePanel = "tasks_change_price_date";
                            pimcore.globalmanager.add(changePriceDatePanel, new pimcore.tool.genericiframewindow(changePriceDatePanel, "/admin/tasks/change/date", "pimcore_icon_data_group_date", "Change Price Dates"));
                        }
                    }, {
                        text: "Asset Cleanup",
                        iconCls: "pimcore_icon_cleanup",
                        handler: function () {
                            var assetCleanupPanel = "tasks_asset_cleanup";
                            pimcore.globalmanager.add(assetCleanupPanel, new pimcore.tool.genericiframewindow(assetCleanupPanel, "/admin/tasks/cleanup/assets", "pimcore_icon_cleanup", "Asset Cleanup"));
                    }
                }],
                cls: "pimcore_navigation_flyout"
            });

            pimcore.layout.toolbar.prototype.tasksMenu = this.tasksMenu;

            this.tasksNavEl.on("mousedown", toolbar.showSubMenu.bind(toolbar.tasksMenu));

            pimcore.plugin.broker.fireEvent("mdsMenuReady", toolbar.tasksMenu);
        }
    }
});

var TrueRomanceBundlePlugin = new pimcore.plugin.TrueRomanceBundle();
