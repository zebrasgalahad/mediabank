<?php

namespace TrueRomanceBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class TrueRomanceBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/trueromance/js/pimcore/startup.js'
        ];
    }

    public function getCssPaths()
    {
        return [
            '/bundles/trueromance/css/style.css',
        ];
    }
}
