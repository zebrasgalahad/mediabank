<?php

namespace TrueRomanceBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ChangePricesCommand extends AbstractCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'prices:swap';

    protected function configure()
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        \Pimcore\Model\Version::disable();

        $list = new DataObject\Article\Listing();

        $limit = 1000;
        $page = 0;

        $list->setLimit($limit);
        $list->setOffset($page);
        $list->setUnpublished(true);

        $listCount = count($list->load());

        $locale = 'de_DE';

        while ($listCount > 0) {
            $articles = $list->getObjects();

            /** @var DataObject\Article article */
            foreach ($articles as $article) {
                $oldCustomerPrice = $article->getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT($locale);
                $oldNrpPrice = $article->getPRODUCT_PRICE_NRP_PRICE_AMOUNT($locale);

                $article->setPRODUCT_PRICE_NRP_PRICE_AMOUNT(floatval($oldCustomerPrice), $locale);
                $article->setPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT(floatval($oldNrpPrice), $locale);

                try {
                    $article->save();
                } catch (\Throwable $th) {
                    //throw $th;
                }


                $output->writeln('Article id: ' . $article->getId() . ' UPDATED!');
            }

            $page += 1;
            $list->setOffset($limit * $page);
            $listCount = count($list->load());
        }

        $output->writeln('Done!');
    }
}
