<?php

namespace TrueRomanceBundle\Command;

use Pimcore\Console\AbstractCommand;
use Pimcore\Db\ZendCompatibility\QueryBuilder;
use Pimcore\Model\DataObject;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Controller\ExportController;
use TrueRomanceBundle\Library\Export\Catalog\CatalogAssetsToZip;
use TrueRomanceBundle\Services\AssetZipExport\AssetZipExportDBHelper;

ini_set("max_execution_time", 43200);

class ExportCatalogAssetsToZipCommand extends AbstractCommand
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'catalog:zip:assets';

    public static $time;

    protected function configure()
    {
    }

    public static function calcMemoryUsage($precision = 2)
    {
        $bytes = memory_get_usage();

        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        error_log("SBD Memory usage: " . round($bytes, $precision) . ' ' . $units[$pow]);

        error_log("Max exec time:" . ini_get("max_execution_time"));
    }

    public static function calcTime()
    {
        $seconds = time() - self::$time;

        $minResult = floor($seconds / 60);

        if ($minResult < 10) {
            $minResult = 0 . $minResult;
        }

        $secResult = ($seconds / 60 - $minResult) * 60;

        if ($secResult < 10) {
            $secResult = 0 . $secResult;
        }

        error_log("$minResult min - $secResult sec");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        self::$time = time();

        $assetZipExportServiceHelper = $this->getContainer()->get('trueromance.export.zip_assets_helper');
        /* @var $assetZipExportServiceHelper AssetZipExportDBHelper */

        $catalogAssetsToZipService = $this->getContainer()->get('trueromance.export.zip.catalog_assets');
        $translator = $this->getContainer()->get('translator');
        $notificationService = $this->getContainer()->get('Pimcore\Model\Notification\Service\NotificationService');

        $catalogsList = new DataObject\Catalog\Listing();

        $openExportZipJobs = $assetZipExportServiceHelper->getOpenJobs();

        $catalogIds = [];

        foreach ($openExportZipJobs as $openJob) {
            $catalogIds[] = $openJob['CatalogId'];
        }

        if (empty($catalogIds)) {
            return 0;
        }

        $catalogsList->addConditionParam('o_id IN(' . implode(",", $catalogIds) . ')');

        $catalogs = $catalogsList->getObjects();

        /** @var DataObject\Catalog $catalog */
        foreach ($catalogs as $catalog) {
            foreach ($openExportZipJobs as $openJob) {
                $this->io->text("Processing catalog {$catalog->getCATALOG_CATALOG_NAME($openJob['Locale'])}");

                $params['catalog'] = $catalog->getId();
                $params['io'] = $this->io;
                $params['locale'] = $openJob['Locale'];

                if ($openJob['exportType'] == CatalogAssetsToZip::ZIP_EXPORT_IMAGES_PROPERTY) {
                    $params['zip_mode'] = CatalogAssetsToZip::ZIP_IMAGES;
                    $assetZipExportServiceHelper->setInProgressStatus($openJob['id'], CatalogAssetsToZip::ZIP_EXPORT_IMAGES_PROPERTY, $params['locale']);
                    $assetsFolder = $catalogAssetsToZipService->saveToZip($params);
                    $assetZipExportServiceHelper->setFinishedStatus($openJob['id'], CatalogAssetsToZip::ZIP_EXPORT_IMAGES_PROPERTY, $params['locale']);

                    //Notify admin users about export
                    $notificationService->sendToUser(
                        intval($openJob['UserId']),
                        0,
                        $translator->trans('assets.zip.success'),
                        $translator->trans('assets.zip.assets_folder'),
                        $assetsFolder
                    );
                } else {
                    $params['zip_mode'] = CatalogAssetsToZip::ZIP_PRODUCT_SHEETS;
                    $assetZipExportServiceHelper->setInProgressStatus($openJob['id'], CatalogAssetsToZip::ZIP_EXPORT_SHEETS_PROPERTY, $params['locale']);
                    $sheetsFolder = $catalogAssetsToZipService->saveToZip($params);
                    $assetZipExportServiceHelper->setFinishedStatus($openJob['id'], CatalogAssetsToZip::ZIP_EXPORT_SHEETS_PROPERTY, $params['locale']);

                    //Notify admin users about export
                    $notificationService->sendToUser(
                        intval($openJob['UserId']),
                        0,
                        $translator->trans('sheets.zip.success'),
                        $translator->trans('sheets.zip.sheets_folder'),
                        $sheetsFolder
                    );
                }
            }
        }

        return 0;
    }
}
