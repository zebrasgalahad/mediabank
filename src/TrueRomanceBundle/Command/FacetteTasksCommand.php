<?php

namespace TrueRomanceBundle\Command;

use Pimcore\Console\AbstractCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use TrClassUpdateBundle\Library\Import;
use TrClassUpdateBundle\Library\Export;
use TrClassUpdateBundle\Library\Checker;
use TrClassUpdateBundle\Library\Config;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrClassUpdateBundle\Library\Backup;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use TrueRomanceBundle\Library\Index\Task as ArticleIndex;

class FacetteTasksCommand extends AbstractCommand
{

    protected function configure()
    {
        $this->setName('tr:index')->setDescription('Create facettes and search index')
                ->addOption(
                        'update', null, InputOption::VALUE_NONE, 'Update index')
                ->addOption(
                        'create', null, InputOption::VALUE_NONE, 'Truncate table, create index')
                ->addOption(
                        'skip-images', null, InputOption::VALUE_NONE, 'Skip image creating on updating ArticleAssets (suppose all existing)')
                ->addOption(
                        'offset', null, InputOption::VALUE_OPTIONAL, 'Offset to start updating index')
                ->addOption(
                        'limit', null, InputOption::VALUE_OPTIONAL, 'Limit for updating index (only with option offset!)')
        ->setHelp('Create facettes and search index');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set("memory_limit", "14000M");

        $options = $input->getOptions();

        if ($options["quiet"] === true) {
            CliHelper::disableOutput();
        }

        $result = [];
        foreach (["update", "create"] as $key) {
            if ($options[$key]) {
                $result[] = $key;
            }
        }

        if($options["offset"] !== false && $options["offset"] !== null && $options["limit"] !== false && $options["limit"] !== null) {
            $index = new ArticleIndex(\Pimcore::getContainer(), null, (int)$options["offset"], (int)$options["limit"]);
        } else {
            $index = new ArticleIndex(\Pimcore::getContainer());
        }

        if($options["update"]) {
            $index->skipImageCreating = $options["skip-images"];

            $index->updateAll();

            return;
        }

        if($options["create"]) {
            $index->createAll();

            return;
        }

        if (count($result) === 0 || count($result) > 1) {
            throw new \Exception("You have to use at least one option!");
        }
    }
}
