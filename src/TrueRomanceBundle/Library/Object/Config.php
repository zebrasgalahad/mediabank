<?php

namespace TrueRomanceBundle\Library\Object;

class Config {

    /**
     *
 * @var \Pimcore\Model\DataObject\Configuration 
     */
    private static $configurationObject;
    
    public function __construct() {
        $this->loadObject();
    }

    public function loadObject() {
        $configurationListing = new \Pimcore\Model\DataObject\Configuration\Listing();

        if ($configurationListing->count() === 0) {
            throw new \Exception("Could not find a valid configuration object from class \Pimcore\Model\DataObject\Configuration!");
        }

        self::$configurationObject = $configurationListing->current();
    }

    /**
     * 
     * @param string $property
     * @param string $locale - e.g. "de_DE"
     * @return mixed
     */
    public function get($property, $locale = null) {
        $getter = "get" . ucfirst($property);
        
        if(!method_exists(self::$configurationObject, $getter)) {
            throw new \Exception("There is no property like '$property' in configuration object!");
        }
        
        if ($locale === null) {
            $value = self::$configurationObject->$getter();
        } else {
            $value = self::$configurationObject->$getter($locale);
        }
        
        if(!$value) {
            throw new \Exception("No value given for property $property from configuration object!");
        }
        
        return $value;
    }
}
