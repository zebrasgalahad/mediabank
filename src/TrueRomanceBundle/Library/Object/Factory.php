<?php

namespace TrueRomanceBundle\Library\Object;

use Pimcore\Model\DataObject;

class Factory
{

    /**
     * 
     * @param string $class
     * @param string $filename
     * @param int $parentId
     * @param array $data
     * @param bool $saveInstant
     * @return \TrueRomanceBundle\Library\Object\class
     */
    public static function createNew(string $class, string $filename, int $parentId, array $data = null, bool $saveInstant = true, string $locale = null)
    {
        $newObject = new $class();

        $newObject->setParentId($parentId);

        $newObject->setPublished(true);

        $newObject->setKey($filename);

        if ($data !== null) {
            foreach ($data as $key => $value) {
                $setter = "set" . ucfirst($key);

                if (method_exists($newObject, $setter) === false) {
                    continue;
                }

                if ($locale) {
                    try {
                        $newObject->$setter($value, $locale);
                    } catch (\Exception $ex) {
                        $newObject->$setter($value);
                    }
                } else {
                    $newObject->$setter($value);
                }
            }
        }

        if ($saveInstant === true) {
            $newObject->save();
        }

        return $newObject;
    }

    /**
     * 
     * @param string $class
     * @param array $data
     * @param int $id
     * @return type
     */
    public static function updateExistingById(string $class, array $data, int $id, bool $saveInstant = true, $locale = null)
    {
        $existingObject = $class::getById($id);

        foreach ($data as $key => $value) {
            $setter = "set" . ucfirst($key);

            if (method_exists($existingObject, $setter) === false) {
                continue;
            }

            if ($locale) {
                try {
                    $existingObject->$setter($value, $locale);
                } catch (\Exception $ex) {
                    $existingObject->$setter($value);
                }
            } else {
                $existingObject->$setter($value);
            }
        }

        if ($saveInstant === true) {
            $existingObject->save();
        }

        return $existingObject;
    }

    /**
     * 
     * @param string $class
     * @param array $data
     * @param string $path
     * @return DataObject
     */
    public static function updateExistingByPath(string $class, array $data, string $path, bool $saveInstant = true, $locale = null)
    {
        $existingObject = $class::getByPath($path);

        return self::updateExistingById($class, $data, $existingObject->getId(), $saveInstant, $locale);
    }

    /**
     * 
     * @param string $path
     * @return bool
     */
    public static function objectExistsByPath(string $path): bool
    {
        return is_object(\Pimcore\Model\Object::getByPath($path));
    }
}
