<?php

namespace TrueRomanceBundle\Library\Object;

class CatalogGroup {

    /**
     *
     * @var \Pimcore\Model\DataObject\CatalogGroup 
     */
    private static $catalogGroupList;

    public function __construct() {
        $this->loadObjectListing();
    }

    public function loadObjectListing() {
        $catalogGroupListing = new \Pimcore\Model\DataObject\Configuration\Listing();

        if ($catalogGroupListing->count() === 0) {
            throw new \Exception("Could not find a valid catalogGroup object from class \Pimcore\Model\DataObject\CatalogGroup!");
        }

        self::$catalogGroupList = $catalogGroupListing;
    }

    public function get($catalogGroupName) {

        $catalogGroupObject = \Pimcore\Model\DataObject\CatalogGroup::getByName($catalogGroupName)->current();

        if ($catalogGroupObject) {
            $catalogs = $catalogGroupObject->getCatalogs();
        } else {
            throw new \Exception("There is no object with name '$catalogGroupName'!");
        }

        return $catalogs;
    }

}
