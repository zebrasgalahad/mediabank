<?php

namespace TrueRomanceBundle\Library\Export\Catalog;

define('CATALOG_XML_EXPORT_FOLDER', PIMCORE_PROJECT_ROOT . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'catalog');

use Carbon\Carbon;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Data\InputQuantityValue;
use Symfony\Component\Translation\TranslatorInterface;
use TrueRomanceBundle\Library\Tools\ObjectHelper;
use TrueRomanceBundle\Library\Tools\ObjectOption;
use Pimcore\Model\DataObject\ArticleAssets;
use TrueRomanceBundle\Library\Data\Objects\UdxinfoData;

class XMLExportCatalog extends CatalogExport
{
    /**
     * @var \Pimcore\Db\Connection
     */
    private $dbConnection;

    /**
     * Configuration
     *
     * @var DataObject\Configuration $configuration
     */
    private $configuration;

    /**
     * Translator
     *
     * @var TranslatorInterface $translator
     */
    private $translator;

    /**
     * @var string $tempFilePath
     */
    private $tempFilePath;

    /**
     * @var string $filePath
     */
    private $_filePath;

    /**
     * @var DataObject\XMLExportConfiguration
     */
    private $xmlConfig;

    private $collectionTable;

    private $collectionTableLocalized;

    /**
     * Constructor
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->configuration = \Pimcore\Config::getWebsiteConfig()->configuration;
        $this->translator = $translator;

        $this->collectionTable = ObjectHelper::getFieldCollectionTableByClass(ArticleAssets::class, "AssetImage");
        $this->collectionTableLocalized = ObjectHelper::getFieldCollectionTableByClass(ArticleAssets::class, "AssetImage", "de_DE");

        $this->dbConnection = \Pimcore::getContainer()->get("doctrine")->getConnections()["default"];
    }

    /**
     * Export catalog to xml
     *
     * @param array $params
     * @return string $filePath
     */
    public function export(array $params) : string
    {
        $catalogs = [];

        //Check if exporting one or more catalogs
        if (count($params['catalog']) > 1) {
            $catalogIds = "'" . implode("','", $params['catalog']) . "'";
            $catalogList = new DataObject\Catalog\Listing();
            $catalogList->setCondition("o_id IN ($catalogIds)");
            $fileName = 'catalog_export_' . uniqid();

            $catalogs = $catalogList->getObjects();
        } else {
            $catalogId = current($params['catalog']);
            $catalog = DataObject\Catalog::getById((int) $catalogId);
            $fileName = $catalog->getGROUP_NAME($params['language']) ?: 'export_catalog';

            $catalogs[] = $catalog;
        }

        $this->_filePath = CATALOG_XML_EXPORT_FOLDER . DIRECTORY_SEPARATOR . $fileName . '.xml';
        //File for saving category product map data so we avoid two foreachs over articles
        $this->tempFilePath = CATALOG_XML_EXPORT_FOLDER . DIRECTORY_SEPARATOR . uniqid() . '.xml';

        $this->xmlConfig = DataObject\XMLExportConfiguration::getById($params['configuration']);

        $this->createXML($catalogs, $params);

        return $this->_filePath;
    }

    /**
     * Create xml file for given catalog
     *
     * @param DataObject\Catalog[] $catalogs
     * @param array $params
     * @return void
     */
    private function createXML(array $catalogs, array $params) : void
    {
        $bmecat = $params['BMECat'];

        if (file_exists($this->_filePath)) {
            unlink($this->_filePath);
        }

        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'utf-8');
        $xml->startDtd('BMECAT SYSTEM "bmecat_new_catalog_2005.dtd"');
        $xml->endDtd();

        $xml->startElement('BMECAT');
            $xml->writeAttribute('version', $bmecat);

            $this->writeHeaderData($xml, $params);

            $this->writeNewCatalogNodeData($xml, $catalogs, $params);
        $xml->endElement();

        $xml->endDocument();

        file_put_contents($this->_filePath, $xml->flush(true), FILE_APPEND);

        $doc = new \DOMDocument();
        $doc->load($this->_filePath);

        $docFragment = $doc->createDocumentFragment();
        $docFragment->appendXML(file_get_contents($this->tempFilePath));

        if ($docFragment->childNodes->length != 0) {
            $doc->getElementsByTagName('T_NEW_CATALOG')[0]->appendChild($docFragment);
        }

        $doc->formatOutput = true;
        $doc->saveXML();
        $doc->save($this->_filePath);

        unlink($this->tempFilePath);
    }

    /**
     * Creates header data for xml
     *
     * @param \XMLWriter $xml
     * @param array $params
     *
     * @return void
     */
    private function writeHeaderData(\XMLWriter $xml, array $params) : void
    {
        $locale = $params['language'];
        $buyerId = (int) $params['buyer'];
        $supplierId = (int) $params['supplier'];

        $xml->startElement('HEADER');

            $this->writeHeaderCatalogNodeData($xml, $locale);
            $this->writeHeaderBuyerNodeData($xml, $buyerId);
            $this->writeHeaderSupplierNodeData($xml, $supplierId, $locale);
            $this->writeHeaderUDXNodeData($xml);

        $xml->endElement();

        file_put_contents($this->_filePath, $xml->flush(true), FILE_APPEND);
    }

    /**
     * Header UDX node data
     *
     * @param \XMLWriter $xml
     *
     * @return void
     */
    private function writeHeaderUDXNodeData(\XMLWriter $xml) : void
    {
        $headerUDXData = $this->configuration->getHeaderUserDefinedExtensions();

        $xml->startElement('USER_DEFINED_EXTENSIONS');
            $xml->writeElement('UDX.EDXF.VERSION', $headerUDXData->getUDX_EDXF_VERSION());
        $xml->endElement();
    }

    /**
     * Header buyer node data
     *
     * @param \XMLWriter $xml
     * @param int $buyerId
     *
     * @return void
     */
    private function writeHeaderBuyerNodeData(\XMLWriter $xml, int $buyerId) : void
    {
        $headerBuyerData = DataObject\Buyer::getById($buyerId);

        $buyerTags = $this->xmlConfig->getBuyerFields()->getItems()[0];

        $xml->startElement($buyerTags->getBUYER());
            $xml->writeElement($buyerTags->getBUYER_ID_SUPPLIER_SPECIFIC(), $headerBuyerData->getBUYER_BUYER_ID_SUPPLIER_SPECIFIC());
            $xml->writeAttribute('type', 'supplier_specific');
            $xml->writeElement($buyerTags->getBUYER_NAME(), $headerBuyerData->getBUYER_BUYER_BUYER_NAME());
            $xml->startElement($buyerTags->getBUYER_ADDRESS());
                $xml->writeAttribute('type', 'buyer');
                $xml->writeElement($buyerTags->getBUYER_STREET(), $headerBuyerData->getBUYER_ADRESS_STREET());
                $xml->writeElement($buyerTags->getBUYER_ZIP(), $headerBuyerData->getBUYER_ADRESS_ZIP());
                $xml->writeElement($buyerTags->getBUYER_CITY(), $headerBuyerData->getBUYER_ADRESS_CITY());
                $xml->writeElement($buyerTags->getBUYER_COUNTRY(), $headerBuyerData->getBUYER_ADRESS_COUNTRY());
            $xml->endElement();
        $xml->endElement();
    }

    /**
     * Header supplier node data
     *
     * @param \XMLWriter $xml
     * @param int $supplierId
     * @param string $locale
     *
     * @return void
     */
    private function writeHeaderSupplierNodeData(\XMLWriter $xml, int $supplierId, string $locale) : void
    {
        $supplierHeaderData = DataObject\Supplier::getById($supplierId);

        $supplierTags = $this->xmlConfig->getSupplierFields()->getItems()[0];

        $xml->startElement($supplierTags->getSUPPLIER());
            $xml->writeElement($supplierTags->getSUPPLIER_ID(), $supplierHeaderData->getSUPPLIER_SUPPLIER_ID_SUPPLIER_SPECIFIC($locale));
            $xml->writeAttribute('type', 'supplier_specific');
            $xml->writeElement($supplierTags->getSUPPLIER_NAME(), $supplierHeaderData->getSUPPLIER_SUPPLIER_NAME($locale));
            $xml->startElement($supplierTags->getSUPPLIER_ADDRESS());
            $xml->writeAttribute('type', 'supplier');
            $xml->writeElement($supplierTags->getSUPPLIER_NAME(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_NAME($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_STREET(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_STREET($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_ZIP(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_ZIP($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_CITY(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_CITY($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_STATE(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_STATE($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_COUNTRY(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_COUNTRY($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_PHONE(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_PHONE($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_FAX(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_FAX($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_EMAIL(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_EMAIL($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_URL(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_URL($locale));
            $xml->writeElement($supplierTags->getSUPPLIER_ADDRESS_REMARKS(), $supplierHeaderData->getSUPPLIER_ADDRESS_SUPPLIER_ADDRESS_REMARKS($locale));
            $xml->endElement();
        $xml->endElement();
    }

    /**
     * Header catalog node data
     *
     * @param \XMLWriter $xml
     * @param string $locale
     *
     * @return void
     */
    private function writeHeaderCatalogNodeData(\XMLWriter $xml, string $locale) : void
    {
        $catalogHeaderData = $this->configuration->getHeaderCatalog();

        $xml->startElement('CATALOG');
            $xml->writeElement('LANGUAGE', $catalogHeaderData->getCATALOG_LANGUAGE($locale));
            $xml->writeElement('CATALOG_ID', $catalogHeaderData->getCATALOG_CATALOG_ID());
            $xml->writeElement('CATALOG_VERSION', $catalogHeaderData->getCATALOG_CATALOG_VERSION());
            $xml->writeElement('CATALOG_NAME', $catalogHeaderData->getCATALOG_CATALOG_NAME($locale));
            $xml->startElement('DATETIME');
                $xml->writeAttribute('type', 'generation_date');
                $xml->writeElement('DATE', $this->formatDate(Carbon::now()));
            $xml->endElement();
            $xml->writeElement('TERRITORY', $catalogHeaderData->getCATALOG_TERRITORY($locale));
            $xml->writeElement('CURRENCY', $catalogHeaderData->getCATALOG_CURRENCY($locale));
            $xml->writeElement('PRICE_FLAG', $catalogHeaderData->getCATALOG_PRICE_FLAG_INCL_FREIGHT() ? 'true' : 'false');
            $xml->writeAttribute('type', 'incl_freight');
            $xml->writeElement('PRICE_FLAG', $catalogHeaderData->getCATALOG_PRICE_FLAG_INCL_PACKING() ? 'true' : 'false');
            $xml->writeAttribute('type', 'incl_packing');
            $xml->writeElement('PRICE_FLAG', $catalogHeaderData->getCATALOG_PRICE_FLAG_INCL_ASSURANCE() ? 'true' : 'false');
            $xml->writeAttribute('type', 'incl_assurance');
            $xml->writeElement('PRICE_FLAG', $catalogHeaderData->getCATALOG_PRICE_FLAG_INCL_DUTY() ? 'true' : 'false');
            $xml->writeAttribute('type', 'incl_duty');
        $xml->endElement();
    }

    /**
     * Catalog node data
     *
     * @param \XMLWriter $xml
     * @param DataObject\Catalog[] $catalogs
     * @param array $params
     *
     * @return array $newCatalogNodeData
     */
    private function writeNewCatalogNodeData(\XMLWriter $xml, array $catalogs, array $params) : void
    {
        $locale = $params['language'];
        $bmecat = $params['BMECat'];
        $separator = $params['textmode'];

        $xml->startElement('T_NEW_CATALOG');

        $this->writeCatalogGroupSystemNodeData($xml, $catalogs, $locale);

        $this->writeProductsData($xml, $catalogs, $bmecat, $separator, $locale);

        $xml->endElement();
    }

    /**
     * Get group system node data
     *
     * @param \XMLWriter $xml
     * @param DataObject\Catalog[] $catalog
     * @param string $locale
     *
     * @return void
     */
    private function writeCatalogGroupSystemNodeData(\XMLWriter $xml, array $catalogs, string $locale) : void
    {
        if (count($catalogs) > 1) {
            $catalogsFolder = $this->configuration->getCatalogsFolder();
            $rootId = $catalogsFolder->getId();

            $rootGroupName = 'Catalogs';
            $rootGroupOrder = 0;
        } else {
            $catalog = current($catalogs);

            $rootId = $catalog->getId();
            $rootGroupName = $catalog->getGROUP_NAME($locale);
            $rootGroupOrder = $catalog->getGROUP_ORDER();
        }

        $groupTags = $this->xmlConfig->getCatalogGroupFields()->getItems()[0];

        $xml->startElement($groupTags->getCATALOG_GROUP_SYSTEM());
            $xml->startElement($groupTags->getCATALOG_STRUCTURE());
                $xml->writeAttribute('type', 'root');
                $xml->writeElement($groupTags->getCATALOG_GROUP_ID(), 1);
                $xml->writeElement($groupTags->getCATALOG_GROUP_NAME(), $rootGroupName);
                $xml->writeElement($groupTags->getCATALOG_PARENT_ID(), 0);
                $xml->writeElement($groupTags->getCATALOG_GROUP_ORDER(), $rootGroupOrder);
            $xml->endElement();

        if (count($catalogs) > 1) {
            /** @var DataObject\Catalog $category */
            foreach ($catalogs as $catalog) {
                $xml->startElement($groupTags->getCATALOG_STRUCTURE());
                $xml->writeAttribute('type', 'node');
                    $xml->writeElement($groupTags->getCATALOG_GROUP_ID(), $catalog->getId());
                    $xml->writeElement($groupTags->getCATALOG_GROUP_NAME(), $catalog->getGROUP_NAME($locale));
                    $xml->writeElement($groupTags->getCATALOG_PARENT_ID(), $catalog->getParentId() == $rootId ? 1 : $catalog->getParentId());
                    $xml->writeElement($groupTags->getCATALOG_GROUP_ORDER(), $catalog->getGROUP_ORDER());
                $xml->endElement();
            }
        }

        $limit = 1000;
        $offset = 0;

        $categoriesList = $this->getCatalogCategories($catalogs, 0, 0, false);
        $categoriesCount = $categoriesList->count();

        $pages = round($categoriesCount / $limit);

        for ($i = 0; $i <= $pages; $i++) {
            if ($i > 0) {
                $offset = $i * $limit;
            }

            if ($offset > $categoriesCount) {
                continue;
            }

            $categories = $this->getCatalogCategories($catalogs, $limit, $offset);

            /** @var DataObject\CatalogCategory $category */
            foreach ($categories as $category) {
                $catalogCategoryList = new DataObject\CatalogCategory\Listing();
                $catalogCategoryList->setCondition('o_parentId = ?', [$category->getId()]);

                if ($catalogCategoryList->count() > 0) {
                    $type = 'node';
                } else {
                    $type = 'leaf';
                }

                $xml->startElement($groupTags->getCATALOG_STRUCTURE());
                $xml->writeAttribute('type', $type);
                    $xml->writeElement($groupTags->getCATALOG_GROUP_ID(), $category->getId());
                    $xml->writeElement($groupTags->getCATALOG_GROUP_NAME(), $category->getGROUP_NAME($locale));
                    $xml->writeElement($groupTags->getCATALOG_PARENT_ID(), $category->getParentId() == $rootId ? 1 : $category->getParentId());
                    $xml->writeElement($groupTags->getCATALOG_GROUP_ORDER(), $category->getGROUP_ORDER());
                $xml->endElement();
            }
        }

        unset($categories);
        \Pimcore::collectGarbage();

        $xml->endElement();
    }

    /**
     * Get product node data
     *
     * @param \XMLWriter $xml
     * @param DataObject\Catalog[] $catalogs
     * @param string $bmecat
     * @param string $locale
     *
     * @return void
     */
    private function writeProductsData(
        \XMLWriter $xml,
        array $catalogs,
        string $bmecat,
        string $separator,
        string $locale
    ) : void {
        switch ($bmecat) {
            case self::BMECAT_1_2:
                $productKey = 'ARTICLE';
                $idKey = 'AID';
                break;

            case self::BMECAT_2005:
                $productKey = 'PRODUCT';
                $idKey = 'PID';
                break;
        }

        $groupTags = $this->xmlConfig->getCatalogGroupFields()->getItems()[0];

        //XML writer for productCatalogMap
        $xmlWriter2 = new \XMLWriter();
        $xmlWriter2->openMemory();

        foreach ($catalogs as $catalog) {
            $limit = 1000;
            $offset = 0;

            $articlesList = $catalog->getCatalogArticles(0, 0, false);
            $articlesCount = $articlesList->count();

            $pages = round($articlesCount / $limit);

            $articleCounter = 0;
            for ($i = 0; $i <= $pages; $i++) {
                if ($i > 0) {
                    $offset = $i * $limit;
                }

                if ($offset > $articlesCount) {
                    continue;
                }

                $articles = $catalog->getCatalogArticles($limit, $offset);
                /** @var DataObject\Article $article */
                foreach ($articles as $article) {
                    ++$articleCounter;

                    $xml->startElement($productKey);
                    $xml->writeAttribute('mode', 'new');
                        $xml->writeElement('SUPPLIER_' . $idKey, $article->getSUPPLIER_PID());

                        $this->writeProudctDetailsNodeData($xml, $article, $bmecat, $idKey, $productKey, $locale);
                        $this->writeProductFeaturesNodeData($xml, $article, $productKey);
                        $this->writeProductOrderDetailsNodeData($xml, $article, $productKey);
                        $this->writeProductPriceDetailsNodeData($xml, $article, $catalog, $productKey, $locale);
                        $this->writeProductAssetsNodeData($xml, $article);
                        $this->writeProductUserDefinedExtensionsNodeData($xml, $article, $productKey, $separator, $locale);
                    $xml->endElement();

                    //Write productCatalogGroupMap data to another file and at the end merge those two files
                    $xmlWriter2->startElement($groupTags->getPRODUCT_TO_CATALOGGROUP_MAP());
                        $xmlWriter2->writeAttribute('mode', 'new');
                        $xmlWriter2->writeElement($groupTags->getPROD_ID(), $article->getSUPPLIER_PID());
                        $xmlWriter2->writeElement($groupTags->getPRODUCT_TO_CATALOGGROUP_MAP_GROUP_ID(), $article->getParentId());
                    $xmlWriter2->endElement();

                    error_log("Article nr. $articleCounter");
                }
                $articles = null;

                \Pimcore::collectGarbage();

                file_put_contents($this->tempFilePath, $xmlWriter2->flush(true), FILE_APPEND);
                file_put_contents($this->_filePath, $xml->flush(true), FILE_APPEND);
            }

            $articlesList = null;
            \Pimcore::collectGarbage();
        }

        file_put_contents($this->_filePath, $xml->flush(true), FILE_APPEND);
        file_put_contents($this->tempFilePath, $xmlWriter2->flush(true), FILE_APPEND);
    }

    /**
     * Product product details node
     *
     * @param \XMLWriter $xml
     * @param DataObject\Article $article
     * @param string $bmecat
     * @param string $idKey
     * @param string $productKey
     * @param string $locale
     *
     * @return void
     */
    private function writeProudctDetailsNodeData(
        \XMLWriter $xml,
        DataObject\Article $article,
        string $bmecat,
        string $idKey,
        string $productKey,
        string $locale
    ) : void {
        $statusType = $article->getPRODUCT_STATUS_NEW_PRODUCT() ? 'new_product' :  'core_product';

        $productTags = $this->xmlConfig->getProductFields()->getItems()[0];

        $brand = $article->getBrand() ? ObjectOption::valueToKey($article, "Brand", $article->getBrand()) : '';

        $xml->startElement($productKey . '_' . $productTags->getDETAILS());
            $xml->writeElement($productTags->getDESCRIPTION_SHORT(), $article->getDESCRIPTION_SHORT($locale));
            $xml->writeElement($productTags->getDESCRIPTION_LONG(), $article->getDESCRIPTION_LONG($locale));
            $xml->writeElement($productTags->getEAN(), $article->getEAN());
            $xml->writeElement($productTags->getSUPPLIER_ALT() . '_' . $idKey, $article->getSUPPLIER_PID());
            $xml->writeElement($productTags->getMANUFACTURER() . '_' . $idKey, $article->getMANUFACTURER_PID());
            $xml->writeElement($productTags->getMANUFACTURER_NAME(), $brand);
            $xml->writeElement($productTags->getMANUFACTURER_TYPE_DESCR(), $article->getMANUFACTURER_TYPE_DESCR());
            $xml->startElement($productTags->getSPECIAL_TREATMENT_CLASS());
                $xml->writeAttribute('type', $this->translator->trans('special_treatment_class_type', [], null, $locale));
                $xml->text($article->getSPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE() == 'true' ? strtoupper($this->translator->trans('Yes', [], null, $locale)) : strtoupper($this->translator->trans('No', [], null, $locale)));
            $xml->endElement();
            $xml->startElement($productKey . '_' . $productTags->getSTATUS());
                $xml->writeAttribute('type', $statusType);
                $xml->text($article->getPRODUCT_STATUS_NEW_PRODUCT($locale) ? 'Neu' : 'Kernsortiment');
            $xml->endElement();
        $xml->endElement();
    }

    /**
     * Get product features node data
     *
     * @param \XMLWriter $xml,
     * @param DataObject\Article $article
     * @param string $productKey
     * @return void
     */
    private function writeProductFeaturesNodeData(
        \XMLWriter $xml,
        DataObject\Article $article,
        string $productKey
    ) : void {
        $featureGroups = $article->getFEATURE();
        $productTags = $this->xmlConfig->getProductFields()->getItems()[0];

        $xml->startElement($productKey . '_' . $productTags->getFEATURES());
            $xml->writeElement($productTags->getREFERENCE_FEATURE_SYSTEM_NAME(), $article->getREFERENCE_FEATURE_SYSTEM_NAME() ?: 'Proficlass 5.0');
            $xml->writeElement($productTags->getREFERENCE_FEATURE_GROUP_ID(), $article->getREFERENCE_FEATURE_GROUP_ID());
            $xml->writeElement($productTags->getREFERENCE_FEATURE_GROUP_NAME(), $article->getREFERENCE_FEATURE_GROUP_NAME());

        if ($featureGroups) {
            foreach ($featureGroups->getItems() as $groupValues) {
                foreach ($groupValues as $featureKey => $featureValues) {
                    $keyConfig = \Pimcore\Model\DataObject\Classificationstore\KeyConfig::getById($featureKey);
                    foreach ($featureValues as $feature) {
                        if ($feature instanceof InputQuantityValue) {
                            $featureValue = $feature->getValue();
                            $featureUnit = ($feature instanceof InputQuantityValue && $feature->getUnit()) ? $feature->getUnit()->getAbbreviation() : null;
                        } else {
                            $featureValue = $feature;
                            $featureUnit = null;
                        }

                        $xml->startElement($productTags->getFEATURE());
                            $xml->writeElement($productTags->getFNAME(), $keyConfig->getName());

                        if (!empty($featureValue)) {
                            $xml->writeElement($productTags->getFVALUE(), is_numeric($featureValue) ? $this->formatMeasurement($featureValue) : $featureValue);
                        }

                        if (!empty($featureUnit)) {
                            $xml->writeElement($productTags->getFUNIT(), $featureUnit);
                        }

                            $xml->writeElement($productTags->getFDESCR(), $keyConfig->getDescription());

                        $xml->endElement();
                    }
                }
            }
        }

        unset($featureGroups);
        \Pimcore::collectGarbage();

        $xml->endElement();
    }

    /**
     * Get product product order details node
     *
     * @param \XMLWriter $xml
     * @param DataObject\Article $article
     * @param string $idKey
     * @param string $productKey
     *
     * @return void
     */
    private function writeProductOrderDetailsNodeData(
        \XMLWriter $xml,
        DataObject\Article $article,
        string $productKey
    ) : void {
        $productTags = $this->xmlConfig->getProductFields()->getItems()[0];

        $xml->startElement($productKey . '_' . $productTags->getORDER_DETAILS());
            $xml->writeElement($productTags->getORDER_UNIT(), $article->getORDER_UNIT());
            $xml->writeElement($productTags->getCONTENT_UNIT(), $article->getCONTENT_UNIT());
            $xml->writeElement($productTags->getNO_CU_PER_OU(), $this->formatMeasurement($article->getNO_CU_PER_OU()));
            $xml->writeElement($productTags->getQUANTITY_MIN(), $this->formatMeasurement($article->getQUANTITY_MIN()));
            $xml->writeElement($productTags->getQUANTITY_INTERVAL(), $this->formatMeasurement($article->getQUANTITY_MIN()));
            $xml->writeElement($productTags->getPRICE_QUANTITY(), $article->getPRICE_QUANTITY());
        $xml->endElement();
    }

    /**
     * Get product price details node
     *
     * @param \XMLWriter $xml
     * @param DataObject\Article $article
     * @param string $idKey
     * @param string $productKey
     * @return void
     */
    private function writeProductPriceDetailsNodeData(
        \XMLWriter $xml,
        DataObject\Article $article,
        DataObject\Catalog $catalog,
        string $productKey,
        string $locale
    ) : void {
        $productTags = $this->xmlConfig->getProductFields()->getItems()[0];

        $xml->startElement($productKey . '_' . $productTags->getPRICE_DETAILS());
            $xml->startElement($productTags->getDATETIME());
                $xml->writeAttribute('type', 'valid_start_date');
                $xml->writeElement($productTags->getDATE(), $this->formatDate($article->getDATETIME_VALID_START_DATE($locale)));
            $xml->endElement();
            $xml->startElement($productTags->getDATETIME());
                $xml->writeAttribute('type', 'valid_end_date');
                $xml->writeElement($productTags->getDATE(), $this->formatDate($article->getDATETIME_VALID_END_DATE($locale)));
            $xml->endElement();
            $xml->startElement($productKey . '_' . $productTags->getPRICE());
                $xml->writeAttribute('price_type', 'net_customer');
                $xml->writeElement($productTags->getPRICE_AMOUNT(), $this->formatPrice($article->getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT($locale)));
                $xml->writeElement($productTags->getPRICE_CURRENCY(), $catalog->getCATALOG_CURRENCY($locale));
                $xml->writeElement($productTags->getTAX(), $this->formatPrice($article->getPRODUCT_PRICE_nrp_TAX($locale)));
                $xml->writeElement($productTags->getLOWER_BOUND(), $this->formatPrice($article->getQUANTITY_MIN()));
            $xml->endElement();
            $xml->startElement($productKey . '_' . $productTags->getPRICE());
                $xml->writeAttribute('price_type', 'nrp');
                $xml->writeElement($productTags->getPRICE_AMOUNT(), $this->formatPrice($article->getPRODUCT_PRICE_NRP_PRICE_AMOUNT($locale)));
                $xml->writeElement($productTags->getPRICE_CURRENCY(), $catalog->getCATALOG_CURRENCY($locale));
                $xml->writeElement($productTags->getTAX(), $this->formatPrice($article->getPRODUCT_PRICE_nrp_TAX($locale)));
                $xml->writeElement($productTags->getLOWER_BOUND(), $this->formatPrice($article->getQUANTITY_MIN()));
            $xml->endElement();
        $xml->endElement();
    }

    /**
     * Get product assets node data
     *
     * @param \XMLWriter $xml
     * @param DataObject\Article $article
     *
     * @return void
     */
    private function writeProductAssetsNodeData(\XMLWriter $xml, DataObject\Article $article) : void
    {
        //Article has one child that is ArticleAssets objects containg all assets for given article
        $articleAsset = current($article->getChildren());

        $productTags = $this->xmlConfig->getProductFields()->getItems()[0];

        if ($articleAsset) {
            $allAssets = $this->getArticleAssetsAssets($articleAsset->getId());

            $firstAssetSet = false;

            $xml->startElement($productTags->getMIME_INFO());

            foreach ($allAssets as $asset) {
                $assetSource = $asset["image_id"] ? $asset["filename"] : null;

                $xml->startElement($productTags->getMIME());
                    $xml->writeElement($productTags->getMIME_SOURCE(), $assetSource);
                    $xml->writeElement($productTags->getMIME_DESCR(), $asset["asset_title"]);
                    $xml->writeElement($productTags->getMIME_TYPE(), $asset["mimetype"]);
                    $xml->writeElement($productTags->getMIME_PURPOSE(), !$firstAssetSet ? 'normal' : 'others');
                $xml->endElement();

                $firstAssetSet = true;
            }

            $xml->endElement();
        }
    }

    private function getArticleAssetsAssets($articleAssetsId)
    {
        // TODO: https://n1factory.atlassian.net/browse/SBD-40
//        $query = "SELECT * FROM(
//SELECT ai.*, ass.id as image_id, ass.mimetype, ass.filename, CONCAT(ai.o_id, ai.index, ai.fieldname) as unique_key1 FROM {$this->collectionTable} as ai
//LEFT JOIN assets as ass ON
//ail.asset_relation = ass.id
//WHERE ai.o_id = ?
//AND ai.fieldname IN ('img_Heroshot', 'img_application', 'img_product_other')
//AND ail.asset_relation IS NOT NULL
//ORDER BY ai.fieldname, ai.index
//) as query1
//LEFT JOIN (
//SELECT ail.*, CONCAT(ail.ooo_id, ail.index, ail.fieldname) as unique_key2 FROM {$this->collectionTableLocalized} as ail
//WHERE ail.ooo_id = ?
//AND ail.fieldname IN ('img_Heroshot', 'img_application', 'img_product_other')
//AND ail.language = 'de_DE'
//AND ail.item_bmecat = 1
//ORDER BY ail.fieldname, ail.index
//) as query2 ON query1.unique_key1 = query2.unique_key2
//WHERE query2.ooo_id IS NOT NULL
//ORDER BY FIELD(query1.fieldname,'img_Heroshot','img_application','img_product_other'), query2.index";
//
//        $result = $this->dbConnection->fetchAll($query, [$articleAssetsId, $articleAssetsId]);

        return [];
    }

    /**
     * Get product udx node data
     *
     * @param \XMLWriter $xml
     * @param DataObject\Article $article
     * @param string $separator
     * @param string $locale
     *
     * @return void
     */
    private function writeProductUserDefinedExtensionsNodeData(
        \XMLWriter $xml,
        DataObject\Article $article,
        string $productKey,
        string $separator,
        string $locale
    ) : void {
        switch ($separator) {
            case self::TEXTMODE_HTML:
                $separator = '</br>';
                break;

            case self::TEXTMODE_PLAIN:
                $separator = '|';
                break;
        }

        $productTags = $this->xmlConfig->getProductFields()->getItems()[0];

        $uspList = $this->getBlockData($article->getUsp_list($locale), 'usp_row', $separator);
        $scopeOfDelivery = $this->getBlockData($article->getScope_of_delivery_list($locale), 'scope_of_delivery_row', $separator);
        $warranties = $this->getBlockData($article->getWarranties_list($locale), 'warranties_row', $separator);
        $tecList = $this->getBlockData($article->getTec_specs_list($locale), 'tech_specs_row', $separator);
        $applicationsList = $this->getBlockData($article->getAPPLICATION_LIST($locale), 'application_row', $separator);

        $xml->startElement($productTags->getUSER_DEFINED_EXTENSIONS());
            $xml->writeElement($productTags->getMANUFACTURER_ACROYNM(), $article->getMANUFACTURER_ACRONYM());
            $xml->writeElement($productTags->getLANGTEXT(), $uspList);
            $xml->writeElement($productTags->getLIEFERUMFANG(), $scopeOfDelivery);
            $xml->writeElement($productTags->getTECHNISCHE_DATEN(), $tecList);
            $xml->writeElement($productTags->getGARANTIEBEDINGUNGEN(), $warranties);
            $xml->writeElement($productTags->getANWENDUNGSBEISPIELE(), $applicationsList);
            $xml->writeElement($productTags->getDISCOUNT_GROUP_MANUFACTURER(), $article->getDISCOUNT_GROUP_MANUFACTURER($locale));
            $xml->startElement($productTags->getPACKING_UNITS());
                $xml->startElement($productTags->getPACKING_UNIT());
                    //$xml->writeElement($productTags->getPACKING_UNIT_QUANTITY_MIN(), $this->formatMeasurement($article->getNO_CU_PER_OU()));
                    //$xml->writeElement($productTags->getPACKING_UNIT_QUANTITY_MAX(), $this->formatMeasurement($article->getNO_CU_PER_OU()));
                    $xml->writeElement($productTags->getPACKING_UNIT_LENGTH(), $this->formatMeasurement($article->getPACKING_UNITS_LENGTH()));
                    $xml->writeElement($productTags->getPACKING_UNIT_LENGTH_UNIT(), $article->getPACKING_UNITS_LENGTH_UNIT());
                    $xml->writeElement($productTags->getPACKING_UNIT_WIDTH(), $this->formatMeasurement($article->getPACKING_UNITS_WIDTH()));
                    $xml->writeElement($productTags->getPACKING_UNIT_WIDTH_UNIT(), $article->getPACKING_UNITS_WIDTH_UNIT());
                    $xml->writeElement($productTags->getPACKING_UNIT_DEPTH(), $this->formatMeasurement($article->getPACKING_UNITS_DEPTH()));
                    $xml->writeElement($productTags->getPACKING_UNIT_DEPTH_UNIT(), $article->getPACKING_UNITS_DEPTH_UNIT());
                    $xml->writeElement($productTags->getPACKING_UNIT_WEIGHT(), $this->formatMeasurement($article->getPACKING_UNITS_WEIGHT()));
                    $xml->writeElement($productTags->getPACKING_UNIT_WEIGHT_UNIT(), $article->getPACKING_UNITS_WEIGHT_UNIT());
                $xml->endElement();
            $xml->endElement();
            $xml->startElement($productTags->getREACH());
                $xml->writeElement($productTags->getREACH_INFO(), $article->getREACH_INFO() ? 'true' : 'false');
            $xml->endElement();
            $xml->startElement($productKey . '_' . $productTags->getLOGISTICS_DETAILS());
                $xml->startElement($productTags->getCUSTOMS_TARIFF_NUMBER());
                    $xml->writeElement($productTags->getCUSTOMS_NUMBER(), $article->getPRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER());
                $xml->endElement();
                $xml->writeElement($productTags->getCOUNTRY_OF_ORIGIN(), $article->getPRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN());
            $xml->endElement();

            $this->handleAdditionalUDXs($xml, $article, $locale);
        $xml->endElement();
    }

    /**
     * Handle user defined extensions
     *
     * @param \XMLWriter $xml
     * @param DataObject\Article $article
     * @param string $locale
     * @return void
     */
    private function handleAdditionalUDXs(\XMLWriter $xml, DataObject\Article $article, string $locale)
    {
        /** @var DataObject\UdxInfo $udxInfo */
        $udxInfo = $this->configuration->getHeaderUserDefinedExtensions();

        $udxs = $udxInfo->getAdditionalUDX();

        foreach ($udxs as $udx) {
            if ($udx['XMLTagName'] && !empty($udx['XMLTagName']->getData()) && $udx['ArticleField'] && $udx['ArticleField']->getData()) {
                $data = UdxinfoData::formatData($article, $udx['ArticleField']->getData(), $locale);
                $xml->writeElement($udx['XMLTagName']->getData(), $data);
            }
        }
    }
}
