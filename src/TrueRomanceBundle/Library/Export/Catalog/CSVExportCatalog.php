<?php

namespace TrueRomanceBundle\Library\Export\Catalog;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Data\InputQuantityValue;
use Symfony\Component\Translation\TranslatorInterface;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\CSV;
use TrueRomanceBundle\Library\Data\AbstractData;
use TrueRomanceBundle\Model\FieldCollections\CSVExportAssetsHeaders;

define('CATALOG_CSV_EXPORT_FOLDER', PIMCORE_PROJECT_ROOT . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'catalog');

class CSVExportCatalog extends CatalogExport
{
    /**
     * Translator
     *
     * @var TranslatorInterface $translator
     */
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Export catalog to csv
     *
     * @param array $params
     * @return string $filePath
     */
    public function export(array $params) : string
    {
        $catalog = DataObject\Catalog::getById((int) $params['catalog']);
        $locale = $params['language'];
        $separator = $params['textmode'];

        $catalogName = $catalog->getGROUP_NAME($params['language']) ?: 'export_catalog';
        $filePath = CATALOG_CSV_EXPORT_FOLDER . DIRECTORY_SEPARATOR . $catalogName . '.csv';

        //Delete old file
        if (file_exists($filePath)) {
            unlink($filePath);
        }

        $csvConfig = DataObject\CSVExportConfiguration::getById($params['header_config']);

        $this->insertCatalogDataToCsv($catalog, $csvConfig, $filePath, $separator, $locale);

        return $filePath;
    }

    /**
     * Insert catalog data to csv
     *
     * @param DataObject\Catalog $catalog
     * @param DataObject\CSVExportConfiguration $csvConfig
     * @param string $filePath
     * @param string $separator
     * @param string $locale
     * @return void
     */
    private function insertCatalogDataToCsv(
        DataObject\Catalog $catalog,
        DataObject\CSVExportConfiguration $csvConfig,
        string $filePath,
        string $separator,
        string $locale
    ) {
        $deepestLevel = $this->getCatalogChildDeepestLevel($catalog);

        $this->createCSVHeaders($csvConfig, $filePath, $deepestLevel, $locale);

        $limit = 1000;
        $offset = 0;
        $articlesList = $catalog->getCsvCatalogArticles(0, 0, false);

        $articlesCount = $articlesList->count();

        $pages = round($articlesCount / $limit);

        for ($i = 0; $i <= $pages; $i++) {
            if ($i > 0) {
                $offset = $i * $limit;
            }

            if ($offset > $articlesCount) {
                continue;
            }

            $articles = $catalog->getCsvCatalogArticles($limit, $offset);

            /** @var DataObject\Article $article */
            foreach ($articles as $article) {
                $this->insertArticleDataToCsv($article, $filePath, $deepestLevel, $separator, $locale);
            }
        }

        unset($articles);
    }

    /**
     * Create csv file headers
     *
     * @param DataObject\CSVExportConfiguration $csvConfig
     * @param string $filePath
     * @param integer $deepestLevel
     * @return void
     */
    private function createCSVHeaders(DataObject\CSVExportConfiguration $csvConfig, string $filePath, int $deepestLevel, $locale = 'de_DE')
    {
        $csv = new CSV();
        $csv->delimiter = ';';

        $assetsHeaders = $csvConfig->getAssetsHeaders()->getItems()[0];
        /* @var $assetsHeaders CSVExportAssetsHeaders */

        $articleHeaders = $csvConfig->getArticleHeaders()->getItems()[0];

        $fields = [
            $this->translator->trans($articleHeaders->getPIM_ID(), [], null, $locale),
            $this->translator->trans($articleHeaders->getSUPPLIER_PID(), [], null, $locale),
            $this->translator->trans($articleHeaders->getMANUFACTURER_TYPE_DESCR(), [], null, $locale),
            $this->translator->trans($articleHeaders->getBrand(), [], null, $locale),
            $this->translator->trans($articleHeaders->getDESCRIPTION_SHORT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getDescription_short2(), [], null, $locale),
            $this->translator->trans($articleHeaders->getDESCRIPTION_LONG(), [], null, $locale),
            $this->translator->trans($articleHeaders->getUsp_list(), [], null, $locale),

            $this->translator->trans($articleHeaders->getScope_of_delivery_list(), [], null, $locale),
            $this->translator->trans($articleHeaders->getTec_specs_list(), [], null, $locale),
            $this->translator->trans($articleHeaders->getWarranties_list(), [], null, $locale),
            $this->translator->trans($articleHeaders->getAPPLICATION_LIST(), [], null, $locale),

            $this->translator->trans($articleHeaders->getEAN(), [], null, $locale),
            $this->translator->trans($articleHeaders->getDISCOUNT_GROUP_MANUFACTURER(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPRODUCT_PRICE_NRP_PRICE_AMOUNT_(), [], null, $locale),
            $this->translator->trans($articleHeaders->getCustomer_special_price_amount(), [], null, $locale),

            $this->translator->trans($articleHeaders->getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPRODUCT_PRICE_nrp_PRICE_CURRENCY(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPRODUCT_PRICE_nrp_TAX(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPRICE_NRP_PRICE_AMOUNT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPrice_nrp_quantity(), [], null, $locale),

            $this->translator->trans($articleHeaders->getList_price2_amount(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price2_quantity(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price3_amount(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price3_quantity(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price4_amount(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price4_quantity(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price5_amount(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price5_quantity(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price6_amount(), [], null, $locale),
            $this->translator->trans($articleHeaders->getList_price6_quantity(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPRICE_QUANTITY(), [], null, $locale),
            $this->translator->trans($articleHeaders->getORDER_UNIT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getNO_CU_PER_OU(), [], null, $locale),
            $this->translator->trans($articleHeaders->getCONTENT_UNIT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getQUANTITY_MIN(), [], null, $locale),

            $this->translator->trans($articleHeaders->getQUANTITY_MIN_UNIT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPACKING_UNITS_LENGTH(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPACKING_UNITS_WIDTH(), [], null, $locale),

            $this->translator->trans($articleHeaders->getPACKING_UNITS_DEPTH(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPACKING_UNITS_LENGTH_UNIT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPACKING_UNITS_WEIGHT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPACKING_UNITS_WEIGHT_UNIT(), [], null, $locale),

            $this->translator->trans($articleHeaders->getPRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER(), [], null, $locale),
            $this->translator->trans($articleHeaders->getPRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN(), [], null, $locale),
            $this->translator->trans($articleHeaders->getREFERENCE_FEATURE_GROUP_ID(), [], null, $locale),
            $this->translator->trans($articleHeaders->getREFERENCE_FEATURE_GROUP_NAME(), [], null, $locale),
        ];

        for ($i=1; $i <= $deepestLevel; $i++) {
            $fields[] = $this->translator->trans($articleHeaders->getCATALOG_STRUCTURE_LEVEL() . $i, [], null, $locale);
        }

        $fields = array_merge($fields, [
            $this->translator->trans($articleHeaders->getPRODUCT_STATUS_NEW_PRODUCT(), [], null, $locale),
            $this->translator->trans($articleHeaders->getNew_from(), [], null, $locale),
            $this->translator->trans($articleHeaders->getDiscontinued_product(), [], null, $locale),
            $this->translator->trans($articleHeaders->getDiscontinued_from(), [], null, $locale),

            $this->translator->trans($articleHeaders->getFollowup_article(), [], null, $locale),
            $this->translator->trans($articleHeaders->getArticle_status(), [], null, $locale),
            $this->translator->trans($articleHeaders->getSPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE(), [], null, $locale),
            $this->translator->trans($articleHeaders->getMASTER_SERVICE(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getHEROSHOT_BILD_1(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getHEROSHOT_BILD_2(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getCONTENT_BILD_3(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_4(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_5(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_6(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_7(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_8(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_9(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_10(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAPPLICATION_BILD_11(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getPRODUCT_OTHER_BILD_10(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRODUCT_OTHER_BILD_12(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRODUCT_OTHER_BILD_13(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRODUCT_OTHER_BILD_14(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRODUCT_OTHER_BILD_15(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getHEROSHOT_BILD_16(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getVIDEO(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getCATALOG_ASSET_VIDEO(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getPRODUCT_DATA_SHEET_INFO_1(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRODUCT_DATA_SHEET_INFO_2(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRODUCT_DATA_SHEET_INFO_3(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getAWARD_1_LOW(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAWARD_1_HIGH(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAWARD_2_LOW(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAWARD_2_HIGH(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getAWARD_3_HIGH(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getCATALOG_ASSET_LOGO(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getCATALOG_ASSET_ICON(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getPRESS_TEXT_1(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRESS_TEXT_2(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getPRESS_TEXT_3(), [], null, $locale),

            $this->translator->trans($assetsHeaders->getCATALOG_ASSET_CERTIFICATE(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getCATALOG_ASSET_DOWNLOAD(), [], null, $locale),
            $this->translator->trans($assetsHeaders->getCATALOG_ASSET_PRICELIST_DOWNLOAD(), [], null, $locale),
        ]);

        //Create new csv file
        $csv->save($filePath, [], false, $fields);
    }

    /**
     * Insert article data into csv (look at the $fields variable in createCSVHeader() data must be in same order)
     *
     * @param DataObject\Article $article
     * @param string $filePath
     * @param integer $deepestLevel
     * @param string $separator
     * @param string $locale
     * @return void
     */
    private function insertArticleDataToCsv(
        DataObject\Article $article,
        string $filePath,
        int $deepestLevel,
        string $separator,
        string $locale
    ) {
        $csv = new CSV();
        $csv->delimiter = ';';

        switch ($separator) {
            case self::TEXTMODE_HTML:
                $separator = '</br>';
                break;

            case self::TEXTMODE_PLAIN:
                $separator = '|';
                break;
        }

        $uspList = $this->getBlockData($article->getUsp_list($locale), 'usp_row', $separator);
        $scopeOfDelivery = $this->getBlockData($article->getScope_of_delivery_list($locale), 'scope_of_delivery_row', $separator);
        $warranties = $this->getBlockData($article->getWarranties_list($locale), 'warranties_row', $separator);
        $tecList = $this->getBlockData($article->getTec_specs_list($locale), 'tech_specs_row', $separator);
        $applicationsList = $this->getBlockData($article->getAPPLICATION_LIST($locale), 'application_row', $separator);

        $articleData = [
            'pim_id' => $article->getId(),
            'supplier_pid' => $article->getSUPPLIER_PID(),
            'manufacturer_type_descr' => $article->getMANUFACTURER_TYPE_DESCR(),
            'brand' => $this->getDisplayName($article, 'Brand'),
            'description_short' => $article->getDESCRIPTION_SHORT($locale),
            'description_short_2' => $article->getDescription_short2($locale),
            'description_long' => $article->getDESCRIPTION_LONG($locale),
            'usp_list' => $uspList,
            'scope_of_delivery_list' => $scopeOfDelivery,
            'tec_specs_list' => $tecList,
            'warranties_list' => $warranties,
            'application_list' => $applicationsList,
            'ean' => $article->getEAN(),
            'discount_group_manufacturer' => $article->getDISCOUNT_GROUP_MANUFACTURER($locale),
            'product_price_net_customer_price_amount' => $article->getPRODUCT_PRICE_NRP_PRICE_AMOUNT($locale),
            'customer_special_price_amount' => $article->getCustomer_special_price_amount($locale),
            'product_price_nrp_price_amount' => $article->getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT($locale),
            'product_price_nrp_price_currency' => $article->getPRODUCT_PRICE_nrp_PRICE_CURRENCY($locale),
            'product_price_nrp_tax' => $article->getPRODUCT_PRICE_nrp_TAX($locale),
            'nrp_price_amount' => $article->getPRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT($locale),
            'nrp_price_quantity' => $article->getPrice_nrp_quantity($locale),
            'list_price2_amount' => $article->getList_price2_amount($locale),
            'list_price2_quantity' => $article->getList_price2_quantity($locale),
            'list_price3_amount' => $article->getList_price3_amount($locale),
            'list_price3_quantity' => $article->getList_price3_quantity($locale),
            'list_price4_amount' => $article->getList_price4_amount($locale),
            'list_price4_quantity' => $article->getList_price4_quantity($locale),
            'list_price5_amount' => $article->getList_price5_amount($locale),
            'list_price5_quantity' => $article->getList_price5_quantity($locale),
            'list_price6_amount' => $article->getList_price6_amount($locale),
            'list_price6_quantity' => $article->getList_price6_quantity($locale),
            'price_quantity' => $article->getPRICE_QUANTITY(),
            'order_unit' => $article->getORDER_UNIT(),
            'no_cu_per_ou' => $article->getNO_CU_PER_OU(),
            'content_unit' => $article->getCONTENT_UNIT(),
            'quantity_min' => $article->getQUANTITY_MIN(),
            'quantity_min_unit' => $article->getQUANTITY_MIN_UNIT(),
            'packing_units_length' => $article->getPACKING_UNITS_LENGTH(),
            'packing_units_width' => $article->getPACKING_UNITS_WIDTH(),
            'packing_units_depth' => $article->getPACKING_UNITS_DEPTH(),
            'packing_units_length_unit' => $article->getPACKING_UNITS_LENGTH_UNIT(),
            'packing_units_weight' => $article->getPACKING_UNITS_WEIGHT(),
            'packing_units_weight_unit' => $article->getPACKING_UNITS_WEIGHT_UNIT(),
            'product_logistic_details_customs_number' => $article->getPRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER(),
            'product_logistic_details_customs_origin' => $article->getPRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN(),
            'proficlass_group_name' => $article->getREFERENCE_FEATURE_GROUP_ID(),
            'proficlass_group_description' => $article->getREFERENCE_FEATURE_GROUP_NAME(),
        ];

        //Insert catalog structure data
        $parent = $article->getParent();

        while ($parent instanceof DataObject\CatalogCategory) {
            $catalogCategories[] = $parent;
            $parent = $parent->getParent();
        }

        $catalogCategories = array_reverse($catalogCategories);

        for ($i=1; $i <= $deepestLevel; $i++) {
            $key = "catalog_structure_level_$i";

            $articleData[$key] = isset($catalogCategories[$i-1]) ? $catalogCategories[$i-1]->getGROUP_NAME($locale) : null;
        }

        $articleData = array_merge($articleData, [
            'product_status_new_product' => $article->getPRODUCT_STATUS_NEW_PRODUCT($locale) ? $this->translator->trans('Yes', [], null, $locale) : $this->translator->trans('No', [], null, $locale),
            'new_form' => $this->formatDate($article->getNew_from($locale)),
            'discontinued_product' => $article->getDiscontinued_product($locale) ? $this->translator->trans('Yes', [], null, $locale) : $this->translator->trans('No', [], null, $locale),
            'discontinued_from' => $this->formatDate($article->getDiscontinued_from($locale)),
            'followup_article' => $article->getFollowup_article($locale) ? $article->getFollowup_article($locale)->getSUPPLIER_PID() : null,
            'article_status' => AbstractData::getSelectDisplayName($article, 'article_status', $locale),
            'special_treatment_class' => $article->getSPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE() == 'true' ? $this->translator->trans('Yes', [], null, $locale) : $this->translator->trans('No', [], null, $locale),
            'master_service' => $article->getMaster_service($locale) ? $this->translator->trans('Yes', [], null, $locale) : $this->translator->trans('No', [], null, $locale),
        ]);

        $assetsData = $this->getArticleAssetData($article, $locale);

        $articleData = array_merge($articleData, $assetsData);

        $csv->save($filePath, [$articleData], true);
    }

    /**
     * Gets article assets data for csv
     *
     * @param DataObject\Article $article
     * @param string $locale
     * @return array
     */
    private function getArticleAssetData(DataObject\Article $article, string $locale) : array
    {
        $articleAssets = $article->getChildren();

        if (empty($articleAssets)) {
            return [];
        }

        $data = [
            'heroshot_bild_1_45' => '',
            'heroshot_bild_2' => '',
            'content_bild_3_kit' => '',
            'application_bild_4' => '',
            'application_bild_5' => '',
            'application_bild_6' => '',
            'application_bild_7' => '',
            'application_bild_8' => '',
            'application_bild_9' => '',
            'application_bild_10' => '',
            'application_bild_11' => '',
            'product_other_bild_10' => '',
            'others_bild_12' => '',
            'others_bild_13' => '',
            'others_bild_14' => '',
            'others_bild_15' => '',
            'heroshot_bild_16' => '',
            'video' => '',
            'catalog_asset_video' => '',
            'product_data_sheet_info' => '',
            'product_data_sheet_info_1' => '',
            'product_data_sheet_info_2' => '',
            'award_1_low' => '',
            'award_1_high' => '',
            'award_2_low' => '',
            'award_2_high' => '',
            'award_3_high' => '',
            'catalog_asset_logo' => '',
            'catalog_asset_icon' => '',
            'press_text_1' => '',
            'press_text_2' => '',
            'press_text_3' => '',
            'catalog_asset_certificate' => '',
            'catalog_asset_download' => '',
            'catalog_asset_pricelist_download' => ''
        ];

        foreach ($articleAssets as $articleAsset) {
            // use always de_DE locale to match asset_title
            $localizedAssetRelations = $articleAsset->getLocalizedFields()->getItems()['de_DE'];

            $asset = $articleAsset->getAsset_relation($locale);

            if (!$asset) {
                continue;
            }

            if (!$articleAsset->getItem_bmecat()) {
                continue;
            }

            if ($articleAsset instanceof DataObject\AssetFile) {
                switch (trim($localizedAssetRelations['asset_title'])) {
                    case 'Produktinfoblatt - PDF (INFO)':
                        $data['product_data_sheet_info'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Produktinfoblatt - PPT (INFO1)':
                        $data['product_data_sheet_info_1'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Produktinfoblatt - DOC (INFO2)':
                        $data['product_data_sheet_info_2'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Pressetext 1 - DOC (PRESS)':
                        $data['press_text_1'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Pressetext 2 - DOC (PRESS1)':
                        $data['press_text_2'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Pressetext 3 - DOC (PRESS2)':
                        $data['press_text_3'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Catalog Asset Certificate':
                        $data['catalog_asset_certificate'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Catalog Asset Download':
                        $data['catalog_asset_download'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Catalog Asset Pricelist Download':
                        $data['catalog_asset_pricelist_download'] = $asset ? $asset->getFilename() : null;
                        break;
                }
            } elseif ($articleAsset instanceof DataObject\AssetImage) {
                switch (trim($localizedAssetRelations['asset_title'])) {
                    case 'Bild 1 Heroshot 45° (WEB)':
                        $data['heroshot_bild_1_45'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 2 Heroshot Frontal (WEB1)':
                        $data['heroshot_bild_2'] = $asset ? $asset->getFilename() : null;

                    case 'Bild 3 Inhaltsbild (KIT)':
                        $data['content_bild_3_kit'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 4 Anwendung (ANW)':
                        $data['application_bild_4'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 5 Anwendung (ANW1)':
                        $data['application_bild_5'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 6 Anwendung (ANW2)':
                        $data['application_bild_6'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 7 Anwendung (ANW3)':
                        $data['application_bild_7'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 8 Anwendung (ANW4)':
                        $data['application_bild_8'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 9 Anwendung (ANW5)':
                        $data['application_bild_9'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 10 Anwendung (ANW6)':
                        $data['application_bild_10'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 11 Anwendung (ANW7)':
                        $data['application_bild_11'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Produktzeichnung (WEB3)':
                        $data['product_other_bild_10'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 12 Others (OTH)':
                        $data['others_bild_12'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 13 Others (OTH1)':
                        $data['others_bild_13'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 14 Others (OTH2)':
                        $data['others_bild_14'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 15 Others (OTH3)':
                        $data['others_bild_15'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Bild 16 Heroshot Frontal EPS (WEB2)':
                        $data['heroshot_bild_16'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Auszeichnung 1 - JPG (AWARD_LOW)':
                        $data['award_1_low'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Auszeichnung 1 - EPS (AWARD_HIGH)':
                        $data['award_1_high'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Auszeichnung 2 - JPG (AWARD1_LOW)':
                        $data['award_2_low'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Auszeichnung 2 - EPS (AWARD1_HIGH)':
                        $data['award_2_high'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Auszeichnung 3 - EPS (AWARD2_HIGH)':
                        $data['award_3_high'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Catalog Asset Logo':
                        $data['catalog_asset_logo'] = $asset ? $asset->getFilename() : null;
                        break;

                    case 'Catalog Asset Icon':
                        $data['catalog_asset_icon'] = $asset ? $asset->getFilename() : null;
                        break;
                }
            } elseif ($articleAsset instanceof DataObject\AssetVideo) {
                switch (trim($localizedAssetRelations['asset_title'])) {
                    case 'Video':
                        if (trim($localizedAssetRelations['asset_title'])) {
                            $data['video'] = $asset && $asset->getData() ? $asset->getData()->getFilename() : null;
                        }
                        break;

                    case 'Catalog Asset Video':
                        if (trim($localizedAssetRelations['asset_title'])) {
                            $data['catalog_asset_video'] = $asset && $asset->getData() ? $asset->getData()->getFilename() : null;
                        }
                        break;
                }

            }
        }

        return $data;
    }

    private function getFeaturesData(DataObject\Article $article)
    {
        $featureGroups = $article->getFEATURE();

        $data = [];

        if ($featureGroups) {
            foreach ($featureGroups->getItems() as $key => $features) {
                foreach ($features as $key => $feature) {
                    $keyConfig = \Pimcore\Model\DataObject\Classificationstore\KeyConfig::getById($key);
                    foreach ($feature as $key => $value) {
                        if ($value instanceof InputQuantityValue) {
                            $featureValue = $value->getValue();
                            $featureUnit = ($value instanceof InputQuantityValue && $value->getUnit()) ? $value->getUnit()->getAbbreviation() : null;
                        } else {
                            $featureValue = $value;
                            $featureUnit = null;
                        }

                        $data[] = [
                            'fname' => $keyConfig->getName(),
                            'fvalue' => is_numeric($featureValue) ? $this->formatMeasurement($featureValue) : $featureValue,
                            'funit' => $featureUnit,
                            'fdescr' => $keyConfig->getDescription(),
                        ];
                    }
                }
            }
        }

        return json_encode($data);
    }

    /**
     * Returns deepest level
     *
     * @param DataObject\Catalog $catalog
     * @return integer
     */
    private function getCatalogChildDeepestLevel(DataObject\Catalog $catalog) : int
    {
        $db = \Pimcore\Db::get();
        $articleList = new DataObject\Article\Listing();
        $quotedPath = $articleList->quote($catalog->getRealFullPath());
        $quotedWildcardPath = $articleList->quote(str_replace('//', '/', $catalog->getRealFullPath() . '/') . '%');

        $sqlQuery = "SELECT MAX(LENGTH(o_path) - LENGTH(REPLACE(o_path, '/', ''))) FROM object_100 WHERE o_path = $quotedPath OR o_path LIKE $quotedWildcardPath";

        $deepestLevel = $db->fetchColumn($sqlQuery) - 3;

        return intval($deepestLevel);
    }
}
