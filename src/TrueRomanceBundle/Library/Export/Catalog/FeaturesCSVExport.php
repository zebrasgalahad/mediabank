<?php

namespace TrueRomanceBundle\Library\Export\Catalog;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Data\InputQuantityValue;
use TrueRomanceBundle\Library\CSV;
use Symfony\Component\Translation\TranslatorInterface;

define('FEATURES_CSV_EXPORT_FOLDER', PIMCORE_PROJECT_ROOT . DIRECTORY_SEPARATOR . 'script' . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'export' . DIRECTORY_SEPARATOR . 'features');

class FeaturesCSVExport extends CatalogExport
{
    /** @var CSV $_csv */
    private $_csv;

    /**
     * Translator
     *
     * @var TranslatorInterface $translator
     */
    private $translator;

    /**
     * FeaturesCSVExport constructor
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator  = $translator;

        $this->_csv = new CSV();
    }

    /**
     * Export features as csv
     *
     * @param array $params
     * @return string
     */
    public function export(array $params) : string
    {
        $catalog = DataObject\Catalog::getById((int) $params['catalog']);
        $locale = $params['language'];
        $bmecat = $params['BMECat'];

        $catalogName = $catalog->getGROUP_NAME($params['language']) ?: 'export_catalog';
        $filePath = FEATURES_CSV_EXPORT_FOLDER . DIRECTORY_SEPARATOR . $catalogName . '.csv';

        $this->createCSV($filePath, $bmecat, $locale);
        $this->insertFeaturesToCSV($catalog, $filePath, $locale);

        return $filePath;
    }

    private function insertFeaturesToCSV(
        DataObject\Catalog $catalog,
        string $filePath,
        string $locale
    ) {

        $limit = 1000;
        $offset = 0;
        $articlesList = $catalog->getCatalogArticles(0, 0, false);
        $articlesCount = $articlesList->count();

        $pages = round($articlesCount / $limit);

        for ($i = 0; $i <= $pages; $i++) {
            if ($i > 0) {
                $offset = $i * $limit;
            }

            if ($offset > $articlesCount) {
                continue;
            }

            $articles = $catalog->getCatalogArticles($limit, $offset);

            /** @var DataObject\Article $article */
            foreach ($articles as $article) {
                $articleId = $article->getSUPPLIER_PID();
                $featureSystemName = $article->getREFERENCE_FEATURE_SYSTEM_NAME();
                $featureGroupId= $article->getREFERENCE_FEATURE_GROUP_ID();
                $featureGroupName = $article->getREFERENCE_FEATURE_GROUP_NAME();

                $featuresData = [];

                if (!empty($article->getFEATURE()->getItems())) {
                    foreach ($article->getFEATURE()->getItems() as $groupId => $features) {
                        foreach ($features as $key => $feature) {
                            $keyConfig = \Pimcore\Model\DataObject\Classificationstore\KeyConfig::getById($key);

                            foreach ($feature as $key => $value) {
                                if ($value instanceof InputQuantityValue) {
                                    $featureValue = $value->getValue();
                                    $featureUnit = ($value instanceof InputQuantityValue && $value->getUnit()) ? $value->getUnit()->getAbbreviation() : null;
                                } else {
                                    $featureValue = $value;
                                    $featureUnit = null;
                                }
                            }

                            $featuresData[] = [
                                $articleId,
                                $featureSystemName,
                                $featureGroupId,
                                $featureGroupName,
                                $keyConfig->getName(),
                                $keyConfig->getDescription(),
                                is_numeric($featureValue) ? $this->formatMeasurement($featureValue) : $featureValue,
                                $featureUnit,
                            ];
                        }
                    }
                }

                $this->_csv->save($filePath, $featuresData, true);
            }
        }
    }

    private function createCSV(string $filePath, string $bmecat, $locale = 'de_DE')
    {
        switch ($bmecat) {
            case self::BMECAT_1_2:
                $idKey = 'AID';
                break;

            case self::BMECAT_2005:
                $idKey = 'PID';
                break;
        }

        $headings = [
            $this->translator->trans('Supplier_' . $idKey, [], null, $locale),
            $this->translator->trans('Feature_System', [], null, $locale),
            $this->translator->trans('Klasse_ID', [], null, $locale),
            $this->translator->trans('Klasse_Name', [], null, $locale),
            $this->translator->trans('Merkmal_ID', [], null, $locale),
            $this->translator->trans('Merkmal_Name', [], null, $locale),
            $this->translator->trans('Wert', [], null, $locale),
            $this->translator->trans('Einheit', [], null, $locale),
        ];

        //Create new csv file
        $this->_csv->save($filePath, [], false, $headings);
    }
}
