<?php

namespace TrueRomanceBundle\Library\Export\Catalog;

use Pimcore\File;
use Pimcore\Model\Asset;
use Pimcore\Model\Asset\Folder;
use Pimcore\Model\DataObject;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\FolderHelper;
use ZipArchive;
use TrueRomanceBundle\Command\ExportCatalogAssetsToZipCommand as LoggerTool;

class CatalogAssetsToZip
{
    const ZIP_IMAGES = '1';
    const ZIP_PRODUCT_SHEETS = '2';

    const ZIP_EXPORT_IMAGES_PROPERTY = 'catalog_export_images';
    const ZIP_EXPORT_SHEETS_PROPERTY = 'catalog_export_sheets';

    /**
     * @param array $params
     * @return Folder
     * @throws \Exception
     */
    public function saveToZip(array $params) : Folder
    {
        $zipMode = $params['zip_mode'];
        $catalogId = $params['catalog'];
        $io = $params['io'];
        $locale = $params['locale'];

        $catalog = DataObject\Catalog::getById(intval($catalogId));

        $folder = false;

        if ($zipMode == self::ZIP_IMAGES) {
            $folder = $this->getCatalogImagesZip($catalog, $io, $locale);
        } elseif ($zipMode == self::ZIP_PRODUCT_SHEETS) {
            $folder = $this->getCatalogProductsSheetsZip($catalog, $io, $locale);
        }

        return $folder;
    }

    /**
     * @param DataObject\Catalog $catalog
     * @param $io
     * @return Folder
     * @throws \Exception
     */
    private function getCatalogImagesZip(DataObject\Catalog $catalog, $io, $locale = 'de_DE') : Folder
    {
        LoggerTool::calcTime();
        LoggerTool::calcMemoryUsage();

        $limit = 1000;
        $offset = 0;
        $assetsList = $catalog->getCatalogImageAssets(0, 0, false, $locale);
        $assetsCount = $assetsList->count();

        $io->text("Found {$assetsCount} image assets.");

        $assetsList = null;
        unset($assetsList);

        $zip = new ZipArchive;
        $numberOfZipFiles = 1;

        $catalogAssetsFolder = FolderHelper::getOrCreateFolder('/catalogs//' . $catalog->getKey() . '/assets', 'asset');

        $this->removeOldZips($catalogAssetsFolder->getRealFullPath());

        $catalogName = $catalog->getGROUP_NAME($locale) ?: $catalog->getKey();

        $zipFileCoreName = "BMECAT_IMAGES_{$catalogName}_Part_";

        $zipFilePath = PIMCORE_ASSET_DIRECTORY . $catalogAssetsFolder->getRealFullPath();

        $zipFilename = $zipFileCoreName . $numberOfZipFiles;

        $zipFile = $zipFilePath . "/$zipFilename.zip";

        $io->progressStart($assetsCount);

        if (!is_file($zipFile)) {
            $zipState = $zip->open($zipFile, \ZipArchive::CREATE);
        } else {
            $zipState = $zip->open($zipFile);
        }

        if ($zipState === true) {
            $i = 0;

            $zipSize = 0;

            while ($offset <= $assetsCount) {
                LoggerTool::calcTime();
                LoggerTool::calcMemoryUsage();

                /** @var Asset\Listing $articleAssets */
                $articleAssets = $catalog->getCatalogImageAssets($limit, $offset, true, $locale);

                foreach ($articleAssets as $asset) {
                    $io->progressAdvance();

                    $assetPath = PIMCORE_ASSET_DIRECTORY . $asset->getRealFullPath();

                    $filename = $asset->getFilename();

                    if (!is_file($assetPath)) {
                        continue;
                    }

                    if (!empty($zip->statName($filename))) {
                        continue;
                    }

                    $zip->addFile($assetPath, $filename);

                    $zipSize += $zip->statIndex($i)['size'];

                    $i++;

                    //If zip file is larger than 1GB create new zip file
                    if ($zipSize >= 1073741824) {
                        LoggerTool::calcTime();
                        LoggerTool::calcMemoryUsage();

                        $zip->deleteName($filename);
                        $zip->close();

                        unset($zip);

                        $this->createZipAsset($zipFilePath . "/$zipFilename.zip", "$zipFilename.zip", $catalogAssetsFolder->getId());

                        $zip = new ZipArchive;
                        $zipFilename = $zipFileCoreName . ++$numberOfZipFiles;

                        $zipFile = $zipFilePath . "/$zipFilename.zip";

                        if (!is_file($zipFile)) {
                            $zipState = $zip->open($zipFile, \ZipArchive::CREATE);
                        } else {
                            $zipState = $zip->open($zipFile);
                        }

                        if ($zipState === true) {
                            $i = 0;

                            $zipSize = 0;

                            $zip->addFile($assetPath, $filename);
                        }
                    }
                }

                $offset += $limit;
            }
        }

        $zip->close();

        $io->progressFinish();

        if (is_file($zipFilePath . "/$zipFilename.zip")) {
            $this->createZipAsset($zipFilePath . "/$zipFilename.zip", "$zipFilename.zip", $catalogAssetsFolder->getId());
        }

        return $catalogAssetsFolder;
    }

    /**
     * @param DataObject\Catalog $catalog
     * @param $io
     * @return Folder
     * @throws \Exception
     */
    private function getCatalogProductsSheetsZip(DataObject\Catalog $catalog, $io, $locale = 'de_DE') : Folder
    {
        LoggerTool::calcTime();
        LoggerTool::calcMemoryUsage();

        $limit = 1000;
        $offset = 0;
        $assetsList = $catalog->getCatalogFileAssets(0, 0, false, $locale);
        $assetsCount = $assetsList->count();

        $io->text("Found {$assetsCount} file assets.");

        $assetsList = null;
        unset($assetsList);

        $zip = new ZipArchive;
        $numberOfZipFiles = 1;

        $productInformationFolder = FolderHelper::getOrCreateFolder('/catalogs//' . $catalog->getKey() . '/productInformation', 'asset');

        $this->removeOldZips($productInformationFolder->getRealFullPath());

        $catalogName = File::getValidFilename($catalog->getGROUP_NAME($locale) ?: $catalog->getKey());

        $zipFileCoreName = "BMECAT_PRODUCTSHEETS_{$catalogName}_Part_";

        $zipFilePath = PIMCORE_ASSET_DIRECTORY . $productInformationFolder->getRealFullPath();

        $zipFilename = $zipFileCoreName . $numberOfZipFiles;

        $zipFile = $zipFilePath . "/$zipFilename.zip";

        $io->progressStart($assetsCount);

        if (!is_file($zipFile)) {
            $zipState = $zip->open($zipFile, \ZipArchive::CREATE);
        } else {
            $zipState = $zip->open($zipFile);
        }

        if ($zipState === true) {
            $i = 0;

            $zipSize = 0;

            while ($offset <= $assetsCount) {
                LoggerTool::calcTime();
                LoggerTool::calcMemoryUsage();

                /** @var Asset\Listing $articleAssets */
                $articleAssets = $catalog->getCatalogFileAssets($limit, $offset, $locale);

                foreach ($articleAssets as $asset) {
                    $io->progressAdvance();

                    $assetPath = PIMCORE_ASSET_DIRECTORY . $asset->getRealFullPath();

                    $filename = $asset->getFilename();

                    if (!is_file($assetPath)) {
                        continue;
                    }

                    if (!empty($zip->statName($filename))) {
                        continue;
                    }

                    $zip->addFile($assetPath, $filename);

                    $zipSize += $zip->statIndex($i)['size'];

                    $i++;

                    //If zip file is larger than 1GB create new zip file
                    if ($zipSize >= 1073741824) {
                        $zip->deleteName($filename);
                        $zip->close();

                        unset($zip);

                        $this->createZipAsset($zipFilePath . "/$zipFilename.zip", "$zipFilename.zip", $productInformationFolder->getId());

                        $zip = new ZipArchive;
                        $zipFilename = $zipFileCoreName . ++$numberOfZipFiles;

                        $zipFile = $zipFilePath . "/$zipFilename.zip";

                        if (!is_file($zipFile)) {
                            $zipState = $zip->open($zipFile, \ZipArchive::CREATE);
                        } else {
                            $zipState = $zip->open($zipFile);
                        }

                        if ($zipState === true) {
                            $i = 0;

                            $zipSize = 0;

                            $zip->addFile($assetPath, $filename);
                        }
                    }
                }

                $offset += $limit;
            }
        }

        $zip->close();

        $io->progressFinish();

        if (is_file($zipFilePath . "/$zipFilename.zip")) {
            $this->createZipAsset($zipFilePath . "/$zipFilename.zip", "$zipFilename.zip", $productInformationFolder->getId());
        }

        return $productInformationFolder;
    }

    /**
     * @param string $zipFilePath
     * @param string $fileName
     * @param int $parentFolderId
     * @throws \Exception
     */
    private function createZipAsset(string $zipFilePath, string $fileName, int $parentFolderId)
    {
        $asset = new Asset();

        $asset->setParentId($parentFolderId)
            ->setData(file_get_contents($zipFilePath))
            ->setFilename($fileName)
            ->save();
    }

    /**
     * Remove old assets
     *
     * @param string $folderPath
     * @return void
     */
    private function removeOldZips(string $folderPath)
    {
        $folder = Asset\Folder::getByPath($folderPath);

        $assets = $folder->getChildren();

        foreach ($assets as $asset) {
            $asset->delete();
        }
    }
}
