<?php

namespace TrueRomanceBundle\Library\Export\Catalog;

use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Concrete;

class CatalogExport
{
    //TextMode
    const TEXTMODE_PLAIN = 'plain';
    const TEXTMODE_HTML = 'html';

    //BMECat
    const BMECAT_1_2 = '1.2';
    const BMECAT_2005 = '2005';

    /**
     * Get data from block element and implode it using separator
     *
     * @param array $data
     * @param string $fieldname
     * @param string $separator
     * @return string $implodedData
     */
    protected function getBlockData(array $data, string $fieldname, string $separator) : string
    {
        $formatedData = [];

        foreach ($data as $element) {
            $formatedData[] = $element[$fieldname]->getData();
        }

        $implodedData = implode($separator, $formatedData);

        return $implodedData;
    }

    protected function formatPrice($priceValue)
    {
        return number_format(floatval($priceValue), 2, '.', '');
    }

    protected function formatMeasurement($value)
    {
        return number_format(floatval($value), 3, '.', '');
    }

    /**
     * Get display name for select field
     *
     * @param Concrete $object
     * @param string $fieldName
     * @return string
     */
    protected function getDisplayName(
        Concrete $object,
        string $fieldName
    ) : string {
        $method = 'get' . ucfirst($fieldName);

        $options = DataObject\Service::getOptionsForSelectField($object, $fieldName);

        return $options[$object->$method()] ?: '';
    }

    protected function formatDate($date)
    {
        if (!$date) {
            return null;
        }

        $formatedDate = $date->format('Y-m-d');

        return $formatedDate;
    }

    /**
     * Get catalog categories for all give catalogs
     *
     * @param DataObject\Catalog[] $catalogs
     * @param integer $limit
     * @param integer $offset
     * @param boolean $getObjects
     * @return void
     */
    public function getCatalogCategories(
        array $catalogs,
        int $limit = 0,
        int $offset = 0,
        bool $getObjects = true
    ) {
        $cateogryList = new DataObject\CatalogCategory\Listing();

        $condition = 'o_published = 1 AND (';

        $lastKey = end(array_keys($catalogs));
        foreach ($catalogs as $key => $catalog) {
            $quotedPath = $cateogryList->quote($catalog->getRealFullPath());
            $quotedWildcardPath = $cateogryList->quote(str_replace('//', '/', $catalog->getRealFullPath() . '/') . '%');

            if ($key === $lastKey) {
                $condition .= 'o_path = ' . $quotedPath . ' OR o_path LIKE ' . $quotedWildcardPath;
            } else {
                $condition .= 'o_path = ' . $quotedPath . ' OR o_path LIKE ' . $quotedWildcardPath . ' OR ';
            }
        }

        $condition .= ')';

        $cateogryList->setLimit($limit);
        $cateogryList->setOffset($offset);

        $cateogryList->setCondition($condition);

        if (!$getObjects) {
            return $cateogryList;
        } else {
            return $cateogryList->getObjects();
        }
    }

    /**
     * Get catalog categories for all give catalogs
     *
     * @param DataObject\Catalog[] $catalogs
     * @param integer $limit
     * @param integer $offset
     * @param boolean $getObjects
     * @return void
     */
    public function getArticles(
        array $catalogs,
        int $limit = 0,
        int $offset = 0,
        bool $getObjects = true
    ) {
        $articleList = new DataObject\Article\Listing();

        $subCondition = '';

        $lastKey = end(array_keys($catalogs));
        foreach ($catalogs as $key => $catalog) {
            $quotedPath = $articleList->quote($catalog->getRealFullPath());
            $quotedWildcardPath = $articleList->quote(str_replace('//', '/', $catalog->getRealFullPath() . '/') . '%');

            if ($key === $lastKey) {
                $subCondition .= 'o_path = ' . $quotedPath . ' OR o_path LIKE ' . $quotedWildcardPath;
            } else {
                $subCondition .= 'o_path = ' . $quotedPath . ' OR o_path LIKE ' . $quotedWildcardPath . ' OR ';
            }
        }

        $categoryClassId = \Pimcore\Model\DataObject\CatalogCategory::classId();

        $condition = "
            o_published = 1 AND
            data_export_share = 'true' AND
            o_parentId IN (
                SELECT o_id FROM object_$categoryClassId
                WHERE o_published = 1 AND
                ($subCondition)
            )
        ";

        $articleList->setCondition($condition);
        $articleList->setLimit($limit);
        $articleList->setOffset($offset);

        if (!$getObjects) {
            return $articleList;
        } else {
            return $articleList->getObjects();
        }
    }
}
