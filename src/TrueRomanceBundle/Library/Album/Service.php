<?php

namespace TrueRomanceBundle\Library\Album;

use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use TrueRomanceBundle\Library\Index\Config;
use TrueRomanceBundle\Library\Data\Collection;

class Service {

    const ALBUM_TABLE = "true_romance_album";
    const ALBUM_ASSET_TABLE = "true_romance_album_asset";
    const ALBUM_USER_TABLE = "true_romance_album_user";

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     *
     * @var string
     */
    private $locale;

    /**
     *
     * @var TrueRomanceBundle\Library\Object\Config
     */
    private $config;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $request;

    public function __construct(Container $container)
    {
        $this->container = $container;

        $this->config = $container->get("trueromance.object.config");

        $this->request = $container->get("request_stack")->getCurrentRequest();

        $this->locale = $this->request ? $this->request->getLocale() : "de_DE";
    }

    public function create($data)
    {
        $albumData = [
            "name" => $data["name"] ? $data["name"] : "",
            "public" => $data["public"] ? $data["public"] : 0,
            "owner_user_id" => $data["owner_user_id"] ? $data["owner_user_id"] : 0,
            "created" => time(),
            "Locale" => $this->locale
        ];

        Database::get()->insertOrUpdate(self::ALBUM_TABLE, $albumData);

        $albumId = Database::get()->lastInsertId();

        $userAlbumData = [
            "album_id" => $albumId,
            "user_id" => $data["user_id"],
            "Locale" => $this->locale
        ];

        Database::get()->insertOrUpdate(self::ALBUM_USER_TABLE, $userAlbumData);

        if ($data["asset_ids"]) {
            foreach ($data["asset_ids"] as $index => $assetId) {
                $assetAlbumData = [
                    "album_id" => $albumId,
                    "asset_id" => $assetId,
                    "sort" => $index,
                    "updated" => microtime(true),
                    "Locale" => $this->locale
                ];

                Database::get()->insert(self::ALBUM_ASSET_TABLE, $assetAlbumData);
            }
        }

        return $this->get($albumId, $data["owner_user_id"]);
    }

    public function getAlbumOnly($albumId)
    {
        $albumTable = self::ALBUM_TABLE;

        return Database::get()->fetchRow("SELECT * FROM $albumTable WHERE album_id = ? AND Locale = '{$this->locale}'", [$albumId]);
    }

    public function get($albumId, $userId, $onlyFirstPreviewImage = false)
    {
        $data = [];

        $assetData = $this->container->get("trueromance.asset.data");

        $albumTable = self::ALBUM_TABLE;
        $data["album"] = Database::get()->fetchRow("SELECT * FROM $albumTable WHERE album_id = ? AND Locale = '{$this->locale}'", [$albumId]);

        $albumAssetTable = self::ALBUM_ASSET_TABLE;
        $data["assets"] = Database::get()->fetchAll("SELECT * FROM $albumAssetTable WHERE album_id = ? AND Locale = '{$this->locale}' ORDER BY updated ASC", [$albumId]);

        $albumUserTable = self::ALBUM_USER_TABLE;
        $data["users"] = Database::get()->fetchAll("SELECT * FROM $albumUserTable WHERE album_id = ? AND Locale = '{$this->locale}'", [$albumId]);

        if (!$data["album"] || empty($data["album"])) {
            return [];
        }

        if (count($data["assets"]) > 0) {
            foreach ($data["assets"] as $index => &$asset) {
                $assetInstance = \Pimcore\Model\Asset::getById($asset["asset_id"]);

                if(!$assetInstance) {
                    $data["assets"][0]["preview_web_path"] = Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage();
                    $data["assets"][0]["preview_web_width"] = 220;
                    $data["assets"][0]["preview_web_height"] = 220;
                    $data["assets"][0]["album_with_assets"] = true;

                    continue;
                }

                $articleInstance = $assetData->getArticleObject($asset["asset_id"]);

                $asset["article_number"] = $articleInstance ? $articleInstance->getSUPPLIER_PID() : $assetInstance->getFilename();
                $asset["preview_web_path"] = "";
                $asset["preview_web_width"] = 220;
                $asset["preview_web_height"] = 220;
                $asset["album_with_assets"] = true;

                if ($assetInstance instanceof \Pimcore\Model\Asset\Image) {
                    $thumbnail = $assetInstance->getThumbnail("listView", false);

                    if (strpos($thumbnail->getPath(), "filetype-not-supported") !== false) {
                        $asset["preview_web_path"] = Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage();
                    } else {
                        $asset["preview_web_path"] = Config::ASSET_PREVIEW_PREFIX_PATH . $thumbnail->getPath();
                        $asset["preview_web_width"] = $thumbnail->getWidth();
                        $asset["preview_web_height"] = $thumbnail->getHeight();
                    }
                } else {
                    $asset["preview_web_path"] = Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage();
                }

                if ($onlyFirstPreviewImage === true) {
                    break;
                }
            }

            $data["assets"] = $this->container->get("trueromance.collection.data")->additionalCollectionData($data["assets"]);
        } else {
            if (!$data["assets"]) {
                $data["assets"][0]["preview_web_path"] = Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage();
                $data["assets"][0]["preview_web_width"] = 220;
                $data["assets"][0]["preview_web_height"] = 220;
                $data["assets"][0]["album_with_assets"] = false;
            }
        }

        if ($onlyFirstPreviewImage === false && $userId !== false) {
            $data["navigation"] = $this->getDetailPageNavigation($albumId, $userId);
        }

        $data["album_user_view_data"] = $this->albumDetailViewUserData($data);

        return $data;
    }

    public function getAssetDetailAlbumNavigation($assetId, $albumId)
    {
        Database::get()->query("SET @i=0;");

        $sqlRaw = "
SELECT
   second_result.*
FROM
   (
      SELECT
         @i:=@i+1 AS i,
         result.*
      FROM
         (
			SELECT DISTINCT
			   traa.*
			FROM
			   true_romance_album_asset traa
			WHERE
			   traa.album_id = ? AND traa.Locale = '{$this->locale}'
               ###SUB_QUERY###
            ORDER BY traa.updated ASC
         )
         as result
   )
   as second_result
###SUB_CONDITION### ;";

        $sqlExecute = str_replace("###SUB_QUERY###", "", $sqlRaw);

        $sqlExecute = str_replace("###SUB_CONDITION###", "WHERE second_result.asset_id = ?", $sqlExecute);

        $targetData = Database::get()->fetchRow($sqlExecute, [$albumId, $assetId]);

        $autoIncrementId = $targetData["i"];

        $whereInSelectionData = [];
        if ($autoIncrementId > 1) {
            $whereInSelectionData = [
                $autoIncrementId - 1,
                $autoIncrementId,
                $autoIncrementId + 1
            ];
        } else {
            $whereInSelectionData = [
                $autoIncrementId,
                $autoIncrementId + 1
            ];
        }

        $whereInSelectionString = implode(",", $whereInSelectionData);

        Database::get()->query("SET @i=0;");

        $executeQuery = str_replace("###SUB_QUERY###", "  ", $sqlRaw);

        $executeQuery = str_replace("###SUB_CONDITION###", " WHERE second_result.i IN ($whereInSelectionString) ", $executeQuery);

        $filteredList = Database::get()->fetchAll($executeQuery, [$albumId]);

        $links = [];
        foreach ($filteredList as $currentIndex => $filteredEntry) {
            if ((int) $filteredEntry["asset_id"] === (int) $assetId) {
                if (isset($filteredList[$currentIndex - 1]) === true) {
                    $links["prev"] = $filteredList[$currentIndex - 1]["asset_id"];
                } else {
                    $links["prev"] = false;
                }

                if (isset($filteredList[$currentIndex + 1]) === true) {
                    $links["next"] = $filteredList[$currentIndex + 1]["asset_id"];
                } else {
                    $links["next"] = false;
                }

                break;
            }
        }

        return $links;
    }

    private function getDetailPageNavigation($albumId, $userId)
    {
        Database::get()->query("SET @i=0;");

        $sqlRaw = "
SELECT
   second_result.*
FROM
   (
      SELECT
         @i:=@i+1 AS i,
         result.*
      FROM
         (
			SELECT DISTINCT
			   tra.*
			FROM
			   true_romance_album tra,
			   true_romance_album_user as trau
			WHERE
			   owner_user_id = ?
			   AND tra.Locale = '{$this->locale}'
			   OR
			   (
				  tra.album_id = trau.album_id
				  AND trau.user_id = ?
				  AND trau.Locale = '{$this->locale}'
			   )
               ###SUB_QUERY###
            ORDER BY created DESC
         )
         as result
   )
   as second_result
###SUB_CONDITION### ;";

        $sqlExecute = str_replace("###SUB_QUERY###", "", $sqlRaw);

        $sqlExecute = str_replace("###SUB_CONDITION###", "WHERE second_result.album_id = ?", $sqlExecute);

        $targetData = Database::get()->fetchRow($sqlExecute, [$userId, $userId, $albumId]);

        $autoIncrementId = $targetData["i"];

        $whereInSelectionData = [];
        if ($autoIncrementId > 1) {
            $whereInSelectionData = [
                $autoIncrementId - 1,
                $autoIncrementId,
                $autoIncrementId + 1
            ];
        } else {
            $whereInSelectionData = [
                $autoIncrementId,
                $autoIncrementId + 1
            ];
        }

		if($whereInSelectionData[0] === null) {
			unset($whereInSelectionData[0]);
		}

        $whereInSelectionString = implode(",", $whereInSelectionData);

        Database::get()->query("SET @i=0;");

        $executeQuery = str_replace("###SUB_QUERY###", "  ", $sqlRaw);

        $executeQuery = str_replace("###SUB_CONDITION###", " WHERE second_result.i IN ($whereInSelectionString) ", $executeQuery);

        $filteredList = Database::get()->fetchAll($executeQuery, [$userId, $userId]);

        $links = [];
        foreach ($filteredList as $currentIndex => $filteredEntry) {
            if ((int) $filteredEntry["album_id"] === (int) $albumId) {
                if (isset($filteredList[$currentIndex - 1]) === true) {
                    $links["prev"] = $filteredList[$currentIndex - 1]["album_id"];
                } else {
                    $links["prev"] = false;
                }

                if (isset($filteredList[$currentIndex + 1]) === true) {
                    $links["next"] = $filteredList[$currentIndex + 1]["album_id"];
                } else {
                    $links["next"] = false;
                }

                break;
            }
        }

        return $links;
    }

    public function delete($albumId)
    {
        foreach ([self::ALBUM_TABLE, self::ALBUM_ASSET_TABLE, self::ALBUM_USER_TABLE] as $table) {
            $query = "DELETE FROM {$table} WHERE album_id = ? AND Locale = '{$this->locale}'";

            Database::get()->query($query, [$albumId]);
        }
    }

    public function deleteAsset($albumId, $assetId, $userId)
    {
        $table = self::ALBUM_ASSET_TABLE;

        $query = "DELETE FROM {$table} WHERE album_id = ? AND asset_id = ? AND Locale = '{$this->locale}'";

        Database::get()->query($query, [$albumId, $assetId]);

        return $this->get($albumId, $userId);
    }

    public function getList($userId, $limit = null)
    {
        $albumTable = self::ALBUM_TABLE;
        $albumUserTable = self::ALBUM_USER_TABLE;

        $limitCondition = "";
        if ($limit !== null) {
            $limit = " LIMIT " . (int) $limit;
        }

        $albumIds = Database::get()->fetchAll("SELECT DISTINCT tra.* FROM $albumTable tra, $albumUserTable as trau
WHERE owner_user_id = ? AND tra.Locale = '{$this->locale}'
OR (tra.album_id = trau.album_id AND trau.user_id = ? AND tra.Locale = '{$this->locale}') ORDER BY created DESC $limit", [$userId, $userId]);

        $albums = [];
        foreach ($albumIds as $albumId) {
            $albums[] = $this->get($albumId["album_id"], $userId, true);
        }

        return $albums;
    }

    public function addAssetsToAlbum($albumId, $assets, $userId)
    {
        foreach ($assets as $assetId) {
            Database::get()->insertOrUpdate(self::ALBUM_ASSET_TABLE, ["album_id" => $albumId, "asset_id" => $assetId, "updated" => microtime(true), "Locale" => $this->locale]);
        }

        return $this->get($albumId, $userId);
    }

    public function addUserToAlbum($albumId, $userId)
    {
        Database::get()->insertOrUpdate(self::ALBUM_USER_TABLE, ["album_id" => $albumId, "user_id" => $userId, "Locale" => $this->locale]);

        return $this->get($albumId, $userId);
    }

    public function addExternUserToAlbum($albumId, $userId, $externUserEmail)
    {
        Database::get()->insertOrUpdate(self::ALBUM_USER_TABLE, ["album_id" => $albumId, "user_id" => $userId, "extern_user_email" => $externUserEmail, "Locale" => $this->locale]);

        return $this->get($albumId, $userId);
    }

    public function updateAlbum($albumId, $userId, $data)
    {
        Database::get()->updateWhere(self::ALBUM_TABLE, $data, "album_id = $albumId AND Locale = '{$this->locale}'");

        return $this->get($albumId, $userId);
    }

    public function removeUserFromAlbum($albumId, $userId)
    {
        Database::get()->deleteWhere(self::ALBUM_USER_TABLE, "user_id= '$userId' AND album_id = '$albumId' AND Locale = '{$this->locale}'");

        return $this->get($albumId, $userId);
    }

    public function getUserAlbumCount($userId): int
    {
        $albumTable = self::ALBUM_TABLE;

        $albumUserTable = self::ALBUM_USER_TABLE;

        $query = "SELECT COUNT(*) FROM (
SELECT tra.album_id FROM true_romance_album as tra, true_romance_album_user as trau WHERE tra.owner_user_id = ? AND tra.Locale = '{$this->locale}'
OR tra.album_id = trau.album_id
AND trau.user_id = ?
AND trau.Locale = '{$this->locale}'
GROUP BY tra.album_id) as temp";

        return (int) Database::get()->fetchOne($query, [$userId, $userId]);
    }

    /**
     *
     * @param string $userId
     * @return array
     */
    public function homePageAlbumData($userId)
    {
        $finalData = [];

        $limit = $this->config->get("Misc_Albums_Count_List", $this->locale);

        $albumList = $this->getList($userId, $limit);

        foreach ($albumList as $album) {
            $finalData[$album["album"]["album_id"]] = [
                "creation_date" => (date('d.m.Y', $album["album"]["created"])),
                "album_name" => $album["album"]["name"],
                "preview_web_path" => $album["assets"][0]["preview_web_path"],
                "preview_web_width" => $album["assets"][0]["preview_web_width"],
                "preview_web_height" => $album["assets"][0]["preview_web_height"]
            ];
        }

        return $finalData;
    }

    /**
     *
     * @param array $data
     * @return array
     */
    private function albumDetailViewUserData($data)
    {
        $albumData = [];

        $albumData["album_owner_id"] = $data["album"]["owner_user_id"];
        $albumData["customer_full_name"] = $this->getCustomerNameById($data["album"]["owner_user_id"]);
        $albumData["creation_date"] = date('d.m.Y H:i', $data["album"]["created"]) . " " . $this->container->get("translator")->trans("time");
        $albumData["is_shared_album"] = $this->isSharedAlbum($data);

        return $albumData;
    }

    /**
     *
     * @param array $data
     * @return boolean
     */
    private function isSharedAlbum($data)
    {
        $user = $this->container->get("trueromance.user.permissions")->getUser();

        if(is_object($user) === false) {
            return true;
        }

        $userId = $user->getId();

        if ((int) $data["album"]["owner_user_id"] === (int) $userId) {
            return false;
        }

        foreach ($data["users"] as $user) {
            if ((int) $user["user_id"] === $userId && $user["extern_user_email"] !== NULL) {
                return true;
            } else if ($user["user_id"] !== $userId) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param string $customerId
     * @return string
     */
    private function getCustomerNameById($customerId)
    {
        $customer = \Pimcore\Model\DataObject\Customer::getById($customerId);

        if (!$customer) {
            return "";
        }

        $customerFullName = $customer->getFirstname() . " " . $customer->getLastname();

        return $customerFullName;
    }

}
