<?php

namespace TrueRomanceBundle\Library\Frontend\Javascript;

class Data {
    
    /**
     *
     * @var array 
     */
    private static $data = array();
    
    /**
     * 
     * @param array $key
     * @param array $data
     */
    public static function add($key, $data) {
        self::$data[$key] = $data;
    }
    
    /**
     * 
     * @param boolean $asJson
     * @return string|array
     */
    public static function get($asJson = true) {
        $returnData = $asJson === true ? json_encode(self::$data, true) : self::$data;
        
        return $returnData;
    }
    
}

