<?php

namespace TrueRomanceBundle\Library;

/**
 * Get field definitions from specific object
 *
 * @author lukadrezga
 */
class FieldDefinitions {
    
    public static function getFieldDefinitionsByKey($object, $field, $key) {
        $fieldDefinitionOptions = $object->getClass()->getFieldDefinition($field)->getOptions();
        
        foreach ($fieldDefinitionOptions as $option) {
            if (in_array($key, $option) === true) {
                return $option["key"];
            }
        }
        
        return "";
    }
}
