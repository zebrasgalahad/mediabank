<?php

namespace TrueRomanceBundle\Library\Tools\Cli;

class Helper {
    
    const OUTPUT_INFO = "info";
    
    const OUTPUT_ERROR = "error";
    
    const OUTPUT_SUCCESS = "success";
    
    const OUTPUT_ALL = "all";
    
    /**
     * If set to false, echoing output is disabled
     * @var bool 
     */
    public static $echoOutput = true;
    
    /**
     *
     * @var array 
     */
    private static $outputBuffer = [
        "info" => [],
        "error" => [],
        "success" => []
    ];

    /**
     * Default ouput color
     * @param string $message
     */
    public static function info(string $message) {
        self::$outputBuffer["info"][] = "INFO: " . $message;
        
        self::echoOutput($message);
    }
    
    /**
     * Red color output
     * @param string $message
     */
    public static function error(string $message) {
        self::$outputBuffer["error"][] = "ERROR: " . $message;
        
        self::echoOutput("\033[31m{$message}\033[0m");
    }

    /**
     * Green color output
     * @param string $message
     */
    public static function success(string $message) {
        self::$outputBuffer["success"][] = "SUCCESS: " . $message;
        
        self::echoOutput("\033[32m{$message}\033[0m");
    }
    
    /**
     * 
     * @param string $string
     */
    private static function echoOutput(string $string) {
        if(self::$echoOutput === true) {
            echo PHP_EOL . $string . PHP_EOL;
        }
    }
    
    /**
     * 
     * @param boolean $asArray
     * @return array|string
     */
    public static function getOuputBuffer($asArray = false, $type = "all") {
        if($asArray === true && $type === self::OUTPUT_ALL) {
            return self::$outputBuffer;
        }
        
        if($asArray === true && $type !== self:: OUTPUT_ALL) {
            return self::$outputBuffer[$type];
        }        
        
        $outputBuffer = self::$outputBuffer;
        
        if($type !== self::OUTPUT_ALL) {
            $outputBuffer = [];
            
            $outputBuffer[$type] = self::$outputBuffer[$type];
        } 
        
        $output = "";
        foreach ($outputBuffer as $key => $lines) {
            $output .= PHP_EOL;
            
            if(count($lines) > 0) {
                $output .= implode(PHP_EOL, $lines);
            }
            
            $output .= PHP_EOL;
        }

        $output = preg_replace('/^[ \t]*[\r\n]+/m', '', $output);
        
        return $output;
    }
    
    public static function disableOutput()
    {
        self::$echoOutput = false;
    }

    public static function enableOutput()
    {
        self::$echoOutput = true;
    }
    
    /**
     * 
     * @param string $type
     * @param string $filepath
     */
    public static function saveFile ($filepath = "output_buffer.log", $type = "all", $append = false) {
        $data = self::getOuputBuffer(false, $type);
        
        if($append === true) {
            file_put_contents($filepath, $data, FILE_APPEND);
        } else {
            file_put_contents($filepath, $data);
        }
    }
}
