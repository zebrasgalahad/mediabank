<?php

namespace TrueRomanceBundle\Library\Tools\Cli\Thumbnail;

class Result
{
    public $filepath;
    public $webpath;
    public $width;
    public $height;
    public $filename;
    public $type;

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    public function getWidth()
    {
        return $this->width;
    }

    public function getHeight()
    {
        return $this->height;
    }

    public function setWidth($width)
    {
        $this->width = $width;
    }

    public function setHeight($height)
    {
        $this->height = $height;
    }

    public function getFilepath()
    {
        return $this->filepath;
    }

    public function getWebpath()
    {
        return $this->webpath;
    }

    public function setFilepath($filepath)
    {
        $this->filepath = $filepath;
    }

    public function setWebpath($webpath)
    {
        $this->webpath = $webpath;
    }
}
