<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TrueRomanceBundle\Library\Tools\Cli;

class Thumbnail
{
    /**
     *
     * @param \Pimcore\Model\Asset $asset
     * @param string $configName
     * @param boolean $forceUpdate
     * @param string $colorspace
     * @param int $dpi
     * @param int $quality
     * @return Thumbnail\Result
     */
    public static function create(\Pimcore\Model\Asset $asset, $configName, $forceUpdate = false, $colorspace = 'RGB', $quality = 72, $dpi = null)
    {
        $filePaths = self::getFilePaths($asset, $configName);

        if(is_file($filePaths["file"]) === true && $forceUpdate === false) {
            return self::createResult($filePaths);
        }

        $resizeMethodParam = self::getResizeMethodParam($configName, $asset);

        $sourceFilepath = PIMCORE_WEB_ROOT . "/var/assets" . $asset->getRealFullPath();

        $gsBinary = \Pimcore\Config::getWebsiteConfig()->gs_binary;

        $convertBinary = \Pimcore\Config::getWebsiteConfig()->convert_binary;

        $sourceFilepath = str_replace(" ", "\ ", $sourceFilepath);

        if($asset->getMimetype() === "application/postscript") {
            $command = "$gsBinary -sDEVICE=jpeg -dJPEGQ=100 -dNOPAUSE -dBATCH -dSAFER -dEPSCrop -r300 -sOutputFile={$filePaths["file"]} {$sourceFilepath}";

            exec($command, $output);

            $command = "$convertBinary -sampling-factor 4:2:0 -strip -quality {$quality} -interlace JPEG -colorspace {$colorspace} $resizeMethodParam {$filePaths["file"]} {$filePaths["file"]}";

            exec($command);
        } else {
            $dpiCommand = " ";
            if($dpi !== null) {
                $dpiCommand = " -density $dpi ";
            }

            $command = "$convertBinary -sampling-factor 4:2:0 -strip -quality {$quality} -interlace JPEG $dpiCommand -colorspace {$colorspace} $resizeMethodParam {$sourceFilepath} {$filePaths["file"]}";

            exec($command, $output, $return);
        }

        return self::createResult($filePaths);
    }

    private static function createResult($filePaths)
    {
        // TODO: Fix later for Pimcore version 6
        //$imagick = new \Imagick($filePaths["file"]);

        $result = new Thumbnail\Result();

        $result->setWebpath($filePaths["web"]);

        $result->setFilepath($filePaths["file"]);

        //$result->setWidth($imagick->getImageWidth());

        //$result->setHeight($imagick->getimageheight());

        return $result;
    }


    private static function getFileBaseDirectory()
    {
        $basePath = PIMCORE_WEB_ROOT . "/var/tmp/custom-thumbnails";

        if(is_dir($basePath) === false) {
            mkdir($basePath, 0777, true);
        }

        return $basePath;
    }

    private static function getWebBaseDirectory()
    {
        $basePath = "/var/tmp/custom-thumbnails";

        return $basePath;
    }

    private static function getFilePaths($asset, $configName)
    {
        $fileBasepath = self::getFileBaseDirectory();

        $filename = "/thumbnail_" .  "__" . $asset->getId() . "__" . strtolower($configName) . ".jpg";

        $webBasePath = self::getWebBaseDirectory();

        return [
            "file" => $fileBasepath . $filename,
            "web" => $webBasePath . $filename,
        ];
    }

    private static function getResizeMethodParam($configName, $asset)
    {
        $thumbnailConfig = \Pimcore\Model\Asset\Image\Thumbnail\Config::getByAutoDetect($configName);

        $configItem = $thumbnailConfig->items[0];

        $result = "";
        if($configItem["method"] === "scaleByHeight") {
            $result = "-geometry x{$configItem["arguments"]["height"]}";
        }

        if($configItem["method"] === "scaleByWidth") {
            $assetWidth = $asset->getWidth();

            if($assetWidth <= (int)$configItem["arguments"]["width"]) {
                return "";
            }

            $result = "-resize {$configItem["arguments"]["width"]}";
        }

        return $result;
    }
}
