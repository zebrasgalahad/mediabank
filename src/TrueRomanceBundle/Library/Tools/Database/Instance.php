<?php

namespace TrueRomanceBundle\Library\Tools\Database;

use \TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

class Instance {

    /**
     *
     * @var array 
     */
    private static $classIdMappings = [];
    
    /**
     * 
     * @param type $class - e.g. \Pimcore\Model\DataObject\Product (must have preceding slash)
     * @param type $id - the if of object
     * @param type $locale - e.g. de_DE
     * @return array
     */
    public static function getByClassAndId($class, $id, $locale = null) {
        if($class === "\Pimcore\Model\Asset") {
            return  self::getAsset($id);
        }
        
        if(isset(self::$classIdMappings[$class]) === false) {
            $classInstance = new $class();
            
            self::$classIdMappings[$class] = $classInstance->getClassId();
        }
        
        $classId = self::$classIdMappings[$class];
        
        if($locale !== null) {
            $table = "object_localized_query_{$classId}_{$locale}";
            $tableColumnId = "ooo_id";
        } else {
            $table = "object_query_{$classId}";
            $tableColumnId = "oo_id";
        }
        
        $result = Database::getDatabaseObject()->fetchRow("SELECT * FROM {$table} WHERE {$tableColumnId} = ?", [$id]);
        
        return $result;
    }

    /**
     * 
     * @param string|int $id
     * @return array
     */
    private static function getAsset ($id) {
        $result = Database::getDatabaseObject()->fetchRow("SELECT * FROM assets WHERE id = ?", [$id]);
        
        return $result;
    }
    
}
