<?php

namespace TrueRomanceBundle\Library\Tools;

class ObjectOption {

    /**
     * 
     * @param \Pimcore\Model\DataObject $object
     * @param string $attribute
     * @param mixed $optionValue
     * @param boolean $localized
     * @return mixed
     */
    public static function keyToValue($object, $attribute, $optionValue, $localized = false) {
        if($optionValue === false || $optionValue === null) {
            return $optionValue;
        }
        
        $attributeInstanceOptions = self::getAttributeInstanceOptions($object, $attribute, $localized);
        
        if(is_array($optionValue) === true) {
            $result = [];
            foreach ($optionValue as $singleOptionValue) {
                foreach ($attributeInstanceOptions as $option) {
                    if($option["key"] == $singleOptionValue) {
                        $result = $option["value"];
                    }
                }                    
            }
            
            return $result;
        } else {
            foreach ($attributeInstanceOptions as $option) {
                if($option["key"] == $optionValue) {
                    return $option["value"];
                }
            }            
        }
        
        return null;        
    }
    
    /**
     * 
     * @param \Pimcore\Model\DataObject $object
     * @param string $attribute
     * @param mixed $optionValue
     * @param boolean $localized
     * @return mixed
     */    
    public static function valueToKey($object, $attribute, $optionValue, $localized = false) {
        if($optionValue === false || $optionValue === null) {
            return $optionValue;
        }
        
        $attributeInstanceOptions = self::getAttributeInstanceOptions($object, $attribute, $localized);
        
        if(is_array($optionValue) === true) {
            $result = [];
            foreach ($optionValue as $singleOptionValue) {
                foreach ($attributeInstanceOptions as $option) {
                    if($option["value"] == $singleOptionValue) {
                        $result = $option["key"];
                    }
                }                    
            }
            
            return $result;
        } else {
            foreach ($attributeInstanceOptions as $option) {
                if($option["value"] == $optionValue) {
                    return $option["key"];
                }
            }            
        }
        
        return null;        
    }
    
    /**
     * 
     * @param \Pimcore\Model\DataObject $object
     * @param string $attribute
     * @param boolean $localized
     * @return mixed
     */
    public static function getAttributeInstanceOptions($object, $attribute, $localized) {        
        if(method_exists($object, "getClass") === true) {
            $fieldDefinitions = $object->getClass()->getFieldDefinitions();
        } else {
            $fieldDefinitions = $object->getDefinition()->getFieldDefinitions();
        }
        
        $singleFieldDefinition = null;
        if($localized === true) {
            $singleFieldDefinition = $fieldDefinitions["localizedfields"]->getFielddefinition($attribute);
        } else {
            $singleFieldDefinition = $fieldDefinitions[$attribute];
        }
        
        $attributeInstanceOptions = $singleFieldDefinition->options;        
        
        return $attributeInstanceOptions;
    }

}
