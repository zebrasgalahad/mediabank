<?php

namespace TrueRomanceBundle\Library\Tools;

class ObjectHelper
{

    public static function getQueryTableByClass($class, $locale = null)
    {
        $classId = self::getClassIdByClass($class);

        if($locale) {
            return "object_localized_query_{$classId}_{$locale}";
        }        
        
        return "object_query_$classId";
    }

    public static function getStoreTableByClass($class)
    {
        $classId = self::getClassIdByClass($class);

        return "object_store_$classId";
    }    
    
    public static function getRelationTableByClass($class)
    {
        $classId = self::getClassIdByClass($class);

        return "object_relations_$classId";
    }        
    
    public static function getFieldCollectionTableByClass($class, $fieldCollectionName, $locale = null)
    {
        $classId = self::getClassIdByClass($class);
        
        if($locale === null) {
            return "object_collection_{$fieldCollectionName}_{$classId}";
        } else {
            return "object_collection_{$fieldCollectionName}_localized_{$classId}";
        }
    }


    private static function getClassIdByClass($class)
    {
        $classInstance = new $class();

        $classId = $classInstance->getClassId();

        return $classId;
    }
    
    
}
