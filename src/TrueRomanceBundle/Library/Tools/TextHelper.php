<?php

namespace TrueRomanceBundle\Library\Tools;

class TextHelper {

    /**
     * 
     * @param string $string
     * @return string
     */
    public static function searchAndRemoveDuplicateWords($string) {
        $words = explode(" ", $string);

        $wordstats = array();

        foreach ($words as $word) {
            $wordstats[strtolower($word)] ++;
        }

        foreach ($wordstats as $value) {
            if ($value >= 2) {
                $string = implode(" ", array_unique($words));
                $string = str_replace(" - ", " ", $string);
                $string = str_replace(" | ", " ", $string);
                $string = str_replace(" / ", " ", $string);
            }
        }

        return $string;
    }

    /**
     * 
     * @param string $text
     * @param string $word
     * @param int $characters_before
     * @param int $characters_after
     * @return string
     */
    public static function highlightAndCutText($text, $word, $characters_before = 100, $characters_after = 100) {
        $text = $text;

        $pos = strpos($text, $word);
        $start = $characters_before < $pos ? $pos - $characters_before : 0;
        $len = $pos + strlen($word) + $characters_after - $start;
        $text = mb_substr($text, $start, $len);
        
        $finalText = str_ireplace($word, '<span class="highlight" >' . $word . '</span>', $text);
        
        return $finalText;
    }

}
