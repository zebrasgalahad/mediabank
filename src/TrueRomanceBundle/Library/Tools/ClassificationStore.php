<?php

namespace TrueRomanceBundle\Library\Tools;

use Pimcore\Model\DataObject\Classificationstore\GroupConfig;
use Pimcore\Model\DataObject\Classificationstore\KeyConfig;

class ClassificationStore
{

    public static function createGroupIfNotExists(string $name, int $classificationStoreId, string$description = "")
    {
        $config = \Pimcore\Model\DataObject\Classificationstore\GroupConfig::getByName($name, $classificationStoreId);

        if (!$config) {
            $config = new \Pimcore\Model\DataObject\Classificationstore\GroupConfig();

            $config->setStoreId($classificationStoreId);

            $config->setName($name);

            $config->setDescription($description);

            $config->save();
        }

        return $config;
    }

    public static function createAttributeIfNotExists(array $attribute, int $classificationStoreId)
    {
        $rawAttribute = trim($attribute["type"]);
        if ($rawAttribute === "wysiwyg") {
            $targetAttribute = "wysiwyg";
        } elseif ($rawAttribute === "") {
            $targetAttribute = "input";
        } else {
            $targetAttribute = "inputQuantityValue";
        }

        $keyConfig = \Pimcore\Model\DataObject\Classificationstore\KeyConfig::getByName($attribute["property"], $classificationStoreId);

        $unitId = "";
        $unit = null;
        if ($targetAttribute === "inputQuantityValue") {
            $unit = \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::createUnitIfNotExists($rawAttribute);

            if (is_object($unit) === true) {
                $unitId = $unit->getId();
            }
        }

        $definition = [
            'fieldtype' => $targetAttribute,
            'name' => $attribute["property"],
            'title' => $attribute["property"],
            'datatype' => 'data',
            'validUnits' => [$unitId],
            'defaultUnit' => $unitId
        ];

        if (!$keyConfig) {
            $keyConfig = new \Pimcore\Model\DataObject\Classificationstore\KeyConfig();
        }

        $keyConfig->setName($attribute["property"]);

        $keyConfig->setTitle($attribute["property"]);
        $keyConfig->setType($targetAttribute);
        $keyConfig->setStoreId($classificationStoreId);
        $keyConfig->setEnabled(true);
        $keyConfig->setDefinition(json_encode($definition));
        $keyConfig->save();

        return $keyConfig;
    }

    public static function assignKeyToGroup(GroupConfig $group, KeyConfig $key, $sorter)
    {
        $keyGroupRelation = new \Pimcore\Model\DataObject\Classificationstore\KeyGroupRelation();

        $keyGroupRelation->setGroupId($group->getId());

        $keyGroupRelation->setKeyId($key->getId());

        $keyGroupRelation->setEnabled(true);

        $keyGroupRelation->setSorter($sorter + 1);

        $keyGroupRelation->save();
    }

    public static function deleteGroupsAndKeyDefinitions(int $classificationStoreId)
    {
        self::deleteKeyDefinitions($classificationStoreId);
        self::deleteGroups($classificationStoreId);
    }

    public static function deleteGroups(int $classificationStoreId)
    {
        $groupListing = new GroupConfig\Listing();

        $groupListing->addConditionParam("storeId", $classificationStoreId);

        foreach ($groupListing->load() as $group) {
            $group->delete();
        }
    }

    public static function deleteKeyDefinitions($classificationStoreId)
    {
        $keyDefinitionsListing = new KeyConfig\Listing();

        $keyDefinitionsListing->addConditionParam("storeId", $classificationStoreId);

        foreach ($keyDefinitionsListing->load() as $key) {
            $key->delete();
        }
    }
}
