<?php

namespace TrueRomanceBundle\Library\Tools;

/**
 * Description of QuantityValueUnitMapping
 *
 * @author lukadrezga
 */
class QuantityValueUnitMapping {

    protected static $mapping;
    
    /**
     * 
     * @return array
     */
    public static function unitMapping() {
        if(self::$mapping !== null) {
            return self::$mapping;
        }
        
        $units = new \Pimcore\Model\DataObject\QuantityValue\Unit\Listing();

        $mapping = [];

        foreach ($units->load() as $unit) {
            $mapping[$unit->getAbbreviation()] = $unit;
        }

        self::$mapping = $mapping;
        
        return $mapping;
    }
    
    /**
     * 
     * @param string $unitKey
     */
    public static function createUnitIfNotExists($unitKey, $unitValue = null) {
        $units = new \Pimcore\Model\DataObject\QuantityValue\Unit\Listing();

        $units->addConditionParam("abbreviation = '$unitKey'");
        
        if (empty($units->load()) === true) {
            $unit = new \Pimcore\Model\DataObject\QuantityValue\Unit();

            $data = [
                "abbreviation" => $unitKey,
                "longname" => $unitValue ? $unitValue : $unitKey
            ];

            $unit->setValues($data);
            
            $unit->save();
            
            return $unit;
        }
        
        return $units->load()[0];
    }

}
