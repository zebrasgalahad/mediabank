<?php

namespace TrueRomanceBundle\Library\Tools;

use Pimcore\Model\Asset\Image\Thumbnail\Config;

class Thumbnail
{
    public static $includes = ["application/postscript"];

    public static function create($asset, $thumbnailConfig)
    {
        if(in_array($asset->getMimetype(), self::$includes) === true) {
            $assetSourcePath = PIMCORE_ASSET_DIRECTORY . $asset->getFullpath();

            $exploded = explode(".", $asset->getFilename());

            $prefix = $exploded[count($exploded) -1];

            $targetTempPath = PIMCORE_TEMPORARY_DIRECTORY . "/" . uniqid() . ".jpg";

            copy($assetSourcePath, $targetTempPath);

            $imagick = new \Imagick($targetTempPath);

            $imagick->setImageFormat("jpg");
            
            $imagick->transformImageColorspace(\Imagick::COLORSPACE_RGB);  

            $imagick->writeimage($targetTempPath);
            
            $tempAsset = new \Pimcore\Model\Asset();
            $tempAsset = $asset->setData(file_get_contents($targetTempPath));

            $thumbnail = $tempAsset->getThumbnail($thumbnailConfig, false);

            unlink($targetTempPath);
        } else {
            $thumbnail = $asset->getThumbnail($thumbnailConfig, false);
        }
        
        return $thumbnail;        
    }
}
