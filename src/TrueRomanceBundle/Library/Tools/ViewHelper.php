<?php

namespace TrueRomanceBundle\Library\Tools;

class ViewHelper
{
    public $queryParams;

    public function __construct($queryParams)
    {
        $this->queryParams = $queryParams;
    }

    /**
     * 
     * @param array $filterToAdd
     * @param bool $addCurrent
     * @param array $excludeFilter
     * @return string
     */
    public function createLink($filterToAdd = null, $addCurrent = true, $excludeFilter = [], $asArray = false)
    {
        $params = $this->queryParams;

        if (count($excludeFilter) > 0 && isset($params["f"]) === true) {
            foreach ($excludeFilter as $singleExcludeFilter) {
                unset($params["f"][$singleExcludeFilter]);
            }
        }

        $filter = [];
        if ($addCurrent === true) {
            $filter = $params;
        } else {
            $filter = [];
        }

        if ($filterToAdd) {
            foreach ($filterToAdd as $filterToAddKey => $filterToAddValues) {
                foreach ($filterToAddValues as $filterToAddValue) {
                    $filter["f"][$filterToAddKey][] = $filterToAddValue;
                }
            }
        }

        if($asArray === true) {
            return $filter;
        }
        
        $url = "?" . http_build_query($filter, 'flags_');

        return $url;
    }

    /**
     * 
     * @param array $excluded
     * @return string
     */
    public function getCurrentParamsAsHiddenInputFields($excluded = [])
    {
        $html = "";

        if(!$this->queryParams) {
            return $html;
        }
        if ($this->queryParams["f"]) {
            foreach ($this->queryParams["f"] as $key => $values) {
                foreach ($values as $value) {
                    $html .= '<input type="hidden" name="f[' . $key . '][]" value="' . $value . '" />';
                }
            }
        }

        unset($this->queryParams["f"]);
        
        foreach ($this->queryParams as $key => $value) {
            if (count($excluded) > 0 && in_array($key, $excluded) === true) {
                continue;
            }

            $html .= '<input type="hidden" name="' . $key . '" value="' . $value . '" />';
        }

        return $html;
    }

    /**
     * 
     * @param string $filterKey
     * @param mixed $filterValue
     * @return boolean
     */
    public function isActiveFilterValue($filterKey, $filterValue)
    {
        $filters = $this->queryParams["f"];

        if (isset($filters[$filterKey]) === false) {
            return false;
        }


        foreach ($filters[$filterKey] as $checkFilterValue) {

            if ($checkFilterValue === $filterValue) {
                return true;
            }
        }

        return false;
    }

    /**
     * 
     * @param string $key
     * @return string
     */
    public function getCurrentRequestParamIfSet($key)
    {
        if (isset($this->queryParams[$key]) === true) {
            return $this->queryParams[$key];
        }

        return "";
    }
}
