<?php

namespace TrueRomanceBundle\Library\Tools;

class Url {

    /**
     * 
     * @param string $str
     * @return string
     */
    public static function slugify(string $str): string {
        $str = trim($str);

        $search = array('Ș', 'Ț', 'ş', 'ţ', 'Ş', 'Ţ', 'ș', 'ț', 'î', 'â', 'ă', 'Î', 'Â', 'Ă', 'ë', 'Ë', '®', 'ä', 'Ä,', 'ö', 'Ö', 'ü', 'Ü', 'ß');
        $replace = array('s', 't', 's', 't', 's', 't', 's', 't', 'i', 'a', 'a', 'i', 'a', 'a', 'e', 'E', '', 'ae', 'Ae,', 'oe', 'Oe', 'ue', 'Ue', 'ss');

        $str = str_ireplace($search, $replace, strtolower(trim($str)));
        $str = preg_replace('/[^\w\d\-\ ]/', '-', $str);
        $str = str_replace(' ', '-', $str);

        return preg_replace('/\-{2,}/', '-', $str);
    }

}
