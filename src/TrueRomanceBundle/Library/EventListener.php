<?php

namespace TrueRomanceBundle\Library;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Model\DataObject\AssetFile;
use Pimcore\Model\DataObject\AssetImage;
use Pimcore\Model\DataObject\AssetVideo;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Pimcore\Model\DataObject\Article;
use Pimcore\Model\DataObject\ArticleAssets;
use Pimcore\Model\DataObject\CatalogAssets;
use Pimcore\Model\DataObject\Catalog;
use Pimcore\Event\Model\DataObjectEvent;
use Symfony\Component\VarDumper\VarDumper;

class EventListener extends \Symfony\Component\EventDispatcher\EventDispatcher
{
    protected $container;

    protected static $loggingType = 'event';

    public function __construct(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function onPreUpdate(ElementEventInterface $e)
    {

        if (!$e instanceof DataObjectEvent) {
            return;
        }

        $targetObject = $e->getObject();

        if($targetObject instanceof Article || $targetObject instanceof Catalog ||
            $targetObject instanceof AssetFile || $targetObject instanceof AssetImage ||
            $targetObject instanceof AssetVideo
        ) {
            $indexService = new Index\Task($this->container);

            $indexService->updateSingleAsset($targetObject);
        }

        if($targetObject instanceof Catalog) {
            $indexService = new Index\Task($this->container);

            $indexService->updateCatalogAssets($targetObject);

            $indexService->updateCatalogEntry($targetObject);
        }
    }

    public function onPreDelete(ElementEventInterface $e)
    {
        if (!$e instanceof DataObjectEvent) {
            return;
        }

        $targetObject = $e->getObject();

        $indexService = new Index\Task($this->container);

        $indexService->deleteEntriesFromDatabaseByObjectId($targetObject->getId());
    }

    public function onAssetPreDelete(ElementEventInterface $e)
    {
        if (!$e instanceof DataObjectEvent) {
            return;
        }

        $targetObject = $e->getObject();

        Index\Entry\Facet::deleteEntriesFromTableByObjectId($targetObject->getId());
        Index\Entry\Index::deleteEntryByObjectId($targetObject->getId());
    }
}
