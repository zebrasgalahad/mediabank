<?php

namespace TrueRomanceBundle\Library;

class FolderHelper
{
    private static $availableFolderTypes = [
        'object' => \Pimcore\Model\DataObject\Folder::class,
        'asset' => \Pimcore\Model\Asset\Folder::class,
        'document' => \Pimcore\Model\Document\Folder::class,
    ];

    /**
     * Create new folder or get one if already exists
     * $folderType ['document', 'object', 'asset']
     *
     * @param string $folderPath
     * @param string $folderType
     */
    public static function getOrCreateFolder(string $folderPath, string $folderType)
    {
        if (!array_key_exists($folderType, self::$availableFolderTypes)) {
            throw new \Exception('Invalid $folderType, must be one of [object, asset, document]');
        }

        $class = self::$availableFolderTypes[$folderType];

        $folder = $class::getByPath($folderPath);

        //If folder exists return it
        if ($folder) {
            return $folder;
        }

        $folderNames = explode('/', $folderPath);

        $path = '';
        $parentFolder = $class::getById(1);

        foreach ($folderNames as $folderName) {
            $path .= '/' . $folderName;

            $folder = $class::getByPath($path);

            if (!$folder) {
                $folder = new $class();
                $folder->setParentId($parentFolder->getId());

                if (method_exists($folder, 'setFilename')) {
                    $folder->setFilename($folderName);
                }

                if (method_exists($folder, 'setKey')) {
                    $folder->setKey($folderName);
                }

                $folder->save();
            }

            $parentFolder = $folder;
        }

        return $folder;
    }
}
