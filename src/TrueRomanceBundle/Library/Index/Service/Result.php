<?php

namespace TrueRomanceBundle\Library\Index\Service;

class Result
{
    public $facets;
    public $catalogs;
    public $assets;
    public $collections;
    public $newestAssets;
    public $newestCollections;
    public $specialAlbums;
    public $viewHelper;
    public $queryParams;
    public $mainFacets;
    public $categories;
    public $meta;
    public $albums;

    public function getCategories()
    {
        return $this->categories;
    }

    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    public function getMeta()
    {
        return $this->meta;
    }

    public function setMeta($meta)
    {
        $this->meta = $meta;
    }

    public function getMainFacets(): array
    {
        return $this->mainFacets;
    }

    public function setMainFacets($mainFacets)
    {
        $this->mainFacets = $mainFacets;
    }

    public function getQueryParams()
    {
        return $this->queryParams;
    }

    public function setQueryParams($queryParams)
    {
        $this->queryParams = $queryParams;
    }

    public function getNewestAssets()
    {
        return $this->newestAssets;
    }

    public function getNewestCollections()
    {
        return $this->newestCollections;
    }

    public function getSpecialAlbums()
    {
        return $this->specialAlbums;
    }

    public function setNewestAssets($newestAssets)
    {
        $this->newestAssets = $newestAssets;
    }

    public function setNewestCollections($newestCollections)
    {
        $this->newestCollections = $newestCollections;
    }

    public function setSpecialAlbums($specialAlbums)
    {
        $this->specialAlbums = $specialAlbums;
    }

    public function __construct(array $queryParams)
    {
        $this->queryParams = $queryParams;
    }

    public function getFacets(): array
    {
        return $this->facets;
    }

    public function getCatalogs(): array
    {
        return $this->catalogs;
    }

    public function getAssets(): array
    {
        return $this->assets;
    }

    public function getCollections(): array
    {
        return $this->collections;
    }

    public function setFacets($facets)
    {
        $this->facets = $facets;
    }

    public function setCatalogs($catalogs)
    {
        $this->catalogs = $catalogs;
    }

    public function setAssets($assets)
    {
        $this->assets = $assets;
    }

    public function setCollections($collections)
    {
        $this->collections = $collections;
    }

    public function getViewHelper(): \TrueRomanceBundle\Library\Tools\ViewHelper
    {
        return $this->viewHelper;
    }
    
    function getAlbums()
    {
        return $this->albums;
    }

    function setAlbums($albums)
    {
        $this->albums = $albums;
    }

}
