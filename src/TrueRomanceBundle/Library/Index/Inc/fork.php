<?php

require_once __DIR__ . '/../../../../../script/executables/startup.php';

ini_set("memory_limit", "14000M");

$indexService = new \TrueRomanceBundle\Library\Index\Task($kernel->getContainer());

$objectId = $argv[1];

$skipImages = $argv[2];

$indexService->skipImageCreating = $skipImages == 1 ? true : false;

$indexService->updateSingleAsset($argv[1]);