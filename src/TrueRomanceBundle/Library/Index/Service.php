<?php

namespace TrueRomanceBundle\Library\Index;

use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;
use Symfony\Component\DependencyInjection\Container;
use TrueRomanceBundle\Library\Object\Config;
use TrueRomanceBundle\Library\Tools\Url;

class Service
{
    public $queryParams;
    private $facetTable = "true_romance_asset_facett";
    private $indexTable = "true_romance_asset_index";

    private $viewHelper;

    private $container;

    /**
     *
     * @var \TrueRomanceBundle\Library\User\Permissions
     */
    private $permissions;

    /**
     *
     * @var \Pimcore\Translation\Translator
     */
    private $translator;

    /**
     *
     * @var Config
     */
    private $config;

    /**
     *
     * @var array
     */
    private $allowedCountries;

    /**
     * @var string
     */
    private $locale;

    const MODE_CATALOG_OVERVIEW = 'catalog_overview';
    const MODE_CATALOG_SEARCH = 'catalog_search';
    const MODE_CATALOG_NO_SEACRH = 'catalog_no_search';

    public function __construct(array $queryParams, Container $container)
    {
        $this->queryParams = $queryParams;

        $this->container = $container;

        $this->translator = $container->get("translator");

        $this->permissions = $container->get("trueromance.user.permissions");

        $this->config = $this->container->get("trueromance.object.config");

        $this->viewHelper = new \TrueRomanceBundle\Library\Tools\ViewHelper($this->queryParams);

        $this->allowedCountries = $this->getAllowedUserCountries();

        $this->locale = $queryParams['locale'];
    }

    private function getAllowedUserCountries()
    {
        return $this->permissions->getAllowedCountries();
    }

    private function addAllowedUserCountriesQuery($originQuery = null): string
    {
        $countries = $this->permissions->getAllowedCountries();

        if(empty($countries)) {
            throw new \Exception("User must have at least one country set in permissions to see assets!");
        }

        $subs = [];
        foreach ($countries as $country) {
            $subs[] = "oq.countries LIKE '%,$country,%'";
        }
        $query .= " AND (";
        $query .= implode(" OR ", $subs);
        $query .= ") ";

        if($query) {
            return str_replace("{{COUNTRIES_SUB}}", $query, $originQuery);
        }

        return $query;
    }

    public function get(): Service\Result
    {
        $this->normalizeQueryParams();

        $this->queryParams['limit'] = $this->container->get("trueromance.object.config")->get("Misc_List_Assets_Count_List", $this->queryParams["locale"]);

        $result = new Service\Result($this->queryParams);

        $result->viewHelper = $this->viewHelper;

        $result->setNewestAssets($this->getNewestAssets());

        $result->setCollections($this->getCollections());

        $result->setCatalogs($this->getCatalogCounts());

        $result->setAssets($this->getFilteredAssets());

        $filteredFacets = $this->getFilteredFacets();

        $result->setMainFacets($this->getTopLevelFacets($filteredFacets));

        $result->setMeta($this->createMetaData($result->getCatalogs()));

        $result->setCategories($filteredFacets["category"]);

        $userId = $this->container->get("trueromance.user.permissions")->getUser()->getId();

        $albumData = $this->container->get("trueromance.album.service")->homePageAlbumData($userId);

        $result->setAlbums($albumData);

        unset($filteredFacets["asset_type"]);

        unset($filteredFacets["catalog"]);

        unset($filteredFacets["category"]);

        $result->setFacets($filteredFacets);

        $this->viewHelper->queryParams = $this->queryParams;

        return $result;
    }

    protected function normalizeQueryParams()
    {
        if($this->queryParams["top-facets"]) {
            $explodedTopFacets = explode(":", $this->queryParams["top-facets"]);

            if(count($explodedTopFacets) === 0) {
                return;
            }

            if(!$this->queryParams["f"]) {
                $this->queryParams["f"] = [];
            }

            if(!$this->queryParams["f"][$explodedTopFacets[0]]) {
                $this->queryParams["f"][$explodedTopFacets[0]] = [];
            }

            $this->queryParams["f"][$explodedTopFacets[0]][] = $explodedTopFacets[1];
        }
    }

    protected function createMetaData($catalogCounts): array
    {
        $activeCatalog = null;
        foreach ($catalogCounts as $catalog) {
            if($this->viewHelper->isActiveFilterValue("catalog", $catalog["object_id"]) === true) {
                $activeCatalog = $catalog;
            }
        }

        if(!$activeCatalog) {
            foreach ($catalogCounts as $catalog) {
                if($catalog["active"] === true) {
                    $activeCatalog = $catalog;
                }
            }
        }

        $limit = $this->queryParams["limit"] ? $this->queryParams["limit"] : 10;

        return [
            "count" => $activeCatalog["count"],
            "pages" => (int) ceil($activeCatalog["count"] / $limit),
        ];
    }

    public function getAssetDetailNavigation($assetId, $filters)
    {
        Database::get()->query("SET @i=0;");

        $query = "
SELECT
   second_result.*
FROM
   (
      SELECT
         @i:=@i+1 AS i,
         result.*
      FROM
         (
			SELECT DISTINCT
			   oq.* {{SEARCH_SUB_QUERY_1}}
			FROM
			   {$this->indexTable} oq
			WHERE
			   oq.locale = ?
               AND oq.asset_type != 'Catalog'
               {{CATALOGS_QUERY}}
               {{COUNTRIES_SUB}}
               {{SUB_QUERY}}
               {{SEARCH_SUB_QUERY_2}}
               {{SEARCH_SUB_QUERY_3}}
         )
         as result
   )
   as second_result
###SUB_CONDITION### ;";

        if($filters["f"]) {
            $query = $this->createSubQuery($filters["f"], $query);
        } else {
            $query = $this->createSubQuery(null, $query);
        }

        $query = $this->addAllowedUserCountriesQuery($query);

        $query = $this->addAllowedCatalogsQuery($query);

        $query = $this->createTextSearchSubQuery($query);

        $firstQuery = str_replace("###SUB_CONDITION###", "WHERE second_result.object_id = ?", $query);

        $firstQuery = $this->addAllowedCatalogsQuery($firstQuery);

        $params = [$filters["locale"], $assetId];

        $targetData = Database::get()->fetchRow($firstQuery, $params);

        $autoIncrementId = $targetData["i"];

        $whereInSelectionData = [];
        if($autoIncrementId > 1) {
            $whereInSelectionData = [
                $autoIncrementId - 1,
                $autoIncrementId,
                $autoIncrementId + 1
            ];
        } else {
            $whereInSelectionData = [
                $autoIncrementId,
                $autoIncrementId + 1
            ];
        }

        $whereInSelectionString = implode(",", $whereInSelectionData);

        Database::get()->query("SET @i=0;");

        $executeQuery = str_replace("###SUB_CONDITION###", " WHERE second_result.i IN ($whereInSelectionString) ", $query);

        $filteredList = Database::get()->fetchAll($executeQuery, [$filters["locale"]]);

        $links = [];
        foreach ($filteredList as $currentIndex => $filteredEntry) {
            if ((int) $filteredEntry["object_id"] === (int) $assetId) {
                if (isset($filteredList[$currentIndex - 1]) === true) {
                    $links["prev"] = $filteredList[$currentIndex - 1]["object_id"];
                } else {
                    $links["prev"] = false;
                }

                if (isset($filteredList[$currentIndex + 1]) === true) {
                    $links["next"] = $filteredList[$currentIndex + 1]["object_id"];
                } else {
                    $links["next"] = false;
                }

                break;
            }
        }

        return $links;
    }

    protected function getCollections()
    {
        $limit = $this->config->get("Misc_Newest_Collections_Count_List", $this->queryParams["locale"]);

        $query = "SELECT * {{SEARCH_SUB_QUERY_1}} FROM {$this->indexTable} as oq "
        . "WHERE oq.asset_type = 'collection' AND oq.locale = ? {{CATALOGS_QUERY}} {{COUNTRIES_SUB}} {{SUB_QUERY}} {{SEARCH_SUB_QUERY_2}} {{SEARCH_SUB_QUERY_3}}"
        . "LIMIT 0, $limit";

        $query = $this->addAllowedCatalogsQuery($query);

        $query = $this->addAllowedUserCountriesQuery($query);

        $query = $this->createSubQuery($this->queryParams["f"], $query, ["top_level_facets"]);

        $query = $this->createTextSearchSubQuery($query);

        $collections = Database::get()->fetchAll($query, [$this->queryParams["locale"]]);

        return $collections;
    }

    protected function getNewestAssets()
    {
        $limit = $this->config->get("Misc_Newest_Assets_Count_List", $this->queryParams["locale"]);

        $query = "
            SELECT * {{SEARCH_SUB_QUERY_1}}
            FROM {$this->indexTable} as oq
            WHERE oq.product_status_new_product IS NOT NULL
            AND oq.locale = ?
            AND oq.asset_type != 'Catalog' {{CATALOGS_QUERY}} {{COUNTRIES_SUB}} {{SUB_QUERY}} {{SEARCH_SUB_QUERY_2}} {{SEARCH_SUB_QUERY_3}}
            ORDER BY RAND(), product_status_new_product DESC
            LIMIT $limit
        ";

        $query = $this->addAllowedCatalogsQuery($query);

        $query = $this->createSubQuery($this->queryParams["f"], $query, ["top_level_facets"]);

        $query = $this->createTextSearchSubQuery($query);

        $query = $this->addAllowedUserCountriesQuery($query);

        $assets = Database::get()->fetchAll($query, [$this->queryParams["locale"]]);

        $documentLink = $this->container->get("trueromance.object.config")->get("SITE_ROOT_PAGE", $this->queryParams["locale"])->getFullPath();

        $singleAsset["link"] = "";
        foreach ($assets as &$singleAsset) {
            if(!$singleAsset["description_short"]) {
                continue;
            }
            $singleAsset["link"] = $documentLink . "/" . Url::slugify($singleAsset["description_short"]);
        }

        return $assets;
    }

    protected function getTopLevelFacets($filteredFacets)
    {
        $topLevelFacets = $filteredFacets["asset_type"];

        if(!$topLevelFacets) {
            $topLevelFacets = [];
        }

        if($topLevelFacets) {
            foreach ($topLevelFacets as &$topLevelFacet) {
                $topLevelFacet["facet_title"] = $this->translator->trans($topLevelFacet["facet_title"]);
                $topLevelFacet["facet_value_display"] = $this->translator->trans($topLevelFacet["facet_value_display"]);
            }
        }

        ## only show filter if there is something to filter!
        if($filteredFacets["product_status_new_product"]) {
            array_unshift($topLevelFacets, [
                "facet_id" => "product_status_new_product",
                "facet_value" => "all",
                "facet_value_display" => $this->translator->trans("facet.value.new_status.all"),
                "facet_title" => $this->translator->trans("facet.title.new_status.all"),
                "count" => "5"
            ]);
        }

        array_unshift($topLevelFacets, [
            "facet_id" => "top_level_facets",
            "facet_value" => "",
            "facet_value_display" => $this->translator->trans("facet.value.top_level_facets.all"),
            "facet_title" => $this->translator->trans("facet.title.top_level_facets.all"),
            "count" => "5"
        ]);

        return $topLevelFacets;
    }

    public function isFiltered(): bool
    {
        if(!$this->queryParams["f"]) {
            return false;
        }

        $counter = 0;
        foreach ($this->queryParams["f"] as $key => $values) {
            if($key !== "catalog") {
                ++$counter;
            }
        }

        if($this->queryParams["search"]) {
            ++$counter;
        }

        return $counter !== 0;
    }

    public function isFilteredByCatalog()
    {
        return $this->queryParams["f"] && $this->queryParams["f"]["catalog"];
    }

    protected function getFilteredAssets()
    {
        $page = $this->queryParams["page"] ? $this->queryParams["page"] : 1;

        $limit = $this->queryParams["limit"] ? $this->queryParams["limit"] : 10;

        if ($page === 1) {
            $offset = 0;
        } else {
            $offset = ($page - 1) * $limit;
        }

        $query = "SELECT * {{SEARCH_SUB_QUERY_1}} FROM {$this->indexTable} as oq WHERE oq.locale = ? AND asset_type != 'Catalog' {{CATALOGS_QUERY}} {{COUNTRIES_SUB}} {{SUB_QUERY}} {{SEARCH_SUB_QUERY_2}} {{SEARCH_SUB_QUERY_3}} LIMIT $offset, $limit";

        $query = $this->addAllowedCatalogsQuery($query);

        $query = $this->addAllowedUserCountriesQuery($query);

        $query = $this->createSubQuery($this->queryParams["f"], $query, ["top_level_facets"]);

        $query = $this->createTextSearchSubQuery($query);

        $result = Database::getDatabaseObject()->fetchAll($query, [$this->queryParams["locale"]]);

        return $result;
    }

    protected function getFilteredFacets()
    {
        $filter = $this->queryParams["f"] ? $this->queryParams["f"] : null;

        if($filter) {
           $filteredFacets = $this->getFacetData($filter);

           $unfilteredFacets = $this->getFacetData();

           $combinedFilters = $this->processFacets($unfilteredFacets, $filteredFacets);
        } else {
            $combinedFilters = $this->getFacetData();
        }

        $combinedFilters["category"] = $this->buildCategoryTree();

        return $combinedFilters;
    }

    protected function addAllowedCatalogsQuery($query)
    {
        $implodedCatalogIds = $this->permissions->getImplodedAllowedCatalogIds();

        $replacement = " AND catalog_id IN ($implodedCatalogIds) ";

        $query = str_replace("{{CATALOGS_QUERY}}", $replacement, $query);

        return $query;
    }

    protected function getCatalogCounts()
    {
        $filter = $this->queryParams["f"] ? $this->queryParams["f"] : null;

        $implodedCatalogIds = $this->permissions->getImplodedAllowedCatalogIds();

        $queryCatalogs = "SELECT * FROM {$this->indexTable} as oq WHERE asset_type = 'Catalog' AND locale = '{$this->locale}' {{CATALOGS_QUERY}}";

        $queryCatalogs = $this->addAllowedUserCountriesQuery($queryCatalogs);

        $queryCatalogs = $this->addAllowedCatalogsQuery($queryCatalogs);

        $catalogs = Database::getDatabaseObject()->fetchAll($queryCatalogs);

        $catalogsResult = [];
        $counter = [
            "catalog" => null,
            "count" => 0
        ];

        foreach ($catalogs as $catalogIndex => $catalog) {
            $rawQueryCatalogCount = "SELECT COUNT(*) as count {{SEARCH_SUB_QUERY_1}} FROM {$this->indexTable} as oq where oq.locale = ? AND asset_type != 'Catalog' {{COUNTRIES_SUB}} "
            . " {{SUB_QUERY}} AND oq.catalog_id = ? {{SEARCH_SUB_QUERY_2}} {{SEARCH_SUB_QUERY_3}}";

            $rawQueryCatalogCount = $this->addAllowedUserCountriesQuery($rawQueryCatalogCount);

            $queryCatalogCount = $this->createSubQuery($filter, $rawQueryCatalogCount, ["top_level_facets", "catalog"]);

            $queryCatalogCount = $this->createTextSearchSubQuery($queryCatalogCount);

            $count = Database::get()->fetchOne($queryCatalogCount, [$this->queryParams["locale"], $catalog["object_id"]]);

            if($count > $counter["count"]) {
                $counter = [
                    "catalog" => $catalog["catalog"],
                    "count" => $count,
                    "index" => $catalogIndex,
                    "object_id" => $catalog["object_id"]
                ];
            }

            $singleCatalogResult = array_merge($catalog, [
                "title" => $catalog["catalog"],
                "count" => $count,
                "link" => $this->viewHelper->createLink(["catalog" => [$catalog["catalog"]]], true, ["catalog"]),
                "link_data" => $this->viewHelper->createLink(["catalog" => [$catalog["catalog"]]], true, ["catalog"], true),
                "active" => false,
            ]);

            $catalogsResult[] = $singleCatalogResult;
        }

        if($this->isFilteredByCatalog() === false) {
            if(!$this->queryParams["f"]) {
                $this->queryParams["f"] = [];
            }

            if($counter["catalog"] === null) {
                $this->queryParams["f"]["catalog"] = [$catalogs["0"]["object_id"]];
            } else {
                $this->queryParams["f"]["catalog"] = [$counter["object_id"]];
            }

            $catalogsResult[$counter["index"]]["active"] = true;
        } else {
            foreach ($catalogsResult as &$catalogResult) {
                if($catalogResult["object_id"] == $this->queryParams["f"]["catalog"][0]) {
                    $catalogResult["active"] = true;
                } else {
                    $catalogResult["active"] = false;
                }
            }
        }

        return $catalogsResult;
    }

    /**
     *
     * @param array $allFacets
     * @param array $filteredFacets
     * @return array
     */
    protected function processFacets($allFacets, $filteredFacets) {
        $currentFilters = $this->queryParams["f"] ? $this->queryParams["f"] : [];

        unset($currentFilters["catalog"]);

        foreach ($allFacets as $facetKey => &$facetValues) {
            $sectionChecked = false;
            foreach ($facetValues as &$facetValue) {
                $facetValue["facet_value_display"] = $this->translator->trans($facetValue["facet_value_display"]);
                $facetValue["facet_title"] = $this->translator->trans($facetValue["facet_title"]);

                if(isset($currentFilters[$facetKey]) && in_array($facetValue["facet_value"], $currentFilters[$facetKey])) {
                    $facetValue["checked"] = true;

                    $sectionChecked = true;
                } else {
                    $facetValue["checked"] = false;
                }

                if(isset($filteredFacets[$facetKey]) === true) {
                    $facetValue["available"] = false;

                    foreach ($filteredFacets[$facetKey] as $filteredFacetsValue) {
                        if($filteredFacetsValue["facet_value"] == $facetValue["facet_value"]) {
                            $facetValue["available"] = true;
                        }
                    }
                } else {
                    $facetValue["available"] = false;
                }
            }

            foreach ($facetValues as &$facetValue) {
                $facetValue["section"] = $sectionChecked;
            }
        }

        return $allFacets;
    }

    private function getPlainFacetData($filter = null)
    {
        if (!$filter) {
            $query = "SELECT pf.facet_id, pf.facet_value, pf.facet_value_display, pf.facet_title, count(*) AS count
FROM {$this->facetTable} pf, {$this->indexTable} oq
WHERE pf.facet_value != ''
AND pf.object_id = oq.object_id
AND pf.locale = ?
AND oq.asset_type != 'Catalog'
AND oq.locale = ?
{{CATALOGS_QUERY}}
{{COUNTRIES_SUB}}
{{SUB_QUERY}}
GROUP BY pf.facet_value, pf.facet_id, pf.facet_title
ORDER BY pf.facet_id, pf.facet_value ASC";
        } else {
            $query = "SELECT pf.facet_id, pf.facet_value, pf.facet_value_display, pf.facet_title, count(*) AS count {{SEARCH_SUB_QUERY_1}}
FROM {$this->indexTable} as oq, {$this->facetTable} as pf
WHERE oq.object_id = pf.object_id
AND pf.facet_value != ''
AND pf.locale = ?
AND oq.asset_type != 'Catalog'
AND oq.locale = ?
{{CATALOGS_QUERY}}
{{COUNTRIES_SUB}}
{{SUB_QUERY}}
{{SEARCH_SUB_QUERY_2}}
{{SEARCH_SUB_QUERY_3}}
GROUP BY pf.facet_value, pf.facet_id, pf.facet_title
ORDER BY pf.facet_id, pf.facet_value ASC";
        }

        $query = $this->addAllowedCatalogsQuery($query);

        $query = $this->addAllowedUserCountriesQuery($query);

        $query = $this->createSubQuery($filter, $query, ["top_level_facets"]);

        $query = $this->createTextSearchSubQuery($query);

        $result = Database::getDatabaseObject()->fetchAll($query, [$this->queryParams["locale"], $this->queryParams["locale"]]);

        return $result;
    }

    protected function createTextSearchSubQuery(string $query): string
    {
        $replacements = [
            "{{SEARCH_SUB_QUERY_1}}" => "",
            "{{SEARCH_SUB_QUERY_2}}" => "",
            "{{SEARCH_SUB_QUERY_3}}" => ""
        ];

        if(!$this->queryParams["search"]) {
            return str_replace(array_keys($replacements), array_values($replacements), $query);
        }

        $searchTerm = $this->queryParams["search"];

        $rawExplodedSearchTerms = explode(" ", $searchTerm);

        $explodedSearchTerms = explode(" ", $searchTerm);

        $explodedSearchTerms = array_map(function($string) {
            return '+"' . $string . '"';
        }, $explodedSearchTerms);


        $searchTerm = "'" . implode(" ", $explodedSearchTerms) . "'";

        if(count($rawExplodedSearchTerms) > 0)  {
            $likeQuery = " ( ";
            foreach ($rawExplodedSearchTerms as $rawTerm) {
                $likeQuery .= "  oq.search_text LIKE '%$rawTerm%' OR";
            }

            $likeQuery = substr($likeQuery, 0, -2);

            $likeQuery .= " ) ";
        } else {
            $likeQuery = " ";
        }

        $replacements = [
            "{{SEARCH_SUB_QUERY_1}}" => ", MATCH (oq.description_short, oq.search_text) AGAINST ($searchTerm) AS relevance,
                            MATCH (oq.description_short) AGAINST ($searchTerm) AS title_relevance",
            "{{SEARCH_SUB_QUERY_2}}" => "AND ($likeQuery OR MATCH (oq.description_short, oq.search_text) AGAINST ($searchTerm IN BOOLEAN MODE) )",
            "{{SEARCH_SUB_QUERY_3}}" => ""
         ];

        $query = str_replace(array_keys($replacements), array_values($replacements), $query);

        return $query;
    }

    /**
     *
     * @param array $filters
     * @param string $baseQuery
     * @return string
     */
    protected function createSubQuery($filters = null, $baseQuery = null, $ignored = []) {
        $subQuery = "";
        if ($filters) {
            $resultFilters = [];

            $tableAlias = "oq";
            foreach ($filters as $filterKey => $filterValues) {
                if(in_array($filterKey, $ignored) === true) {
                    continue;
                }

                $resultSubFilter = [];
                foreach ($filterValues as $filterValue) {
                    if($filterKey === "catalog") {
                        $filterKey = "catalog_id";
                    }

                    if($filterKey === "product_status_new_product" && $filterValue === "all") {
                        $subFilter = "$tableAlias.$filterKey > 0";
                    } else {
                        $subFilter = "$tableAlias.$filterKey = '$filterValue'";
                    }

                    $resultSubFilter[] = $subFilter;
                }

                $subQuery .= " AND (" . implode(" OR ", $resultSubFilter) . ")";
            }
        }

        if($baseQuery !== null) {
            $baseQuery = str_replace("{{SUB_QUERY}}", $subQuery, $baseQuery);

            return $baseQuery;
        }

        return $subQuery;
    }

    private function buildCategoryTree()
    {
        $params = [
            "catalog" => [$this->queryParams["f"]["catalog"][0]],
        ];

        $unfiltered = $this->getFilteredCategories($params);

        $filtered = $this->getFilteredCategories($this->queryParams["f"]);

        foreach ($unfiltered as $index => &$unfilteredCategory) {
            $unfilteredCategory["available"] = false;
            foreach ($filtered as $filteredCategory) {
                if($filteredCategory["category_id"] === $unfilteredCategory["category_id"]) {
                    $unfilteredCategory["available"] = true;
                }
            }
        }

        $categoryTree = $this->buildTree($unfiltered);

        $categories = [];
        if($categoryTree[array_keys($categoryTree)[0]]["sub"]) {
            $categories = array_values($categoryTree[array_keys($categoryTree)[0]]["sub"]);
        }

        return $categories;
    }

    private function buildTree(array &$elements, $parentId = 0) {

        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['category_id']);
                if ($children) {
                    $element['sub'] = array_values($children);
                }
                $branch[$element['category_id']] = $element;
            }
        }
        return $branch;
    }

    private function getFilteredCategories($filter)
    {
        $query = "SELECT
   af.*
   {{SEARCH_SUB_QUERY_1}}
FROM
   {$this->indexTable} as oq,
   {$this->facetTable} as af
WHERE
   oq.locale = '{$this->locale}'
   AND af.object_id = oq.object_id
   AND af.facet_id = 'category'
   AND oq.asset_type != 'Catalog'
   {{CATALOGS_QUERY}}
   {{COUNTRIES_SUB}}
   {{SUB_QUERY}}
   {{SEARCH_SUB_QUERY_2}}
   {{SEARCH_SUB_QUERY_3}}
GROUP BY
   af.category_id
ORDER BY
   af.facet_value_display";

        $query = $this->addAllowedUserCountriesQuery($query);

        $query = $this->addAllowedCatalogsQuery($query);

        $query = $this->createSubQuery($filter, $query, ["top_level_facets"]);

        $query = $this->createTextSearchSubQuery($query);

        $result = Database::getDatabaseObject()->fetchAll($query, [$this->queryParams["locale"]]);

        return $result;
    }

    private function buildCategoryTreeRecursive($data)
    {
        $result = [];
        foreach ($data as $element) {
            if ($element["parent_id"] == 0) {
                $element["sub"] = [];

                $result[] = $element;
            } else {
                $this->addCategoryRecursive($result, $element);
            }
        }

        return $result;
    }

    private function addCategoryRecursive(&$result, &$element)
    {
        foreach ($result as &$singleResult) {
            if ($singleResult["category_id"] == $element["parent_id"]) {
                $singleResult["sub"][] = $element;
            } else {
                if ($singleResult["sub"] && count($singleResult["sub"]) >= 1) {
                    $this->addCategoryRecursive($singleResult["sub"], $element);
                }
            }
        }
    }

    /**
     *
     * @param array $filter
     * @return array
     */
    private function getFacetData(array $filter = []): array
    {
        if(count($filter) > 0) {
            $rawFacetData = $this->getPlainFacetData($filter);
        } else {
            $rawFacetData = $this->getPlainFacetData(["catalog" => [$this->queryParams["f"]["catalog"][0]]]);
        }

        $resultFacetData = [];
        foreach ($rawFacetData as $facetData) {
            if (isset($resultFacetData[$facetData["facet_id"]]) === false) {
                $resultFacetData[$facetData["facet_id"]] = [];
            }

            $resultFacetData[$facetData["facet_id"]][] = $facetData;
        }

        return $resultFacetData;
    }
}
