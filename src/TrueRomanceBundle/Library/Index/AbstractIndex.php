<?php

namespace TrueRomanceBundle\Library\Index;

use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

class AbstractIndex
{
    
    /**
     * 
     * @param \Pimcore\Model\DataObject $object
     */
    public static function deleteFacetTableEntries($object) {
        self::deleteEntriesFromTableByObjectId($object->getId());
    }

    /**
     * 
     * @param int|string $objectId
     */
    protected static function deleteEntriesFromTableByObjectId($objectId) {
        $database = Database::getDatabaseObject();

        $table = static::getTable();

        $database->query("DELETE FROM {$table} WHERE object_id = ?", [$objectId]);
    }

    /**
     * 
     * @param int|string $objectId
     * @param int|string $facetId
     * @param string $facetTitle
     * @param string $facetName
     * @param string $facetValue
     * @param string $country
     * @param string $language
     */
    protected static function createFacetEntry($objectId, $facetId, $facetTitle, $facetName,
            $facetValue, $locale) {
        $databaseEntry = [
            "object_id" => $objectId,
            "facet_id" => $facetId,
            "facet_title" => $facetTitle,
            "facet_name" => $facetName,
            "facet_value" => $facetValue,
            "language" => $locale,
        ];
        
        Database::getDatabaseObject()->insert(static::getTable(), $databaseEntry);
    }

    /**
     * 
     * @param int|string $objectId
     */
    protected static function deleteSearchEntryByObjectId($objectId) {
        Database::getDatabaseObject()->query("DELETE FROM search_data WHERE object_id = ?",
                [$objectId]);
    }

    /**
     * 
     * @param array $searchData
     */
    protected static function insertSearchEntry($searchData) {
        Database::getDatabaseObject()->insert("search_data", $searchData);
    }
}
