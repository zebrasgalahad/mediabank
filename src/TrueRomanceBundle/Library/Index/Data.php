<?php

namespace TrueRomanceBundle\Library\Index;

use Pimcore\Model\DataObject\Catalog;
use Pimcore\Model\DataObject\CatalogCategory;
use Pimcore\Model\DataObject\ArticleAssets;
use Pimcore\Model\DataObject\Article;

class Data
{
    public $catalog;
    public $categories;
    public $article;
    public $assets;
    public $containerId;

    /**
     * @return mixed
     */
    public function getContainerId()
    {
        return $this->containerId;
    }

    /**
     * @param mixed $containerId
     */
    public function setContainerId($containerId): void
    {
        $this->containerId = $containerId;
    }

    /**
     *
     * @return Catalog
     */
    public function getCatalog()
    {
        return $this->catalog;
    }

    /**
     *
     * @return CatalogCategory[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getArticle()
    {
        return $this->article;
    }

    public function getAssets()
    {
        return $this->assets;
    }

    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;
    }

    public function setCategories($categories)
    {
        $this->categories = $categories;
    }

    public function setArticle($article)
    {
        $this->article = $article;
    }

    public function setAssets($assets)
    {
        $this->assets = $assets;
    }
}
