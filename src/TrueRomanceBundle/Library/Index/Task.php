<?php

namespace TrueRomanceBundle\Library\Index;

use Pimcore\Model\DataObject;
use Pimcore\Model\Element\ElementInterface;
use Pimcore\Model\DataObject\ArticleAssets;
use Pimcore\Model\DataObject\CatalogAssets;
use Pimcore\Model\DataObject\Article;
use Pimcore\Model\WebsiteSetting;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Index\Data;
use Symfony\Component\DependencyInjection\Container;
use Pimcore\Model\DataObject\Catalog;
use TrueRomanceBundle\Library\Index\Entry\Index;
use TrueRomanceBundle\Library\Tools\Cli\Thumbnail as CustomThumbnail;

class Task extends AbstractIndex
{
    public $locales = ["de_DE"];

    public $defaultLocale = "de_DE";

    public static $monthMapping = [
        "de_DE" => [
            1 => "Januar",
            2 => "Februar",
            3 => "März",
            4 => "April",
            5 => "Mai",
            6 => "Juni",
            7 => "Juli",
            8 => "August",
            9 => "September",
            10 => "Oktober",
            11 => "November",
            12 => "Dezember",
        ]
    ];

    /**
     * @var bool
     */
    public $skipImageCreating;

    private $container;
    private $baseFolderObject;
    private $replacements;

    private $allAllowedCountries;

    private $assetData;

    private $offset;

    private $limit;

    /**
     *
     * @var \TrueRomanceBundle\Library\Object\Config
     */
    private $config;
    private $mappingConfig;
    const FILTER_KEY_PRODUCT_NEW = "product_status_new_product";
    const FILTER_KEY_IMAGE_TYPE = "image_type";
    const FILTER_KEY_IMAGE_ORIENTATION = "image_orientation";
    const FILTER_KEY_FILE_TYPE = "file_type";
    const FILTER_KEY_VIDEO_TYPE = "video_type";
    const FILTER_KEY_ASSET_TYPE = "asset_type";
    const FILTER_KEY_SALES_DOC_TYPE = "salesdoc_type";
    const FILTER_KEY_CATALOG = "catalog";

    public function __construct(Container $container, $locales = null, $offset = null, $limit = null)
    {
        $skipImageCreation = WebsiteSetting::getByName('skip_image_creation') ? (bool) WebsiteSetting::getByName('skip_image_creation')->getData() : false;

        $this->skipImageCreating = $skipImageCreation;

        $this->container = $container;

        $this->config = $this->container->get("trueromance.object.config");

        if ($locales !== null) {
            $this->locales = $locales;
        }

        if($offset !== null && $limit !== null) {
            $this->offset = $offset;
            $this->limit = $limit;
        }

        $this->assembleReplacements();

        $this->allAllowedCountries = $this->getAllAllowedCountries();

        $this->assetData = $this->container->get("trueromance.asset.data");

        $this->locales = \Pimcore\Tool::getValidLanguages();
    }

    private function getAllAllowedCountries(): string
    {
        $assetFile = \Pimcore\Model\DataObject\Fieldcollection\Definition::getByKey("AssetFile");

        return $assetFile->getFieldDefinitions()['allowed_countries']->restrictTo;
    }

    /**
     * Creates replacements for object paths, e.g. "/collection" is replaced
     */
    private function assembleReplacements()
    {
        $catalogListing = new Catalog\Listing();

        $replacements = [];
        foreach ($catalogListing as $catalog) {
            foreach ($catalog->getChildren() as $catalogChild) {
                if (strpos($catalogChild->getKey(), "collection") === false) {
                    $replacements[] = "/" . $catalogChild->getKey();
                }
            }
        }

        $replacements[] = "/collection";

        $replacements[] = "/" . $this->getBaseFolder()->getKey();

        $this->replacements = $replacements;
    }

    private function replaceReplacementsFromPath(string $path): string
    {
        foreach ($this->replacements as $replacement) {
            $path = str_replace($replacement, "", $path);
        }

        return $path;
    }

    public function updateAll()
    {
        $baseFolder = $this->getBaseFolder();

        $catalogChilds = $this->getChildArticleAssetsObjectsFromObject($baseFolder, Catalog\Listing::class);

        foreach ($catalogChilds as $catalogChild) {
            foreach ($this->locales as $locale) {
                $this->updateCatalogEntry($catalogChild, $locale);
                $this->updateCatalogAssets($catalogChild, $locale);
            }
        }

        $key = $this->container->get("trueromance.object.config")->get("Catalogs_Base_Folder")->getKey();

        $sql = "SELECT o_id as id FROM objects WHERE o_className = 'AssetImage' OR o_className = 'AssetFile' OR o_className = 'AssetVideo' AND o_published = 1 AND o_path LIKE '%/$key/%'";

        if($this->limit !== null && $this->offset !== null) {
            $sql .= " LIMIT {$this->offset}, {$this->limit}";
        }

        $ids = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject()->fetchAll($sql);

        $childCount = count($ids);

        foreach ($ids as $index => $assetChildId) {
            $assetChildInstance = DataObject::getById($assetChildId['id']);

            $this->updateSingleAsset($assetChildInstance);

            $count = $index + 1;

            $message = "$count of $childCount article asset processed";
        }
    }

    public function createAll()
    {
        Entry\Facet::clearTable();

        Entry\Index::clearTable();

        $this->updateAll();
    }

    public function updateCatalogEntry(Catalog $catalogChild)
    {
        foreach ($this->locales as $locale) {
            $entryBaseData = [
                "object_id" => $catalogChild->getId(),
                "object_path" => $this->replaceReplacementsFromPath($catalogChild->getFullpath())
            ];

            $thumbnail = $this->createThumbnail($catalogChild->getLIST_PREVIEW_IMAGE(), "catalogListView");

            $entryData = array_merge([
                self::FILTER_KEY_ASSET_TYPE => "Catalog",
                self::FILTER_KEY_FILE_TYPE => "",
                self::FILTER_KEY_CATALOG => $catalogChild->getLIST_PREVIEW_NAME(),
                "name" => $catalogChild->getLIST_PREVIEW_NAME(),
                "preview_web_path" => $thumbnail ? $thumbnail->preview_web_path : '',
                "original_web_path" => $thumbnail ? $thumbnail->original_web_path : '',
                "search_text" => "",
                "locale" => $locale,
                "catalog_id" => $catalogChild->getId(),
            ], $entryBaseData);

            Entry\Index::insertEntry($entryData);
        }
    }

    private function createThumbnail($image, $type)
    {
        if(!$image) {
            return null;
        }

        $thumbnail = $image->getThumbnail($type, false);

        if(strpos($thumbnail->getPath(), "filetype-not-supported") !== false) {
            $thumbnail->preview_web_path = Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage();
            $thumbnail->original_web_path = "";
        } else {
            $thumbnail->preview_web_path = Config::ASSET_PREVIEW_PREFIX_PATH . $thumbnail->getPath();
            $thumbnail->original_web_path = Config::ASSET_PREVIEW_PREFIX_PATH . $image->getFullpath();
        }

        return $thumbnail;
    }

    public function _updateSingleAsset($objectId)
    {
        $execFile = __DIR__ . "/Inc/fork.php";

        $skipImages = $this->skipImageCreating === true ? 1 : 0;

        $command = "php $execFile $objectId $skipImages";

        exec($command, $output);

        foreach ($output as $singleLine) {
            echo $singleLine . PHP_EOL;
        }
    }

    public function deleteEntriesFromDatabaseByObjectId($objectid)
    {
        $object = \Pimcore\Model\DataObject::getById($objectid);

        if ($object instanceof Article) {
            $data = $this->transformToAssetArticles($object);

            foreach ($data as $assetArticle) {
                $this->deleteArticleAssetsAssets($assetArticle);
            }
        }

        if($object instanceof ArticleAssets) {
            $this->deleteArticleAssetsAssets($object);
        }

        if($object instanceof CatalogAssets) {
            $this->deleteArticleAssetsAssets($object);
        }

        if($object instanceof DataObject\AssetFile || $object instanceof DataObject\AssetImage ||
            $object instanceof DataObject\AssetVideo) {
            Entry\Facet::deleteEntriesFromTableByObjectId($object->getId());
            Entry\Index::deleteEntryByObjectId($object->getId());
        }
    }

    private function deleteArticleAssetsAssets($assetObject)
    {
        foreach (Config::$assetFileAttributes as $mappingData) {
            $getter = "get" . ucfirst($mappingData["field"]);

            if (method_exists($assetObject, $getter) === false) {
                    continue;
            }

            if ($assetObject->$getter() !== null) {
                foreach ($assetObject->$getter() as $fieldCollectionAsset) {
                    if($fieldCollectionAsset->getAsset_relation()) {
                        $assetId = $fieldCollectionAsset->getAsset_relation()->getId();

                        Entry\Facet::deleteEntriesFromTableByObjectId($assetId);
                        Entry\Index::deleteEntryByObjectId($assetId);
                    }
                }
            }
        }
    }

    public function updateSingleAsset($object)
    {
        if(!$object) {
            return;
        }

        $articleAssets = [];
        if (!$object instanceof DataObject\AssetFile && !$object instanceof DataObject\AssetImage && !$object instanceof DataObject\AssetVideo) {
            if($object instanceof Catalog) {
                $articleAssets = $this->getAssetObjectChildsFromObject($object, "assets");
            }

            if($object instanceof Article) {
                $articleAssets = $this->getAssetObjectChildsFromObject($object);
            }
        } else {
            $articleAssets[] = $object;
        }

        foreach ($articleAssets as $singleArticleAssets) {
            foreach ($this->locales as $locale) {
                $this->updateSingleAssetFile($singleArticleAssets, $locale);
            }
        }
    }

    public function updateCatalogAssets(Catalog $catalog)
    {
        $catalogAssets = $this->getAssetObjectChildsFromObject($catalog, "assets");

        foreach ($catalogAssets as $catalogAsset) {
            foreach ($this->locales as $locale) {
                $this->updateSingleAssetFile($catalogAsset, $locale);
            }
        }
    }

    public function updateSingleAssetFile($assetObject, $locale)
    {
        $data = $this->getDataFromAssetArticle($assetObject);

        Index::deleteEntryByAssetObjectIdAndLocale($assetObject->getId(), $locale);

        if ($assetObject->getItem_mediathek($locale) !== true) {
            return;
        }

        $filterData = $this->createFacetEntries($assetObject, $data, $locale);

        $filterData["additional_data"] = [
            "description_short" => $assetObject->getAsset_title($locale) ?? $assetObject->getAsset_title($this->defaultLocale),
            "ean_number" => $assetObject->getAsset_title($locale) ?? $assetObject->getAsset_title($this->defaultLocale),
            "article_number" => $assetObject->getSupplier_pid($locale) ?? $assetObject->getSupplier_pid($this->defaultLocale),
            "container_id" => $data->getContainerId(),
        ];

        foreach (["author", "copyright", "source"] as $key) {
            $getter = "get" . ucfirst($key);

            if (trim($assetObject->$getter()) !== "") {
                $filterData["additional_data"][$key] = $assetObject->$getter();
            }
        }

        if (trim($filterData["search_text"]) === "") {
            $filterData["search_text"] = $this->createAssetSearchText($assetObject, $locale);
        }

        $filterData["additional_data"] = json_encode($filterData["additional_data"]);

        if (!$filterData) {
            $filterData = [];
        }

        $this->createIndexEntry($assetObject, $data, $locale, $filterData);
    }

    private function getBaseFolder(): \Pimcore\Model\DataObject\Folder
    {
        if ($this->baseFolderObject) {
            return $this->baseFolderObject;
        }

        $this->baseFolderObject = $this->config->get("Catalogs_Base_Folder");

        return $this->baseFolderObject;
    }

    private function updateSingleAssetObject($articleAssets, string $locale)
    {
        $data = $this->getDataFromAssetArticle($articleAssets);

        if(!$data->getCatalog()) {
            return;
        }

        $indexContainerEntries = Entry\Index::getByContainerId($data->getAssets()->getId());

        Entry\Index::deleteByContainerId($data->getContainerId());

        if($data->getArticle() && $data->getArticle()->getPublished() === false) {
            return;
        }

        if($data->getAssets() && $data->getAssets()->getPublished() === false) {
            return;
        }

        $fileMappingData = Config::$assetFileAttributes;

        $assetObject = $data->getAssets();
        foreach ($fileMappingData as $mappingData) {
            $getter = "get" . ucfirst($mappingData["field"]);

            if (method_exists($assetObject, $getter) === false) {
                    continue;
            }

            if ($assetObject->$getter() !== null) {
                foreach ($assetObject->$getter() as $fieldCollectionAsset) {
                    if($fieldCollectionAsset->getItem_mediathek() !== true) {
                        $assetObject = $this->getAssetByType($fieldCollectionAsset, $locale);

                        Entry\Facet::deleteEntryByObjectIdAndLocale($assetObject->getId(), $locale);

                        continue;
                    }

                    $filterData = $this->createFacetEntries($fieldCollectionAsset, $data, $locale);

                    if(!$filterData) {
                        $filterData = [];
                    }

                    $this->createIndexEntry($fieldCollectionAsset, $data, $locale, $filterData, $indexContainerEntries);
                }
            }
        }
    }

    private function createIndexEntry($fieldCollectionAsset, Data $data, string $locale, array $filterData, $indexContainerEntries = array())
    {
        $asset = $this->getAssetByType($fieldCollectionAsset, $locale);

        if(!$asset) {
            return;
        }

        if($asset) {
            $categoryId = $this->createFacetCategories($asset, $data, $locale);
        }

        $replacement = $data->getArticle() === NULL ? "" : $data->getArticle();

        $objectPath = $this->replaceReplacementsFromPath($replacement);

        $catalogAssetPath = "/{$data->getCatalog()->getKey()}/assets";

        $catalogAssetData = $this->getCatalogAssetData($asset->getId(), $locale);

        $entry = [
            "object_id" => $asset->getId(),
            "article_ean" => $data->getArticle() !== NULL ? $data->getArticle()->getEAN() : "" ,
            "article_number" => $data->getArticle() !== NULL ? $data->getArticle()->getSUPPLIER_PID() : $catalogAssetData["article_number"],
            "object_path" => $data->getArticle() !== NULL ? str_replace("/" . $data->getArticle()->getKey(), "", $objectPath) : $catalogAssetPath,
            "original_web_path" => Config::ASSET_ORIGINAL_PREFIX_PATH . $asset->getPath() . $asset->getKey(),
            "absolute_file_path" => $asset->getFileSystemPath(),
            "description_long" => $data->getArticle() !== NULL ? $data->getArticle()->getDESCRIPTION_LONG() : $catalogAssetData["description_long"],
            "description_short" => $data->getArticle() !== NULL ? $data->getArticle()->getDESCRIPTION_SHORT() : $catalogAssetData["description_short"],
            "search_text" => $this->createAssetSearchText($fieldCollectionAsset, $locale),
            "locale" => $locale,
            "category" => $categoryId,
            "catalog_id" => $data->getCatalog()->getId(),
            "container_id" => $data->getContainerId(),
            "asset_object_id" => $fieldCollectionAsset->getId(),
        ];

        if(!$fieldCollectionAsset->getAllowed_countries()) {
            $entry["countries"] = "," . $this->allAllowedCountries . ",";
        } else {
            $entry["countries"] = "," . implode(",", $fieldCollectionAsset->getAllowed_countries()) . ",";
        }

        $entry = array_merge($entry, $filterData);

        if($this->skipImageCreating === false) {
            $entry["preview_web_width"] = 220;
            $entry["preview_web_height"] = 220;

            if($asset->getType() === "Bild" || $asset->getType() === "image") {
                $thumbnail = CustomThumbnail::create($asset, "listView", true);

                $entry["preview_web_path"] = $thumbnail->getWebpath();
                $entry["preview_web_width"] = $thumbnail->getWidth();
                $entry["preview_web_height"] = $thumbnail->getHeight();
            } else {
                $entry["preview_web_path"] = $this->assetData->getAssetTypeFileImage($asset);
            }
        } else {
            $objectEntry = $this->findArrayEntryByKeyValue("object_id", $entry["object_id"], $indexContainerEntries);

            if(is_array($objectEntry) === true) {
                $entry["preview_web_width"] = $objectEntry["preview_web_width"];
                $entry["preview_web_height"] = $objectEntry["preview_web_height"];
                $entry["preview_web_path"] = $objectEntry["preview_web_path"];
            }
        }

        $entry['product_status_new_product'] = $this->isProductNew($fieldCollectionAsset);

        Entry\Index::insertEntry($entry);
    }

    /**
     * @param mixed $fieldCollectionAsset
     * @return string
     */
    private function isProductNew($fieldCollectionAsset)
    {
        $articleInstance = $fieldCollectionAsset->getParent();
        /* @var $articleInstance \Pimcore\Model\DataObject\Article */

        $isNew = $articleInstance->getPRODUCT_STATUS_NEW_PRODUCT();

        return $isNew === true ? "1" : "0";
    }

    private function findArrayEntryByKeyValue($key, $value, $array)
    {
        foreach ($array as $arrayEntry) {
            if($arrayEntry[$key] == $value) {
                return $arrayEntry;
            }
        }

        return null;
    }

    /**
     * @param mixed $fieldCollectionAsset
     * @param string $locale
     * @return string
     */
    private function createAssetSearchText($fieldCollectionAsset, string $locale)
    {
        // catalog assets
        if (strpos($fieldCollectionAsset->getFullPath(), "assets") !== false) {
            $catalog = $fieldCollectionAsset->getParent()->getParent();
            /* @var $catalog Catalog */

            if ($catalog && $catalog instanceof Catalog) {
                return implode(" ", [
                    $catalog->getCATALOG_CATALOG_NAME($locale),
                    $catalog->getCATALOG_CATALOG_ID(),
                    $catalog->getSUPPLIER_SUPPLIER_NAME($locale),
                    $fieldCollectionAsset->getAsset_title($locale),
                    $fieldCollectionAsset->getSupplier_pid($locale),
                    $fieldCollectionAsset->getDESCRIPTION_SHORT($locale),
                    $fieldCollectionAsset->getDESCRIPTION_LONG($locale),
                    $fieldCollectionAsset->getDESCRIPTION_META($locale)
                ]);
            }
        }

        $articleInstance = $fieldCollectionAsset->getParent();
        /* @var $articleInstance \Pimcore\Model\DataObject\Article */

        if (!$articleInstance) {
            return implode(" ", [
                $fieldCollectionAsset->getAsset_title($locale),
                $fieldCollectionAsset->getSupplier_pid($locale),
                $fieldCollectionAsset->getDESCRIPTION_SHORT($locale),
                $fieldCollectionAsset->getDESCRIPTION_LONG($locale),
                $fieldCollectionAsset->getDESCRIPTION_META($locale)
            ]);
        }

        $searchText = implode(" ", [
            $articleInstance->getSUPPLIER_PID(),
            $articleInstance->getEAN(),
            $articleInstance->getDESCRIPTION_SHORT($locale),
            $articleInstance->getDescription_short2($locale),
            $articleInstance->getDESCRIPTION_LONG($locale),
            $fieldCollectionAsset->getAsset_title($locale),
            $fieldCollectionAsset->getSupplier_pid($locale),
            $fieldCollectionAsset->getDESCRIPTION_SHORT($locale),
            $fieldCollectionAsset->getDESCRIPTION_LONG($locale),
            $fieldCollectionAsset->getDESCRIPTION_META($locale)
        ]);

        return $searchText;
    }

    private function getAssetByType($assetObject, $locale)
    {
        $asset = null;

        if (method_exists($assetObject, "getVideo_type") === true) {
            if(!$assetObject->getAsset_relation($locale) && !$assetObject->getAsset_relation($this->defaultLocale)) {
                return null;
            }

            if($assetObject->getAsset_relation($locale)) {
                $asset = $assetObject->getAsset_relation($locale)->getData();
            }

            if(!$asset) {
                $asset = $assetObject->getAsset_relation($this->defaultLocale)->getData();
            }
        } else {
            if(!$assetObject->getAsset_relation($locale) && !$assetObject->getAsset_relation($this->defaultLocale)) {
                return null;
            }

            if($assetObject->getAsset_relation($locale)) {
                $asset = $assetObject->getAsset_relation($locale);
            }

            if(!$asset) {
                $asset = $assetObject->getAsset_relation($this->defaultLocale);
            }
        }

        return $asset;
    }

    private function createFacetCategories($asset, Data $data, $locale): int
    {
        $categories = array_reverse($data->getCategories());

        $catalog = $data->getCatalog();

        $data = [
            "object_id" => $asset->getId(),
            "category_id" => $catalog->getId(),
            "object_path" => "/" . $catalog->getId(),
            "additional" => "/" . $catalog->getKey(),
            "facet_id" => "category",
            "facet_title" => "facet.title.category",
            "facet_value" => $catalog->getKey(),
            "facet_value_display" => $catalog->getLIST_PREVIEW_NAME(),
            "locale" => $locale,
            "parent_id" => 0,
            "order" => 0
        ];

        Entry\Facet::insertEntry($data);

        $parentId = $catalog->getId();
        $pathIds = [$catalog->getId()];
        $pathKeys = [$catalog->getKey()];
        $resultCategoryID = null;
        foreach ($categories as $category) {
            $pathIds[] = $category->getId();

            $resultCategoryID = $category->getId();

            $pathKeys[] = $category->getKey();

            $data = [
                "object_id" => $asset->getId(),
                "category_id" => $category->getId(),
                "object_path" => "/" . implode("/", $pathIds),
                "additional" => "/" . implode("/", $pathKeys),
                "facet_id" => "category",
                "facet_title" => "facet.title.category",
                "facet_value" => $category->getKey(),
                "facet_value_display" => $category->getGROUP_NAME($locale),
                "locale" => $locale,
                "parent_id" => $parentId,
                "order" => $category->getGROUP_ORDER()
            ];

            $parentId = $category->getId();

            Entry\Facet::insertEntry($data);
        }

        return (int)$resultCategoryID;
    }

    private function createFacetEntries($fileAsset, Data $data, string $locale)
    {
        if (!$fileAsset->getAsset_relation()) {
            return;
        }

        $assetObject = $this->getAssetByType($fileAsset, $locale);

        if(!$assetObject) {
            return;
        }

        $objectId = $assetObject->getId();

        Entry\Facet::deleteEntryByObjectIdAndLocale($objectId, $locale);

        $replacement = $data->getArticle() === NULL ? "" : $data->getArticle();

        $objectName = $replacement !== "" ? $data->getArticle()->getKey() : "";

        $objectPath = $this->replaceReplacementsFromPath($replacement);

        $objectPath = str_replace("/" . $objectName, "", $objectPath);

        $returnData = [];

        $entryBaseData = [
            "object_id" => $objectId,
            "object_path" => $objectPath
        ];

        ## Catalog
        $entryData = array_merge([
            "facet_id" => self::FILTER_KEY_CATALOG,
            "facet_title" => "facet.title.catalog",
            "facet_value" => $data->getCatalog()->getLIST_PREVIEW_NAME(),
            "facet_value_display" => $data->getCatalog()->getLIST_PREVIEW_NAME(),
            "locale" => $locale,
                ], $entryBaseData);

        $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

        Entry\Facet::insertEntry($entryData);

        ## Asset type
        if($fileAsset->getAsset_type()) {
            $assetType = $fileAsset->getAsset_type();

            $entryData = array_merge([
                "facet_id" => self::FILTER_KEY_ASSET_TYPE,
                "facet_title" => "facet.title.asset_type",
                "facet_value" => $assetType,
                "facet_value_display" => "facet.value.asset_type.$assetType",
                "locale" => $locale,
                    ], $entryBaseData);

            $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

            Entry\Facet::insertEntry($entryData);
        }

        ## Sales Doc type
        if (method_exists($fileAsset, "getSalesdoc_type") === true) {
            if($fileAsset->getSalesdoc_type()) {
                $salesdocType = $fileAsset->getSalesdoc_type();

                $entryData = array_merge([
                    "facet_id" => self::FILTER_KEY_SALES_DOC_TYPE,
                    "facet_title" => "facet.title." . self::FILTER_KEY_SALES_DOC_TYPE,
                    "facet_value" => $salesdocType,
                    "facet_value_display" => "facet.value." . self::FILTER_KEY_SALES_DOC_TYPE . ".$salesdocType",
                    "locale" => $locale,
                        ], $entryBaseData);

                $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

                Entry\Facet::insertEntry($entryData);
            }
        }

        ### Product is new
        $articleStatusMonth = "";
        if ($data->getArticle() && $data->getArticle()->getPRODUCT_STATUS_NEW_PRODUCT() && $data->getArticle()->getNew_from()) {
            setlocale(LC_TIME, $locale);

            $monthAsNumber = $data->getArticle()->getNew_from()->format("n");

            $articleStatusMonth = self::$monthMapping["de_DE"][$monthAsNumber];

            $entryData = array_merge([
                "facet_id" => self::FILTER_KEY_PRODUCT_NEW,
                "facet_title" => "facet.title.new_status",
                "facet_value" => $data->getArticle()->getNew_from()->getTimestamp(),
                "facet_value_display" => $articleStatusMonth,
                "locale" => $locale,
                    ], $entryBaseData);

            Entry\Facet::insertEntry($entryData);

            $returnData[self::FILTER_KEY_PRODUCT_NEW . "_date"] = $data->getArticle()->getNew_from()->format("Y-m-d H:i:s");

            $returnData[$entryData["facet_id"]] = $entryData["facet_value"];
        }

        ### Image type
        if (method_exists($fileAsset, "getImage_type") === true) {
            if ($fileAsset->getImage_type()) {
                $imageType = $fileAsset->getImage_type();

                $entryData = array_merge([
                    "facet_id" => self::FILTER_KEY_IMAGE_TYPE,
                    "facet_title" => "facet.title.image_type",
                    "facet_value" => $imageType,
                    "facet_value_display" => "facet.value.image_type.$imageType",
                    "locale" => $locale,
                        ], $entryBaseData);

                $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

                Entry\Facet::insertEntry($entryData);
            }
        }

        ### Image orientation
        if (method_exists($fileAsset, "getImage_orientation") === true) {
            if ($fileAsset->getImage_orientation()) {
                $entryData = array_merge([
                    "facet_id" => self::FILTER_KEY_IMAGE_ORIENTATION,
                    "facet_title" => "facet.title.image_orientation",
                    "facet_value" => $fileAsset->getImage_orientation(),
                    "facet_value_display" => "facet.value.image_orientation." . $fileAsset->getImage_orientation(),
                    "locale" => $locale,
                        ], $entryBaseData);

                $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

                Entry\Facet::insertEntry($entryData);
            }
        }

        ### File type
        $fileTypeMapping = $this->getMimeTypeMappingsConfiguration($locale);

        if ($fileTypeMapping[$assetObject->getMimetype()]) {
            $fileType = $fileTypeMapping[$assetObject->getMimetype()];
        } else {
            $fileType = $assetObject->getMimetype();
        }

        $entryData = array_merge([
            "facet_id" => self::FILTER_KEY_FILE_TYPE,
            "facet_title" => "facet.title.file_type",
            "facet_value" => $fileType,
            "facet_value_display" => $fileType,
            "locale" => $locale,
                ], $entryBaseData);

        $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

        Entry\Facet::insertEntry($entryData);

        ### File type
        if (method_exists($fileAsset, "getVideo_type") === true) {
            if ($fileAsset->getVideo_type()) {
                $entryData = array_merge([
                    "facet_id" => self::FILTER_KEY_VIDEO_TYPE,
                    "facet_title" => "facet.title.video_type",
                    "facet_value" => $fileAsset->getVideo_type(),
                    "facet_value_display" => "facet.value.video_type." . $fileAsset->getVideo_type(),
                    "locale" => $locale,
                        ], $entryBaseData);

                $returnData[$entryData["facet_id"]] = $entryData["facet_value"];

                Entry\Facet::insertEntry($entryData);
            }
        }

        return $returnData;
    }

    private function getCatalogAssetData($assetId, $locale)
    {
        return $this->assetData->getCatalogAssetData($assetId, $locale);
    }

    private function getMimeTypeMappingsConfiguration($locale)
    {
        if ($this->mappingConfig !== null) {
            return $this->mappingConfig;
        }

        $mappings = $this->config->get("Filter_File_Type_Mapping");

        $this->mappingConfig = [];
        foreach ($mappings as $mapping) {
            $this->mappingConfig[$mapping["Mime_Type"]->getData()] = $mapping["Mapping_Text"]->getData();
        }

        return $this->mappingConfig;
    }

    private function transformToAssetArticles(ElementInterface $object)
    {
        return $this->getAssetObjectChildsFromObject($object);
    }

    private function getAssetObjectChildsFromObject (ElementInterface $object, $additionalPath = null)
    {
        $listing = new DataObject\Listing();

        $conditionFilters = [];

        if($additionalPath) {
            $path = $object->getRealFullPath() . '/' . $additionalPath . '/';
        } else {
            $path = $object->getRealFullPath() . '/';
        }

        $conditionFilters[] = '(o_path = ?)';
        $conditionFilters[] = "(o_classId IN ('AssetFile', 'AssetImage', 'AssetVideo'))";

        $listing->setCondition(implode(' AND ', $conditionFilters), [$path]);

        return $listing;
    }

    private function getChildArticleAssetsObjectsFromObject(ElementInterface $object, $listClass): array
    {
        $listing = new $listClass();

        $conditionFilters = [];

        $quotedPath = $listing->quote($object->getRealFullPath());

        $quotedWildcardPath = $listing->quote(str_replace('//', '/', $object->getRealFullPath() . '/') . '%');
        $conditionFilters[] = '(o_path = ' . $quotedPath . ' OR o_path LIKE ' . $quotedWildcardPath . ')';

        $listing->setCondition(implode(' AND ', $conditionFilters));

        $result = [];
        foreach ($listing as $listElement) {
            $result[] = $listElement;
        }

        return $result;
    }

    private function getDataFromAssetArticle($articleAssets): Data
    {
        $data = new Data();

        $data->setAssets($articleAssets);

        $categories = [];
        while ($articleAssets->getParentId() !== 1) {
            $articleAssets = \Pimcore\Model\DataObject::getById($articleAssets->getParentId());

            if ($articleAssets instanceof \Pimcore\Model\DataObject\Article) {
                $data->setArticle($articleAssets);
            }

            if ($articleAssets instanceof \Pimcore\Model\DataObject\CatalogCategory) {
                if(!$articleAssets->getEXCLUDE_FROM_CATEGORY_TREE()) {
                    $categories[] = $articleAssets;
                }
            }

            if ($articleAssets instanceof \Pimcore\Model\DataObject\Catalog) {
                $data->setCatalog($articleAssets);
            }
        }

        $data->setContainerId($articleAssets->getParent()->getId());

        $data->setCategories($categories);

        return $data;
    }
}
