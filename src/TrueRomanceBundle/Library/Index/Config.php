<?php

namespace TrueRomanceBundle\Library\Index;

class Config
{
    const ASSET_PREVIEW_PREFIX_PATH = "/var/tmp/image-thumbnails";

    const ASSET_ORIGINAL_PREFIX_PATH = "/var/assets";

    public static function getAssetPlaceholderImage()
    {
        $config = \Pimcore\Config::getWebsiteConfig()->configuration;

        return $config->getAsset_Placeholder_Image() ? $config->getAsset_Placeholder_Image()->getFullPath() : "";
    }

    public static function getAssetTypeFileImage($type)
    {
        $mapping = [
            "application/msword" => self::ASSET_ORIGINAL_PREFIX_PATH . "/word.png",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document" => self::ASSET_ORIGINAL_PREFIX_PATH . "/word.png",
            "application/pdf" => self::ASSET_ORIGINAL_PREFIX_PATH . "/PDF.png",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation" => self::ASSET_ORIGINAL_PREFIX_PATH . "/powerpoint.png",
            "application/vnd.ms-powerpoint" => self::ASSET_ORIGINAL_PREFIX_PATH . "/powerpoint.png",
            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" => self::ASSET_ORIGINAL_PREFIX_PATH . "/exel.png",
            "application/vnd.ms-excel" => self::ASSET_ORIGINAL_PREFIX_PATH . "/exel.png",
            "application/zip" => self::ASSET_ORIGINAL_PREFIX_PATH . "/zip.png"
        ];

        return $mapping[$type];
    }

    public static $assetFileAttributes = [
        "Auszeichnung 1 - EPS (AWARD_HIGH)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Auszeichnung 1 - JPG (AWARD_LOW)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Auszeichnung 2 - EPS (AWARD1_HIGH)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Auszeichnung 2 - JPG (AWARD1_LOW)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Auszeichnung 3 - EPS (AWARD2_HIGH)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 1 Heroshot 45° (WEB)" => [
            "field" => "img_Heroshot",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 2 Heroshot Frontal (WEB1)" => [
            "field" => "img_Heroshot",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 3 Inhaltsbild (KIT)" => [
            "field" => "img_product_other",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 4 Anwendung (ANW)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 5 Anwendung (ANW1)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 6 Anwendung (ANW2)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 7 Anwendung (ANW3)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 8 Anwendung (ANW4)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 9 Anwendung (ANW5)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 10 Anwendung (ANW6)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 11 Anwendung (ANW7)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 12 Others (OTH)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 13 Others (OTH1)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 14 Others (OTH2)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 15 Others (OTH3)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Bild 16 Heroshot Frontal EPS (WEB2)" => [
            "field" => "img_Heroshot",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Produktzeichnung (WEB3)" => [
            "field" => "img_product_other",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Pressetext 1 - DOC (PRESS)" => [
            "field" => "file_pr_text",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Pressetext 2 - DOC (PRESS1)" => [
            "field" => "file_pr_text",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Pressetext 3 - DOC (PRESS2)" => [
            "field" => "file_pr_text",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Produktinfoblatt - PDF (INFO)" => [
            "field" => "file_productdatasheet",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Produktinfoblatt - PPT (INFO1)" => [
            "field" => "file_productdatasheet",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Produktinfoblatt - DOC (INFO2)" => [
            "field" => "file_productdatasheet",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Video" => [
            "field" => "video_asset",
            "type" => "\Pimcore\Model\DataObject\AssetVideo",
        ],
        "Catalog Asset Logo" => [
            "field" => "img_logo",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Catalog Asset Icon" => [
            "field" => "img_icon",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
        ],
        "Catalog Asset Video" => [
            "field" => "video_asset",
            "type" => "\Pimcore\Model\DataObject\AssetVideo",
        ],
        "Catalog Asset Certificate" => [
            "field" => "file_certificate",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Catalog Asset Download" => [
            "field" => "data_bundle",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ],
        "Catalog Asset Pricelist Download" => [
            "field" => "catalog_download",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
        ]
    ];

    private static $classFilterMapping = [
        "\Pimcore\Model\DataObject\AssetImage" => "image",
        "\Pimcore\Model\DataObject\AssetFile" => "salesdoc",
        "\Pimcore\Model\DataObject\AssetVideo" => "video"
    ];
}
