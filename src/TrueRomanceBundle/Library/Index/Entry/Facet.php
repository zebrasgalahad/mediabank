<?php

namespace TrueRomanceBundle\Library\Index\Entry;

use TrueRomanceBundle\Library\Index\Entry\AbstractEntry;

class Facet extends AbstractEntry
{

    protected static $loggingType = 'data-facet';
    
    public static function getTable(): string
    {
        return "true_romance_asset_facett";
    }
}
