<?php

namespace TrueRomanceBundle\Library\Index\Entry;

use TrueRomanceBundle\Library\Index\Entry\AbstractEntry;
use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

class Index extends AbstractEntry
{
    protected static $loggingType = 'data-index';

    public static function getTable(): string
    {
        return "true_romance_asset_index";
    }

    public static function insertEntry(array $data)
    {
        $table = static::getTable();

        Database::getDatabaseObject()->query("DELETE FROM $table WHERE object_id = ? AND  asset_type = ? and locale = ?", [$data["object_id"], $data["asset_type"], $data["locale"]]);

        Database::getDatabaseObject()->insert(static::getTable(), $data);
    }

    public static function deleteByContainerId($containerId)
    {
        Database::getDatabaseObject()->deleteWhere(self::getTable(), "container_id=" . $containerId . "  AND asset_type != 'Catalog'");
    }

    public static function getByContainerId($containerId)
    {
        $table = self::getTable();

        return Database::getDatabaseObject()->fetchAll("SELECT * FROM $table WHERE container_id = ?", [$containerId]);
    }

    /**
     *
     * @param int|string $objectId
     */
    public static function deleteEntryByObjectId($objectId)
    {
        $table = static::getTable();

        Database::getDatabaseObject()->query("DELETE FROM {$table} WHERE object_id = ? AND asset_type != 'Catalog'", [$objectId]);
    }

    public static function deleteEntryByAssetObjectIdAndLocale($objectId, $locale)
    {
        $table = static::getTable();

        Database::getDatabaseObject()->query("DELETE FROM {$table} WHERE asset_object_id = ? AND locale = '{$locale}' AND asset_type != 'Catalog'", [$objectId]);
    }

    /**
     *
     * @param int|string $objectId
     */
    public static function deleteEntriesFromTableByObjectId($objectId)
    {
        $database = Database::getDatabaseObject();

        $table = static::getTable();

        $database->query("DELETE FROM {$table} WHERE object_id = ? AND asset_type != 'Catalog'", [$objectId]);
    }


}
