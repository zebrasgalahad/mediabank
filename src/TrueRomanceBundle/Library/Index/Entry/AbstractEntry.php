<?php

namespace TrueRomanceBundle\Library\Index\Entry;

use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

class AbstractEntry
{
    /**
     *
     * @param \Pimcore\Model\DataObject $object
     */
    public static function deleteFacetTableEntries($object)
    {
        self::deleteEntriesFromTableByObjectId($object->getId());
    }

    /**
     *
     * @param int|string $objectId
     */
    public static function deleteEntriesFromTableByObjectId($objectId)
    {
        $database = Database::getDatabaseObject();

        $table = static::getTable();

        $database->query("DELETE FROM {$table} WHERE object_id = ?", [$objectId]);
    }

    /**
     *
     * @param int|string $objectId
     * @param int|string $facetId
     * @param string $facetTitle
     * @param string $facetName
     * @param string $facetValue
     * @param string $country
     * @param string $language
     */
    public static function createEntry($entry)
    {
        Database::getDatabaseObject()->insert(static::getTable(), $entry);
    }

    /**
     *
     * @param int|string $objectId
     * @param string $locale
     */
    public static function deleteEntryByObjectIdAndLocale($objectId, $locale)
    {
        $table = static::getTable();

        Database::getDatabaseObject()->query("DELETE FROM {$table} WHERE object_id = ? and locale = ?", [$objectId, $locale]);
    }

    public static function insertEntry(array $data)
    {
        Database::getDatabaseObject()->insert(static::getTable(), $data);
    }

    public static function clearTable ($facetId = null) {
        $table = static::getTable();
        if($facetId === null) {
            Database::getDatabaseObject()->executeQuery("TRUNCATE $table");
        } else {
            Database::getDatabaseObject()->deleteWhere($table, "facet_id = '$facetId'");
        }
    }

    public static function replaceParentIds($search, $replace)
    {
        Database::getDatabaseObject()->update(static::getTable(), ["parent_id" => $replace], ["parent_id" => $search]);
    }

    public static function getAllByFacetId($facetId, $groupBy = '')
    {
        $table = static::getTable();

        return Database::getDatabaseObject()->fetchAll("SELECT * FROM $table WHERE facet_id = ? $groupBy", [$facetId]);
    }
}
