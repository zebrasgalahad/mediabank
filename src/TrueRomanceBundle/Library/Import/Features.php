<?php

namespace TrueRomanceBundle\Library\Import;

use Pimcore\Model\DataObject\Article;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use Pimcore\Model\DataObject\Classificationstore\GroupConfig;
use Pimcore\Model\DataObject\Classificationstore;

class Features
{

    protected static $productData;
    
    protected static $properties;
    
    protected static $classificationStoreId;
    
    public static function import($productData, $properties, $classificationStoreId)
    {
        self::$productData = $productData;
        
        self::$productData = $properties;
        
        self::$classificationStoreId = $classificationStoreId;
        
        foreach ($productData as $product) {
            self::importFeatures($product);
        }
    }
    
    private static function importFeatures($product) {
        $productObject = Article::getBySUPPLIER_PID($product["SUPPLIER_PID"])->current();
        
        if(!$productObject) {
            CliHelper::error("No product found for supplier id {$product["SUPPLIER_PID"]}!");
            
            return;
        }     
        
        $productObject->setREFERENCE_FEATURE_SYSTEM_NAME($product["PRODUCT_FEATURES"]["REFERENCE_FEATURE_SYSTEM_NAME"]);
        $productObject->setREFERENCE_FEATURE_GROUP_ID($product["PRODUCT_FEATURES"]["REFERENCE_FEATURE_GROUP_ID"]);
        $productObject->setREFERENCE_FEATURE_GROUP_NAME($product["PRODUCT_FEATURES"]["REFERENCE_FEATURE_GROUP_NAME"]);
        
        $featureGroup = self::getFeatureGroup($product["PRODUCT_FEATURES"]["REFERENCE_FEATURE_GROUP_ID"]);
        
        if(!$featureGroup) {
            CliHelper::error("No group with id {$product["PRODUCT_FEATURES"]["REFERENCE_FEATURE_GROUP_ID"]} found...proceeding - Product: {$product["SUPPLIER_PID"]} ({$productObject->getId()})");
            
            return;
        }
        
        if ($product["PRODUCT_FEATURES"]["FEATURE"] !== NULL) {
            $featureBlockData = self::getDataForProductFeatureBlock($featureGroup, $product["PRODUCT_FEATURES"]["FEATURE"], true);
        } else {
            $featureBlockData = self::getDataForProductFeatureBlock($featureGroup, $product["PRODUCT_FEATURES"], false);
        }            
        
        $classificationStore = new Classificationstore($featureBlockData);

        $classificationStore->setObject($productObject);

        $classificationStore->setFieldname("FEATURE");

        $classificationStore->setActiveGroups([$featureGroup->getId() => true]);

        $classificationStore->save();

        $classificationStore->setObject($productObject);

        $classificationStore->save();
        
        $productObject->setFEATURE($classificationStore);
        
        $productObject->save();    
        
        CliHelper::success("Product features added: {$product["SUPPLIER_PID"]} ({$productObject->getId()})!");
    }
    
    /**
     * 
     * @param \Pimcore\Model\DataObject\Classificationstore\GroupConfig $group
     * @param array $features
     * @param bool $keyDefinitions
     * @return array
     */
    protected static function getDataForProductFeatureBlock(GroupConfig $group, array $features, $keyDefinitions = true): array
    {
        $data = [];

        if ($keyDefinitions === false) {
            return $data[$group->getId()] = [];
        }

        $key = [];

        if (is_array($features[0]) === false) {
            $features = [$features];
        }
        
        foreach ($group->getRelations() as $relation) {
            foreach ($features as $index => $feature) {
                if ($feature["FNAME"] === $relation->getName()) {
                    $valueType = null;
                    $value = "";
                    $unit = null;

                    if (array_key_exists("FVALUE", $features[$index]) === true) {
                        $value = $features[$index]["FVALUE"];
                    }
                    
                    if (array_key_exists("FUNIT", $features[$index]) === true) {
                        $unit = \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::unitMapping()[$features[$index]["FUNIT"]];
                    }
                    
                    if ($relation->getType() === "inputQuantityValue") {
                        $unitId = is_object($unit) ? $unit->getId() : "";

                        $valueType = new \Pimcore\Model\DataObject\Data\InputQuantityValue($value, $unitId);
                    } else {
                        $valueType = $value;
                    }

                    $key[$relation->getKeyId()] = [
                        "default" => $valueType
                    ];

                    $data[$group->getId()] = $key;
                }
            }
        }
        
        return $data;
    }    
    
    public static function getFeatureGroup($groupName) {
        return GroupConfig::getByName($groupName, self::$classificationStoreId);
    }    
    
    protected static function getPropertyById($propertyId)
    {
        foreach (self::$properties as $property) {
            if ($property["Merkmal_ID"] === $propertyId) {
                return $property;
            }
        }

        return null;        
    }    
}
