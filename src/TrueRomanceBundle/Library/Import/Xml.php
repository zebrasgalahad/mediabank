<?php

namespace TrueRomanceBundle\Library\Import;

use TrueRomanceBundle\Library\Import\Pimcore\Folder;

use TrueRomanceBundle\Library\Tools\Url;
use TrueRomanceBundle\Library\Import\Config;

class Xml {

    /**
     *
     * @var Xml\Data 
     */
    protected $xmlData;

    /**
     *
     * @var Config 
     */
    protected $config;
    
    /**
     *
     * @var \Pimcore\Model\DataObject\Catalog 
     */
    protected $catalog;
    
    public function __construct(Xml\Data $xmlData) {
        $this->xmlData = $xmlData;
    }

    public function import() {
        $this->assembleConfig();
        
        \Pimcore::collectGarbage();
        
        $this->createFolderStructure();
        
        \Pimcore::collectGarbage();
        
        $this->createProducts();
    }
    
    private function assembleConfig () {
        $catalogname = Url::slugify($this->xmlData->get(["T_NEW_CATALOG", "CATALOG_GROUP_SYSTEM", "CATALOG_STRUCTURE", "0", "GROUP_NAME"]));
        
        $this->config = new Config();
        
        $this->config->setCatalogname($catalogname);
    }
    
    private function createProducts() {
        new Elements\Product(new Folder(), $this->xmlData, $this->config);
    }
    
    private function createFolderStructure() {
        $folderStructure = new Elements\FolderStructure(new Folder(), $this->xmlData, $this->config);
        
        $this->config->setCatalog($folderStructure->catalogRootObject);
    }
    
    private function addCatalogToCatalogRoot() {
        
    }
}
