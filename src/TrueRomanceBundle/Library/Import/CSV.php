<?php

namespace TrueRomanceBundle\Library\Import;

use TrueRomanceBundle\Library\Object\Factory as ObjectFactory;
use TrueRomanceBundle\Library\Tools\Url;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;


class CSV
{
    public static function import($singleProductData, $locale, $parentFolder)
    {
            \Pimcore::collectGarbage();
            
            \Pimcore\Cache\Runtime::getInstance()->clear();

            $productInstance = \Pimcore\Model\DataObject\Article::getBySUPPLIER_PID($singleProductData["SUPPLIER_PID"])->current();

            $singleProductData["SAP_EXTRACT"] = true;
            
            $productKey = Url::slugify($singleProductData["SUPPLIER_PID"]);
            
            if ($productInstance) {
                CliHelper::info("Updating article with SKU " . $singleProductData["SUPPLIER_PID"]);
                
                unset($singleProductData["ORDER_UNIT"]);

                $productObject = ObjectFactory::updateExistingById(\Pimcore\Model\DataObject\Article::class, $singleProductData, $productInstance->getId(), true, $locale);
            } else {
                CliHelper::success("Importing article with SKU " . $singleProductData["SUPPLIER_PID"]);
                
                $singleProductData["MANUFACTURER_NAME"] = "Stanley Black & Decker";
                $singleProductData["MANUFACTURER_ACRONYM"] = "SBDINC";
                $singleProductData["REACH_INFO"] = false;
                $singleProductData["PRODUCT_PRICE_nrp_PRICE_CURRENCY"] = "EUR";
                $singleProductData["PRODUCT_PRICE_nrp_TAX"] = 0.19;
                $singleProductData["PRICE_QUANTITY"] = 1;
                $singleProductData["ORDER_UNIT"] = "ST";
                $singleProductData["NO_CU_PER_OU"] = 1;
                $singleProductData["CONTENT_UNIT"] = "ST";
                $singleProductData["QUANTITY_MIN"] = 1;
                $singleProductData["QUANTITY_MIN_UNIT"] = "ST";
                $singleProductData["STEPQUANTITY"] = 1;
                
                $productObject = ObjectFactory::createNew(\Pimcore\Model\DataObject\Article::class, $productKey, $parentFolder->getId(), $singleProductData,
                                true, $locale);
                
                $productObject->setPublished(false);
                
                $productObject->save();
                
                ObjectFactory::createNew(\Pimcore\Model\DataObject\ArticleAssets::class, "assets", $productObject->getId(), [],
                                true, $locale);
            }
    }
}
