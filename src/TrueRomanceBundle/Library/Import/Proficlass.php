<?php

namespace TrueRomanceBundle\Library\Import;

use TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping;
use Pimcore\Model\DataObject\Classificationstore\GroupConfig;
use Pimcore\Model\DataObject\Classificationstore\KeyConfig;
use Pimcore\Model\DataObject\Classificationstore\KeyGroupRelation;
use Pimcore\Model\DataObject\QuantityValue\Unit\Listing as UnitListing;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

class Proficlass
{
    private static $data = [];

    private static $classificationStoreId;

    public static function importUnits($data)
    {
        foreach ($data as $entry) {
            QuantityValueUnitMapping::createUnitIfNotExists(utf8_encode($entry["Einheitkurzbezeichnung"]), utf8_encode($entry["Einheitbezeichnung"]));
        }
    }

    public static function importClasses($data, $classificationStoreId)
    {
        self::$data = $data;

        self::$classificationStoreId = $classificationStoreId;

        $count = count($data["klassen"]);
        $addCounter = 0;
        $startTime = time();
        foreach ($data["klassen"] as $class) {
            ++$addCounter;

            $relapseTime = (time() - $startTime);

            $estimatedTime = ($relapseTime / $addCounter) * ($count - $addCounter);

            CliHelper::info("$addCounter von $count Klassen...");

            CliHelper::info("Übrige Zeit: " . \format_time($estimatedTime) . " / Vergangen bis jetzt: " . \format_time($relapseTime));               
            
            self::importClass($class);
            
            CliHelper::success("Creating/updating class {$class["Klasse_ID"]}/{$class["Klassenbezeichnung"]}");
        }
    }

    protected static function importClass($class)
    {
        $properties = self::getPropertiesForClass($class["Klasse_ID"]);
        
        foreach ($properties as $index => $property) {
            $units = self::getAllowedUnitsForProperty($property["Einheit_ID"]);
            
            $properties[$index]["Einheiten"] = $units;
        }
        
        $class["Merkmale"] = $properties;

        self::createGroupAndAttributes($class);
    }

    protected static function createGroupAndAttributes($class)
    {
        $featureGroup = self::createFeatureGroup($class["Klasse_ID"], $class["Klassenbezeichnung"]);
        
        foreach ($class["Merkmale"] as $index => $classProperty) {
            $attribute = self::createFeatureGroupAttribute($classProperty);
            
            self::assignKeyToGroup($featureGroup, $attribute, $index + 1);
        }
    }

    /**
     * 
     * @param Pimcore\Model\DataObject\Classificationstore\GroupConfig $group
     * @param Pimcore\Model\DataObject\Classificationstore\KeyConfig $key
     * @param int $sorter
     */
    protected function assignKeyToGroup(GroupConfig $group, KeyConfig $key, $sorter) {
        $keyGroupRelation = new KeyGroupRelation();

        $keyGroupRelation->setGroupId($group->getId());

        $keyGroupRelation->setKeyId($key->getId());
        
        $keyGroupRelation->setEnabled(true);
        
        $keyGroupRelation->setSorter($sorter);
        
        $keyGroupRelation->save();
    }
    
    protected static function createFeatureGroupAttribute($property)
    {
        $type = $property["Merkmal"]["Datentyp"];
        
        $mandatoryField = false;
        
        $keyConfig = KeyConfig::getByName(utf8_encode($property["Merkmal"]["Merkmal_ID"]), self::$classificationStoreId);
        
        if($type === "numerisch") {
            $units = $property["Einheiten"];
            
            $validUnitIds = [];
            
            if(count($units) === 0) {
                $attributeConfig = [
                    "name" => utf8_encode($property["Merkmal"]["Merkmal_ID"]),
                    "description" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                    "datatype" => "data",
                    "fieldtype" => "numeric",
                    "title" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                    "mandatory" => $mandatoryField,
                ];
            } else {
                foreach ($units as $unit) {
                    $pimcoreUnit = self::getUnitByAbbreviation(utf8_encode($unit["Einheitkurzbezeichnung"]));

                    $validUnitIds[] = $pimcoreUnit["id"];
                }

                $attributeConfig = [
                    "name" => utf8_encode($property["Merkmal"]["Merkmal_ID"]),
                    "description" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                    "datatype" => "data",
                    "fieldtype" => "inputQuantityValue",
                    "title" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                    "mandatory" => $mandatoryField,
                    "defaultUnit" => $validUnitIds[0],
                    "validUnits" => $validUnitIds                
                ];                
            }
        } elseif ($type === "alphanumerisch") {
            $options = [];
            foreach ($property["Werte"] as $value) {
                $options[] = [
                    "key" => utf8_encode(str_replace('"', "", $value["Wertbezeichnung"])),
                    "value" => utf8_encode(str_replace('"', "", $value["Wert_ID"]))
                ];
            }
            
            $attributeConfig = [
                "name" => utf8_encode($property["Merkmal"]["Merkmal_ID"]),
                "description" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                "datatype" => "data",
                "fieldtype" => "select",
                "title" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                "mandatory" => $mandatoryField,
                "yesLabel" => "yes",
                "noLabel" => "no",
                "emptyLabel" => "empty",
                "options" => $options
            ];               
            
        } elseif ($type === "logisch") {
            $attributeConfig = [
                "name" => utf8_encode($property["Merkmal"]["Merkmal_ID"]),
                "description" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                "datatype" => "data",
                "fieldtype" => "booleanSelect",
                "title" => utf8_encode($property["Merkmal"]["Merkmalbezeichnung"]),
                "mandatory" => $mandatoryField,
                "yesLabel" => "yes",
                "noLabel" => "no",
                "emptyLabel" => "empty",
            ];            
        }
        
        if ($keyConfig) {
            $keyConfig->setDefinition(json_encode($attributeConfig));

            $keyConfig->save();
        } else {
            $keyConfig = new KeyConfig();

            $keyConfig->setName($attributeConfig["name"]);

            $keyConfig->setTitle($attributeConfig["title"]);
            $keyConfig->setType($attributeConfig["fieldtype"]);
            $keyConfig->setDescription($attributeConfig["title"]);
            $keyConfig->setStoreId(self::$classificationStoreId);
            $keyConfig->setEnabled(true);
            $keyConfig->setDefinition(json_encode($attributeConfig));
            $keyConfig->save();                 
        }
        
        return $keyConfig;
    }

    protected static function getUnitByAbbreviation($abbreviation)
    {
        $units = new UnitListing();
        
        $unit = \Pimcore\Db::get()->fetchRow("SELECT * FROM quantityvalue_units WHERE abbreviation = '$abbreviation'");
        
        return $unit;
    }


    protected static function createFeatureGroup($groupName, $description)
    {
        $config = GroupConfig::getByName($groupName, self::$classificationStoreId);

        if (!$config) {
            $config = new GroupConfig();
        }         
        
        $config->setStoreId(self::$classificationStoreId);

        $config->setName(utf8_encode($groupName));

        $config->setDescription(utf8_encode($description));

        $config->save();        
        
        return $config;
    }
    
    protected static function getAllowedUnitsForProperty($unitId)
    {
        if (trim($unitId) === "") {
            return [];
        }

        $targetUnits = [];
        foreach (self::$data["einheiten"] as $unit) {
            if ($unit["Einheit_ID"] === $unitId) {
                $targetUnits[] = $unit;
            }
        }

        return $targetUnits;
    }

    protected static function getAllowedValuesForProperty($classPropertyNumber)
    {
        $result = [];
        foreach (self::$data["klassenmerkmalewerte"] as $entry) {
            if ($classPropertyNumber === $entry["KlasseMerkmal_Nr"]) {
                $value = self::getValueById($entry["Wert_ID"]);
                $result[] = $value;
            }
        }

        return $result;
    }

    protected static function getValueById($valueId)
    {
        foreach (self::$data["werte"] as $value) {
            if ($value["Wert_ID"] === $valueId) {
                return $value;
            }
        }

        return null;
    }

    protected static function getPropertyById($propertyId)
    {
        foreach (self::$data["merkmale"] as $property) {
            if ($property["Merkmal_ID"] === $propertyId) {
                return $property;
            }
        }

        return null;        
    }

    protected static function getPropertiesForClass($classId)
    {
        $foundProperties = [];
        foreach (self::$data["klassenmerkmale"] as &$property) {
            $property["Merkmal"] = self::getPropertyById($property["Merkmal_ID"]);
            
            if ($property["Klasse_ID"] === $classId) {
                $allowedValues = self::getAllowedValuesForProperty($property["KlasseMerkmal_Nr"]);

                $property["Werte"] = $allowedValues;

                $foundProperties[] = $property;
            }
        }

        $sortedProperties = array_column($foundProperties, 'Sortier_Nr');
        
        array_multisort($sortedProperties, SORT_ASC, $foundProperties);
        
        return $foundProperties;
    }
}
