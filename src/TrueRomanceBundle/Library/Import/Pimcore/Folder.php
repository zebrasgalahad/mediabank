<?php

namespace TrueRomanceBundle\Library\Import\Pimcore;

use Pimcore\Model\DataObject\Folder as PimcoreObjectFolder;

class Folder {

    /**
     * 
     * @param string $key
     * @param string $path
     * @param int $parentId
     */
    public function createFolder(string $key, string $path, int $parentId): PimcoreObjectFolder {
        $folder = new PimcoreObjectFolder();

        $folder->setKey($key);

        $folder->setPath($path);

        $folder->setParentId($parentId);

        $folder->save();

        return $folder;
    }

    /**
     * 
     * @param string $path
     * @return bool
     */
    public function folderExists(string $path) : bool {
        return \Pimcore\Model\DataObject\Folder::getByPath($path) !== null && \Pimcore\Model\DataObject\Folder::getByPath($path) !== false;
    }

    /**
     * 
     * @param string $key
     * @param string $path
     * @return PimcoreObjectFolder
     */
    public function createFolderIfNotExists(string $key, string $path) : PimcoreObjectFolder {
        $fullpath = $path . "/" . $key;

        if ($this->folderExists($fullpath) === false) {
            $parent = \Pimcore\Model\DataObject\Folder::getByPath($path);

            return $this->createFolder($key, $path, $parent->getId());
        } else {
            return \Pimcore\Model\DataObject\Folder::getByPath($fullpath);
        }
    }

}
