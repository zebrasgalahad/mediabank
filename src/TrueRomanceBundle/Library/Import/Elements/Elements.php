<?php

namespace TrueRomanceBundle\Library\Import\Elements;

use TrueRomanceBundle\Library\Import\Pimcore\Folder;
use TrueRomanceBundle\Library\Import\Xml\Data;
use TrueRomanceBundle\Library\Import\Config;

abstract class Elements {

    /**
     *
     * @var Folder 
     */
    protected $pimcoreFolder;

    /**
     *
     * @var Data 
     */
    protected $data;
    
    /**
     *
     * @var Config 
     */
    protected $config;
    
    protected $element;
    
    public function __construct(Folder $pimcoreFolder, Data $data, Config $config) {
        $this->pimcoreFolder = $pimcoreFolder;
        
        $this->data = $data;
        
        $this->config = $config;
        
        $this->create();
    }

    public function create() {
        
    }
    
    public function getElement() {
        return $this->element;
    }
}
