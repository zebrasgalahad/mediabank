<?php

namespace TrueRomanceBundle\Library\Import\Elements;

use TrueRomanceBundle\Library\Import\Pimcore\Folder;
use TrueRomanceBundle\Library\Import\Elements\Elements;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

class FolderStructure extends Elements
{
    const CATALOGS_ROOTPATH_NAME = "catalogs";

    /**
     *
     * @var \Pimcore\Model\DataObject\Catalog 
     */
    public $catalogRootObject;
    
    public function create()
    {
        $folderData = $this->data->get(["T_NEW_CATALOG", "CATALOG_GROUP_SYSTEM", "CATALOG_STRUCTURE"]);

        $catalogsRootFolder = $this->pimcoreFolder->createFolderIfNotExists(self::CATALOGS_ROOTPATH_NAME, "/");

        $catalogFolderName = \TrueRomanceBundle\Library\Tools\Url::slugify($folderData[0]["GROUP_NAME"]);

        $rootObject = null;
        foreach ($folderData as $folderSingleData) {
            $data = [
                "GROUP_ID" => $folderSingleData["GROUP_ID"],
                "GROUP_NAME" => $folderSingleData["GROUP_NAME"],
                "PARENT_ID" => $folderSingleData["PARENT_ID"],
                "GROUP_ORDER" => (int) $folderSingleData["GROUP_ORDER"],
            ];
            
            $objectClass = \Pimcore\Model\DataObject\CatalogCategory::class;
            if($rootObject === null) {
                $objectClass = \Pimcore\Model\DataObject\Catalog::class;
            }            
            
            if ($rootObject === null) {
                $rootObject = $catalogsRootFolder;
            } else {
                $rootObject = null;
                
                $rootObject = \Pimcore\Model\DataObject\CatalogCategory::getByGROUP_ID($folderSingleData["PARENT_ID"])->current();
                
                if(!$rootObject) {
                    $rootObject = \Pimcore\Model\DataObject\Catalog::getByGROUP_ID($folderSingleData["PARENT_ID"])->current();
                }
            }

            if(!$rootObject) {
                CliHelper::error("Could not find parent: " . implode(" - ", $data));
                
                continue;
            }
            
            $objectKey = \TrueRomanceBundle\Library\Tools\Url::slugify($folderSingleData["GROUP_NAME"]);

            $objectPath = $rootObject->getFullpath() . "/" . $objectKey;
            
            if (\TrueRomanceBundle\Library\Object\Factory::objectExistsByPath($objectPath) === false) {
                $currentObject = \TrueRomanceBundle\Library\Object\Factory::createNew($objectClass, $objectKey,
                                $rootObject->getId(), $data);
            } else {
                $currentObject = \TrueRomanceBundle\Library\Object\Factory::updateExistingByPath($objectClass, $data, $objectPath);
            }

            if ($currentObject->getFullpath() === "/" . self::CATALOGS_ROOTPATH_NAME . "/" . $catalogFolderName) {
                $currentObject = $this->addCatalogRootObjectData($currentObject);
                
                $this->catalogRootObject = $currentObject;
            }

            CliHelper::success("Added/updated: $objectPath");
        }
    }

    private function addCatalogRootObjectData($catalogObject)
    {
        $catalogData = $this->data->get(["HEADER", "CATALOG"]);
        
        $objectData = [
            "CATALOG_LANGUAGE" => $catalogData["LANGUAGE"],
            "CATALOG_CATALOG_ID" => $catalogData["CATALOG_ID"],
            "CATALOG_CATALOG_VERSION" => $catalogData["CATALOG_VERSION"],
            "CATALOG_CATALOG_NAME" => $catalogData["CATALOG_NAME"],
            "CATALOG_LANGUAGE_DATETIME" => $catalogData["DATETIME"]["DATE"],
            "CATALOG_TERRITORY" => $catalogData["TERRITORY"],
            "CATALOG_CURRENCY" => $catalogData["CURRENCY"],
            "CATALOG_PRICE_FLAG_INCL_FREIGHT" => $catalogData["PRICE_FLAG"][0],
            "CATALOG_PRICE_FLAG_INCL_PACKING" => $catalogData["PRICE_FLAG"][1],
            "CATALOG_PRICE_FLAG_INCL_ASSURANCE" => $catalogData["PRICE_FLAG"][2],
            "CATALOG_PRICE_FLAG_INCL_DUTY" => $catalogData["PRICE_FLAG"][3],
        ];   
        
        $catalogObject->setValues($objectData);
        
        $buyerData = $this->data->get(["HEADER", "BUYER"]);

        $objectData = [
            "BUYER_BUYER_ID_SUPPLIER_SPECIFIC" => $buyerData["BUYER_ID"],
            "BUYER_BUYER_BUYER_NAME" => $buyerData["BUYER_NAME"],
            "BUYER_ADRESS_STREET" => $buyerData["ADRESS"]["STREET"],
            "BUYER_ADRESS_ZIP" => $buyerData["ADRESS"]["ZIP"],
            "BUYER_ADRESS_CITY" => $buyerData["ADRESS"]["CITY"],
            "BUYER_ADRESS_COUNTRY" => $buyerData["ADRESS"]["COUNTRY"],
        ];   
        
        $catalogObject->setValues($objectData);
        
        $supplierData = $this->data->get(["HEADER", "SUPPLIER"]);

        $objectData = [
            "SUPPLIER_SUPPLIER_ID_SUPPLIER_SPECIFIC" => $supplierData["SUPPLIER_ID"],
            "SUPPLIER_SUPPLIER_NAME" => $supplierData["SUPPLIER_NAME"],
            "SUPPLIER_ADDRESS_SUPPLIER_NAME" => $supplierData["ADDRESS"]["NAME"],
            "SUPPLIER_ADDRESS_SUPPLIER_STREET" => $supplierData["ADDRESS"]["STREET"],
            "SUPPLIER_ADDRESS_SUPPLIER_ZIP" => $supplierData["ADDRESS"]["ZIP"],
            "SUPPLIER_ADDRESS_SUPPLIER_CITY" => $supplierData["ADDRESS"]["CITY"],
            "SUPPLIER_ADDRESS_SUPPLIER_STATE" => $supplierData["ADDRESS"]["STATE"],
            "SUPPLIER_ADDRESS_SUPPLIER_COUNTRY" => $supplierData["ADDRESS"]["COUNTRY"],
            "SUPPLIER_ADDRESS_SUPPLIER_PHONE" => $supplierData["ADDRESS"]["PHONE"],
            "SUPPLIER_ADDRESS_SUPPLIER_FAX" => $supplierData["ADDRESS"]["FAX"],
            "SUPPLIER_ADDRESS_SUPPLIER_EMAIL" => $supplierData["ADDRESS"]["EMAIL"],
            "SUPPLIER_ADDRESS_SUPPLIER_URL" => $supplierData["ADDRESS"]["URL"],
            "SUPPLIER_ADDRESS_SUPPLIER_ADDRESS_REMARKS" => $supplierData["ADDRESS"]["ADDRESS_REMARKS"],
        ];    
        
        $catalogObject->setValues($objectData);
        
        $catalogObject->save();
        
        return $catalogObject;
    }
}
