<?php

namespace TrueRomanceBundle\Library\Import\Elements;

use TrueRomanceBundle\Library\Object\Factory;
use Pimcore\Model\DataObject\Catalog as CatalogObject;
use TrueRomanceBundle\Library\Import\Elements\Elements;

class Catalog extends Elements {

    public function create() {
        $catalogObjectRoot = $this->pimcoreFolder->createFolderIfNotExists("config", $this->config->getFolderObjectsCatalogFolder());
        
        $targetCatalogPath = $this->config->getFolderObjectsCatalogFolder() . "/config/catalog";
        
        $catalogData = $this->data->get(["HEADER", "CATALOG"]);
        
        $objectData = [
            "CATALOG_LANGUAGE" => $catalogData["LANGUAGE"],
            "CATALOG_CATALOG_ID" => $catalogData["CATALOG_ID"],
            "CATALOG_CATALOG_VERSION" => $catalogData["CATALOG_VERSION"],
            "CATALOG_CATALOG_NAME" => $catalogData["CATALOG_NAME"],
            "CATALOG_LANGUAGE_DATETIME" => $catalogData["DATETIME"]["DATE"],
            "CATALOG_TERRITORY" => $catalogData["TERRITORY"],
            "CATALOG_CURRENCY" => $catalogData["CURRENCY"],
            "CATALOG_PRICE_FLAG_INCL_FREIGHT" => $catalogData["PRICE_FLAG"][0],
            "CATALOG_PRICE_FLAG_INCL_PACKING" => $catalogData["PRICE_FLAG"][1],
            "CATALOG_PRICE_FLAG_INCL_ASSURANCE" => $catalogData["PRICE_FLAG"][2],
            "CATALOG_PRICE_FLAG_INCL_DUTY" => $catalogData["PRICE_FLAG"][3],
        ];
        
        if(Factory::objectExistsByPath($targetCatalogPath) === false) {
            $this->element = Factory::createNew(CatalogObject::class, "catalog", $catalogObjectRoot->getId(), $objectData);
        } else {
            $this->element = Factory::updateExistingByPath(CatalogObject::class, $objectData, $targetCatalogPath);
        }
    }
    
}
