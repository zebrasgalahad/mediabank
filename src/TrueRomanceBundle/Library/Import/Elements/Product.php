<?php

namespace TrueRomanceBundle\Library\Import\Elements;

use TrueRomanceBundle\Library\Import\Elements\Elements;
use Pimcore\Model\DataObject\CatalogFolder;
use TrueRomanceBundle\Library\Object\Factory as ObjectFactory;
use TrueRomanceBundle\Library\Tools\Url;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

class Product extends Elements
{
    private $productMapping;
    private $maxImportProducts;

    public function create()
    {
        $this->productMapping = $this->data->get(["T_NEW_CATALOG", "PRODUCT_TO_CATALOGGROUP_MAP"]);

        $productData = $this->data->get(["T_NEW_CATALOG", "PRODUCT"]);

        $productCount = count($productData);

        $groupClass = new Product\Feature\Group($this->data);

        CliHelper::info("Adding $productCount articles");

        foreach ($productData as $index => $singleProductData) {
            \Pimcore::collectGarbage();
            
            \Pimcore\Cache\Runtime::getInstance()->clear();
            
            $singleProductData["SUPPLIER_PID"] = $singleProductData["SUPPLIER_PID"] ? $singleProductData["SUPPLIER_PID"] : $singleProductData["SUPPLIER_ALT_PID"];

            if (!$singleProductData["SUPPLIER_PID"]) {
                CliHelper::error("No supplier id for product: " . implode(" - ", $singleProductData));

                continue;
            }

            $parentFolder = $this->getProductParentFolder($singleProductData["SUPPLIER_PID"]);
            
            if (!$parentFolder) {
                CliHelper::error("No parent folder for product: " . implode(" - ", $singleProductData));

                $importFolderPath = "/import";
                $parentFolder = \Pimcore\Model\DataObject\Folder::getByPath("/import");
                
                CliHelper::error("Saving product in path $importFolderPath");
            }

            $productInstanceData = [];
            $pKeys = ["PRODUCT_DETAILS", "PRODUCT_ORDER_DETAILS"];
            
            $productInstanceData["PRODUCT_STATUS_NEW_PRODUCT"] = false;
            if (is_array($singleProductData["PRODUCT_DETAILS"]["PRODUCT_STATUS"]) === true && $singleProductData["PRODUCT_DETAILS"]["PRODUCT_STATUS"]["@attributes"]["type"] === "really_new_product") {
                $productInstanceData["PRODUCT_STATUS_NEW_PRODUCT"] = true;
            } else {
                $productInstanceData["PRODUCT_STATUS_NEW_PRODUCT"] = false;
            }

            foreach ($pKeys as $pKey) {
                $productInstanceData = array_merge($singleProductData[$pKey], $productInstanceData);
            }

            $productInstanceData["SPECIAL_TREATMENT_CLASS_GEFAHRSTOFFE"] = $productInstanceData["SPECIAL_TREATMENT_CLASS"] === "NEIN" ? "false" : "true";
            
            $productInstanceData["SUPPLIER_PID"] = $singleProductData["SUPPLIER_PID"];

            $productInstanceData["CONTENT_UNIT"] = $singleProductData["PRODUCT_ORDER_DETAILS"]["ORDER_UNIT"];
            $productInstanceData["CONTENT_UNIT"] = $singleProductData["PRODUCT_ORDER_DETAILS"]["CONTENT_UNIT"];
            $productInstanceData["NO_CU_PER_OU"] = (float) $singleProductData["PRODUCT_ORDER_DETAILS"]["NO_CU_PER_OU"];
            $productInstanceData["QUANTITY_MIN"] = (float) $singleProductData["PRODUCT_ORDER_DETAILS"]["QUANTITY_MIN"];
            $productInstanceData["QUANTITY_INTERVAL"] = (float) $singleProductData["PRODUCT_ORDER_DETAILS"]["QUANTITY_INTERVAL"];
            $productInstanceData["PRICE_QUANTITY"] = (float) $singleProductData["PRODUCT_ORDER_DETAILS"]["PRICE_QUANTITY"];

            $productInstanceData["DATETIME_VALID_START_DATE"] = new \DateTime($singleProductData["PRODUCT_PRICE_DETAILS"]["DATETIME"][0]["DATE"]);
            $productInstanceData["DATETIME_VALID_END_DATE"] = new \DateTime($singleProductData["PRODUCT_PRICE_DETAILS"]["DATETIME"][1]["DATE"]);
            $productInstanceData["PRODUCT_PRICE"] = $singleProductData["PRODUCT_PRICE_DETAILS"]["DATETIME"][1]["DATE"];

            $productInstanceData["PRODUCT_PRICE_NRP_PRICE_AMOUNT"] = new \DateTime($singleProductData["PRODUCT_PRICE_DETAILS"]["DATETIME"][1]["DATE"]);

            $productInstanceData["REFERENCE_FEATURE_SYSTEM_NAME"] = $singleProductData["PRODUCT_FEATURES"]["REFERENCE_FEATURE_SYSTEM_NAME"];
            $productInstanceData["REFERENCE_FEATURE_GROUP_ID"] = $singleProductData["PRODUCT_FEATURES"]["REFERENCE_FEATURE_GROUP_ID"];
            $productInstanceData["REFERENCE_FEATURE_GROUP_NAME"] = $singleProductData["PRODUCT_FEATURES"]["REFERENCE_FEATURE_GROUP_NAME"];

            $productInstanceData = $this->createPrices($productInstanceData, $singleProductData);

            $productInstanceData = $this->createUserDefinedExtensions($productInstanceData, $singleProductData);

            if (!$singleProductData["SUPPLIER_PID"]) {
                CliHelper::error("Product has no supplier id: " . implode(" - ", $singleProductData["PRODUCT_DETAILS"]));

                continue;
            }

            $productKey = Url::slugify($singleProductData["SUPPLIER_PID"]);

            $locale = $this->config->getLocale();
            
            $productPath = $parentFolder->getFullPath() . "/" . $productKey;
            if (ObjectFactory::objectExistsByPath($productPath) === true) {
                $productObject = ObjectFactory::updateExistingByPath(\Pimcore\Model\DataObject\Article::class, $productInstanceData, $productPath, false, $locale);
            } else {
                $productObject = ObjectFactory::createNew(\Pimcore\Model\DataObject\Article::class, $productKey, $parentFolder->getId(), $productInstanceData,
                                false, $locale);
            }

            /* @var $productObject \Pimcore\Model\DataObject\Article */

            if($singleProductData["PRODUCT_FEATURES"]) {
                $featureGroup = $groupClass->createFeatureGroupElement($singleProductData["PRODUCT_FEATURES"]);

                if ($singleProductData["PRODUCT_FEATURES"]["FEATURE"] !== NULL) {
                    $featureBlockData = $this->getDataForProductFeatureBlock($featureGroup, $singleProductData["PRODUCT_FEATURES"]["FEATURE"], true);
                } else {
                    $featureBlockData = $this->getDataForProductFeatureBlock($featureGroup, $singleProductData["PRODUCT_FEATURES"], false);
                }

                $classificationStore = new \Pimcore\Model\DataObject\Classificationstore($featureBlockData);

                $classificationStore->setObject($productObject);

                $classificationStore->setFieldname("FEATURE");

                $classificationStore->setActiveGroups([$featureGroup->getId() => true]);

                $classificationStore->save();

                $classificationStore->setObject($productObject);

                $classificationStore->save();
                
                $productObject->setFEATURE($classificationStore);
            } else {
                CliHelper::error("NOTICE: No product features found for product " . $productObject->getSUPPLIER_PID() . " - nevertheless importing Product");
            }

            $productObject->save();

            \TrueRomanceBundle\Library\Tools\Cli\Helper::success($index + 1 . " of $productCount: " . $productObject->getFullpath() . " - ID : " . $productObject->getId());
            
            if ($this->maxImportProducts !== null && is_int($this->maxImportProducts) === true && $index > $this->maxImportProducts) {
                break;
            }
        }
    }

    private function createUserDefinedExtensions($productInstanceData, $singleProductData)
    {
        $replaceKey = "UDX.EDXF.";

        foreach ($singleProductData["USER_DEFINED_EXTENSIONS"] as $key => $value) {
            if ($key === "UDX.EDXF.PACKING_UNITS") {
                $targetPreKey = "PACKING_UNITS_";

                foreach ($value["UDX.EDXF.PACKING_UNIT"] as $subKey => $subValues) {
                    $targetSubkey = str_replace($replaceKey, "", $subKey);

                    $productInstanceData[$targetPreKey . $targetSubkey] = $subValues;
                }

                continue;
            }

            if ($key === "UDX.EDXF.REACH") {
                $productInstanceData["REACH_INFO"] = $value["UDX.EDXF.REACH.INFO"] === "false" ? false : true;

                continue;
            }
            
            $udxMappings = [
                "UDX.EDXF.LANGTEXT" => [
                    "field" => "usp_list",
                    "block" => "usp_row"
                ],
                "UDX.EDXF.LIEFERUMFANG" => [
                    "field" => "scope_of_delivery_list",
                    "block" => "scope_of_delivery_row"
                ],
                "UDX.EDXF.TECHNISCHE_DATEN" => [
                    "field" => "tec_specs_list",
                    "block" => "tech_specs_row"
                ],
                "UDX.EDXF.GARANTIEBEDINGUNGEN" => [
                    "field" => "warranties_list",
                    "block" => "warranties_row"                    
                ],
                "UDX.EDXF.ANWENDUNGSBEISPIELE" => [
                    "field" => "APPLICATION_LIST",
                    "block" => "application_row"
                ],                
            ];
            
            if($udxMappings[$key]) {
                $explodedValue = explode("<br/>", $value);
                
                foreach ($explodedValue as $index => $singleValue) {
                    $explodedValue[$index] = strip_tags($singleValue);
                }
                
                $mappings = $udxMappings[$key];
                
                $blockElements = [];
                foreach ($explodedValue as $singleValue) {
                    $blockElements[] = [$mappings["block"] => new \Pimcore\Model\DataObject\Data\BlockElement($mappings["field"], "text", $singleValue)];
                }
                
                $productInstanceData[$mappings["field"]] = $blockElements;
            }
            
            if ($key === "PRODUCT_LOGISTIC_DETAILS") {
                $productInstanceData["PRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER"] = $value["CUSTOMS_TARIFF_NUMBER"]["CUSTOMS_NUMBER"];
                $productInstanceData["PRODUCT_LOGISTIC_DETAILS_COUNTRY_OF_ORIGIN"] = $value["COUNTRY_OF_ORIGIN"];

                continue;
            }

            if (is_array($value) === true && in_array("        ", $value) === true) {
                $productInstanceData[$key] = "";

                continue;
            }

            $targetkey = str_replace($replaceKey, "", $key);

            $productInstanceData[$targetkey] = $value;
        }

        return $productInstanceData;
    }

    private function createPrices($productInstanceData, $singleProductData)
    {
        if (!$singleProductData["PRODUCT_PRICE_DETAILS"]["PRODUCT_PRICE"]) {
            CliHelper::error("No prices found for product " . $productInstanceData["SUPPLIER_PID"]);

            return;
        }

        $productPrices = [];
        if($singleProductData["PRODUCT_PRICE_DETAILS"]["PRODUCT_PRICE"]["@attributes"]) {
            $productPrices = [$singleProductData["PRODUCT_PRICE_DETAILS"]["PRODUCT_PRICE"]];
        } else {
            $productPrices = $singleProductData["PRODUCT_PRICE_DETAILS"]["PRODUCT_PRICE"];
        }
        
        foreach ($productPrices as $productPrice) {
            if(!$productPrice["@attributes"]) {
                CliHelper::error("No price type found: " . $productInstanceData["SUPPLIER_PID"]);
                
                continue;
            }
            
            $type = $productPrice["@attributes"]["price_type"];

            $productInstanceData["PRODUCT_PRICE_{$type}_PRICE_AMOUNT"] = $productPrice["PRICE_AMOUNT"];
            $productInstanceData["PRODUCT_PRICE_{$type}_PRICE_CURRENCY"] = $productPrice["PRICE_CURRENCY"];
            $productInstanceData["PRODUCT_PRICE_{$type}_TAX"] = $productPrice["TAX"];
            $productInstanceData["price_{$type}_quantity"] = (int)$productPrice["LOWER_BOUND"];
        }

        return $productInstanceData;
    }

    private function getProductParentFolder(string $supplierId)
    {
        $folder = null;
        foreach ($this->productMapping as $mappingElement) {
            if ($mappingElement["PROD_ID"] === $supplierId) {
                $folder = \Pimcore\Model\DataObject\CatalogCategory::getByGROUP_ID($mappingElement["CATALOG_GROUP_ID"])->current();
                
                if(!$folder) {
                    $folder = \Pimcore\Model\DataObject\Catalog::getByGROUP_ID($mappingElement["CATALOG_GROUP_ID"])->current();
                }
            }
        }

        return $folder;
    }

    /**
     * 
     * @param \Pimcore\Model\DataObject\Classificationstore\GroupConfig $group
     * @param array $features
     * @param bool $keyDefinitions
     * @return array
     */
    private function getDataForProductFeatureBlock(\Pimcore\Model\DataObject\Classificationstore\GroupConfig $group, array $features, $keyDefinitions = true): array
    {
        $data = [];

        if ($keyDefinitions === false) {
            return $data[$group->getId()] = [];
        }

        $key = [];

        if (is_array($features[0]) === false) {
            $features = [$features];
        }

        foreach ($group->getRelations() as $relation) {
            foreach ($features as $index => $feature) {
                if ($feature["FNAME"] === $relation->getName()) {
                    $valueType = null;
                    $value = "";
                    $unit = null;

                    if (array_key_exists("FVALUE", $features[$index]) === true) {
                        $value = $features[$index]["FVALUE"];
                    }

                    if (array_key_exists("FUNIT", $features[$index]) === true) {
                        $unit = \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::unitMapping()[$features[$index]["FUNIT"]];
                    }

                    if ($relation->getType() === "inputQuantityValue") {
                        $unitId = is_object($unit) ? $unit->getId() : "";

                        $valueType = new \Pimcore\Model\DataObject\Data\InputQuantityValue($value, $unitId);
                    } else {
                        $valueType = $value;
                    }

                    $key[$relation->getKeyId()] = [
                        "default" => $valueType
                    ];

                    $data[$group->getId()] = $key;
                }
            }
        }
        
        return $data;
    }
}
