<?php

namespace TrueRomanceBundle\Library\Import\Elements\Product\Feature;

use Pimcore\Model\DataObject\Classificationstore\GroupConfig;
use Pimcore\Model\DataObject\Classificationstore\KeyConfig;
use Pimcore\Model\DataObject\Classificationstore\KeyGroupRelation;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\Import\Elements\Elements;

/**
 * Description of Group
 *
 * @author lukadrezga
 */
class Group extends Elements {

    /**
     *
     * @var string
     */
    private $articleClassId;

    /**
     *
     * @var array
     */
    private $xmlData;

    /**
     *
     * @var string
     */
    private $classificationStoreId;

    /**
     *
     * @var Pimcore\Model\DataObject\ClassDefinition 
     */
    private $articleClass;

    /**
     * 
     * @param \TrueRomanceBundle\Library\Import\Xml\Data $xmlData
     */
    public function __construct(\TrueRomanceBundle\Library\Import\Xml\Data $xmlData) {
        $this->xmlData = $xmlData;

        $articleObject = new \Pimcore\Model\DataObject\Article();

        $classId = $articleObject->getClass()->getId();

        $this->articleClassId = $classId;

        $articleClass = \Pimcore\Model\DataObject\ClassDefinition::getById($this->articleClassId);

        $this->articleClass = $articleClass;

        $storeId = $this->articleClass->getFieldDefinitions()["FEATURE"]->getStoreId();

        $this->classificationStoreId = $storeId;
    }

    public function createFeatureGroupElement(array $productFeatureData) {
        $name = $productFeatureData["REFERENCE_FEATURE_GROUP_ID"];
        $description = $productFeatureData["REFERENCE_FEATURE_GROUP_NAME"];
        
        $config = GroupConfig::getByName($name, $this->classificationStoreId);

        if (!$config) {
            $config = new GroupConfig();

            $config->setStoreId($this->classificationStoreId);

            $config->setName($name);

            $config->setDescription($description);

            $config->save();

//            CliHelper::success("Created Group with name {$name} and id {$config->getId()}");
        } 

        $this->createKeyDefinitions($config, $productFeatureData);
        
        if (is_array($productFeatureData["FEATURE"][0]) === false) {
            $data = $productFeatureData["FEATURE"];

            $productFeatureData = ["FEATURE" => [$data]];
        }
        
        return $config;
    }

    public function deleteGroups() {
        $groupListing = new GroupConfig\Listing();

        $groupListing->addConditionParam("storeId", $this->classificationStoreId);

        foreach ($groupListing->load() as $group) {
            $group->delete();
            CliHelper::success("Group {$group->getName()} deleted");
        }

        CliHelper::success("Finished");
    }

    public function deleteKeyDefinitions() {
        $keyDefinitionsListing = new KeyConfig\Listing();

        $keyDefinitionsListing->addConditionParam("storeId", $this->classificationStoreId);

        foreach ($keyDefinitionsListing->load() as $key) {
            $key->delete();
            CliHelper::success("Key Definition {$key->getName()} deleted");
        }

        CliHelper::success("Finished");
    }

    /**
     * 
     * @param Pimcore\Model\DataObject\Classificationstore\GroupConfig $group
     * @param array $productFeatureData
     */
    private function createKeyDefinitions(GroupConfig $group, array $productFeatureData) {

        if ($productFeatureData["FEATURE"] !== NULL) {

            if (is_array($productFeatureData["FEATURE"][0]) === false) {
                $data = $productFeatureData["FEATURE"];

                $productFeatureData = ["FEATURE" => [$data]];
            }

            foreach ($productFeatureData["FEATURE"] as $index => $features) {
                $keyConfig = KeyConfig::getByName($features["FNAME"], $this->classificationStoreId);

                $fieldType = "input";

                $unitId = "";

                $name = $features["FNAME"];

                $title = $features["FDESCR"];

                $description = $features["FDESCR"];

                if (array_key_exists("FUNIT", $features) === true) {
                    $fieldType = "inputQuantityValue";

                    $unit = \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::unitMapping()[$features["FUNIT"]] !== NULL ? \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::unitMapping()[$features["FUNIT"]] :
                            \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::createUnitIfNotExists($features["FUNIT"]);

                    if (is_object($unit) === true) {
                        $unitId = $unit->getId();
                    }
                }

                $definition = [
                    'fieldtype' => $fieldType,
                    'name' => $name,
                    'title' => $title,
                    'datatype' => 'data',
                    'validUnits' => [$unitId],
                    'defaultUnit' => $unitId
                ];
                
                if ($keyConfig) {
                    $keyConfig->setDefinition(json_encode($definition));

                    $keyConfig->save();
                    
                    $this->assignKeyToGroup($group, $keyConfig, $index);

                    if ($features["FUNIT"] !== NULL && $fieldType === "inputQuantityValue") {
                        \TrueRomanceBundle\Library\Tools\QuantityValueUnitMapping::createUnitIfNotExists($features["FUNIT"]);
                    }
                    
                    continue;
                }

                $keyConfig = new KeyConfig();
                
                $keyConfig->setName($name);

                $keyConfig->setTitle($title);
                $keyConfig->setType($fieldType);
                $keyConfig->setDescription($description);
                $keyConfig->setStoreId($this->classificationStoreId);
                $keyConfig->setEnabled(true);
                $keyConfig->setDefinition(json_encode($definition));
                $keyConfig->save();

                $this->assignKeyToGroup($group, $keyConfig, $index);
            }
        }
    }

    /**
     * 
     * @param Pimcore\Model\DataObject\Classificationstore\GroupConfig $group
     * @param Pimcore\Model\DataObject\Classificationstore\KeyConfig $key
     * @param int $sorter
     */
    private function assignKeyToGroup(GroupConfig $group, KeyConfig $key, $sorter) {
        $keyGroupRelation = new KeyGroupRelation();

        $keyGroupRelation->setGroupId($group->getId());

        $keyGroupRelation->setKeyId($key->getId());
        
        $keyGroupRelation->setEnabled(true);
        
        $keyGroupRelation->setSorter($sorter + 1);
        
        $keyGroupRelation->save();
        
//        CliHelper::success("Key {$key->getName()} assigned to Group {$group->getName()}");
    }

    /**
     * 
     * @param array $Ids
     */
    public function addGroupIdsToArticleClassDefinition(array $Ids) {
        $this->articleClass->getFieldDefinitions()["FEATURE"]->allowedGroupIds = implode(",", $Ids);

        $this->articleClass->save();
    }
}
