<?php

namespace TrueRomanceBundle\Library\Import;

class Config
{
    const FOLDER_OBJECTS_BASEPATH = "/catalogs";
    /**
     *
     * @var string 
     */
    protected $catalogname;

    /**
     *
     * @var \Pimcore\Model\DataObject\Catalog 
     */
    protected $catalog;
    protected $locale = "de_DE";

    public function getLocale()
    {
        return $this->locale;
    }

    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    public function getCatalog(): \Pimcore\Model\DataObject\Catalog
    {
        return $this->catalog;
    }

    public function setCatalog(\Pimcore\Model\DataObject\Catalog $catalog)
    {
        $this->catalog = $catalog;
    }

    public function getCatalogname()
    {
        return $this->catalogname;
    }

    public function setCatalogname(string $catalogname)
    {
        $this->catalogname = $catalogname;
    }

    public function getFolderObjectsBasepath(): string
    {
        return self::FOLDER_OBJECTS_BASEPATH;
    }

    public function getFolderObjectsCatalogFolder()
    {
        return $this->getFolderObjectsBasepath() . "/" . $this->getCatalogname();
    }
}
