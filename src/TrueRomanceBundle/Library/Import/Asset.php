<?php

namespace TrueRomanceBundle\Library\Import;

use Pimcore\Model\Asset as AssetModel;
use Pimcore\Model\DataObject;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Database\AbstractDatabase;
use TrueRomanceBundle\Library\Import\Asset\Folder as AssetFolder;
use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;
use Pimcore\Model\DataObject\Article;
use Pimcore\Model\DataObject\ArticleAssets;
use Pimcore\Log\ApplicationLogger;

class Asset
{
    public static $mapping = [
        "Auszeichnung 1 - EPS (AWARD_HIGH)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "award.image",
            "attribute" => "image_type"
        ],
        "Auszeichnung 1 - JPG (AWARD_LOW)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "award.image",
            "attribute" => "image_type"
        ],
        "Auszeichnung 2 - EPS (AWARD1_HIGH)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "award.image",
            "attribute" => "image_type"
        ],
        "Auszeichnung 2 - JPG (AWARD1_LOW)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "award.image",
            "attribute" => "image_type"
        ],
        "Auszeichnung 3 - EPS (AWARD2_HIGH)" => [
            "field" => "img_award",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "award.image",
            "attribute" => "image_type"
        ],
        "Bild 1 Heroshot 45° (WEB)" => [
            "field" => "img_Heroshot",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "product.cutout",
            "attribute" => "image_type"
        ],
        "Bild 2 Heroshot Frontal (WEB1)" => [
            "field" => "img_Heroshot",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "product.cutout",
            "attribute" => "image_type"
        ],
        "Bild 3 Inhaltsbild (KIT)" => [
            "field" => "img_product_other",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "boxcontent.image",
            "attribute" => "image_type"
        ],
        "Bild 4 Anwendung (ANW)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 5 Anwendung (ANW1)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 6 Anwendung (ANW2)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 7 Anwendung (ANW3)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 8 Anwendung (ANW4)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 9 Anwendung (ANW5)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 10 Anwendung (ANW6)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 11 Anwendung (ANW7)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "application.image",
            "attribute" => "image_type"
        ],
        "Bild 12 Others (OTH)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "",
            "attribute" => "image_type"
        ],
        "Bild 13 Others (OTH1)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "",
            "attribute" => "image_type"
        ],
        "Bild 14 Others (OTH2)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "",
            "attribute" => "image_type"
        ],
        "Bild 15 Others (OTH3)" => [
            "field" => "img_application",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "",
            "attribute" => "image_type"
        ],
        "Bild 16 Heroshot Frontal EPS (WEB2)" => [
            "field" => "img_Heroshot",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "product.cutout",
            "attribute" => "image_type"
        ],
        "Produktzeichnung (WEB3)" => [
            "field" => "img_product_other",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "drawing.image",
            "attribute" => "image_type"
        ],
        "Pressetext 1 - DOC (PRESS)" => [
            "field" => "file_pr_text",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "pressrelease",
            "attribute" => "salesdoc_type"
        ],
        "Pressetext 2 - DOC (PRESS1)" => [
            "field" => "file_pr_text",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "pressrelease",
            "attribute" => "salesdoc_type"
        ],
        "Pressetext 3 - DOC (PRESS2)" => [
            "field" => "file_pr_text",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "pressrelease",
            "attribute" => "salesdoc_type"
        ],
        "Produktinfoblatt - PDF (INFO)" => [
            "field" => "file_productdatasheet",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "productinformation",
            "attribute" => "salesdoc_type"
        ],
        "Produktinfoblatt - PPT (INFO1)" => [
            "field" => "file_productdatasheet",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "productinformation",
            "attribute" => "salesdoc_type"
        ],
        "Produktinfoblatt - DOC (INFO2)" => [
            "field" => "file_productdatasheet",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "productinformation",
            "attribute" => "salesdoc_type"
        ],
        "Video" => [
            "field" => "video_asset",
            "type" => "\Pimcore\Model\DataObject\AssetVideo",
            "internal_type" => "video",
            "attribute" => "asset_type"
        ],
        "Catalog Asset Logo" => [
            "field" => "img_logo",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "",
            "attribute" => "image_type"
        ],
        "Catalog Asset Icon" => [
            "field" => "img_icon",
            "type" => "\Pimcore\Model\DataObject\AssetImage",
            "internal_type" => "",
            "attribute" => "image_type"
        ],
        "Catalog Asset Video" => [
            "field" => "video_asset",
            "type" => "\Pimcore\Model\DataObject\AssetVideo",
            "internal_type" => "video",
            "attribute" => "asset_type"
        ],
        "Catalog Asset Certificate" => [
            "field" => "file_certificate",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "",
            "attribute" => "salesdoc_type"
        ],
        "Catalog Asset Download" => [
            "field" => "data_bundle",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "",
            "attribute" => "salesdoc_type"
        ],
        "Catalog Asset Pricelist Download" => [
            "field" => "catalog_download",
            "type" => "\Pimcore\Model\DataObject\AssetFile",
            "internal_type" => "",
            "attribute" => "salesdoc_type"
        ]
    ];

    private static $classFilterMapping = [
        "\Pimcore\Model\DataObject\AssetImage" => "image",
        "\Pimcore\Model\DataObject\AssetFile" => "salesdoc",
        "\Pimcore\Model\DataObject\AssetVideo" => "video"
    ];

    private static $typeMapping = [
        "image_type" => "AssetImage",
        "salesdoc_type" => "AssetFile",
        "asset_type" => "AssetVideo"
    ];

    public static function import(array $data)
    {
        $locale = $data['locale'];

        $asset = self::createImageIfNotExists($data);

        $articleListing = new Article\Listing();

        $searchId = trim($data["Supplier_AID"]);

        $articleListing->addConditionParam("SUPPLIER_PID = ?", [$searchId]);

        $article = $articleListing->current();

        if (!$article) {
            CliHelper::error("ERROR: No article found for product number {$data["Supplier_AID"]}");

            return;
        }

        $articleChildren = $article->getChildren();

        $articleChildrenIds = [];

        foreach ($articleChildren as $articleChild) {
            $articleChildrenIds[] = $articleChild->getId();
        }

        $settings = self::$mapping[trim($data["Mime_Purpose"])];

        self::handleArticleAssetImport($asset, $articleChildrenIds, $locale, $settings, $data, $article);
    }

    /**
     * @param \Pimcore\Model\Asset $asset
     * @param Article $article
     */
    private static function handleArticleAssetImport($asset, $articleChildrenIds, $locale = 'de_DE', $settings, $data, Article $article)
    {
        $db = AbstractDatabase::get();

        $assetType = self::$typeMapping[$settings['attribute']];

        if (!empty($articleChildrenIds)) {
            $implodedIds = implode(",", $articleChildrenIds);

            $assetId = $asset->getId();

            $query = "SELECT DISTINCT ooo_id FROM object_localized_data_{$assetType} WHERE ooo_id IN(" . $implodedIds . ") AND asset_relation = '$assetId'";

            if ($assetType == "AssetFile") {
                $query = "SELECT DISTINCT src_id FROM object_relations_{$assetType} WHERE src_id IN(" . $implodedIds . ") AND dest_id = '$assetId'";
            }

            $results = $db->fetchAll($query);

            // if there is absolute no objects with asset set for this locale, we create new object
            if (empty($results)) {
                self::createNewArticleAsset($settings, $locale, $data, $asset, $article);
            } else {
                self::updateArticleAsset($results, $settings, $locale, $data, $asset, $article);
            }
        }
    }

    private function updateArticleAsset($results, $settings, $locale, $data, $asset, $article)
    {
        $assetType = self::$typeMapping[$settings['attribute']];

        foreach ($results as $result) {
            $id = $assetType == "AssetFile" ? $result['src_id'] : $result['ooo_id'];

            $item = DataObject::getById($id);

            self::setData($item, $settings, $locale, $data, $asset, $article);
        }
    }

    private static function createNewArticleAsset($settings, $locale, $data, $asset, $article)
    {
        $item = new $settings["type"]();

        $key = self::createObjectKey($article, $settings);

        $item->setParent($article)->setPublished(true)->setKey($key);

        self::setData($item, $settings, $locale, $data, $asset, $article);
    }

    private function setData($item, $settings, $locale, $data, $asset, $article)
    {
        $filterAssetType = self::$classFilterMapping[$settings["type"]];

        $item->setAsset_title($data["Mime_Purpose"], $locale);

        $itemBmcat = $data["item_bmecat"] === "true" ? true : false;

        $itemMediathek = $data["item_mediathek"] === "true" ? true : false;

        $item->setItem_bmecat($itemBmcat, $locale);

        $item->setItem_mediathek($itemMediathek, $locale);

        CliHelper::error("Asset-Type: " . $filterAssetType);

        $targetAsset = null;
        if ($asset instanceof \Pimcore\Model\Asset\Video) {
            $targetAsset = new \Pimcore\Model\DataObject\Data\Video();
            $targetAsset->setData($asset);

            $targetAsset->setType("asset");
            $targetAsset->setTitle("Title");
            $targetAsset->setDescription("Description");
        } else {
            $targetAsset = $asset;
        }

        $item->setAsset_relation($targetAsset, $locale);

        $filterType = $settings["internal_type"];

        if ($targetAsset instanceof \Pimcore\Model\Asset\Image) {
            $item = self::setOrientation($item, $targetAsset, $locale);

            $item->setAsset_type($filterAssetType, $locale);

            $item->setImage_type($filterType, $locale);
        }

        if ($settings["type"] == "\Pimcore\Model\DataObject\AssetFile") {
            $item->setAsset_type($filterAssetType, $locale);
            $item->setSalesdoc_type($filterType, $locale);
        }

        $item->save();

        $logMessage = "Asset added for product number {$data["Supplier_AID"]} (ArticleID: {$article->getId()} Type: $filterAssetType) (ArticleAsset ID: {$item->getId()})";

        CliHelper::success($logMessage);

        ApplicationLogger::getInstance()->info($logMessage, ["component" => "MEDIA-IMPORT-TASK"]);
    }

    private static function createObjectKey($article, $settings)
    {
        $assetType = self::$typeMapping[$settings['attribute']];

        $parentId = $article->getId();

        $db = AbstractDatabase::get();

        $query = "SELECT COUNT(*) FROM object_{$assetType} WHERE o_parentId = '$parentId'";

        $count = (int) $db->fetchOne($query);

        $newCount = $count + 1;

        $keyMapping = [
            "AssetImage" => "Image_$newCount",
            "AssetFile" => "File_$newCount",
            "AssetVideo" => "Video_$newCount"
        ];

        return $keyMapping[$assetType];
    }

    public static function moveAssetsFromFtpToServer()
    {
        $host = '80.237.200.146';
        $user = 'sbp-ftp-2';
        $pass = 'dZ%im!ZbGSyY';

        $targetFolder = self::getImageBasePath();

        $commandImages = "wget --ftp-user=$user --ftp-password=$pass ftp://$user@$host/ASSETS_Q2_NPI_APRIL/IMAGES/* -P $targetFolder";

        $commandSheets = "wget --ftp-user=$user --ftp-password=$pass ftp://$user@$host/ASSETS_Q2_NPI_APRIL/SHEETS/* -P $targetFolder";

        exec($commandImages);

        exec($commandSheets);
    }

    public static function deleteFilesFromBaseFolder()
    {
        $filesToDelete = ["JPG", "jpg", "pptx", "png", "mp4", "pdf", "xlsx", "csv", "docx", "doc", "tif"];

        $targetFolder = self::getImageBasePath();

        foreach ($filesToDelete as $file) {
            $command = "rm {$targetFolder}*.$file";

            exec($command);
        }
    }

    private static function setOrientation($item, $asset, $locale = 'de_DE')
    {
        $imageDimensions = self::getAssetDimensions($asset);

        $imageOrientation = Asset\Helper::getImageProportion($imageDimensions["image_width_px"], $imageDimensions["image_height_px"]);

        $item->setImage_orientation($imageOrientation, $locale);

        return $item;
    }

    private static function createAssetObjectChild(Article $article): ArticleAssets
    {
        $articleAssetObject = new ArticleAssets();

        $articleAssetObject->setParent($article);

        $articleAssetObject->setKey("assets");

        $articleAssetObject->setPublished(true);

        $articleAssetObject->save();

        return $articleAssetObject;
    }

    private static function createImageIfNotExists($data)
    {
        $parentFolder = AssetFolder::getFolderByClearName($data["Mime_Purpose"]);

        $assetPath = $parentFolder->getPath() . $parentFolder->getFilename() . "/" . $data["Mime_Source"];

        $asset = AssetModel::getByPath($assetPath);

        $sourceFilepath = self::getImageBasePath() . $data["Mime_Source"];

        if (is_file($sourceFilepath) === false) {
            CliHelper::error("File $sourceFilepath could not be found!");
            die;
        }

        if (!$asset) {
            $asset = new AssetModel();
            $asset->setFilename($data["Mime_Source"]);
            $asset->setData(file_get_contents($sourceFilepath));
            $asset->setParent($parentFolder);
            $asset->save();

            CliHelper::success("File {$data["Mime_Source"]} was created (ID: {$asset->getId()})");
        } else {
            CliHelper::success("File {$data["Mime_Source"]} (ID: {$asset->getId()}) already exists");
        }

        return $asset;
    }

    private static function getImageBasePath(): string
    {
        if (!\Pimcore\Model\WebsiteSetting::getByName("image_import_base_dir")) {
            throw new \Exception("Parameter 'image_import_base_dir' must be set in website settings!");
        }

        return \Pimcore\Model\WebsiteSetting::getByName("image_import_base_dir")->getData();
    }

    /**
     *
     * @param AssetModel $asset
     * @return array
     */
    private static function getAssetDimensions($asset)
    {
        $data = [];

        $data["image_width_px"] = $asset->getWidth();
        $data["image_height_px"] = $asset->getHeight();

        return $data;
    }

}
