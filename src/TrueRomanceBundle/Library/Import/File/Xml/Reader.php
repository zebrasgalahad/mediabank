<?php

namespace TrueRomanceBundle\Library\Import\File\Xml;

use TrueRomanceBundle\Library\Import\Xml\Normalizer;

class Reader {

    public $filepath;
    protected $normalizer;

    public function __construct($filepath, Normalizer $normalizer = null) {
        $this->filepath = $filepath;

        $this->normalizer = $normalizer;
    }

    public function getContents(): string {
        if (file_exists($this->filepath) === false) {
            throw new \Exception("File not found: " . $this->filepath);
        }

        $fileContent = file_get_contents($this->filepath);

        if ($this->normalizer) {
            $fileContent = $this->normalizer->assemble($fileContent);
        }

        return $fileContent;
    }

}
