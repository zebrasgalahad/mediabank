<?php

namespace TrueRomanceBundle\Library\Import\Xml;

class Normalizer {

    private $replaceMapping = [
        "ARTICLE" => "PRODUCT",
        "SUPPLIER_AID" => "SUPPLIER_PID",
        "ART_ID" => "PROD_ID"
    ];

    const REPLACE_STRING_SOURCE = "ARTICLE";
    const REPLACE_STRING_TARGET = "PRODUCT";

    public function assemble(string $xml): string {
         $xml = str_ireplace('<PRODUCT_STATUS type="new_product">NEU</PRODUCT_STATUS>', '<PRODUCT_STATUS type="really_new_product"></PRODUCT_STATUS>', $xml);
        
        foreach ($this->replaceMapping as $search => $replace) {
            $xml = str_replace($search, $replace, $xml);
        }
        
        $xml = str_replace(array("\r\n", "\r", "\n"), "", $xml);
        
        return $xml;
    }

}
