<?php

namespace TrueRomanceBundle\Library\Import\Xml;

class Data {

    public $xmlReader;
    private $data;
    /**
     *
     * @var \SimpleXMLElement 
     */
    public $simpleXmlElement;

    public function __construct(\TrueRomanceBundle\Library\Import\File\Xml\Reader $xmlReader) {
        $this->xmlReader = $xmlReader;

        $this->init();
    }

    public function get(array $keys = null) {
        if ($keys !== null) {
            $targetConfig = $this->data;

            foreach ($keys as $key) {
                if (isset($targetConfig[$key]) === false) {
                    throw new \Exception("Keys don't exist: " . implode(' - ', $keys));
                }

                $targetConfig = $targetConfig[$key];
            }

            return $targetConfig;
        }

        return $this->data;
    }

    public function getArrayKeys(array $section = null) {
        if ($section !== null) {
            return array_keys($this->get($section));
        }

        return array_keys($this->get());
    }

    private function init() {
        $this->simpleXmlElement = simplexml_load_string($this->xmlReader->getContents(), 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOBLANKS | LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED);
        
        $json = json_encode($this->simpleXmlElement);

        $this->data = json_decode($json, TRUE);
    }

}
