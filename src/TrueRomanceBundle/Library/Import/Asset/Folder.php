<?php

namespace TrueRomanceBundle\Library\Import\Asset;

use TrueRomanceBundle\Library\Tools\Url;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

class Folder
{
    public static function createIfNotExists(string $folder)
    {
        $targetFolderName = self::assembleFolderFilename($folder);

        self::createAssetFolder($targetFolderName);
        
        CliHelper::success("Ensured folder $targetFolderName exists");
    }

    public static function assembleFolderFilename($foldername)
    {
        $targetFolderName = Url::slugify($foldername);

        if (substr($targetFolderName, -1) === "-") {
            $targetFolderName = substr($targetFolderName, 0, -1);
        }

        $targetFolderName = str_replace("_", "-", $targetFolderName);

        return $targetFolderName;
    }

    public static function getFolderByClearName($foldername)
    {
        return self::createAssetFolder(self::assembleFolderFilename($foldername));
    }
    
    private static function createAssetFolder($foldername)
    {
        $folder = \Pimcore\Model\Asset\Folder::getByPath("/" . $foldername);
        
        if(!$folder) {
            $newFolder = new \Pimcore\Model\Asset\Folder();
            
            $newFolder->setParentId(1);
            
            $newFolder->setFilename($foldername);
            
            $newFolder->save();
        }
        
        return $folder;
    }
}
