<?php

namespace TrueRomanceBundle\Library\Import\Asset;

/**
 * Helps you import Assets :-)
 *
 * @author lukadrezga
 */
class Helper {

    const IMAGE_SQUARE = 'square';
    const IMAGE_PANORAMA = 'panorama';
    const IMAGE_LANDSCAPE = 'landscape';
    const IMAGE_PORTRAIT = 'portrait';
    const IMAGE_BREAKPOINT = 1.8;

    public static function getImageProportion($width, $height)
    {
        if ($width === $height) {
            return self::IMAGE_SQUARE;
        }

        $proportion = $width / $height;

        if ($proportion < 1) {
            return self::IMAGE_PORTRAIT;
        }

        if ($proportion < self::IMAGE_BREAKPOINT) {
            return self::IMAGE_LANDSCAPE;
        } else {
            return self::IMAGE_PANORAMA;
        }
    }

}
