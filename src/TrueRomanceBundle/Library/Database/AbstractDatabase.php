<?php

namespace TrueRomanceBundle\Library\Database;

class AbstractDatabase
{
    /**
     *
     * @var \Pimcore\Db\Connection
     */
    public $database;

    public function __construct()
    {
        $this->setDatabase(\Pimcore\Db::get());
    }

    public function getDatabase()
    {
        return $this->database;
    }

    public function setDatabase($database)
    {
        $this->database = $database;
    }

    /**
     * 
     * @return \Pimcore\Db\Connection
     */
    public static function getDatabaseObject()
    {
        return \Pimcore\Db::get();
    }

    /**
     * 
     * @return \Pimcore\Db\Connection
     */
    public static function get()
    {
        return self::getDatabaseObject();
    }
}
