<?php

namespace TrueRomanceBundle\Library\Operator;

use Pimcore\DataObject\Import\ColumnConfig\Operator\AbstractOperator;

/**
 * PHP Operator um die Daten aus einer .cvs Datei in Block zu importieren
 *
 * @author lukadrezga
 */
class BlockImport extends AbstractOperator {
    
    private $seperator;
    
    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);
        
        $this->seperator = $config->additionalData;
        
        if($this->seperator === "") {
            $this->seperator = "|";
        }
    }
    
    public function process($element, &$target, array &$rowData, $colIndex, array &$context = [])
    {
        $colData = $rowData[$colIndex];
        
        $this->import($colData, $target);
    }
    
    /**
     * 
     * @param string $colData
     * @param \Pimcore\Model\DataObject $target
     * @return array
     */
    private function createBlockImportData($colData, $target)
    {
        $singleBlockEntry = explode($this->seperator, $colData);
        
        array_pop($singleBlockEntry);
        
        $blockName = $this->getBlockNameFromLabel();

        $articleInstance = new \Pimcore\Model\DataObject\Article();

        $articleClass = $articleInstance->getClass();        
        
        $blockAttributes = [$articleClass->getFieldDefinition("localizedfields")->getFielddefinition($blockName)->getChilds()[0]->name];
        
        $finalData = [];
        
        foreach ($singleBlockEntry as $entry) {
            $singleData[$blockAttributes[0]] = $entry;
            
            $finalData[] = $singleData;
        }
        
        return $finalData;
    }

    /**
     * 
     * @param string $colData
     * @param \Pimcore\Model\DataObject $target
     * @return \Pimcore\Model\DataObject\Data\BlockElement[]
     */
    private function cleanImportData($colData, $target)
    {
        $importData = $this->createBlockImportData($colData, $target);
        
        $finalImportData = [];

        foreach ($importData as $singleData) {
            foreach (array_keys($singleData) as $key) {
                $value = $singleData[$key];

                $singleBlockEntry = new \Pimcore\Model\DataObject\Data\BlockElement($key, "input", $value);

                $data[$key] = $singleBlockEntry;
            }

            $finalImportData[] = $data;
        }
        
        return $finalImportData;
    }

    /**
     * 
     * @param string $colData
     * @param Pimcore\Model\DataObject $target
     * @throws \Exception
     */
    private function import($colData, $target) {
        $importData = $this->cleanImportData($colData, $target);
        
        $configJson = \Symfony\Component\HttpFoundation\Request::createFromGlobals()->get("config");

        $config = json_decode($configJson, true);
        
        $locale = $config["resolverSettings"]["language"];        
        
        if (count($this->getChilds()) === 0) {
            throw new \Exception("Keine Kinderelemente wurden angelegt. Erlaubt sind nur Blocknamen z.B. 'scope_of_delivery (Lieferumfang)'");
        } else if (count($this->getChilds()) > 1) {
            throw new \Exception("Nur ein Kinderelement erlaubt.");
        }
        
        $blockName = $this->getBlockNameFromLabel();
        
        $setter = "set" . ucfirst($blockName);
        
        $target->$setter($importData, $locale);
    }
    
    /**
     * 
     * @return string
     */
    private function getBlockNameFromLabel() {
        $childLabel = $this->getChilds()[0]->getLabel();
        
        $regex = '/(?<=\().+?(?=\))/m';
        
        preg_match($regex, $childLabel, $matches);
        
        return $matches[0];
    }
}
