<?php

namespace TrueRomanceBundle\Library\Operator\Export;

use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator as PimcoreAbstractOperator;

abstract class AbstractOperator extends PimcoreAbstractOperator
{

    protected function getBlockNameFromLabel(): string
    {
        $childLabel = $this->getChilds()[0]->getLabel();

        $regex = '/(?<=\().+?(?=\))/m';

        preg_match($regex, $childLabel, $matches);

        return $matches[0];
    }
}
