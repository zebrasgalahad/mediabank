<?php

namespace TrueRomanceBundle\Library\Operator\Export;

use TrueRomanceBundle\Library\Operator\Export\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;
use TrueRomanceBundle\Library\Tools\ObjectOption;
use Pimcore\Model\DataObject;

class Select extends AbstractOperator
{

    /**
     * 
     * @param Pimcore\Model\DataObject $element
     * @return ResultContainer
     */
    public function getLabeledValue($element): ResultContainer
    {
        $attribute = $this->getBlockNameFromLabel();

        $getter = "get" . ucfirst($attribute);

        $returnData = ObjectOption::valueToKey($element, $attribute, $element->$getter());

        $result = new ResultContainer();

        $result->setValue($returnData);

        return $result;
    }
}
