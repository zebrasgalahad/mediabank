<?php

namespace TrueRomanceBundle\Library\Operator\Import;

use TrueRomanceBundle\Library\Operator\Import\AbstractOperator;
use TrueRomanceBundle\Library\Tools\ObjectOption;

class Select extends AbstractOperator
{

    public function process($element, &$target, array &$rowData, $colIndex, array &$context = [])
    {
        $attribute = $this->getBlockNameFromLabel();

        $setter = "set" . ucfirst($attribute);

        $targetData = ObjectOption::keyToValue($element, $attribute, trim($rowData[$colIndex]));

        $target->$setter($targetData);
    }
}
