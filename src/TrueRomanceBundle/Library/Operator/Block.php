<?php

namespace TrueRomanceBundle\Library\Operator;

use Pimcore\DataObject\GridColumnConfig\Operator\AbstractOperator;
use Pimcore\DataObject\GridColumnConfig\ResultContainer;

/**
 * Block Data in Grid Options anzeigen
 *
 * @author lukadrezga
 */
class Block extends AbstractOperator {
    
    private $seperator;
    
    public function __construct(\stdClass $config, $context = null)
    {
        parent::__construct($config, $context);
        
        $this->seperator = $config->additionalData;
        
        if($this->seperator === "") {
            $this->seperator = "|";
        }             
    }
    
    /**
     * 
     * @param Pimcore\Model\DataObject $element
     * @return ResultContainer
     */
    public function getLabeledValue($element)
    {
        
        $result = new ResultContainer();
        $result->setValue($this->createDataHTMLOutput($element));

        return $result;
    }
    
    /**
     * 
     * @param Pimcore\Model\DataObject $element
     * @return $array
     * @throws \Exception
     */
    private function getBlockData($element) {
        $locale = \Symfony\Component\HttpFoundation\Request::createFromGlobals()->get("language");

        $getter = "";

        if (count($this->getChilds()) === 0) {
            throw new \Exception("Keine Kinderelemente wurden angelegt. Erlaubt sind nur Blocknamen z.B. 'Product Information (Product_Information)'");
        } else if (count($this->getChilds()) > 1) {
            throw new \Exception("Nur ein Kinderelement erlaubt.");
        }
        
        $blockName = $this->getBlockNameFromLabel();
        
        $getter = "get" . ucfirst($blockName);
        
        $finalData = [];

        $blockData = $element->$getter($locale);

        foreach ($blockData as $singleData) {
            $entry = [];
            foreach ($singleData as $label => $data) {
                $entry[$data->getName()] = $data->getData();
            }
            $finalData[] = $entry;
        }
        
        return $finalData;
    }

    /**
     * 
     * @param Pimcore\Model\DataObject $element
     * @return string
     */
    private function createDataHTMLOutput($element) {
        $data = $this->getBlockData($element);
        
        $html = "";
        
        foreach ($data as $dataEntry) {
            foreach ($dataEntry as $entry) {
                $singleLine = $entry . $this->seperator;
                $html .= $singleLine;
            }
        }
        
        return $html;
    }
    
    /**
     * 
     * @return string
     */
    private function getBlockNameFromLabel() {
        $childLabel = $this->getChilds()[0]->getLabel();
        
        $regex = '/(?<=\().+?(?=\))/m';
        
        preg_match($regex, $childLabel, $matches);
        
        return $matches[0];
    }
}
