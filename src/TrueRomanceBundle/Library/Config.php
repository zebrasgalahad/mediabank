<?php

namespace TrueRomanceBundle\Library;

/**
 * Description of Config
 *
 * @author mariobaer
 */
class Config {

    const ENVIRONMENT_DEV = "dev";
    const ENVIRONMENT_STAGE = "stage";
    const ENVIRONMENT_LIVE = "live";
    
    const SEARCH_TYPE_REFERENCE = "reference";
    const SEARCH_TYPE_PRODUCT = "product";
    const SEARCH_TYPE_PRODUCTFAMILY = "product";
    const SEARCH_TYPE_DOWNLOAD = "download";
    
    /**
     *
     * @var \Pimcore\Config 
     */
    private static $pimcoreConfig;
    
    /**
     * Gets config from system.php!
     * @param array $paths
     * @return mixed
     * @throws \Exception
     */
    public static function get($paths) {
        if(self::$pimcoreConfig === null) {
            self::$pimcoreConfig = new \Pimcore\Config();
        }

        $targetConfig = self::$pimcoreConfig->getSystemConfig()->get("trueromance")->toArray();
        
        foreach ($paths as $path) {
            if(isset($targetConfig[$path]) === false) {
                throw new \Exception("Settings missing in system.php:" . implode(' : ', $paths));
            }
            
            $targetConfig = $targetConfig[$path];
        }

        return $targetConfig;
    }

}
