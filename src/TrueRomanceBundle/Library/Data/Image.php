<?php

namespace TrueRomanceBundle\Library\Data;

use \TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

class Image {
    
    /**
     *
     * @var string 
     */
    protected $assetsDatabaseTable = "assets";
    
    /**
     * 
     * @param string $name
     * @param string $limit
     */
    public function get($name, $limit) {
        
        $explodedName = explode(" ", $name);
        
        $query = "SELECT TRIM(CONCAT_WS('', path, filename)) as full_path, id, type, filename, path, customSettings FROM {$this->assetsDatabaseTable} WHERE "
                . "type <> 'document' AND path LIKE '%/Web/%' ";
        
        if ($name && is_array($explodedName)) {
            $query .= "AND ";
            
            $data = [];
            
            foreach ($explodedName as $singleValue) {
                $data[] = " filename LIKE '%$singleValue%' ";
            }
            
            $query .= "(" . implode(" AND ", $data) . ")";
        }
        
        $query .= " LIMIT $limit";
        
        $results = Database::getDatabaseObject()->fetchAll($query);
        
        foreach ($results as $index => $result) {
            
            $dimensions = unserialize($result['customSettings']);
            
            $results[$index]["imageDimensions"] = $dimensions['imageWidth'] . "x" . $dimensions['imageHeight'];
            
            unset($results[$index]['customSettings']);
            
            $results[$index]["image_src"] = \TrueRomanceBundle\Library\Tools\Server::getBaseUrl("/web/var/assets" . $results[$index]["full_path"]);
            
            $results[$index]["imageDimensions"] = $dimensions;
        }
        
        return $results;
        
    }
}

