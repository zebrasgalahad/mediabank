<?php

namespace TrueRomanceBundle\Library\Data;

use Carbon\Carbon;
use Pimcore\Model\DataObject\Data\QuantityValue;
use TrueRomanceBundle\Library\FolderHelper;

class AbstractData
{
    const DATE_FORMAT = 'Y-m-d';

    public function getListDetails(array $list, string $locale = 'de_DE')
    {
        if (empty($list)) {
            return [];
        }

        $data = [];

        foreach ($list as $item) {
            $data[] = $this->getDetails($item, $locale);
        }

        return $data;
    }

    public static function getQuantity(QuantityValue $object = null)
    {
        if (!$object) {
            return null;
        }

        $value = $object->getValue();
        $objectValue = isset($value) ? $object->getValue() : 0;
        $objectUnit = $object->getUnit() ? $object->getUnit()->getAbbreviation() : '';
        $objectLongname = $object->getUnit() ? $object->getUnit()->getLongname() : '';

        return [
            'value' => $objectValue,
            'unit' => $objectUnit,
            'longname' => $objectLongname
        ];
    }

    public static function getSelectDisplayName($model, $fieldName, $locale = null)
    {
        $method = 'get' . ucfirst($fieldName);

        if($locale) {
            $value = $model->$method($locale);
        } else {
            $value = $model->$method();
        }

        $options = \Pimcore\Model\DataObject\Service::getOptionsForSelectField($model, $fieldName);

        return $options[$value];
    }

    public function replaceAddresses($object, $fieldName, $addresses)
    {
        $getMethod = 'get' . ucfirst($fieldName);
        $setMethod = 'set' . ucfirst($fieldName);

        $oldAddresses = $object->$getMethod();

        foreach ($addresses as $address) {
            $newAddress = \Pimcore\Model\Element\Service::cloneMe($address);
            $newAddress->setKey('address-' . uniqid());
            $addressFolder = FolderHelper::getOrCreateFolder('/addresses');
            $newAddress->setParentId($addressFolder->getId());

            $newAddress->save();

            $newAddresses[] = $newAddress;
        }

        if (empty($oldAddresses)) {
            $object->$setMethod($newAddresses);
        } else {
            foreach ($object->$getMethod() as $address) {
                $address->delete();
            }

            $object->$setMethod($newAddresses);
        }
    }

    // Create address formated HTML output
    public static function formatAddress(array $address)
    {
        $addressHtml = '';
        $firstRow = '';
        $secondRow = '';
        $thirdRow = '';

        $firstRow .= $address['street'] ? $address['street'] . ' ' : '';
        $firstRow .= $address['street_number'] ? $address['street_number']  : '';

        if (!empty($firstRow)) {
            $addressHtml .= $firstRow . ' ';
            $addressHtml .= '<br>';
        }

        $secondRow .= $address['zip'] ? $address['zip'] . ' ' : '';
        $secondRow .= $address['city'] ? $address['city']  : '';

        if (!empty($secondRow)) {
            $addressHtml .= $secondRow . ' ';
            $addressHtml .= '<br>';
        }

        $thirdRow .= $address['country'] ? $address['country'] : '';

        if (!empty($thirdRow)) {
            $addressHtml .= $thirdRow;
        }

        return $addressHtml;
    }

    /**
     * Format date
     *
     * @param [type] $date
     * @param string $locale
     * @return mixed
     */
    public static function formatDate($date, $locale = 'de_DE')
    {
        if (!$date) {
            return null;
        }

        if ($date instanceof Carbon) {
            Carbon::setLocale($locale);

            return $date->format(self::DATE_FORMAT);
        }
    }
}
