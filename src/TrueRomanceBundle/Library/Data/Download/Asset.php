<?php

namespace TrueRomanceBundle\Library\Data\Download;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\JsonResponse;
use TrueRomanceBundle\Library\Tools\Cli\Thumbnail as CustomThumbnail;

/**
 * Description of Asset
 *
 * @author lukadrezga
 */
class Asset {

    const ASSET_DOWNLOAD_TYPE_ORIGINAL = "original";
    const ASSET_DOWNLOAD_TYPE_EPS = "eps";
    const ASSET_DOWNLOAD_TYPE_HIRES_CMYK = "hires_cmyk";
    const ASSET_DOWNLOAD_TYPE_HIRES_RGB = "hires_rgb";
    const ASSET_DOWNLOAD_TYPE_OFFICE = "office";
    const ASSET_DOWNLOAD_TYPE_WEB = "web";
    const ASSET_TYPE_IMAGE = "image";
    const ASSET_TYPE_LOGO = "logo";
    const ASSET_TYPE_ICON = "icon";
    const ASSET_TYPE_SALESDOC = "salesdoc";
    const ASSET_TYPE_COLLECTION = "collection";
    const ASSET_TYPE_VIDEO = "video";
    const ASSET_TYPE_CATALOG = "catalog";

    /**
     *
     * @var string
     */
    private $zipArchiveName;

    /**
     * 
     * @var string
     */
    private $zipFileFullPath;
    
    /**
     *
     * @var array 
     */
    private $config = [
        self::ASSET_DOWNLOAD_TYPE_ORIGINAL => [
            'width' => 6000,
            'dpi' => 300,
            'format' => 'JPG',
            'quality' => 100
        ],
        self::ASSET_DOWNLOAD_TYPE_EPS => [
            'width' => 6000,
            'dpi' => 300,
            'format' => 'JPG',
            'quality' => 100
        ],
        self::ASSET_DOWNLOAD_TYPE_OFFICE => [
            'resize_mode' => 'scaleByWidth',
            'width' => 1190,
            'dpi' => 150,
            'format' => 'JPG',
            'quality' => 90
        ],
        self::ASSET_DOWNLOAD_TYPE_WEB => [
            'resize_mode' => 'scaleByWidth',
            'width' => 3500,
            'dpi' => 72,
            'format' => 'JPG',
            'quality' => 85
        ],
        self::ASSET_DOWNLOAD_TYPE_HIRES_CMYK => [
            'resize_mode' => 'scaleByWidth',
            'width' => 3500,
            'dpi' => 300,
            'format' => 'JPG',
            'quality' => 100,
            'color_type' => "CMYK"
        ],
        self::ASSET_DOWNLOAD_TYPE_HIRES_RGB => [
            'resize_mode' => 'scaleByWidth',
            'width' => 3500,
            'dpi' => 300,
            'format' => 'JPG',
            'quality' => 100,
            'color_type' => "RGB"
        ]
    ];

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    private $container;

    /**
     *
     * @var string
     */
    private $locale;

    /**
     *
     * @var \Pimcore\Translation\Translator 
     */
    private $translator;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\RequestStack 
     */
    private $request;

    /**
     *
     * @var \TrueRomanceBundle\Library\Data\Article
     */
    private $assetData;

    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;

        $this->translator = $container->get("translator");

        $this->request = $container->get("request_stack")->getCurrentRequest();

        $this->locale = $this->request->getLocale();

        $this->assetData = $container->get("trueromance.asset.data");
    }

    /**
     * 
     * @return string
     */
    function getZipArchiveName()
    {
        return $this->zipArchiveName;
    }

    /**
     * 
     * @param string $zipArchiveName
     */
    function setZipArchiveName($zipArchiveName)
    {
        $this->zipArchiveName = $zipArchiveName;
    }

    /**
     * 
     * @return string
     */
    function getZipFileFullPath()
    {
        return $this->zipFileFullPath;
    }

    /**
     * 
     * @param string $zipFileFullPath
     */
    function setZipFileFullPath($zipFileFullPath)
    {
        $this->zipFileFullPath = $zipFileFullPath;
    }

    /**
     * 
     * @param \Pimcore\Model\Asset $asset
     * @param string $type
     * @return BinaryFileResponse
     */
    public function downloadOriginalFile($assetData)
    {
        $asset = \Pimcore\Model\Asset\Image::getById($assetData["id"]);
        
        if ($asset) {
            $response = new BinaryFileResponse($asset->getFileSystemPath());
            $response->headers->set('Content-Type', $asset->getMimetype());
            $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $asset->getKey());

            return $response;
        }
    }

    /**
     * 
     * @param array $assetData
     * @param bool $isCreatingZipFile
     * @return \stdClass
     */
    public function getAssetData($assetData, $isCreatingZipFile = false)
    {
        $data = [];
        
        $assets = [];
        $tmpFilePaths = [];
        $filesToDelete = [];
        
        $pathToZipDirecory = $this->createDirectoryForZipFiles();

        $this->deleteOldZipFiles($pathToZipDirecory);
        
        foreach ($assetData as $index => $singleAssetData) {
            $assetId = $singleAssetData["id"];
            $type = $singleAssetData["type"];
            
            $image = \Pimcore\Model\Asset\Image::getById($assetId);
            
            if (in_array($type, $this->thumbnailTypeMapping()) === true && in_array($this->getAssetType($assetId), $this->assetTypeMapping()) === true) {
                if ($isCreatingZipFile === true) {
                    $filename = $this->createDownloadFileName($assetData, $this->config[$type]['format'], $index);
                } else {
                    $filename = $image->getFilename();
                }                
                
                $colorspace = 'RGB';
                $dpi = 150;
                $quality = 100;
                if($type === self::ASSET_DOWNLOAD_TYPE_HIRES_CMYK || $type === self::ASSET_DOWNLOAD_TYPE_HIRES_RGB){
                    $colorspace = strtoupper(str_replace("hires_", "", $type));
                    $dpi = 300;
                    $quality = 100;
                }
                
                if($type === self::ASSET_DOWNLOAD_TYPE_ORIGINAL){
                    $dpi = null;
                    $quality = 100;
                }                    
                
                if($type === self::ASSET_DOWNLOAD_TYPE_OFFICE){
                    $dpi = 150;
                    $quality = 100;
                }              
                
                if($type === self::ASSET_DOWNLOAD_TYPE_WEB){
                    $dpi = 72;
                    $quality = 85;
                }                   
                
                $thumbnail = CustomThumbnail::create($image, $type, true, $colorspace, $quality, $dpi);
                                
                $thumbnail->setFilename($filename);
                
                $thumbnail->setType($type);
                
                if ($isCreatingZipFile === true) {
                    $tmpFilePath = PIMCORE_TEMPORARY_DIRECTORY . "/" . $filename;
                    
                    file_put_contents($tmpFilePath, file_get_contents($thumbnail->getFilepath()));

                    $tmpFilePaths[] = $tmpFilePath;
                    $filesToDelete[] = $tmpFilePath;
                }
            } else {
                $tmpFilePath = PIMCORE_TEMPORARY_DIRECTORY . "/" . $this->assetsArticleNumberMapping($assetData)[$assetId] . "." . $this->getAssetFormat($image);
                
                if ($isCreatingZipFile === true) {
                    file_put_contents($tmpFilePath, file_get_contents($image->getFileSystemPath()));
                    $tmpFilePaths[] = $tmpFilePath;
                } else {
                    $tmpFilePaths[] = $image->getFileSystemPath();
                }

                $assets[] = $image;
            }
            
            $data[] = $thumbnail;
        }
        
        if ($isCreatingZipFile === true) {
            $this->addFilesToZip($pathToZipDirecory, $tmpFilePaths);
        }
        
        $this->deleteTmpFiles($filesToDelete);
        
        return $data;
    }
    
    /**
     * 
     * @param string $assetId
     * @param string $type
     * @return BinaryFileResponse 
     */
    public function download($assetData)
    {
        if ($this->request->get("type") === self::ASSET_DOWNLOAD_TYPE_ORIGINAL) {
            return $this->downloadOriginalFile($assetData[0]);
        } else {
            $data = $this->getAssetData($assetData);
            
            header("Content-Type: image/{$this->config["type"]["format"]}");
            
            header("Cache-Control: no-store, no-cache");
            header("Content-Disposition: attachment; filename={$data[0]->getFilename()}");

            echo readfile($data[0]->getFilepath());
        }
    }

    /**
     * 
     * @param array $data
     */
    public function downloadMultipleAssets($ids, $types)
    {
        $zipArchiveName = $this->createZipDownloadName() . ".zip";

        $this->setZipArchiveName($zipArchiveName);
        
        $data = $this->buildDataArray($ids, $types);
        
        $assetData = $this->getAssetData($data, true);
        
        $path = str_replace(PIMCORE_WEB_ROOT, "", $this->getZipFileFullPath());
        
        return new JsonResponse($path);
        
    }
    
    /**
     * 
     * @param array $ids
     * @param array $types
     * @return array
     */
    private function buildDataArray($ids, $types) 
    {
        $data = [];
        
        foreach ($ids as $index => $id) {
            $data[$index] = [
                "id" => $id,
                "type" => $types[$index]
            ]; 
        }
        
        return $data;
    }

    /**
     * 
     * @param string $zipDirectoryPath
     * @param array $filePaths
     */
    private function addFilesToZip($zipDirectoryPath, $filePaths)
    {
        $zipFilePath = $zipDirectoryPath . $this->zipArchiveName;

        $this->setZipFileFullPath($zipFilePath);

        foreach ($filePaths as $filePath) {
            $path = str_replace(" ", "\ ", $filePath);

            $command = "zip -jrv $zipFilePath $path";

            exec($command);
        }
    }

    /**
     * 
     * @return string
     */
    private function createDirectoryForZipFiles()
    {
        $path = PIMCORE_TEMPORARY_DIRECTORY . "/zip_files/";

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }

        return $path;
    }

    /**
     * 
     * @param string $path
     */
    private function deleteOldZipFiles($path)
    {
        $command = "find $path -type f -mtime +1  -name '*.zip' -execdir rm -- '{}' \;";

        exec($command);
    }

    /**
     * 
     * @param array $filePaths
     */
    private function deleteTmpFiles($filePaths)
    {
        foreach ($filePaths as $path) {
            if (is_file($path) === true) {
                unlink($path);
            }
        }
    }

    /**
     * 
     * @param string $assetId
     * @return string
     */
    private function getAssetType($assetId)
    {
        return $this->assetData->getAssetType($assetId);
    }

    /**
     * 
     * @return array
     */
    private function thumbnailTypeMapping()
    {
        $mapping = [
            self::ASSET_DOWNLOAD_TYPE_HIRES_CMYK,
            self::ASSET_DOWNLOAD_TYPE_HIRES_RGB,
            self::ASSET_DOWNLOAD_TYPE_OFFICE,
            self::ASSET_DOWNLOAD_TYPE_WEB,
            self::ASSET_DOWNLOAD_TYPE_ORIGINAL
        ];

        return $mapping;
    }

    /**
     * 
     * @return array
     */
    private function assetTypeMapping()
    {
        $mapping = [
            self::ASSET_TYPE_IMAGE,
            self::ASSET_TYPE_LOGO,
            self::ASSET_TYPE_ICON
        ];

        return $mapping;
    }

    /**
     * 
     * @param array $assetData
     * @param string $format
     * @param int $index
     * @return string
     */
    private function createDownloadFileName($assetData, $format, $index)
    {
        $assetId = $assetData[$index]["id"];
        
        $assetType = $this->assetData->getAssetType($assetId);

        $assetInstance = \Pimcore\Model\Asset::getById($assetId);

        if ($assetType === self::ASSET_TYPE_COLLECTION) {
            $asset = \Pimcore\Model\Asset::getById($assetId);
            $key = $asset->getKey();

            $key = \Pimcore\File::getValidFilename($key);

            return $key;
        }

        $articleNumber = $this->assetData->getKeyForAssetDownloadName($assetId);
        
        if ($articleNumber["is_article_asset_object"] === true) {
            if ($this->isImage($assetType) === true) {
                return $this->assetsArticleNumberMapping($assetData)[$assetId] . "." . strtolower($format);
            } else {
                return $this->assetsArticleNumberMapping($assetData)[$assetId] . "." . $this->getAssetFormat($assetInstance);
            }
        }
        
        return $assetInstance->getKey();
    }
    
    /**
     * 
     * @param array $assetData
     * @return array
     */
    private function assetsArticleNumberMapping($assetData)
    {
        $data = [];
        
        foreach ($assetData as $singleData) {
            $data[] = [
                "article_number" => $this->assetData->getKeyForAssetDownloadName($singleData["id"])["key"],
                "asset_id" => $singleData["id"]
            ];
        }
        
        $articleNumber = "";
        
        $assetCounter = 0;
        
        $finalData = [];
        
        foreach ($data as $index => $singleEntry) {
            ++$assetCounter;
            
            if ($articleNumber !== $singleEntry["article_number"] && $index > 0) {
                $assetCounter = 1;
            }
            
            $counter = str_pad($assetCounter, 2, "0", STR_PAD_LEFT);
            
            $finalData[$singleEntry["asset_id"]] = $singleEntry["article_number"] . "_$counter";
            
            $articleNumber = $singleEntry["article_number"];
        }
        
        return $finalData;
    }
    
    /**
     * 
     * @param string $type
     * @return boolean
     */
    private function isImage($type)
    {
        return in_array($type, $this->assetTypeMapping());
    }

    /**
     * 
     * @param \Pimcore\Model\Asset $asset
     * @return string
     */
    private function getAssetFormat($asset)
    {
        $filename = $asset->getFilename();

        $format = explode(".", $filename)[1];

        return $format;
    }

    /**
     * 
     * @return string
     */
    private function createZipDownloadName()
    {
        $session = $this->request->getSession();
        
        $increment = $this->createSessionCounter($session);
        
        $time = date("Y-m-d");

        $counter = str_pad($increment + 1, 4, "0", STR_PAD_LEFT);
        
        $hash = md5(\Pimcore::getContainer()->get("trueromance.user.permissions")->getUser()->getId()) . uniqid();
        
        $zipName = "SBD-ORIGINAL-{$hash}-{$time}-{$counter}";

        $this->incrementCounter($increment, $session);

        return $zipName;
    }

    /**
     * restart increment every 24 hours
     */
    private function restartIncrement($session)
    {
        if (!$session->has("date")) {
            $session->set("date", date("Y-m-d"));
        }
        
        if ($session->get("date") !== date("Y-m-d")) {
            $session->set("increment", 0);
        }
    }
    
    /**
     * 
     * @param Symfony\Component\HttpFoundation\Session\Session $session
     * @return int
     */
    private function createSessionCounter($session)
    {
        $this->restartIncrement($session);
        
        $increment = $session->has("increment") ? $session->get("increment") : 0;
        
        return $increment;
        
    }
    
    /**
     * 
     * @param int $increment
     * @param Symfony\Component\HttpFoundation\Session\Session $session
     */
    private function incrementCounter($increment, $session)
    {
        ++$increment;
        $session->set("increment", $increment);
    }
    
    /**
     * 
     * @param Symfony\Component\HttpFoundation\Session\Session $session
     */
    private function clearSession($session) 
    {
        $session->remove("date");
        $session->remove("increment");
    }

}
