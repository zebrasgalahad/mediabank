<?php

namespace TrueRomanceBundle\Library\Data;

/**
 * data for collection (meine Auswahl)
 *
 * @author lukadrezga
 */
class Collection {

    const INDEX_TABLE = "true_romance_asset_index";

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     *
     * @var string
     */
    private $locale;

    /**
     *
     * @var \Pimcore\Translation\Translator
     */
    private $translator;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $request;

    /**
     *
     * @var \TrueRomanceBundle\Library\Data\Asset
     */
    private $assetData;

    /**
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;

        $this->translator = $container->get("translator");

        $this->request = $container->get("request_stack")->getCurrentRequest();

        $this->locale = $this->request->getLocale();

        $this->assetData = $container->get("trueromance.asset.data");
    }

    /**
     *
     * @param array $assetIds
     * @return array
     */
    public function collectionIndexData($assetIds)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        if(!$assetIds) {
            return [];
        }

        $implodedIds = implode(",", $assetIds);

        $query = "SELECT object_id as asset_id, article_number, preview_web_path, preview_web_width, preview_web_height FROM " . self::INDEX_TABLE . " WHERE object_id IN ($implodedIds) AND locale = '{$this->locale}'";

        $result = $db->fetchAll($query);

        $finalData["data"] = $this->additionalCollectionData($result);

        $finalData["locale"] = explode("_", $this->locale)[0];

        return $finalData;
    }

    public function additionalCollectionData($assetsData)
    {
        foreach ($assetsData as &$singleAssetData) {
            $asset = \Pimcore\Model\Asset::getById($singleAssetData["asset_id"]);

            if (!$asset) {
                continue;
            }

            $hasAllDownloadOptions = $this->assetData->hasAssetAllDownloadOptions($asset);

            $hasEpsFile = $this->assetData->getEpsFile($singleAssetData["asset_id"]) !== NULL ? true : false;

            $downloadOptions = [];

            if ($hasAllDownloadOptions) {
                $downloadOptions["options"]["original"] = $this->translator->trans("product.detail.original.file");

                if ($hasEpsFile) {
                    $downloadOptions["options"]["eps"] = $this->translator->trans("product.detail.eps.file");
                }

                $downloadOptions["options"]["hires_cmyk"] = $this->translator->trans("product.detail.hires.cmyk.file");
                $downloadOptions["options"]["hires_rgb"] = $this->translator->trans("product.detail.hires.rgb.file");
                $downloadOptions["options"]["office"] = $this->translator->trans("product.detail.office.file");
                $downloadOptions["options"]["web"] = $this->translator->trans("product.detail.web.file");
            } else {
                $downloadOptions["options"]["original"] = $this->translator->trans("product.detail.original.file");
            }

            $singleAssetData = array_merge($singleAssetData, $downloadOptions);
        }

        return $assetsData;
    }

}
