<?php

namespace TrueRomanceBundle\Library\Data\Objects;

use Pimcore\Model\Asset;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use TrueRomanceBundle\Library\Data\AbstractData;

class UdxinfoData extends AbstractData
{
    /**
     * Get data for additional UDX
     *
     * @param DataObject\Article $article
     * @param string $field
     * @param string $locale
     * @return mixed
     */
    public static function formatData(
        DataObject\Article $article,
        string $fieldName,
        string $locale
    ) {
        $method = 'get' . ucfirst($fieldName);

        //Check if method exists on article
        if (!method_exists($article, $method)) {
            return;
        }

        $classDefinition = ClassDefinition::getById(DataObject\Article::classId());

        //Check if field exists on article object
        if ($classDefinition->getFieldDefinition($fieldName)) {
            $localizedField = false;
            $field = $classDefinition->getFieldDefinition($fieldName);
        } elseif ($classDefinition->getFieldDefinition('localizedfields')->getFieldDefinition($fieldName)) {
            $localizedField = true;
            $field = $classDefinition->getFieldDefinition('localizedfields')->getFieldDefinition($fieldName);
        } else {
            return;
        }

        if ($field instanceof Data\Input) {
            return $localizedField ? $article->$method($locale) : $article->$method();
        }

        if ($field instanceof Data\Numeric) {
            return $localizedField ? $article->$method($locale) : $article->$method();
        }

        if ($field instanceof Data\Select) {
            return self::getSelectDisplayName($article, $fieldName);
        }

        if ($field instanceof Data\BooleanSelect) {
            $data = $localizedField ? $article->$method($locale) : $article->$method();
            return $data ? 'true' : 'false';
        }

        if ($field instanceof Data\Checkbox) {
            $data = $localizedField ? $article->$method($locale) : $article->$method();
            return $data ? 'true' : 'false';
        }

        if ($field instanceof Data\ManyToOneRelation) {
            $data = $localizedField ? $article->$method($locale) : $article->$method();

            if ($data instanceof DataObject\Article) {
                return $data->getSUPPLIER_PID();
            }

            if ($data instanceof Asset) {
                return $data->getFilename();
            }
        }

        if ($field instanceof Data\Datetime || $field instanceof Data\Date) {
            $data = $localizedField ? $article->$method($locale) : $article->$method();
            return AbstractData::formatDate($data, $locale);
        }

        return;
    }
}
