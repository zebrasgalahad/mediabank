<?php

namespace TrueRomanceBundle\Library\Data;

use Pimcore\Image\Adapter\Imagick;
use Pimcore\Model\DataObject;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Index\Config;
use TrueRomanceBundle\Library\Index\Service as IndexService;
use TrueRomanceBundle\Library\Tools\Cli\Thumbnail as CustomThumbnail;

/**
 * Data needed for article detail and list view
 *
 * @author lukadrezga
 */
class Asset {

    const ASSET_TYPE_IMAGE = "image";
    const ASSET_TYPE_LOGO = "logo";
    const ASSET_TYPE_SALESDOC = "salesdoc";
    const ASSET_TYPE_COLLECTION = "collection";
    const ASSET_TYPE_VIDEO = "video";
    const ASSET_TYPE_CATALOG = "catalog";
    const ASSET_TYPE_ICON = "icon";

    const ASSET_FILE_TYPE_DOCX = "DOCX";
    const ASSET_FILE_TYPE_PDF = "PDF";
    const ASSET_FILE_TYPE_PP = "PP";
    const ASSET_FILE_TYPE_EXCEL = "Excel";
    const ASSET_FILE_TYPE_ZIP = "application/zip";

    const DEFAULT_LOCALE = "de_DE";

    private $fileTypeMapping = [
        self::ASSET_FILE_TYPE_DOCX,
        self::ASSET_FILE_TYPE_PP,
        self::ASSET_FILE_TYPE_EXCEL,
        self::ASSET_FILE_TYPE_PDF,
        self::ASSET_FILE_TYPE_ZIP
    ];

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     *
     * @var string
     */
    private $locale;

    /**
     *
     * @var \Pimcore\Translation\Translator
     */
    private $translator;

    /**
     *
     * @var \Symfony\Component\HttpFoundation\RequestStack
     */
    private $request;

    /**
     *
     * @var string
     */
    private $assetType;

    /**
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;

        $this->translator = $container->get("translator");

        $this->request = $container->get("request_stack")->getCurrentRequest();

        $this->locale = $this->request ? $this->request->getLocale() : "de_DE";
    }

    /**
     *
     * @param string $assetId
     * @return \Pimcore\Model\Asset
     */
    private function getAssetById($assetId)
    {
        $asset = \Pimcore\Model\Asset::getById($assetId);

        return $asset;
    }

    /**
     *
     * @param string $assetId
     * @return array
     */
    private function getAssetRequiredByDependencies($assetId)
    {
        $asset = $this->getAssetById($assetId);

        if(!$asset) {
            error_log("Error: Could not find asset with id {$assetId}!");

            return [];
        }

        $assetDependencies = $asset->getDependencies()->getRequiredBy();

        return $assetDependencies;
    }

    /**
     *
     * @param string $assetId
     * @return mixed NULL |\Pimcore\Model\DataObject\ArticleAssets
     */
    public function getArticleAssetObject($assetId)
    {
        $assetDependencies = $this->getAssetRequiredByDependencies($assetId);

        if(count($assetDependencies) === 0) {
            return NULL;
        }

        $articleAssetObjectId = $assetDependencies[0]["id"];

        if ($this->isObjectTypeCatalogAsset($articleAssetObjectId) === false) {

            $articleAssetObject = DataObject::getById($articleAssetObjectId);

            return $articleAssetObject;
        }

        return NULL;
    }

    /**
     *
     * @param string $assetId
     * @return mixed NULL | \Pimcore\Model\DataObject\Article
     */
    public function getArticleObject($assetId)
    {
        if ($this->getArticleAssetObject($assetId) !== NULL) {

            return $this->getArticleAssetObject($assetId)->getParent();
        }

        return NULL;
    }

    /**
     *
     * @param string $assetId
     * @return \Pimcore\Model\DataObject\CatalogAssets
     */
    private function getCatalogAssetObject($assetId)
    {
        $dependencies = $this->getAssetRequiredByDependencies($assetId);

        return \Pimcore\Model\DataObject\CatalogAssets::getById($dependencies[0]["id"]);
    }

    /**
     *
     * @param string $assetId
     * @return array
     */
    public function getKeyForAssetDownloadName($assetId)
    {
        $data = [];

        if ($this->getArticleObject($assetId) !== NULL) {

            $data["is_article_asset_object"] = true;
            $data["key"] = $this->getArticleObject($assetId)->getSUPPLIER_PID();

            return $data;
        }

        $asset = $this->getAssetById($assetId);

        $key = $asset->getKey();

        $data["is_article_asset_object"] = false;
        $data["key"] = $key;

        return $data;
    }

    /**
     *
     * @param string $assetId
     * @return array
     */
    public function getDetailData($assetId)
    {
        $articleObject = $this->getArticleObject($assetId);
        /* @var $articleObject Pimcore\Model\DataObject\Article */

        $asset = $this->getAssetById($assetId);

        if ($this->isCollection($assetId, $this->locale) === true) {
            return $this->getCollectionData($assetId, $this->locale);
        }

        if ($articleObject === NULL) {
            return $this->catalogAssetDetailData($assetId);
        }

        $this->assetType = $this->getAssetType($assetId);

        $articleAssetData = $this->getArticleAssetData($assetId);

        $assetData = $this->getAssetData($assetId);

        $categories = $this->getArticleCategories($articleObject);

        $brand = \TrueRomanceBundle\Library\FieldDefinitions::getFieldDefinitionsByKey($articleObject, "Brand", $articleObject->getBrand());

        $catalog = $this->getArticleAssetCatalog($assetId);

        $catalogName = $catalog ? $catalog->getLIST_PREVIEW_NAME() : "";

        $newFromDate = strtotime(explode(" ", $articleObject->getNew_from()->date)[0]);

        $date = date("j - F", $newFromDate);

        $date = explode(" - ", $date);

        $date[1] = $this->translator->trans($date[1]);

        $translatedDate = implode(" - ", $date);

        $images = $this->isAssetTypeFile($asset) === true ? $this->getAssetTypeFileImagesData($asset) : $this->createDetailViewThumnailImages($asset);

        $data = [];

        $data["asset_id"] = $assetId;
        $data["article_object_id"] = $articleObject->getId();
        $data["description_short"] = $articleObject->getDESCRIPTION_SHORT($this->locale);
        $data["description_long"] = $articleObject->getDESCRIPTION_LONG($this->locale);
        $data["ean_number"] = $articleObject->getEAN();
        $data["article_number"] = $articleObject->getSUPPLIER_PID();
        $data["new_from"] = $translatedDate;
        $data["marke"] = $brand;
        $data["catalog_name"] = $catalogName;
        $data["categories"] = $categories;
        $data["author"] = $articleAssetData["author"];
        $data["source"] = $articleAssetData["source"];
        $data["copyright"] = $articleAssetData["copyright"];
        $data["has_eps_file"] = $this->getEpsFile($assetId) !== NULL ? true : false;
        $data["show_all_download_options"] = $this->hasAssetAllDownloadOptions($asset);
        $data["is_article_asset"] = $this->getArticleAssetObject($assetId) !== NULL ? true : false;
        $data["has_file_info"] = $this->hasFileInfo();
        $data["is_collection"] = $this->isCollection($assetId, $this->locale);

        $data = array_merge($data, $images);

        $data = array_merge($data, $assetData);

        $data = array_merge($data, $this->assetDownloadUrlMapping());

        return $data;
    }

    /**
     *
     * @param string $assetId
     * @return array
     */
    public function catalogAssetDetailData($assetId)
    {
        $this->assetType = $this->getAssetType($assetId);

        $catalogAssetObject = $this->getCatalogAssetObject($assetId);

        $asset = $this->getAssetById($assetId);

        $assetData = $this->getAssetData($assetId);

        $catalogAssetData = $this->getCatalogAssetData($assetId, $this->locale);

        $images = $this->isAssetTypeFile($asset) === true ? $this->getAssetTypeFileImagesData($asset) : $this->createDetailViewThumnailImages($asset);

        $data = [];

        $data["asset_id"] = $assetId;
        $data["catalog_asset_id"] = $catalogAssetObject ? $catalogAssetObject->getId() : NULL;
        $data["catalog_name"] = $catalogAssetObject ? $this->getCatalogAssetCatalogName($catalogAssetObject) : NULL;
        $data["show_all_download_options"] = $this->hasAssetAllDownloadOptions($asset);
        $data["is_article_asset"] = $this->getArticleAssetObject($assetId) !== NULL ? true : false;
        $data["has_file_info"] = $this->hasFileInfo();

        $data = array_merge($data, $images);

        $data = array_merge($data, $catalogAssetData);

        $data = array_merge($data, $assetData);

        $data = array_merge($data, $this->assetDownloadUrlMapping());

        return $data;
    }

    private function createDetailViewThumnailImages(\Pimcore\Model\Asset $asset)
    {
        $thumbnails = [];

        if ($this->hasFileTypeDownloadOption([self::ASSET_TYPE_IMAGE, self::ASSET_TYPE_LOGO, self::ASSET_TYPE_ICON]) === true) {
            $assetDetailFullThumbnail = CustomThumbnail::create($asset, "assetDetailFull");

            $assetDetailThumbnail = CustomThumbnail::create($asset, "assetDetailThumbnail");

            $thumbnails = [
                "asset_path" => $assetDetailFullThumbnail->getWebpath(),
                "asset_detail_full_path" => $assetDetailThumbnail->getWebpath(),
                "has_full_image" => true
            ];

            return $thumbnails;
        }

        return $thumbnails = [
            "asset_path" => Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage(),
            "has_full_image" => false
        ];
    }

    /**
     *
     * @param string $assetId
     * @return \Pimcore\Model\DataObject\Catalog
     */
    private function getArticleAssetCatalog($assetId)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $query = "SELECT object_path FROM true_romance_asset_index WHERE object_id = ?";

        $objectPath = $db->fetchOne($query, [$assetId]);

        $objectPath = ltrim($objectPath, "/");

        $objectPath = explode("/", $objectPath)[0];

        $catalogParentFolder = \Pimcore\Config::getWebsiteConfig()->configuration->getCatalogs_Base_Folder();

        $catalogParentFolderPath = "/{$catalogParentFolder->getKey()}/";

        $catalogObject = \Pimcore\Model\DataObject\Catalog::getByPath("$catalogParentFolderPath" . "$objectPath/");

        return $catalogObject;
    }

    /**
     *
     * @param \Pimcore\Model\DataObject\CatalogAssets $catalogAsset
     * @return string
     */
    private function getCatalogAssetCatalogName(\Pimcore\Model\DataObject\CatalogAssets $catalogAsset)
    {
        $assetsDirectoryId = $catalogAsset->getParentId();

        $catalogId = \Pimcore\Model\DataObject\Folder::getById($assetsDirectoryId)->getParentId();

        $catalog = \Pimcore\Model\DataObject\Catalog::getById($catalogId);

        $catalogName = $catalog->getLIST_PREVIEW_NAME();

        return $catalogName;
    }

    /**
     *
     * @param \Pimcore\Model\DataObject\Article $article
     * @return array
     */
    private function getArticleCategories(\Pimcore\Model\DataObject\Article $article)
    {
        $categories = [];
        while ($article->getParentId() !== 1) {
            $article = \Pimcore\Model\DataObject::getById($article->getParentId());

            if ($article instanceof \Pimcore\Model\DataObject\CatalogCategory) {
                if(!$article->getEXCLUDE_FROM_CATEGORY_TREE()) {
                    $categories[] = $article->getGROUP_NAME($this->locale) ? $article->getGROUP_NAME($this->locale) : $article->getGROUP_NAME(self::DEFAULT_LOCALE);
                }
            }
        }

        return $categories;
    }

    /**
     *
     * @param string $assetId
     * @return array
     */
    private function getArticleAssetData($assetId)
    {
        $articleAsset = $this->getArticleAssetObject($assetId);

        $fieldCollectionData = $this->getAssetObjectRelationData($articleAsset, $assetId);

        $data = [];

        if ($fieldCollectionData === NULL) {
            return $data;
        }

        $data["author"] = $fieldCollectionData['author'] ? $fieldCollectionData['author'] : '';
        $data["source"] = $fieldCollectionData['source'] ? $fieldCollectionData['source'] : '';
        $data["copyright"] = $fieldCollectionData['copyright'] ? $fieldCollectionData['copyright'] : '';

        return $data;
    }

    /**
     *
     * @param string $assetId
     * @param string $locale
     * @return array
     */
    public function getCatalogAssetData($assetId, $locale)
    {
        $data = [];

        $assetDependencies = $this->getAssetRequiredByDependencies($assetId);

        if(count($assetDependencies) === 0) {
            return $data;
        }

        $catalogAssetObject = \Pimcore\Model\DataObject::getById($assetDependencies[0]["id"]);

        $fieldCollectionData = $this->getAssetObjectRelationData($catalogAssetObject, $assetId);

        if ($fieldCollectionData === NULL) {
            return $data;
        }

        $data["title"] = $catalogAssetObject->getAsset_title($locale);
        $data["article_number"] = $catalogAssetObject->getSupplier_pid($locale);
        $data["description_long"] = $catalogAssetObject->getDESCRIPTION_LONG($locale);
        $data["description_short"] = $catalogAssetObject->getDESCRIPTION_SHORT($locale);
        $data["description_meta"] = $catalogAssetObject->getDESCRIPTION_META($locale);
        $data["author"] = $fieldCollectionData['author'];
        $data["source"] = $fieldCollectionData['source'];
        $data["copyright"] = $fieldCollectionData['copyright'];
        $data["is_collection"] = $this->isCollection($assetId, $this->locale);

        return $data;
    }

    /**
     *
     * @param string $assetId
     * @param string $locale
     * @return array
     */
    public function getCollectionData($assetId, $locale)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $query = "SELECT * FROM true_romance_asset_index WHERE asset_type = ? AND object_id = ? AND locale = ?";

        $result = $db->fetchRow($query, ["collection", $assetId, $locale]);

        $asset = $this->getAssetById($assetId);

        $images = $this->isAssetTypeFile($asset) === true ? $this->getAssetTypeFileImagesData($asset) : $this->createDetailViewThumnailImages($asset);

        $data = [];

        $data["asset_id"] = $result["object_id"];
        $data["description_short"] = $result["name"];
        $data["description_long"] = $result["description_short"];
        $data["show_all_download_options"] = $this->hasAssetAllDownloadOptions($this->getAssetById($assetId));
        $data["is_article_asset"] = $this->getArticleAssetObject($assetId) !== NULL ? true : false;
        $data["has_file_info"] = $this->hasFileInfo();
        $data["is_collection"] = true;

        $articleAssetData = $this->getArticleAssetData($assetId);

        $data = array_merge($data, json_decode($result["additional_data"], true));

        $data = array_merge($data, $this->assetDownloadUrlMapping());

        $data = array_merge($data, $images);

        $data = array_merge($data, $this->getAssetData($assetId));

        return $data;
    }

    /**
     *
     * @param string $assetId
     * @param string $locale
     * @return boolean
     */
    private function isCollection($assetId, $locale)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $query = "SELECT object_id FROM true_romance_asset_index WHERE asset_type = ? AND object_id = ? AND locale = ?";

        $result = $db->fetchRow($query, ["collection", $assetId, $locale]);

        if (!empty($result)) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param \Pimcore\Model\DataObject $object
     * @param string $assetId
     * @return mixed
     */
    private function getFieldCollectionData($object, $assetId)
    {
        $assetFileAttributes = \TrueRomanceBundle\Library\Index\Config::$assetFileAttributes;

        foreach ($assetFileAttributes as $attribute) {
            $getter = "get" . ucfirst($attribute["field"]);

            if (method_exists($object, $getter) === false) {
                continue;
            }

            if ($object->$getter() !== NULL) {
                foreach ($object->$getter() as $fieldCollectionAsset) {
                    $assetRelation = $fieldCollectionAsset->getAsset_relation();

                    if ($assetRelation === NULL) {
                        continue;
                    }

                    if ($assetRelation->getType() === "asset") {
                        $id = $assetRelation->getData()->getId();
                    } else {
                        $id = $assetRelation->getId();
                    }

                    if ((int) $assetId === $id) {
                        return $fieldCollectionAsset;
                    }
                }
            }
        }

        return NULL;
    }

    /**
     *
     * @param \Pimcore\Model\DataObject $object
     * @param string $assetId
     * @return mixed
     */
    private function getAssetObjectRelationData($object, $assetId)
    {
        $assetObjectRelation = $object->getAsset_relation($this->locale);

        $data = [
            'author' => $object->getAuthor(),
            'source' => $object->getSource(),
            'copyright' => $object->getCopyright()
        ];

        if ($assetObjectRelation && $assetObjectRelation->getId() == $assetId) {
            $data['asset_relation'] = $assetObjectRelation;

            return $data;
        }

        return $data;
    }

    /**
     *
     * @param string $assetId
     * @return array
     */
    private function getAssetData($assetId)
    {
        $asset = $this->getAssetById($assetId);
        /* @var $asset Pimcore\Model\Asset */

        $data = [];

        $assetPath = $asset->getFileSystemPath();

        if (is_file($assetPath) === false) {
            return $data;
        }

        $assetExifData = [];

        if(method_exists($asset, "getEXIFData") === true) {
            $assetExifData = $asset->getEXIFData();
        }

        $data["filesize"] = formatBytes($asset->getFileSize());

        $data["has_image_data"] = false;

        if (!empty($assetExifData)) {
            // TODO: Fix later for Pimcore version 6
//            $imagic = new \Imagick($assetPath);
//
//            $assetResolution = $imagic->getimageresolution();
//            $dpi = $assetResolution["x"];
            $dpi = 1000;

            $assetCmMeasurements = $this->assetSizeInCm($asset->getWidth(), $asset->getHeight(), $dpi);

            $data["image_width_px"] = $asset->getWidth();
            $data["image_height_px"] = $asset->getHeight();
            $data["dpi"] = $dpi;
            $data["image_width_cm"] = $assetCmMeasurements["width_cm"];
            $data["image_height_cm"] = $assetCmMeasurements["height_cm"];
            $data["has_image_data"] = true;
        }

        return $data;
    }

    /**
     *
     * @param int $width
     * @param int $height
     * @param int $dpi
     * @return array
     */
    private function assetSizeInCm($width, $height, $dpi)
    {
        $data = [];

        $widthInCm = ($width * 2.54) / $dpi;

        $heightInCm = ($height * 2.54) / $dpi;

        $data["width_cm"] = floatval(number_format($widthInCm, 2));
        $data["height_cm"] = floatval(number_format($heightInCm, 2));

        return $data;
    }

    /**
     *
     * @return array
     */
    private function assetDownloadUrlMapping()
    {
        $mapping = [
            "original_file_download_url" => "/{$this->locale}/download/asset",
            "eps_file_download_url" => "/{$this->locale}/download/asset",
            "hires_cmyk_file_download_url" => "/{$this->locale}/download/asset",
            "hires_rgb_file_download_url" => "/{$this->locale}/download/asset",
            "office_file_download_url" => "/{$this->locale}/download/asset",
            "web_file_download_url" => "/{$this->locale}/download/asset"
        ];

        return $mapping;
    }

    /**
     *
     * @param string $assetId
     *
     * @return mixed \Pimcore\Model\Asset | NULL
     */
    public function getEpsFile($assetId)
    {
        $articleAsset = $this->getArticleAssetObject($assetId);

        if (!$articleAsset) {
            return NULL;
        }

        $fieldCollectionData = $this->getAssetObjectRelationData($articleAsset, $assetId);

        $image = $fieldCollectionData['asset_relation'];

        if ($this->hasFileTypeDownloadOption([self::ASSET_TYPE_IMAGE, self::ASSET_TYPE_LOGO, self::ASSET_TYPE_ICON]) && $image && $image->getMimetype() === "application/postscript") {
            return $image;
        }

        return NULL;
    }

    /**
     *
     * @param string $assetId
     * @return string
     */
    public function getAssetType($assetId)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $facettTable = "true_romance_asset_index";

        $query = "SELECT asset_type FROM $facettTable WHERE object_id = ?";

        $assetType = $db->fetchOne($query, [$assetId]);

        return $assetType;
    }

    /**
     *
     * @return boolean
     */
    public function hasAssetAllDownloadOptions($asset)
    {
        $this->assetType = $this->getAssetType($asset->getId());

        $isEpsFile = $this->isAssetEpsFile($asset);

        if ($isEpsFile === false && $this->hasFileTypeDownloadOption([self::ASSET_TYPE_IMAGE, self::ASSET_TYPE_LOGO])) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param mixed \Pimcore\Model\Asset | \Pimcore\Model\Asset\Image $asset
     * @return boolean
     */
    public function isAssetEpsFile($asset)
    {
        $mimeType = $asset->getMimetype();

        if ($mimeType === "application/postscript") {
            return true;
        }

        return false;
    }

    /**
     *
     * @param string $objectId
     * @return boolean
     */
    private function isObjectTypeCatalogAsset($objectId)
    {
        $object = \Pimcore\Model\DataObject::getById($objectId);

        if ($object instanceof \Pimcore\Model\DataObject\CatalogAssets) {
            return true;
        }

        return false;
    }

    /**
     *
     * @param array $assetTypes
     * @return boolean
     */
    private function hasFileTypeDownloadOption($assetTypes)
    {
        foreach ($assetTypes as $type) {
            if ($type === $this->assetType) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @return boolean
     */
    private function hasFileInfo()
    {
        $mapping = [
            "image" => self::ASSET_TYPE_IMAGE,
            "icon" => self::ASSET_TYPE_ICON,
            "logo" => self::ASSET_TYPE_LOGO
        ];

        foreach ($mapping as $assetType) {
            if ($this->assetType === $assetType) {
                return true;
            }
        }

        return false;
    }

    /**
     *
     * @param \Pimcore\Model\Asset $asset
     * @return array
     */
    private function getAssetTypeFileImagesData(\Pimcore\Model\Asset $asset)
    {
        return $data = [
            "asset_path" => $this->getAssetTypeFileImage($asset),
            "asset_detail_full_path" => $this->getAssetTypeFileImage($asset),
            "has_full_image" => true
        ];
    }

    /**
     *
     * @param \Pimcore\Model\Asset $asset
     * @return array
     */
    public function getAssetTypeFileImage(\Pimcore\Model\Asset $asset)
    {
        $assetMimeType = $asset->getMimetype();

        $fileTypeImage = Config::getAssetTypeFileImage($assetMimeType);

        return $fileTypeImage ? $fileTypeImage : Config::ASSET_ORIGINAL_PREFIX_PATH . Config::getAssetPlaceholderImage();
    }

    /**
     *
     * @param \Pimcore\Model\Asset $asset
     * @return string
     */
    private function getAssetFileType(\Pimcore\Model\Asset $asset)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $facettTable = "true_romance_asset_index";

        $query = "SELECT file_type FROM $facettTable WHERE object_id = ?";

        $fileType = $db->fetchOne($query, [$asset->getId()]);

        return $fileType;
    }

    /**
     *
     * @param \Pimcore\Model\Asset $asset
     * @return boolean
     */
    public function isAssetTypeFile(\Pimcore\Model\Asset $asset)
    {
        return in_array($this->getAssetFileType($asset), $this->fileTypeMapping);
    }

}
