<?php

namespace TrueRomanceBundle\Library\User;

use Pimcore\Http\Request\Resolver\EditmodeResolver;
use Symfony\Component\HttpFoundation\Request;


/**
 * User Permissions
 *
 * @author lukadrezga
 */
class Permissions {

    const permissions = [
        "invite_users" => "invite_users",
        "view_mediathek" => "view_mediathek"
    ];
    const INDEX_TABLE = "true_romance_asset_index";
    const ALBUM_TABLE = "true_romance_album";
    const ALBUM_ASSET_TABLE = "true_romance_album_asset";

    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    private $container;

    /**
     *
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage
     */
    private $securityToken;

    /**
     *
     * @var \Pimcore\Model\DataObject\Customer
     */
    private $user;

    /**
     *
     * @var \Pimcore\Db\Connection
     */
    private $database;

    
    /**
     *
     * @var EditmodeResolver 
     */
    private $editmodeResolver;
    
    /**
     *
     * @var Request 
     */
    private $currentRequest;
    
    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;

        $this->editmodeResolver = $this->container->get("Pimcore\Http\Request\Resolver\EditmodeResolver");
        
        $this->currentRequest = $this->container->get("request_stack")->getCurrentRequest();
        
        $this->securityToken = $container->get("security.token_storage");

        $this->database = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        if ($this->securityToken && $this->securityToken->getToken()) {
            $this->user = $this->securityToken->getToken()->getUser();
        }
    }

    /**
     * 
     * @return \Pimcore\Model\DataObject\Customer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * 
     * @return boolean
     */
    public function isLoggedIn()
    {
        if (!is_object($this->user)) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @return array
     */
    private function getUserGroupPermissions()
    {
        $permissions = [];

        if ($this->isLoggedIn()) {
            $userGroups = $this->user->getUserGroup();

            if (empty($userGroups) === false) {
                foreach ($userGroups as $group) {
                    $userGroupPermissions = $group->getUserPermission();

                    if ($userGroupPermissions) {
                        $fieldCollectionItems = $userGroupPermissions->getItems();

                        foreach ($fieldCollectionItems as $item) {
                            foreach (self::permissions as $permission) {
                                $getter = "get" . ucfirst($permission);

                                $permissions[$permission] = $item->$getter();
                            }
                        }
                    }
                }
            }
        }

        return $permissions;
    }

    /**
     * 
     * @return array
     */
    public function getUserIndividualPermissions()
    {
        $permissions = [];

        if ($this->isLoggedIn()) {
            $userPermissions = $this->user->getUserPermission();

            if ($userPermissions) {
                foreach ($userPermissions->getItems() as $item) {
                    foreach (self::permissions as $permission) {
                        $getter = "get" . ucfirst($permission);

                        $permissions[$permission] = $item->$getter();
                    }
                }
            }
        }

        return $permissions;
    }

    public function isEditMode()
    {
        return $this->editmodeResolver->isEditMode($this->currentRequest);
    }
    
    /**
     * 
     * @return array
     */
    private function getUserIndividualCatalogObjects()
    {
        $userCatalogObjects = [];

        if ($this->isLoggedIn()) {
            $userCatalogs = $this->user->getUserCatalogs();

            if ($userCatalogs) {
                foreach ($userCatalogs->getItems() as $catalogs) {
                    foreach ($catalogs->getCatalogs() as $catalog) {
                        $userCatalogObjects[] = $catalog;
                    }
                }
            }
        }

        return $userCatalogObjects;
    }

    /**
     * 
     * @return array
     */
    private function getUserGroupCatalogObjects()
    {
        $userCatalogOBjects = [];

        if ($this->isLoggedIn()) {
            $userGroups = $this->user->getUserGroup();

            if (empty($userGroups) === false) {
                foreach ($userGroups as $group) {
                    $userCatalogs = $group->getUserCatalogs()->getItems();

                    foreach ($userCatalogs as $catalogs) {
                        foreach ($catalogs->getCatalogs() as $catalog) {
                            $userCatalogOBjects[] = $catalog;
                        }
                    }
                }
            }
        }

        return $userCatalogOBjects;
    }

    /**
     * 
     * @return array
     */
    public function getUserIndividualCatalogList()
    {
        $userCatalogList = [];

        if ($this->isLoggedIn()) {
            $userCatalogs = $this->getUserIndividualCatalogObjects();

            if ($userCatalogs) {
                foreach ($userCatalogs as $catalogs) {
                    $userCatalogList[$catalogs->getKey()] = $catalogs->getLIST_PREVIEW_NAME();
                }
            }
        }

        return $userCatalogList;
    }

    /**
     * 
     * @return array
     */
    public function getUserGroupCatalogList()
    {
        $groupCatalogList = [];

        if ($this->isLoggedIn()) {
            $userCatalogs = $this->getUserGroupCatalogObjects();

            if ($userCatalogs) {
                foreach ($userCatalogs as $catalogs) {
                    $groupCatalogList[$catalogs->getKey()] = $catalogs->getLIST_PREVIEW_NAME();
                }
            }
        }

        return $groupCatalogList;
    }

    public function getAllowedCountries()
    {
        return $this->user->getCountries();
    }
    
    /**
     * 
     * @param string $key
     * @return boolean
     */
    public function isCatalogAllowed($key)
    {
        $userIndividualCatalogList = $this->getUserIndividualCatalogList();

        $userGroupCatalogList = $this->getUserGroupCatalogList();

        if (empty($userIndividualCatalogList) === true && empty($userGroupCatalogList) === true) {
            return false;
        }

        if (empty($userIndividualCatalogList) === false) {
            if ($userIndividualCatalogList[$key] !== NULL) {
                return true;
            }
        } else {
            if ($userGroupCatalogList[$key] !== NULL) {
                return true;
            }
        }

        return false;
    }

    /**
     * 
     * @param string $key
     * @return boolean
     */
    public function isAllowed($key)
    {
        $permissions = [];

        if (empty($this->getUserIndividualPermissions()) === true && empty($this->getUserGroupPermissions()) === true) {
            return false;
        }

        if (empty($this->getUserIndividualPermissions()) === false) {
            $permissions = $this->getUserIndividualPermissions();
        } else {
            $permissions = $this->getUserGroupPermissions();
        }

        return $permissions[$key];
    }

    /**
     * 
     * @param string $assetId
     * @return boolean
     */
    public function isAssetAllowed($assetId)
    {
        $userIndividualCatalogs = $this->getUserIndividualCatalogObjects();
        
        $userGroupCatalogs = $this->getUserGroupCatalogObjects();

        if (empty($userIndividualCatalogs) === true && empty($userGroupCatalogs) === true) {
            return false;
        }
        
        if (!empty($userIndividualCatalogs)) {
            foreach ($userIndividualCatalogs as $individualCatalog) {
                if ($this->getAssetIdByCatalogId($individualCatalog->getId(), $assetId) !== false) {
                    return true;
                }
            }
        } else {
            foreach ($userGroupCatalogs as $groupCatalog) {
                if ($this->getAssetIdByCatalogId($groupCatalog->getId(), $assetId) !== false) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * 
     * @param string $albumId
     * @param string $assetId
     * @return boolean
     */
    public function isAlbumAllowed($albumId, $assetId)
    {
        return $this->getAssetByAlbumId($albumId, $assetId) !== false;
    }

    /**
     * 
     * @param int $catalogId
     * @param string $assetId
     * @return string
     */
    private function getAssetIdByCatalogId($catalogId, $assetId)
    {
        $query = "SELECT object_id FROM " . self::INDEX_TABLE . " WHERE catalog_id = ? AND object_id = ?";
        
        $object_id = $this->database->fetchOne($query, [$catalogId, $assetId]);

        return $object_id;
    }

    public function assetIsInIndex($assetId)
    {
        $query = "SELECT object_id FROM " . self::INDEX_TABLE . " WHERE object_id = ?";
        
        $object_id = $this->database->fetchOne($query, [$assetId]);

        return $object_id !== false;        
    }
    
    public function getImplodedAllowedCatalogIds()
    {
        return implode(",", $this->getAllowedCatalogIds());
    }
    
    public function getAllowedCatalogIds()
    {
        $userIndividualCatalogs = $this->getUserIndividualCatalogObjects();
        
        $userGroupCatalogs = $this->getUserGroupCatalogObjects();  
        
        $resultIds = [];
        foreach ($userIndividualCatalogs as $catalog) {
            if(in_array($catalog->getId(), $resultIds) === false) {
                $resultIds[] = $catalog->getId();
            }
        }
        
        foreach ($userGroupCatalogs as $catalog) {
            if(in_array($catalog->getId(), $resultIds) === false) {
                $resultIds[] = $catalog->getId();
            }
        }    
        
        return $resultIds;
    }
    
    /**
     * 
     * @param string $albumId
     * @param string $assetId
     * @return string
     */
    private function getAssetByAlbumId($albumId, $assetId)
    {
        $query = "SELECT aa.asset_id FROM " . self::ALBUM_ASSET_TABLE . " aa, " . self::ALBUM_TABLE . " al "
                . "WHERE al.public = ? "
                . "AND al.album_id = ? "
                . "AND aa.asset_id = ? "
                . "AND aa.album_id = al.album_id";

        $id = $this->database->fetchOne($query, [true, $albumId, $assetId]);

        return $id;
    }

    /**
     * 
     * @return array
     */
    public function userPermissionFrontendData()
    {
        $data = [];

        $userIndividualPermissions = $this->getUserIndividualPermissions();

        $userGroupPermissions = $this->getUserGroupPermissions();

        if (empty($userIndividualPermissions) === true && empty($userGroupPermissions) === true) {
            $this->handleNoPermissionsCase($data);
            return $data;
        }

        if (!empty($userIndividualPermissions)) {
            return $userIndividualPermissions;
        } else {
            return $userGroupPermissions;
        }
    }

    /**
     * 
     * @param array $permissions
     */
    public function handleNoPermissionsCase(&$permissions)
    {
        foreach (self::permissions as $key => $permission) {
            $permissions[$key] = false;
        }
    }

}
