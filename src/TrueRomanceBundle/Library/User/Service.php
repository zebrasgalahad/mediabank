<?php

namespace TrueRomanceBundle\Library\User;

use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

class Service
{

    public function suggestUser($string, $exludeUserId)
    {
        $userInstance = new \Pimcore\Model\DataObject\Customer();

        $classId = $userInstance->getClassId();
        
        $query = "SELECT oo_id as id, email as text FROM object_query_$classId WHERE (firstname LIKE ? OR lastname LIKE ? OR email LIKE ?) AND active = 1 AND oo_id != $exludeUserId LIMIT 20";
        
        $result = Database::get()->fetchAll($query, ["%$string%", "%$string%", "%$string%"]);

        return [
            "total_count" => count($result),
            "incomplete_results" => false,
            "items" => $result
        ];
    }
}
