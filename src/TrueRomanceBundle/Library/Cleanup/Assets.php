<?php


namespace TrueRomanceBundle\Library\Cleanup;


use Pimcore\Model\Asset;
use Pimcore\Model\DataObject\Article;
use Pimcore\Model\DataObject\AssetFile;
use Pimcore\Model\DataObject\AssetImage;
use Pimcore\Model\DataObject\AssetVideo;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Database\AbstractDatabase;
use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;

class Assets
{
    /**
     * @param array $data
     */
    public static function cleanup($data = [])
    {
        $locale = $data['locale'];

        if (!array_key_exists("SUPPLIER_PID", $data) || empty($data) || trim($data["SUPPLIER_PID"]) == "") {
            return;
        }

        $supplierPid = $data["SUPPLIER_PID"];

        $articleInstance = Article::getBySUPPLIER_PID($supplierPid, 1);

        if (!$articleInstance) {
            CliHelper::error("ERROR: No Article with SUPPLIER_PID: {$supplierPid} found!");
            return;
        }

        $articleAssets = $articleInstance->getChildren();

        if (empty($articleAssets)) {
            CliHelper::error("ERROR: Article with SUPPLIER_PID: {$supplierPid} and object id: {$articleInstance->getId()} has no article asset objects!");
            return;
        }

        $assetIdsToDelete = [];

        $db = AbstractDatabase::get();

        foreach ($articleAssets as $articleAsset) {
            $articleAssetClassId = $articleAsset->getO_classId();
            $articleAssetId = $articleAsset->getId();

            $query = "SELECT asset_relation, language FROM object_localized_data_{$articleAssetClassId} WHERE ooo_id = '$articleAssetId'";

            if ($articleAssetClassId == "AssetFile") {
                $query = "SELECT dest_id, position FROM object_relations_{$articleAssetClassId} WHERE src_id = '$articleAssetId'";
            }

            $assetIds = $db->fetchAll($query);

            $arrayKey = $articleAssetClassId == 'AssetFile' ? 'position' : 'language';

            $isAssetSetByMultipleLocales = self::checkIfSameAssetIsSetByMultipleLocales($assetIds, $articleAssetClassId);

            if ($isAssetSetByMultipleLocales) {
                CliHelper::error("ERROR: Same asset is set by multiple locales in $articleAssetClassId object with ID: {$articleAsset->getId()} (Article ID: {$articleInstance->getId()}). Please check one more time if you really want to delete this asset!");
                continue;
            }

            if (empty($assetIds)) {
                continue;
            }

            // there is more than one locale set, delete assets and data only for this locale from article asset object
            if (count($assetIds) > 1) {
                if ($articleAsset->getAsset_relation($locale)) {
                    $assetIdsToDelete[] = $articleAsset->getAsset_relation($locale)->getId();
                }
                self::deleteData($articleAsset, $locale);
                CliHelper::success("SUCCESS: Deleted article asset ({$articleAsset->getId()}) data for locale $locale");
            } else {
                // if asset is set only for one locale and this locale == $locale, delete article asset object
                if ($assetIds[0][$arrayKey] == $locale) {
                    if ($articleAsset->getAsset_relation($locale)) {
                        $assetIdsToDelete[] = $articleAsset->getAsset_relation($locale)->getId();
                    }

                    CliHelper::success("SUCCESS: Deleted article asset object with ID: {$articleAsset->getId()}");
                    $articleAsset->delete();
                }
            }
        }

        if (!empty($assetIdsToDelete)) {
            self::deleteAssets($assetIdsToDelete);
        }
    }

    /**
     * @param array $assetIds
     */
    private static function checkIfSameAssetIsSetByMultipleLocales($assetIds = [], $assetType)
    {
        $arrayKey = $assetType == 'AssetFile' ? 'dest_id' : 'asset_relation';

        foreach ($assetIds as $current_key => $current_array) {
            foreach ($assetIds as $search_key => $search_array) {
                if ($search_array[$arrayKey] !== null && $search_array[$arrayKey] == $current_array[$arrayKey]) {
                    if ($search_key != $current_key) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @param AssetImage|AssetFile|AssetVideo $articleAsset
     * @param $locale
     */
    private static function deleteData($articleAsset, $locale)
    {
        $assetType = $articleAsset->getO_classId();

        $articleAsset->setAsset_relation(null, $locale)->setAsset_title(null, $locale)
                        ->setSupplier_pid(null, $locale)->setDESCRIPTION_SHORT(null, $locale)
                        ->setDESCRIPTION_LONG(null, $locale)->setDESCRIPTION_META(null, $locale)
                        ->setItem_bmecat(false, $locale)->setItem_mediathek(false, $locale)
                        ->setAllowed_countries(null, $locale)->setAsset_type(null, $locale)
                        ->setAuthor(null)->setSource(null)->setCopyright(null);

        if ($assetType == "AssetFile") {
            $articleAsset->setSalesdoc_type(null, $locale);
        }

        if ($assetType == "AssetImage") {
            $articleAsset->setImage_type(null, $locale)->setImage_orientation(null, $locale);
        }

        $articleAsset->save();
    }

    /**
     * @param array $assetIds
     */
    private static function deleteAssets($assetIds = [])
    {
        foreach ($assetIds as $assetId) {
            $asset = Asset::getById($assetId);

            if ($asset) {
                CliHelper::success("SUCCESS: Deleted asset with ID: {$asset->getId()} (Name: {$asset->getFilename()})");
                $asset->delete();
            }
        }
    }
}
