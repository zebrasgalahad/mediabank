<?php

namespace TrueRomanceBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Classificationstore;
use Pimcore\Model\DataObject\Classificationstore\GroupConfig;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Config;

class ArticleImportListener
{

    public function onPreUpdate(ElementEventInterface $e)
    {
        $object = $e->getObject();

        if ($e instanceof DataObjectEvent && $object instanceof DataObject\Article) {
            $currentArticle = $object;

            $currentArticle = $this->setZeroArticleValuesToNull($currentArticle);

            $articleVersions = $currentArticle->getVersions();

            $newClassificationGroupName = $currentArticle->getREFERENCE_FEATURE_GROUP_ID();

            $latestVersion = $articleVersions[count($articleVersions) - 1];

            if (!$latestVersion) {
                $this->updateClassificationGroup($newClassificationGroupName, $currentArticle);

                return;
            }

            if (!$latestVersion->getData()) {
                return;
            }

            $currentClassificationGroupName = $latestVersion->getData()->getREFERENCE_FEATURE_GROUP_ID();

            if ($currentClassificationGroupName === $newClassificationGroupName) {
                return;
            }

            $this->updateClassificationGroup($newClassificationGroupName, $currentArticle);
        }
    }

    private function updateClassificationGroup($classificationGroupName, $currentArticle)
    {
        $groupConfig = GroupConfig::getByName($classificationGroupName, 6);

        $this->setClassificationStore($currentArticle, $groupConfig);
    }

    private function setClassificationStore(DataObject\Article $article, $groupConfig)
    {
        $data[$groupConfig->getId()] = [];

        $classificationStore = new Classificationstore($data);

        $classificationStore->setObject($article);

        $classificationStore->setFieldname("FEATURE");

        $classificationStore->setActiveGroups([$groupConfig->getId() => true]);

        $classificationStore->save();

        $groupConfigDescription = $groupConfig->getDescription();

        $article->setFEATURE($classificationStore);

        $article->setREFERENCE_FEATURE_GROUP_NAME($groupConfigDescription);
    }

    private function setZeroArticleValuesToNull($article)
    {
        $values = Config::getSystemConfig();

        $locales = array_keys($values->toArray()["general"]["fallbackLanguages"]);
        $fieldsToCheck = [
            "PRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT",
            "price_net_customer_quantity",
            "list_price2_amount",
            "list_price2_quantity",
            "list_price3_amount",
            "list_price3_quantity",
            "list_price4_amount",
            "list_price4_quantity",
            "list_price5_amount",
            "list_price5_quantity",
            "list_price6_amount",
            "list_price6_quantity",
            "list_price7_amount",
            "list_price7_quantity",
        ];

        foreach ($fieldsToCheck as $fieldToCheck) {
            $getter = "get" . ucfirst($fieldToCheck);
            $setter = "set" . ucfirst($fieldToCheck);
            foreach ($locales as $locale) {
                if ($article->$getter($locale) == 0 && $article->$getter($locale) !== null) {
                    error_log($getter . " : " . $article->$getter($locale));

                    $article->$setter(1, $locale);
                    $article->$setter(null, $locale);
                }
            }
        }

        return $article;
    }
}
