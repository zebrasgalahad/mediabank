<?php
namespace TrueRomanceBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\DataObject\Fieldcollection\Data;

class XMLExportConfigurationListener
{
    public function onPreAdd(ElementEventInterface $e)
    {
        if ($e instanceof DataObjectEvent && $e->getObject() instanceof DataObject\XMLExportConfiguration) {
            $this->addEmptyFieldCollections($e->getObject());
        }
    }

    private function addEmptyFieldCollections(DataObject\XMLExportConfiguration $object) : void
    {
        $object->setBuyerFields(new Fieldcollection([new Data\XMLExportBuyerFields()]))
            ->setSupplierFields(new Fieldcollection([new Data\XMLExportSupplierFields()]))
            ->setCatalogGroupFields(new Fieldcollection([new Data\XMLExportCatalogGroupFields()]))
            ->setProductFields(new Fieldcollection([new Data\XMLExportProductFields()]));
    }
}
