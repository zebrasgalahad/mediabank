<?php
namespace TrueRomanceBundle\EventListener;

use Pimcore\Event\Model\ElementEventInterface;
use Pimcore\Event\Model\DataObjectEvent;
use Pimcore\Model\DataObject;
use Pimcore\Model\DataObject\Fieldcollection;
use Pimcore\Model\DataObject\Fieldcollection\Data;

class CSVExportConfigurationListener
{
    public function onPreAdd(ElementEventInterface $e)
    {
        if ($e instanceof DataObjectEvent && $e->getObject() instanceof DataObject\CSVExportConfiguration) {
            $this->addEmptyFieldCollections($e->getObject());
        }
    }

    private function addEmptyFieldCollections(DataObject\CSVExportConfiguration $object) : void
    {
        $object->setAssetsHeaders(new Fieldcollection([new Data\CSVExportAssetsHeaders()]))
            ->setArticleHeaders(new Fieldcollection([new Data\CSVExportArticleHeaders()]));
    }
}
