<?php

namespace TrueRomanceBundle\Controller;

use Pimcore\Bundle\ObjectMergerBundle\Controller\AdminController;
use Pimcore\Controller\Traits\TemplateControllerTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\Configuration\TemplatePhp;
use TrueRomanceBundle\Services\Csv\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TrueRomanceBundle\Services\AsyncJobHelper;

/**
 * AssetCleanupController
 *
 * @Route("/admin/tasks")
 */
class AssetCleanupController extends AdminController
{

    use TemplateControllerTrait;

    /**
     * Cleanup assets and article asset objects from CSV file
     *
     * @param Request $request
     * @return void
     *
     * @Route("/cleanup/assets", name="admin-asset-cleanup")
     * @TemplatePhp("TrueRomanceBundle:AssetCleanup:cleanup.html.php")
     */
    public function cleanup(Request $request)
    {
        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        $languagesSelectData = [];

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        if ($request->isMethod("POST")) {
            if (!$request->files->get("file")) {
                $errors[] = "asset.cleanup.error.no_file";
            } else {
                $csv = file_get_contents($request->files->get("file")->getRealPath());

                $csvFilename = $request->files->get("file")->getClientOriginalName();

                $csvValidator = $this->container->get("trueromance.tools.csv.validator");
                /* @var $csvValidator Validator */

                $allowedKeys = $this->getAllowedCsvColumnNames();

                $valid = $csvValidator->isValid($csv, $allowedKeys);

                if($valid !== true) {
                    $errors = $csvValidator->getErrors();
                } else {
                    $this->createTask(["data" => $csvValidator->getData(), "filename" => $csvFilename]);

                    $urlParameters = [
                        "csrfToken" => $request->get('csrfToken'),
                        "success" => true
                    ];

                    return new RedirectResponse($request->getPathInfo() . "?" . http_build_query($urlParameters));
                }
            }
        }

        return [
            "title" => "sap.import.title",
            "success" => $request->get("success") ? true : false,
            "errors" => $errors,
            "success" => $request->get("success") ? true : false,
            "validLocales" => $languagesSelectData
        ];
    }

    private function getAllowedCsvColumnNames(): array
    {
        return explode(";",
                "SUPPLIER_PID");
    }

    private function createTask(array $data)
    {
        $userId = $this->getUser()->getId();

        $creationTime = date('Y-m-d_H:i:s');

        $asyncJobHelper = $this->container->get("trueromance.import.async_job_helper");
        /* @var $asyncJobHelper AsyncJobHelper */

        $targetDir = PIMCORE_PRIVATE_VAR. "/data/";

        if(is_dir($targetDir) === false) {
            mkdir($targetDir, 0755, true);
        }

        $filepath = $targetDir . uniqid() . "_" . time() . ".serialized";

        file_put_contents($filepath, serialize($data));

        $asyncJobHelper->insertFirstData($creationTime, $userId, $filepath, AsyncJobHelper::TYPE_ASSET_CLEANUP);
    }
}
