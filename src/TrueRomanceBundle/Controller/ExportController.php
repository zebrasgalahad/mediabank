<?php

namespace TrueRomanceBundle\Controller;

use Pimcore\Bundle\ObjectMergerBundle\Controller\AdminController;
use Pimcore\Controller\Traits\TemplateControllerTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\Configuration\TemplatePhp;
use Pimcore\Model\DataObject;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Services\AssetZipExport\AssetZipExportDBHelper;
use TrueRomanceBundle\Library\Export\Catalog as CatalogExport;

ini_set("max_execution_time", 43200);
ini_set('memory_limit', '8192M');

/**
 * ExportController
 *
 * @Route("/admin/export")
 */
class ExportController extends AdminController
{
    const ZIP_EXPORT_IMAGES_PROPERTY = 'catalog_export_images';
    const ZIP_EXPORT_SHEETS_PROPERTY = 'catalog_export_sheets';

    use TemplateControllerTrait;

    /** @var CatalogExport\CatalogAssetsToZip $_catalogAssetsToZip */
    private $_catalogAssetsToZip;

    /** @var CatalogExport\CSVExportCatalog $_CSVExportCatalog */
    private $_CSVExportCatalog;

    /** @var CatalogExport\XMLExportCatalog $_XMLExportCatalog */
    private $_XMLExportCatalog;

    /** @var CatalogExport\FeaturesCSVExport $_featuresCSVExport */
    private $_featuresCSVExport;

    /**
     * ExportController constructor
     *
     * @param CatalogExport\CatalogAssetsToZip $catalogAssetsToZip
     * @param CatalogExport\CSVExportCatalog $CSVExportCatalog
     * @param CatalogExport\XMLExportCatalog $XMLExportCatalog
     * @param CatalogExport\FeaturesCSVExport $featuresCSVExport
     */
    public function __construct(
        CatalogExport\CatalogAssetsToZip $catalogAssetsToZip,
        CatalogExport\CSVExportCatalog $CSVExportCatalog,
        CatalogExport\XMLExportCatalog $XMLExportCatalog,
        CatalogExport\FeaturesCSVExport $featuresCSVExport
    ) {
        $this->_catalogAssetsToZip = $catalogAssetsToZip;
        $this->_CSVExportCatalog = $CSVExportCatalog;
        $this->_XMLExportCatalog = $XMLExportCatalog;
        $this->_featuresCSVExport = $featuresCSVExport;
    }

    /**
     * Catalog XML export view
     *
     * @param Request $request
     * @return void
     *
     * @Route("/xml/catalog", name="admin-catalog-xml-export-view")
     * @TemplatePhp("TrueRomanceBundle:Export:catalogXMLExport.html.php")
     */
    public function catalogXMLExportView(Request $request)
    {
        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        $catalogsSelectData = [];
        $languagesSelectData = [];
        $buyersSelectData = [];
        $suppliersSelectData = [];

        //Get catalogs
        $catalogList = new DataObject\Catalog\Listing();
        $catalogs = $catalogList->getObjects();

        /** @var DataObject\Catalog $catalog */
        foreach ($catalogs as $catalog) {
            $catalogsSelectData[$catalog->getId()] = $catalog->getGROUP_NAME($adminLocale);
        }

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        //Get buyers
        $buyersList = new DataObject\Buyer\Listing();
        $buyers = $buyersList->getObjects();

        /** @var DataObject\Buyer $buyer */
        foreach ($buyers as $buyer) {
            $buyersSelectData[$buyer->getId()] = $buyer->getBUYER_BUYER_BUYER_NAME();
        }

        //Get suppliers
        $suppliersList = new DataObject\Supplier\Listing();
        $suppliers = $suppliersList = $suppliersList->getObjects();

        /** @var DataObject\Supplier $supplier */
        foreach ($suppliers as $supplier) {
            $suppliersSelectData[$supplier->getId()] = $supplier->getSUPPLIER_SUPPLIER_NAME();
        }

        //BMECart data
        $BMECatSelectData = [
            CatalogExport\XMLExportCatalog::BMECAT_1_2 => '1.2',
            CatalogExport\XMLExportCatalog::BMECAT_2005 => '2005',
        ];

        //Get configurations
        $configurationsList = new DataObject\XMLExportConfiguration\Listing();
        $configurations = $configurationsList->getObjects();

        /** @var DataObject\XMLExportConfiguration $supplier */
        foreach ($configurations as $config) {
            $configurationsSelectData[$config->getId()] = $config->getKey();
        }

        //Textmode data
        $textmodeSelectData = [
            CatalogExport\CatalogExport::TEXTMODE_HTML => 'Html',
            CatalogExport\CatalogExport::TEXTMODE_PLAIN  => 'Plain Text (pipe as separator)',
        ];

        return [
            'catalogsSelectData' => $catalogsSelectData,
            'buyersSelectData' => $buyersSelectData,
            'languagesSelectData' => $languagesSelectData,
            'suppliersSelectData' => $suppliersSelectData,
            'BMECatSelectData' => $BMECatSelectData,
            'textmodeSelectData' => $textmodeSelectData,
            'configurationsSelectData' => $configurationsSelectData,
        ];
    }

    /**
     * Handle catalog export to xml
     *
     * @param Request $request
     * @return void
     *
     * @Route("/xml/catalog/export", name="admin-catalog-xml-export", methods={ "POST" })
     */
    public function catalogXMLExportAction(Request $request)
    {
        $params = [
            'catalog' => $request->get('catalog'),
            'language' => $request->get('language'),
            'BMECat' => $request->get('BMECat'),
            'textmode' => $request->get('textmode'),
            'buyer' => $request->get('buyer'),
            'supplier' => $request->get('supplier'),
            'configuration' => $request->get('configuration'),
        ];

        $file = $this->_XMLExportCatalog->export($params);

        return $this->file($file);
    }

    /**
     * View for zipping assets
     *
     * @param Request $request
     * @return void
     *
     * @Route("/zip/assets", name="admin-assets-zip-view", methods = { "GET", "POST" })
     *
     * @TemplatePhp("TrueRomanceBundle:Export:assetsView.html.php")
     */
    public function assetsToZipView(Request $request)
    {
        $assetZipExportServiceHelper = $this->container->get('trueromance.export.zip_assets_helper');
        /* @var $assetZipExportServiceHelper AssetZipExportDBHelper */

        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        $admin = $this->getAdminUser();
        $adminId = $admin->getId();

        $translator = $this->get('translator');

        $catalogsSelectData = [];

        //Get catalogs
        $catalogList = new DataObject\Catalog\Listing();
        $catalogs = $catalogList->getObjects();

        /** @var DataObject\Catalog $catalog */
        foreach ($catalogs as $catalog) {
            $catalogsSelectData[$catalog->getId()] = $catalog->getGROUP_NAME($adminLocale);
        }

        $languagesSelectData = [];

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        //Zip options
        $zipOptionsSelect = [
            CatalogExport\CatalogAssetsToZip::ZIP_IMAGES => $translator->trans('admin.assets.zip.export_images'),
            CatalogExport\CatalogAssetsToZip::ZIP_PRODUCT_SHEETS => $translator->trans('admin.assets.zip.export_product_sheets'),
        ];

        //Check if request method is POST
        if ($request->isMethod(Request::METHOD_POST)) {
            $catalogId = $request->get('catalog');
            $zipMode = $request->get('zip_mode');
            $language = $request->get('language');

            $catalog = DataObject\Catalog::getById(intval($catalogId));

            if (!$catalog) {
                throw new NotFoundHttpException('Catalog not found.');
            }

            $propertyName = self::ZIP_EXPORT_IMAGES_PROPERTY;

            if ($zipMode == CatalogExport\CatalogAssetsToZip::ZIP_PRODUCT_SHEETS) {
                $propertyName = self::ZIP_EXPORT_SHEETS_PROPERTY;
            }

            $assetZipExportServiceHelper->insertFirstData(date('Y-m-d_H:i:s'), $adminId, $catalogId, $propertyName, $language);

            $message = $translator->trans('admin.assets.zip.export.scheduled');
        }

        return [
            'catalogsSelectData' => $catalogsSelectData,
            'zipSelectData' => $zipOptionsSelect,
            'message' => $message,
            'languagesSelectData' => $languagesSelectData
        ];
    }

     /**
     * Catalog CSV export view
     *
     * @param Request $request
     * @return void
     *
     * @Route("/csv/catalog", name="admin-catalog-csv-export-view")
     * @TemplatePhp("TrueRomanceBundle:Export:catalogCSVExport.html.php")
     */
    public function catalogCSVExportView(Request $request)
    {
        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        $catalogsSelectData = [];
        $languagesSelectData = [];

        //Get catalogs
        $catalogList = new DataObject\Catalog\Listing();
        $catalogs = $catalogList->getObjects();

        /** @var DataObject\Catalog $catalog */
        foreach ($catalogs as $catalog) {
            $catalogsSelectData[$catalog->getId()] = $catalog->getGROUP_NAME($adminLocale);
        }

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        //Textmode data
        $textmodeSelectData = [
            CatalogExport\CatalogExport::TEXTMODE_HTML => 'Html',
            CatalogExport\CatalogExport::TEXTMODE_PLAIN  => 'Plain Text (pipe as separator)',
        ];

        //Get configurations
        $configurationsList = new DataObject\CSVExportConfiguration\Listing();
        $configurations = $configurationsList->getObjects();

        /** @var DataObject\XMLExportConfiguration $supplier */
        foreach ($configurations as $config) {
            $configurationsSelectData[$config->getId()] = $config->getKey();
        }

        return [
            'catalogsSelectData' => $catalogsSelectData,
            'languagesSelectData' => $languagesSelectData,
            'textmodeSelectData' => $textmodeSelectData,
            'configurationsSelectData' => $configurationsSelectData,
        ];
    }

    /**
     * Handle catalog export to csv
     *
     * @param Request $request
     * @return void
     *
     * @Route("/csv/catalog/export", name="admin-catalog-csv-export", methods={ "POST" })
     */
    public function catalogCSVExportAction(Request $request)
    {
        $params = [
            'catalog' => $request->get('catalog'),
            'language' => $request->get('language'),
            'BMECat' => $request->get('BMECat'),
            'textmode' => $request->get('textmode'),
            'buyer' => $request->get('buyer'),
            'supplier' => $request->get('supplier'),
            'header_config' => $request->get('header_config'),
        ];

        $file = $this->_CSVExportCatalog->export($params);

        return $this->file($file);
    }

    /**
     * Features CSV export view
     *
     * @param Request $request
     * @return void
     *
     * @Route("/csv/features", name="admin-features-csv-export-view")
     * @TemplatePhp("TrueRomanceBundle:Export:featuresCSVExport.html.php")
     */
    public function featuresCSVExportView(Request $request)
    {
        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        $catalogsSelectData = [];
        $languagesSelectData = [];

        //Get catalogs
        $catalogList = new DataObject\Catalog\Listing();
        $catalogs = $catalogList->getObjects();

        /** @var DataObject\Catalog $catalog */
        foreach ($catalogs as $catalog) {
            $catalogsSelectData[$catalog->getId()] = $catalog->getGROUP_NAME($adminLocale);
        }

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        //BMECart data
        $BMECatSelectData = [
            CatalogExport\XMLExportCatalog::BMECAT_1_2 => '1.2',
            CatalogExport\XMLExportCatalog::BMECAT_2005 => '2005',
        ];

        return [
            'catalogsSelectData' => $catalogsSelectData,
            'languagesSelectData' => $languagesSelectData,
            'BMECatSelectData' => $BMECatSelectData,
        ];

    }

    /**
     * Handle features export to csv
     *
     * @param Request $request
     * @return void
     *
     * @Route("/csv/features/export", name="admin-features-csv-export", methods={ "POST" })
     */
    public function featuresCSVExportAction(Request $request)
    {
        $params = [
            'catalog' => $request->get('catalog'),
            'language' => $request->get('language'),
            'BMECat' => $request->get('BMECat'),
            'textmode' => $request->get('textmode'),
        ];

        $file = $this->_featuresCSVExport->export($params);

        return $this->file($file);
    }
}
