<?php

namespace TrueRomanceBundle\Controller;

use Pimcore\Bundle\ObjectMergerBundle\Controller\AdminController;
use Pimcore\Controller\Traits\TemplateControllerTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\Configuration\TemplatePhp;
use TrueRomanceBundle\Services\Csv\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TrueRomanceBundle\Services\AsyncJobHelper;

/**
 * SapImportController
 *
 * @Route("/admin/tasks")
 */
class AssetImportController extends AdminController
{

    use TemplateControllerTrait;

    /**
     * Import asset data from CSV file and ftp server
     *
     * @param Request $request
     * @return void
     *
     * @Route("/import/asset", name="admin-asset-import")
     * @TemplatePhp("TrueRomanceBundle:AssetImport:import.html.php")
     */
    public function import(Request $request)
    {
        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        $languagesSelectData = [];

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        if ($request->isMethod("POST")) {
            if (!$request->files->get("file")) {
                $errors[] = "asset.import.error.no_file";
            } else {
                $csv = file_get_contents($request->files->get("file")->getRealPath());

                $csvValidator = $this->container->get("trueromance.tools.csv.validator");
                /* @var $csvValidator Validator */

                $csvFilename = $request->files->get("file")->getClientOriginalName();

                $allowedKeys = $this->getAllowedCsvColumnNames();

                $valid = $csvValidator->isValid($csv, $allowedKeys, ",");

                if($valid !== true) {
                    $errors = $csvValidator->getErrors();
                } else {
                    $locale = $request->get('locale');

                    $this->createTask(["data" => $csvValidator->getData(), "filename" => $csvFilename, "locale" => $locale]);

                    $urlParameters = [
                        "csrfToken" => $request->get('csrfToken'),
                        "success" => true,
                    ];

                    return new RedirectResponse($request->getPathInfo() . "?" . http_build_query($urlParameters));
                }
            }
        }

        return [
            "title" => "asset.import.title",
            "success" => $request->get("success") ? true : false,
            "errors" => $errors,
            "success" => $request->get("success") ? true : false,
            "validLocales" => $languagesSelectData
        ];
    }

    private function getAllowedCsvColumnNames(): array
    {
        return explode(";",
                "Supplier_AID;Mime_Type;Mime_Source;Mime_Purpose;item_bmecat;item_mediathek");
    }

    private function createTask(array $data)
    {
        $userId = $this->getUser()->getId();

        $creationTime = date('Y-m-d_H:i:s');

        $asyncJobHelper = $this->container->get("trueromance.import.async_job_helper");
        /* @var $asyncJobHelper AsyncJobHelper */

        $targetDir = PIMCORE_PRIVATE_VAR. "/data/";

        if(is_dir($targetDir) === false) {
            mkdir($targetDir, 0755, true);
        }

        $filepath = $targetDir . uniqid() . "_" . time() . ".serialized";

        file_put_contents($filepath, serialize($data));

        $asyncJobHelper->insertFirstData($creationTime, $userId, $filepath, AsyncJobHelper::TYPE_ASSET_IMPORT, $data['locale']);
    }
}
