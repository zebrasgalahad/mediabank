<?php

/**
 * This class is used by every image request from /var/../..
 */

namespace TrueRomanceBundle\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\Asset;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TrueRomanceBundle\Library\User\Permissions;

class AssetPermission extends FrontendController
{
   protected $container;
    
   protected $excludes = ["filetype-not-supported", "SBD-ORIGINAL", "asset_placeholder.jpg"];
   
   public function protectedAssetAction(Request $request, ContainerInterface $container)
    {
        $this->container = $container;
       
        $pathInfo = urldecode($request->getPathInfo());
        
        $asset = $this->getAssetByPathInfo($pathInfo);
        
        $isStaticAsset = false;
        foreach ($this->excludes as $exclude) {
            if(strpos($pathInfo, $exclude) !== false) {
                $isStaticAsset = true;
            }
        }

        if($isStaticAsset === false && $this->checkAssetPermissions($asset, $request) === false) {
            return new \Symfony\Component\HttpFoundation\Response("Not allowed", 403);
        }        

        $assetPath = urldecode(PIMCORE_WEB_ROOT . $pathInfo);
        
        return new BinaryFileResponse($assetPath);
    }   
    
    private function checkAssetPermissions($asset, $request)
    {
        $permissions = $this->container->get("trueromance.user.permissions");

        if($permissions->assetIsInIndex($asset->getId()) === false) {
            return true;
        }
        
        /* @var $permissions \TrueRomanceBundle\Library\User\Permissions */
        
        if(is_array($request->getSession()->get("album")) === true) {
            foreach ($request->getSession()->get("album") as $albumId) {
                if($permissions->isAlbumAllowed($albumId, $asset->getId()) === true) {
                    return true;
                }
            }
        }
        
        if($permissions->isAssetAllowed($asset->getId()) === true) {
            return true;
        }
        
        return false;

    }


    private function getAssetByPathInfo($pathInfo)
    {
        preg_match("/__([0-9]*)__/m", $pathInfo, $matches);
        
        if($matches[1] && is_numeric($matches[1])) {
            $asset = Asset::getById($matches[1]);
            
            if($asset) {
                return $asset;
            }
        }
        
        $pathInfo = str_replace("/var/assets", "", $pathInfo);
        
        $asset = Asset::getByPath($pathInfo);
        
        if($asset) {
            return $asset;
        }
        
        return null;
    }
}
