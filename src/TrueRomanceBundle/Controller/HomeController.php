<?php

namespace TrueRomanceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\VarDumper\VarDumper;
use TrueRomanceBundle\Library\Index\Service as IndexService;
use TrueRomanceBundle\Library\Frontend\Javascript\Data as JsData;

class HomeController extends PermissionController
{

    public function catalogAction(Request $request)
    {
        $params = $request->query->all();

        $params["locale"] = $request->getLocale();

        try {
            $indexService = new IndexService($params, $this->container);

            $this->view->result = $indexService->get();

            $this->view->isFiltered = $indexService->isFiltered();

            $this->view->isFilteredByCatalog = $indexService->isFilteredByCatalog();

            $this->view->isInitialRequest = true;

            $this->view->albumCount = $this->container->get("trueromance.album.service")->getUserAlbumCount($this->getUserId());

            $this->view->container = $this->container;

            $this->view->language = explode("_", $request->getLocale())[0];

            $this->view->contentPageImprint = $this->container->get("trueromance.object.config")->get("Content_Page_Imprint", $request->getLocale())->getFullPath();

            $this->view->contentPageFaq = $this->container->get("trueromance.object.config")->get("Content_Page_FAQ", $request->getLocale())->getFullPath();

            $this->view->contentPageTermsOfUse = $this->container->get("trueromance.object.config")->get("Content_Page_Terms_Of_Use", $request->getLocale())->getFullPath();

            $this->view->contentPageTermsOfBusiness = $this->container->get("trueromance.object.config")->get("Content_Page_Terms_Of_Business", $request->getLocale())->getFullPath();

            JsData::add("requestData", $request->query->all());
        } catch (\Exception $ex) {
            return new \Symfony\Component\HttpFoundation\Response($ex->getMessage(), 403);
        }
    }

    public function contentAction(Request $request)
    {

    }
}
