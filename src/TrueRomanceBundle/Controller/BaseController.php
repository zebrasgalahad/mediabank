<?php

namespace TrueRomanceBundle\Controller;

class BaseController extends \Pimcore\Controller\FrontendController
{
    public function getUserId()
    {
        if($this->container->get("trueromance.user.permissions")->getUser() === "anon.") {
            return false;
        }
        
        return $this->container->get("trueromance.user.permissions")->getUser()->getId();
    }
    
    public function getJson()
    {
        return json_decode(file_get_contents('php://input'), true);
    }    
}
