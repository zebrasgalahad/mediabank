<?php

namespace TrueRomanceBundle\Controller;

use Pimcore\Bundle\ObjectMergerBundle\Controller\AdminController;
use Pimcore\Controller\Traits\TemplateControllerTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\Configuration\TemplatePhp;
use Symfony\Component\VarDumper\VarDumper;

/**
 * ChangeDateController
 *
 * @Route("/admin/tasks")
 */
class ChangeDateTaskController extends AdminController
{

    use TemplateControllerTrait;

    /**
     * Change dates for all articles
     *
     * @param Request $request
     * @return void
     *
     * @Route("/change/date", name="change-date-task")
     * @TemplatePhp("TrueRomanceBundle:ChangeDateTask:change.html.php")
     */
    public function import(Request $request)
    {
        $adminLocale = \Pimcore\Tool::getDefaultLanguage();

        //Get available languages
        $languages = \Pimcore\Tool::getValidLanguages();

        $languagesSelectData = [];

        foreach ($languages as $language) {
            $languagesSelectData[$language] = \Locale::getDisplayLanguage($language, $adminLocale);
        }

        $datefrom = $request->get("date_from");
        $dateTo = $request->get("date_to");

        $asyncJobHelper = $this->container->get('trueromance.import.async_job_helper');
        /* @var $asyncJobHelper \TrueRomanceBundle\Services\AsyncJobHelper */

        $userId = $this->getUser()->getId();

        $type = $asyncJobHelper::TYPE_DATE_CHANGE;

        if ($request->getMethod() === $request::METHOD_POST) {
            $errors = [];

            if (trim($datefrom) === "" || trim($dateTo) === "") {
                $errors[] = "date.change.update.error.input_empty";
            } else if ($this->checkIfDateIsValid($datefrom, $dateTo) === false) {
                $errors[] = "date.change.update.error.input_not_valid";
            }

            if (!empty($errors)) {
                return [
                    "errors" => $errors
                ];
            }

            $creationTime = date('Y-m-d_H:i:s');

            $locale = $request->get('locale');

            $asyncJobHelper->insertDateValidityFirstData($creationTime, $userId, $datefrom, $dateTo, $type, $locale);

            return [
                "title" => "date.change.update.job_started",
                "success" => $request->get("success") ? true : false,
            ];
        }

        return ['validLocales' => $languagesSelectData];
    }

    private function checkIfDateIsValid($dateFrom, $dateTo)
    {
        $dateFromSingleValues = explode("/", $dateFrom);
        $dateToSingleValues = explode("/", $dateTo);

        $dates = [
            "startDateDay" => (int) $dateFromSingleValues[1],
            "startDateMonth" => (int) $dateFromSingleValues[0],
            "startDateYear" => (int) $dateFromSingleValues[2],
            "endDateDay" => (int) $dateToSingleValues[1],
            "endDateMonth" => (int) $dateToSingleValues[0],
            "endDateYear" => (int) $dateToSingleValues[2]
        ];

        return checkdate($dates["startDateMonth"], $dates["startDateDay"], $dates["startDateYear"]) && checkdate($dates["endDateMonth"], $dates["endDateDay"],
                        $dates["endDateYear"]);
    }
}
