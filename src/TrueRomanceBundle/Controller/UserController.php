<?php

namespace TrueRomanceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use TrueRomanceBundle\Controller\PermissionController;
use Symfony\Component\HttpFoundation\JsonResponse;
use TrueRomanceBundle\Library\Index\Service as IndexService;

class UserController extends PermissionController
{

    /**
     * @Route("/{_locale}/ajax/user/suggest")
     */
    public function userSuggestAction(Request $request)
    {
        $userService = new \TrueRomanceBundle\Library\User\Service();

        $users = $userService->suggestUser($request->get("term"), $this->getUserId());

        return new JsonResponse($users);
    }
}
