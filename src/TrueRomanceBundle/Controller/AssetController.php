<?php

namespace TrueRomanceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use TrueRomanceBundle\Library\Index\Service as IndexService;

class AssetController extends BaseController
{
    /**
     * @Route("/{_locale}/ajax/asset/detail")
     */
    public function detailAction(Request $request)
    {
        $params = $request->query->all();

        $params["locale"] = $request->getLocale();

        unset($params["id"]);

        $indexService = new IndexService($params, $this->container);

        $detailData = $this->container->get("trueromance.asset.data")->getDetailData($request->get("id"));

        $detailData["navigation"] = $indexService->getAssetDetailNavigation($request->get("id"), $params);

        if($request->get("albumId")) {
            $detailData["navigation"] = $this->container->get("trueromance.album.service")->getAssetDetailAlbumNavigation($request->get("id"), $request->get("albumId"));

            $detailData["album"] = $this->container->get("trueromance.album.service")->getAlbumOnly($request->get("albumId"));
        }

        return new JsonResponse($detailData);
    }

    /**
     * @Route("/{_locale}/ajax/asset/list")
     */
    public function listAction(Request $request)
    {
        $params = $request->query->all();

        $params["locale"] = $request->getLocale();

        $indexService = new IndexService($params, $this->container);

        $result = $indexService->get();

        return new JsonResponse($result);
    }
}
