<?php

namespace TrueRomanceBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use TrueRomanceBundle\Library\Object\Config;
use TrueRomanceBundle\Library\Object\CatalogGroup;

/**
 * Description of UserInviteController
 *
 * @author tzilling
 */
class UserInviteController extends PermissionController {

    /**
     * @Route("/{_locale}/ajax/user-invitation/form")
     */
    public function userInviteAction()
    {
        $localeService = new \Pimcore\Localization\Locale;
        $pimcoreCountries = $localeService->getDisplayRegions();
        $configCountries = Config::get("Invitation_Country");
        
        $userGroups = \Pimcore\Config::getWebsiteConfig()->configuration->getDisplayed_User_Groups();
        
        $countries = [];
        $configurationData = [];

        foreach ($configCountries as $country) {
            $countries[$country]["name"] = $pimcoreCountries[$country];
            $countries[$country]["value"] = $country;
        }
        
        $configurationData["country"] = $countries;
        
        foreach ($userGroups as $userGroup) {
            $key = strtolower($userGroup->getName());

            foreach ($userGroup->getUserCatalogs() as $catalogs) {
                foreach ($catalogs->getCatalogs() as $catalog) {
                    $configurationData[$key][$catalog->getId()] = [
                            "name" => $catalog->getLIST_PREVIEW_NAME(),
                            "value" => $catalog->getId()
                    ];
                }
            }
        }
        
        $customerData = [
            'lastName' => $this->getUser()->getLastName(),
            'firstName' => $this->getUser()->getFirstName(),
            'email' => $this->getUser()->getEmail(),
        ];


        $responseData = [
            'customer' => $customerData,
            'config' => $configurationData
        ];
        
        return new JsonResponse($responseData);
    }

}
