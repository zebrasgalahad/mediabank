<?php

namespace TrueRomanceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use TrueRomanceBundle\Library\Frontend\Javascript\Data as JsData;

class AlbumController extends BaseController
{
    public function showAction(Request $request)
    {
        $album = $this->container->get("trueromance.album.service")->getAlbumOnly($request->get("id"));
        
        if((int) $album["public"] !== 1) {
            return new \Symfony\Component\HttpFoundation\Response("Not allowed", 403);
        }
        
        $this->view->loginLink = $this->container->get("trueromance.object.config")->get("Login_Page", $request->getLocale())->getFullPath();
        
        $this->view->locale = $request->getLocale();
        
        JsData::add("requestData", $request->query->all());
        
        if(is_array($request->getSession()->get("album")) === false) {
            $request->getSession()->set("album", []);
        }
        
        $albums = $request->getSession()->get("album");
        
        if(in_array($request->get("id"), $albums) === false) {
            $albums[] = $request->get("id");
        }
        
        $request->getSession()->set("album", $albums);
    }
}
