<?php

namespace TrueRomanceBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use TrueRomanceBundle\Library\User\Permissions;

class MyCollectionController extends BaseController
{

    /**
     * @Route("/{_locale}/ajax/collection/asset/list")
     *
     * @param Request $request
     *
     */
    public function assetListAction()
    {
        $data = $this->getJson();

        $collectionData = $this->container->get("trueromance.collection.data")->collectionIndexData($data);

        return new JsonResponse($collectionData);
    }
    
    /**
     * @Route("/{_locale}/ajax/album/list")
     *
     * @param Request $request
     *
     */
    public function albumListAction()
    {
        $albums = $this->container->get("trueromance.album.service")->getList($this->getUserId());

        return new JsonResponse($albums);
    }

    /**
     * @Route("/{_locale}/ajax/album/delete")
     *
     * @param Request $request
     *
     */
    public function albumDeleteAction(Request $request)
    {
        $albums = $this->container->get("trueromance.album.service")->delete($request->get("id"));

        return new JsonResponse($albums);
    }    
    
    /**
     * @Route("/{_locale}/ajax/album/asset/delete")
     *
     * @param Request $request
     *
     */
    public function albumDeleteAssetAction(Request $request)
    {
        $album = $this->container->get("trueromance.album.service")->deleteAsset($request->get("album_id"), $request->get("asset_id"), $this->getUserId());

        return new JsonResponse($album);
    }        
    
    /**
     * @Route("/{_locale}/ajax/collection/album/asset/add")
     *
     * @param Request $request
     *
     */
    public function albumAddAssetsAction()
    {
        $data = $this->getJson();
        
        $album = $this->container->get("trueromance.album.service")->addAssetsToAlbum($data["album_id"], $data["asset_ids"], $this->getUserId());
        
        return new JsonResponse($album);
    }    
    
    /**
     * @Route("/{_locale}/ajax/collection/album/add")
     *
     * @param Request $request
     *
     */
    public function albumAddAction()
    {
        $data = $this->getJson();
        
        $data["owner_user_id"] = $this->getUserId();
        
        $data["user_id"] = $this->getUserId();
        
        $album = $this->container->get("trueromance.album.service")->create($data);
        
        return new JsonResponse($album);
    }
    
    /**
     * @Route("/{_locale}/ajax/album/update")
     *
     * @param Request $request
     *
     */
    public function albumUpdateAction(Request $request)
    {
        $data = $this->getJson();
        
        $data["updated"] = time();
        
        $album = $this->container->get("trueromance.album.service")->updateAlbum($request->get("id"), $this->getUserId(), $data);
        
        return new JsonResponse($album);
    }        
    
    /**
     * @Route("/{_locale}/ajax/album/get")
     *
     * @param Request $request
     *
     */
    public function albumGetAction(Request $request)
    {
        $userId = $this->getUserId();
        
        $album = $this->container->get("trueromance.album.service")->get($request->get("id"), $userId, false);
        
        return new JsonResponse($album);
    }        
    
    /**
     * @Route("/{_locale}/ajax/album/user/count")
     *
     * @param Request $request
     *
     */
    public function albumCountAction()
    {
        $count = $this->container->get("trueromance.album.service")->getUserAlbumCount($this->getUserId());

        return new JsonResponse(["count" => $count]);
    } 
    
    /**
     * @Route("/{_locale}/ajax/album/share")
     *
     * @param Request $request
     *
     */    
    public function albumShareAction()
    {
        if($this->container->get("trueromance.user.permissions")->isAllowed("invite_users") === false) {
            return new \Symfony\Component\HttpFoundation\Response("Not allowed", 403);
        }
        
        $data = $this->getJson();
        
        foreach ($data["formData"] as $user) {
            if($user["name"] === "existing") {
                $this->container->get("trueromance.album.service")->addUserToAlbum($data["albumId"], $user["value"]);
                
                $this->container->get("userauthentication.mailer")->sendInternalUserAlbumShare($user["value"], $data["albumId"]);
            } else {
                $this->container->get("trueromance.album.service")->addExternUserToAlbum($data["albumId"], $this->getUserId(), $user["value"]);
                
                $this->container->get("trueromance.album.service")->updateAlbum($data["albumId"], $this->getUserId(), ["public" => 1]);
                
                $this->container->get("userauthentication.mailer")->sendExternalUserAlbumShare($user["value"], $data["albumId"]);
            }
        }
        
        return new JsonResponse(["success" => true]);
    }
}
