<?php

/**
 * Pimcore
 *
 * This source file is available under two different licenses:
 * - GNU General Public License version 3 (GPLv3)
 * - Pimcore Enterprise License (PEL)
 * Full copyright and license information is available in
 * LICENSE.md which is distributed with this source code.
 *
 * @copyright  Copyright (c) Pimcore GmbH (http://www.pimcore.org)
 * @license    http://www.pimcore.org/license     GPLv3 and PEL
 */

namespace TrueRomanceBundle\Controller;

use Pimcore\Controller\FrontendController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

abstract class PermissionController extends FrontendController
{
    private $request;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        
        if($container->get("trueromance.user.permissions")->isEditMode() === true) {
            return;
        }
        
        if ($container->get("trueromance.user.permissions")->isAllowed("view_mediathek") === false) {
            $loginPage = $container->get("trueromance.object.config")->get("Login_Page", $locale)->getFullPath();
            
            $container->get('security.token_storage')->setToken(null);
            $container->get("request_stack")->getCurrentRequest()->getSession()->invalidate();
            
            $redirect = new \Symfony\Component\HttpFoundation\RedirectResponse($loginPage, 302);
            $redirect->send();
            exit;
        }
    }

    /**
     * @inheritDoc
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        // enable view auto-rendering
        $this->setViewAutoRender($event->getRequest(), true, 'php');

        if ($this->getUser()) {
            $this->view->customerData = [
                'lastName' => $this->getUser()->getLastName(),
                'firstName' => $this->getUser()->getFirstName(),
                'email' => $this->getUser()->getEmail(),
            ];
        }
        
        $this->view->locale = $this->container->get("request_stack")->getCurrentRequest()->getLocale();
        
        $this->view->indexPageLink = $this->container->get("trueromance.object.config")->get("MISC_INDEX_PAGE", $locale)->getFullPath();

        $this->view->loginLink = $this->container->get("trueromance.object.config")->get("Login_Page", $locale)->getFullPath();

        $this->view->logoutLink = $this->container->get("trueromance.object.config")->get("Logout_Page", $locale)->getFullPath();
    }
    
    public function getUserId()
    {
        return $this->container->get("trueromance.user.permissions")->getUser()->getId();
    }
    
    public function getJson()
    {
        return json_decode(file_get_contents('php://input'), true);
    }
}
