<?php

namespace TrueRomanceBundle\Controller;

use Pimcore\Bundle\ObjectMergerBundle\Controller\AdminController;
use Pimcore\Controller\Traits\TemplateControllerTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Pimcore\Controller\Configuration\TemplatePhp;
use TrueRomanceBundle\Services\Csv\Validator;
use Symfony\Component\HttpFoundation\RedirectResponse;
use TrueRomanceBundle\Services\AsyncJobHelper;

/**
 * SapImportController
 *
 * @Route("/admin/tasks")
 */
class SapImportController extends AdminController
{

    use TemplateControllerTrait;

    /**
     * Import SAP data from CSV file
     *
     * @param Request $request
     * @return void
     *
     * @Route("/import/sap", name="admin-sap-import")
     * @TemplatePhp("TrueRomanceBundle:SapImport:import.html.php")
     */
    public function import(Request $request)
    {
        if ($request->isMethod("POST")) {
            if (!$request->files->get("file")) {
                $errors[] = "sap.import.error.no_file";
            } else {
                $csv = file_get_contents($request->files->get("file")->getRealPath());

                $csvFilename = $request->files->get("file")->getClientOriginalName();
                
                $csvValidator = $this->container->get("trueromance.tools.csv.validator");
                /* @var $csvValidator Validator */

                $allowedKeys = $this->getAllowedCsvColumnNames();

                $valid = $csvValidator->isValid($csv, $allowedKeys);
                
                if($valid !== true) {
                    $errors = $csvValidator->getErrors();
                } else {
                    $this->createTask(["data" => $csvValidator->getData(), "filename" => $csvFilename]);
                    
                    $urlParameters = [
                        "csrfToken" => $request->get('csrfToken'),
                        "success" => true
                    ];
                    
                    return new RedirectResponse($request->getPathInfo() . "?" . http_build_query($urlParameters));
                }
            }
        }

        return [
            "title" => "sap.import.title",
            "success" => $request->get("success") ? true : false,
            "errors" => $errors,
            "success" => $request->get("success") ? true : false
        ];
    }

    private function getAllowedCsvColumnNames(): array
    {
        return explode(";",
                "SKU;IDLOCAL;TAXCLASSID;PRODUCTLISTPRICE;PRODUCTLISTPRICECURR;NETPRICEFLAG;LISTPRICEUSED;MINORDERQUANTITY;STEPQUANTITY;QUANTITYUNIT;SHORTDESCRIPTION;CATEGORYLINKN;ONLINEFLAG;AVAILABLE;AASTANDARDEAN;AASTANDARDVK;AASTANDARDVKWAE;VK_EURO;VK_WAERUNG;HEK_EURO;HEK_WAERUNG;ABMASSE;STATWANR;OLDSKU;MFGSKU;SUBSKU;PACKINGLENGTH;PACKINGLENGTHUNIT;PACKINGWIDTH;PACKINGWIDTHUNIT;PACKINGHEIGHT;PACKINGHEIGHTUNIT;GROSSWEIGHT;GROSSWEIGHTUNIT");
    }
    
    private function createTask(array $data)
    {
        $userId = $this->getUser()->getId();
        
        $creationTime = date('Y-m-d_H:i:s');
        
        $asyncJobHelper = $this->container->get("trueromance.import.async_job_helper");
        /* @var $asyncJobHelper AsyncJobHelper */
        
        $targetDir = PIMCORE_PRIVATE_VAR. "/data/";
        
        if(is_dir($targetDir) === false) {
            mkdir($targetDir, 0755, true);
        }
        
        $filepath = $targetDir . uniqid() . "_" . time() . ".serialized";
        
        file_put_contents($filepath, serialize($data));
        
        $asyncJobHelper->insertFirstData($creationTime, $userId, $filepath, AsyncJobHelper::TYPE_SAP_IMPORT);
    }
}
