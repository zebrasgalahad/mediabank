<?php

namespace TrueRomanceBundle\Controller;

use TrueRomanceBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class DownloadController extends BaseController {
    
    /**
     *
     * @var \Symfony\Component\DependencyInjection\ContainerInterface 
     */
    protected $container;

    /**
     *
     * @var \TrueRomanceBundle\Library\Data\Download\Asset
     */
    private $assetDownloadData;

    /**
     * 
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container)
    {
        $this->container = $container;

        $this->assetDownloadData = $container->get("trueromance.asset.download");
    }

    /**
     * @Route("/{_locale}/download/asset", methods={"GET"})
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function downloadAssetAction(Request $request)
    {
        $assetData = [
            0 => [
                "id" => $request->get("id"),
                "type" => $request->get("type")
            ]
        ];
        
        return $this->assetDownloadData->download($assetData);
    }
    
    /**
     * @Route("/{_locale}/download/multiple-assets", methods={"POST"})
     * 
     * @param Request $request
     */
    public function downloadMultipleAssetsAction(Request $request)
    {
        
        $jsonData = $this->getJson();
        
        $objectIds = $jsonData["asset_ids"];
        $downloadTypes = $jsonData["single_download_types"];
        
        return $this->assetDownloadData->downloadMultipleAssets($objectIds, $downloadTypes);
    }
}
