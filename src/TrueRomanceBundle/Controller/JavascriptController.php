<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace TrueRomanceBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class JavascriptController extends BaseController
{
    
    const TRANSLATIONS_TABLE = "translations_website"; 

    /**
     * @Route("/{_locale}/javascript/frontend")
     */
    public function frontendAction(Request $request)
    {
        header('Content-Type: text/javascript');
        
        $content = "var sbd = sbd ? sbd : {};";
        
        $locale = $request->getLocale();
        
        $translations = $this->getTranslations($locale);
        
        $language = explode("_", $request->getLocale())[0];
        
        $localeData = [
            "locale" => $locale,
            "language" => $language,
            "translations" => $translations
        ];
        
        $storeTimeHours = $this->container->get("trueromance.object.config")->get("Collection_Store_Time", $locale);
        
        $userPermissions = $this->container->get("trueromance.user.permissions")->userPermissionFrontendData();
        
        \TrueRomanceBundle\Library\Frontend\Javascript\Data::add("userPermissions", $userPermissions);
        
        \TrueRomanceBundle\Library\Frontend\Javascript\Data::add("storeTimeHours", $storeTimeHours);
        
        \TrueRomanceBundle\Library\Frontend\Javascript\Data::add("i18n", $localeData);
        
        $config = \TrueRomanceBundle\Library\Frontend\Javascript\Data::get(false);
        
        $content .= "sbd.i18n = " . json_encode($config["i18n"]);
        
        unset($config["i18n"]);
        
        $content .= "; sbd.data = " . json_encode($config);
        
        die($content);
        
    }
    
    /**
     * 
     * @param string $locale
     * @return array
     */
    private function getTranslations($locale)
    {
        $db = \TrueRomanceBundle\Library\Database\AbstractDatabase::getDatabaseObject();

        $query = "SELECT * FROM " . self::TRANSLATIONS_TABLE . " WHERE language = ?";

        $translations = $db->fetchAll($query, [$locale]);

        $mappedTranslations = [];
        
        if ($translations !== null && $translations !== false && count($translations) > 0) {
            foreach ($translations as $translationResult) {
                $mappedTranslations[$translationResult["key"]] = $translationResult["text"];
            }
        }
        
        return $mappedTranslations;
    }

}
