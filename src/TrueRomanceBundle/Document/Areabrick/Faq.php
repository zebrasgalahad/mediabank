<?php
namespace TrueRomanceBundle\Document\Areabrick;

use Pimcore\Extension\Document\Areabrick\AbstractTemplateAreabrick;

class Faq extends AbstractTemplateAreabrick
{
    public function getName()
    {
        return 'FAQ';
    }

    public function getDescription()
    {
        return 'Frequently asked questions';
    }

    public function getTemplateLocation()
    {
        return static::TEMPLATE_LOCATION_GLOBAL;
    }
}