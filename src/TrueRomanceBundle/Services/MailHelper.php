<?php

namespace TrueRomanceBundle\Services;

class MailHelper
{
    private $config;
    
    public function __construct()
    {
        $this->config = \Pimcore\Config::getWebsiteConfig()->configuration;
    }
    
    public function getSapImportEmailSubject()
    {
        return $this->config->getSapImportEmailSubject();
    }
    
    public function getSapImportEmailText()
    {
        return $this->config->getSapImportEmailText();
    }
    
    public function getSapImportBccEmails()
    {
        return $this->config->getSapImportBccEmails();
    }
    
    public function getSapImportEmailErrorSubject()
    {
        return $this->config->getSapImportEmailErrorSubject();
    }
    
    public function getSapImportEmailErrorText()
    {
        $this->config->getSapImportEmailErrorText();
    }
    
    public function getAssetImportEmailSubject()
    {
        return $this->config->getAssetImportEmailSubject();
    }
    
    public function getAssetImportEmailText()
    {
        return $this->config->getAssetImportEmailText();
    }
    
    public function getAssetImportBccEmails()
    {
        return $this->config->getAssetImportBccEmails();
    }
    
    public function getAssetImportEmailErrorSubject()
    {
        return $this->config->getAssetImportEmailErrorSubject();
    }
    
    public function getAssetImportEmailErrorText()
    {
        return $this->config->getAssetImportEmailErrorText();
    }
    
    public function getDateChangeEmailSubject()
    {
        return $this->config->getDateChangeEmailSubject();
    }
    
    public function getDateChangeEmailText()
    {
        return $this->config->getDateChangeEmailText();
    }
    
    public function getDateChangeBccEmails()
    {
        return $this->config->getDateChangeBccEmails();
    }
    
    public function getDateChangeEmailErrorSubject()
    {
        return $this->config->getDateChangeEmailErrorSubject();
    }
    
    public function getDateChangeEmailErrorText()
    {
        return $this->config->getDateChangeEmailErrorText();
    }
}
