<?php

namespace TrueRomanceBundle\Services\Csv;

use TrueRomanceBundle\Library\CSV;

class Validator
{
    /**
     *
     * @var CSV
     */
    private $csv;

    private $errors = [];

    private $data;

    public function __construct(CSV $csv)
    {
        $this->csv = $csv;
    }

    /**
     * @param $s
     * @return false|string
     */
    private function removeBomUtf8($s){
        if(substr($s,0,3) == chr(hexdec('EF')).chr(hexdec('BB')).chr(hexdec('BF'))){
            return substr($s,3);
        }else{
            return $s;
        }
    }

    public function isValid(string $csv, array $keys, $delimiter = ';'): bool
    {
        if (trim($csv) === "") {
            $this->errors[] = "String empty";

            return false;
        }

        $csv = $this->removeBomUtf8($csv);

        $this->csv->delimiter = $delimiter;

        $csvData = $this->csv->parse_string($csv);

        $firstLine = trim(strtok($csv, "\n"));

        if(is_array($csvData) && $csvData[0] && $csvData[0][0] && trim($csvData[0][0]) === $firstLine) {
            $this->errors[] = "Wrong delimiter. Expected '$delimiter'";

            return false;
        }

        if(isset($csvData[0]) === false || is_array($csvData[0]) === false) {
            $this->errors[] = "No data";

            return false;
        }

        foreach ($keys as $fieldname) {
            if(isset($csvData[0][$fieldname]) === false) {
                $this->errors[] = "Column not found: {$fieldname}";

                return false;
            }
        }

        $this->data = $csvData;

        return true;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getData(): array
    {
        return $this->data;
    }
}
