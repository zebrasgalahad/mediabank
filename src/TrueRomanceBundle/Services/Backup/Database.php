<?php

namespace TrueRomanceBundle\Services\Backup;

use CustomerPricelistBundle\Tools\Mail;

ini_set("memory_limit", "16384M");

class Database
{
    /**
     *
     * @var int
     */
    private $dumpKeepDays = 10;

    public function backup()
    {
        $this->cleanupOldFiles($this->getDatabaseBackupFolderPath());

        $databaseFile = $this->backupDatabase();

        if ($this->isDumpValid($databaseFile) === false) {
            error_log("DATABASE DUMP FAILED");
            unlink($databaseFile);
            return false;
        }

        error_log("DATABASE DUMP CREATED, CREATING ZIP FILE");

        $targetPath = $this->getDatabaseBackupFolderPath() . date("Y-m-d_H_i_s") . ".zip";

        $tarCommand = "zip -jrv $targetPath $databaseFile";

        error_log($tarCommand);

        @exec($tarCommand, $output);

        unlink($databaseFile);

        return true;
    }

    private function backupDatabase()
    {
        $dbConfig = \Pimcore::getContainer()->get("doctrine")->getConnections()["default"]->getParams();

        $dumpTargetPath = $this->getDatabaseBackupFolderPath() . date("Y-m-d_H_i_s") . "_{$dbConfig["dbname"]}.sql";

        $mysqlCommand = "mysqldump -u'{$dbConfig["user"]}' -p'{$dbConfig["password"]}' -h'{$dbConfig["host"]}' {$dbConfig["dbname"]} > $dumpTargetPath ";

        error_log("CREATING DATABASE DUMP");

        error_log($mysqlCommand);

        @exec($mysqlCommand, $output, $return);

        return $dumpTargetPath;
    }

    private function getDatabaseBackupFolderPath()
    {
        $path = \Pimcore\Model\WebsiteSetting::getByName("database_backup_path")->getData();

        if (is_dir($path) === false) {
            throw new \Exception("$path is not a directory and is required. Set in website settings as 'database_backup_path' (with trailing slash!).");
        }

        return $path;
    }

    private function getFilesFromDirectory($path)
    {
        return preg_grep('/^([^.])/', scandir($path));
    }

    private function cleanupOldFiles($dirPath)
    {
        $files = $this->getFilesFromDirectory($dirPath);

        $targetTimestamp = 60 * 60 * 24 * $this->dumpKeepDays;

        $now = time();
        foreach ($files as $file) {
            $filepath = $dirPath . $file;

            if (is_file($filepath) === true && $now - filemtime($filepath) >= $targetTimestamp) {
                unlink($filepath);
            }
        }
    }

    /**
     * 
     * @param string $file
     * @return bool
     */
    private function isDumpValid($file)
    {
        $isValid = strpos(file_get_contents($file), "-- Dump completed on");

        return $isValid;
    }

    /**
     * 
     * @param int $bytes
     * @param int $precision
     * @return double
     */
    private function formatBytes($bytes, $precision = 2)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB');

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= pow(1024, $pow);

        return round($bytes, $precision);
    }
}
