<?php

namespace TrueRomanceBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class AsyncJobHelper
{
    const DB_TABLE = "tasks";

    const TYPE_SAP_IMPORT = "sap_import";
    const TYPE_ASSET_IMPORT = "asset_import";
    const TYPE_DATE_CHANGE = "date_change";
    const TYPE_ASSET_CLEANUP = "asset_cleanup";

    private $container;
    private $request;
    private $db;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->request = $container->get('request_stack')->getMasterRequest();
        $this->db = \Pimcore\Db::get();
    }

    public function getCurrentJob($type)
    {
        $query = "SELECT * FROM " . self::DB_TABLE . " WHERE JobStatus = 'Open' AND type = '$type' LIMIT 1";

        $result = $this->db->fetchRow($query);

        if(!$result) {
            return false;
        }

        if(is_file($result["SerializedData"])) {
            $fileContent = file_get_contents($result["SerializedData"]);

            $result["SerializedData"] = $fileContent;
        } else {
            $result["SerializedData"] = unserialize($result["SerializedData"]);
        }

        return $result;
    }

    private function getJobInProcess($type)
    {
        $query = "SELECT * FROM " . self::DB_TABLE . " WHERE JobStatus = 'In Progress' AND type = '$type' LIMIT 1";

        $result = $this->db->fetchRow($query);

        return $result;
    }

    public function insertFirstData($creationDate, $userId, $data, $type, $locale = 'de_DE')
    {
        if(is_array($data)) {
            $data = serialize($data);
        }

        $query = "INSERT INTO " . self::DB_TABLE . "(CreationDate, UserId, JobStatus, SerializedData, type, Locale) VALUES ('$creationDate', '$userId', 'Open', '$data', '$type', '$locale');";

        $this->db->query($query);
    }

    public function insertDateValidityFirstData($creationDate, $userId, $startDate, $endDate, $type, $locale)
    {
        $dates = [
            "dateFrom" => $startDate,
            "dateTo" => $endDate,
            "locale" => $locale
        ];

        $serializedData = serialize($dates);

        $query = "INSERT INTO " . self::DB_TABLE . "(CreationDate, UserId, JobStatus, SerializedData, type, Locale) VALUES "
                . "('$creationDate', '$userId', 'Open', '$serializedData', '$type', '$locale');";

        $this->db->query($query);
    }

    public function setInProgressStatus($currentJobId, $type, $locale = 'de_DE')
    {
        $query = "UPDATE " . self::DB_TABLE . " SET JobStatus = 'In Progress' WHERE id = {$currentJobId} AND type = '$type' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function setFinishedStatus($currentJobId, $type, $locale = 'de_DE')
    {
        $query = "UPDATE " . self::DB_TABLE . " SET JobStatus = 'Finished' WHERE id = {$currentJobId} AND type = '$type' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function setJobTableStartDate($currentJobId, $type, $locale = 'de_DE')
    {
        $startDate = date('Y-m-d H:i:s');

        $query = "UPDATE " . self::DB_TABLE . " SET StartDate = '{$startDate}' WHERE id = {$currentJobId} AND type = '$type' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function setJobTableEndDate($currentJobId, $type, $locale = 'de_DE')
    {
        $endDate = date('Y-m-d H:i:s');

        $query = "UPDATE " . self::DB_TABLE . " SET EndDate = '{$endDate}' WHERE id = {$currentJobId} AND type = '$type' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function insertErrorMessageInJobTable($errorMessage, $jobId, $type, $locale = 'de_DE')
    {
        $query = $query = "UPDATE " . self::DB_TABLE . " SET ErrorMessage = '{$errorMessage}' WHERE id = {$jobId} AND type = '$type' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function getAdditionalData($type)
    {
        $jobId = $this->getJobInProcess(self::DB_TABLE)["id"];

        $query = "SELECT ErrorMessage, LogFilePath FROM " . self::DB_TABLE . " WHERE JobStatus = 'In Progress' AND id = $jobId AND type = '$type' LIMIT 1";
        $query = "SELECT ErrorMessage FROM " . self::DB_TABLE . " WHERE JobStatus = 'In Progress' AND id = $jobId AND type = '$type' LIMIT 1";

        $result = $this->db->fetchRow($query);

        return $result;
    }

    public function getUnserializedFileData($type)
    {
        $currentJobId = $this->getCurrentJob(self::DB_TABLE)["id"];

        $query = "SELECT SerializedData FROM " . self::DB_TABLE . " WHERE JobStatus = 'Open' AND id = {$currentJobId} AND type = '$type' LIMIT 1";

        $data = $this->db->fetchOne($query);

        return unserialize($data);
    }

    public function getUserDataByJobId($jobId, $type)
    {
        $query = "SELECT UserId FROM " . self::DB_TABLE . " WHERE id = {$jobId} AND type = '$type' LIMIT 1";

        $userId = $this->db->fetchOne($query);

        $userObject = \Pimcore\Model\User::getById($userId);

        $userData = [
            "userId" => $userId,
            "username" => $userObject->getName(),
            "firstName" => $userObject->getFirstname(),
            "lastName" => $userObject->getLastname(),
            "email" => $userObject->getEmail(),
        ];

        return $userData;
    }
}
