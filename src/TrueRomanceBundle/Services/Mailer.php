<?php

namespace TrueRomanceBundle\Services;

use Pimcore\Mail;

class Mailer
{

    public function send($email, $bcc = [], $subject, $message, $attachments = null)
    {
        $mailInstance = new Mail($subject);

        if (filter_var(trim($email), FILTER_VALIDATE_EMAIL)) {
            $mailInstance->addTo(trim($email));

            if (trim($message) !== "") {
                $mailInstance->setBodyText($message);
            }

            if ($attachments && is_array($attachments)) {
                foreach ($attachments as $filepath) {
                    if (is_file($filepath)) {
                        $swiftAttachment = \Swift_Attachment::fromPath($filepath);

                        $mailInstance->addAttachment($swiftAttachment);
                    }
                }
            }

            foreach ($bcc as $singleMail) {
                $mailInstance->addBcc($singleMail);
            }
            
            $mailInstance->send();
        }
    }
}
