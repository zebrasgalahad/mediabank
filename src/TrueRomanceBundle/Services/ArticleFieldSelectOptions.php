<?php

namespace TrueRomanceBundle\Services;

use Pimcore\Model\DataObject\Article;
use Pimcore\Model\DataObject\ClassDefinition;
use Pimcore\Model\DataObject\ClassDefinition\Data;
use Pimcore\Model\DataObject\ClassDefinition\DynamicOptionsProvider\SelectOptionsProviderInterface;

class ArticleFieldSelectOptions implements SelectOptionsProviderInterface
{
    const ALLOWED_CLASSES = [
        Data\Input::class,
        Data\Numeric::class,
        Data\Select::class,
        Data\Checkbox::class,
        Data\BooleanSelect::class,
        Data\ManyToOneRelation::class,
        Data\Date::class,
        Data\Datetime::class,
    ];

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return array
     */
    public function getOptions($context, $fieldDefinition)
    {
        $result = [];

        $articleClassDefinition = ClassDefinition::getById(Article::classId());

        $allFieldDefinitions = $articleClassDefinition->getFieldDefinitions();

        //Handle localized fields
        $localizedFields = $articleClassDefinition->getFieldDefinitions()['localizedfields'];

        $allLocalizedFields = $localizedFields->getChilds();

        foreach ($localizedFields->getReferencedFields() as $referencedField) {
            $allLocalizedFields = array_merge($allLocalizedFields, $referencedField->getChilds());
        }

        $allFieldDefinitions =  array_merge($allFieldDefinitions, $allLocalizedFields);

        foreach ($allFieldDefinitions as $fieldDefinition) {
            if (in_array(get_class($fieldDefinition), self::ALLOWED_CLASSES)) {
                $result[] = [
                    'key' => $fieldDefinition->getTitle(),
                    'value' => $fieldDefinition->getName(),
                ];
            }
        }

        return $result;
    }

    /**
     * Returns the value which is defined in the 'Default value' field
     * @param $context array
     * @param $fieldDefinition Data
     * @return mixed
     */
    public function getDefaultValue($context, $fieldDefinition)
    {
        return $fieldDefinition->getDefaultValue();
    }

    /**
     * @param $context array
     * @param $fieldDefinition Data
     * @return bool
     */
    public function hasStaticOptions($context, $fieldDefinition)
    {
        return true;
    }
}
