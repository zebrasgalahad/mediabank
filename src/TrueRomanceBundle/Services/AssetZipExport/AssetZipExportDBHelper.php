<?php

namespace TrueRomanceBundle\Services\AssetZipExport;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class AssetZipExportDBHelper
{
    const DB_TABLE = "asset_zip_export";

    private $container;
    private $request;
    private $db;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->request = $container->get('request_stack')->getMasterRequest();
        $this->db = \Pimcore\Db::get();
    }

    public function getCurrentJob($exportType)
    {
        $query = "SELECT * FROM " . self::DB_TABLE . " WHERE JobStatus = 'Open' AND exportType = '$exportType' LIMIT 1";

        $result = $this->db->fetchRow($query);

        if(!$result) {
            return false;
        }

        return $result;
    }

    private function getJobInProcess($exportType)
    {
        $query = "SELECT * FROM " . self::DB_TABLE . " WHERE JobStatus = 'In Progress' AND exportType = '$exportType' LIMIT 1";

        $result = $this->db->fetchRow($query);

        return $result;
    }

    /**
     * @return mixed
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getOpenJobs()
    {
        $query = "SELECT * FROM " . self::DB_TABLE . " WHERE JobStatus = 'Open'";

        $result = $this->db->fetchAll($query);

        return $result;
    }

    public function insertFirstData($creationDate, $userId, $catalogId, $exportType, $locale)
    {
        $query = "INSERT INTO " . self::DB_TABLE . "(CreationDate, UserId, CatalogId, JobStatus, Locale, exportType) VALUES ('$creationDate', '$userId', '$catalogId', 'Open', '$locale', '$exportType');";

        $this->db->query($query);
    }

    public function setInProgressStatus($currentJobId, $exportType, $locale)
    {
        $startDate = date('Y-m-d H:i:s');

        $query = "UPDATE " . self::DB_TABLE . " SET JobStatus = 'In Progress', StartDate = '{$startDate}' WHERE id = {$currentJobId} AND exportType = '$exportType' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function setFinishedStatus($currentJobId, $exportType, $locale)
    {
        $endDate = date('Y-m-d H:i:s');

        $query = "UPDATE " . self::DB_TABLE . " SET JobStatus = 'Finished', EndDate = '{$endDate}' WHERE id = {$currentJobId} AND exportType = '$exportType' AND Locale = '$locale'";

        $this->db->query($query);
    }

    public function setJobTableStartDate($currentJobId, $exportType)
    {
        $startDate = date('Y-m-d H:i:s');

        $query = "UPDATE " . self::DB_TABLE . " SET StartDate = '{$startDate}' WHERE id = {$currentJobId} AND exportType = '$exportType'";

        $this->db->query($query);
    }

    public function setJobTableEndDate($currentJobId, $exportType)
    {
        $endDate = date('Y-m-d H:i:s');

        $query = "UPDATE " . self::DB_TABLE . " SET EndDate = '{$endDate}' WHERE id = {$currentJobId} AND exportType = '$exportType'";

        $this->db->query($query);
    }

    public function insertErrorMessageInJobTable($errorMessage, $jobId, $exportType)
    {
        $query = $query = "UPDATE " . self::DB_TABLE . " SET ErrorMessage = '{$errorMessage}' WHERE id = {$jobId} AND exportType = '$exportType'";

        $this->db->query($query);
    }

    public function getAdditionalData($exportType)
    {
        $jobId = $this->getJobInProcess(self::DB_TABLE)["id"];

        $query = "SELECT ErrorMessage, LogFilePath FROM " . self::DB_TABLE . " WHERE JobStatus = 'In Progress' AND id = $jobId AND exportType = '$exportType' LIMIT 1";
        $query = "SELECT ErrorMessage FROM " . self::DB_TABLE . " WHERE JobStatus = 'In Progress' AND id = $jobId AND exportType = '$exportType' LIMIT 1";

        $result = $this->db->fetchRow($query);

        return $result;
    }

    public function getUserDataByJobId($jobId, $exportType)
    {
        $query = "SELECT UserId FROM " . self::DB_TABLE . " WHERE id = {$jobId} AND exportType = '$exportType' LIMIT 1";

        $userId = $this->db->fetchOne($query);

        $userObject = \Pimcore\Model\User::getById($userId);

        $userData = [
            "userId" => $userId,
            "username" => $userObject->getName(),
            "firstName" => $userObject->getFirstname(),
            "lastName" => $userObject->getLastname(),
            "email" => $userObject->getEmail(),
        ];

        return $userData;
    }
}
