<?php

namespace ClickLayoutBundle;

use Pimcore\Extension\Bundle\AbstractPimcoreBundle;

class ClickLayoutBundle extends AbstractPimcoreBundle
{
    public function getJsPaths()
    {
        return [
            '/bundles/clicklayout/js/pimcore/startup.js'
        ];
    }
}
