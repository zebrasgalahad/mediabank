<div class="add-notification-button" data-toggle="modal" data-target=".notification-modal.add">
    <?php echo $this->translate("notification.add.button.title"); ?>
</div>

<div class="notification-modal add modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"></button>
            <div class="modal-header">
                <p class="title"><?php echo $this->translate("notification.modal.add.title"); ?></p>
            </div>
            <div class="modal-body">
                <form name="notification-form-add">
                    <div class="row">
                        <div class="col-xs-12 col-sm-3">
                            <select class="small" name="interval">
                                <option value=""><?php echo $this->translate("notification.modal.choose-interval"); ?></option>
                                <option value="now"><?php echo $this->translate("notification.modal.choose-interval.now"); ?></option>
                                <option value="each-day"><?php echo $this->translate("notification.modal.choose-interval.each-day"); ?></option>
                                <option value="each-week"><?php echo $this->translate("notification.modal.choose-interval.each-week"); ?></option>
                                <option value="each-month"><?php echo $this->translate("notification.modal.choose-interval.each-month"); ?></option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-3 right">
                            <select class="small" name="catalog">
                                <option value=""><?php echo $this->translate("notification.modal.choose-catalog"); ?></option>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <input type="text" class="small"/>
                        </div>
                        
                    </div>
                    <div class="text-right">
                        <button type="submit" class="button yellow save"><?php echo $this->translate("notification.modal.save"); ?></button>
                    </div>
                </form>
                <div class="notification-button show-saved">
                    <p class="desktop"><?php echo $this->translate("notification.modal.show-saved-notifications"); ?></p>
                    <p class="mobile"><?php echo $this->translate("notification.modal.show-saved-notifications.short"); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->template("@ClickLayoutBundle/Resources/views/Includes/saved-notifications.html.php"); ?>