<div class="notification-modal saved modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal"></button>
            <div class="modal-header">
                <p class="title"><?php echo $this->translate("notifications.modal.saved.title"); ?></p>
            </div>
            <div class="modal-body">
                <form name="notification-form-saved">
                    <!-- foreach(): -->
                    <div class="row">
                        <div class="col-sm-12 col-md-9">
                            <div class="row">
                                <div class="col-xs-12 col-sm-3">
                                    <select class="small" name="interval">
                                        <option value=""><?php echo $this->translate("notification.modal.choose-interval"); ?></option>
                                        <option value="now"><?php echo $this->translate("notification.modal.choose-interval.now"); ?></option>
                                        <option value="each-day"><?php echo $this->translate("notification.modal.choose-interval.each-day"); ?></option>
                                        <option value="each-week"><?php echo $this->translate("notification.modal.choose-interval.each-week"); ?></option>
                                        <option value="each-month"><?php echo $this->translate("notification.modal.choose-interval.each-month"); ?></option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-3 right">
                                    <select class="small" name="catalog">
                                        <option value=""><?php echo $this->translate("notification.modal.choose-catalog"); ?></option>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <input type="text" class="small"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-3">
                            <button type="submit" class="button white save"><?php echo $this->translate("notification.modal.save"); ?></button>
                            <button class="delete"></button>
                            <button class="edit"></button>
                        </div>
                    </div>
                    <!-- endforeach(); -->
                </form>
                <div class="notification-button add-new">
                    <p class="desktop"><?php echo $this->translate("notification.modal.add-new-notification"); ?></p>
                    <p class="mobile"><?php echo $this->translate("notification.modal.add-new-notification.short"); ?></p>
                </div>
            </div>
        </div>
    </div>
</div>