pimcore.registerNS("pimcore.plugin.ClickLayoutBundle");

pimcore.plugin.ClickLayoutBundle = Class.create(pimcore.plugin.admin, {
    getClassName: function () {
        return "pimcore.plugin.ClickLayoutBundle";
    },

    initialize: function () {
        pimcore.plugin.broker.registerPlugin(this);
    },

    pimcoreReady: function (params, broker) {
        // alert("ClickLayoutBundle ready!");
    }
});

var ClickLayoutBundlePlugin = new pimcore.plugin.ClickLayoutBundle();
