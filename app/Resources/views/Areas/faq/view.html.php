<div class="faq-block">
    <div class="container-top">
        <div class="title-block">
            <h1 class="title"><?= $this->input("title", ["placeholder" => "title"]); ?></h1>
            <div class="close-screen"></div>
        </div>
    </div>

    <?php $i = 0; ?>
    <?php while($this->block("faqs")->loop()): ?>
        <?php $i++; ?>
        <div class="entry" id="faq-<?php echo $i; ?>">
            <div class="question"><?= $this->input("question", ["placeholder" => "question"]); ?></div>
            <div class="answer"><?= $this->wysiwyg("answer", ["placeholder" => "answer"]); ?></div>
        </div>
    <?php endwhile; ?>
</div>