<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190312114415 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("DROP TABLE IF EXISTS true_romance_asset_index");
        
        $this->addSql("DROP TABLE IF EXISTS true_romance_asset_facett");
        
        $this->addSql("CREATE TABLE `true_romance_asset_index` (
  `object_id` int(11) NOT NULL,
  `object_path` varchar(2048) DEFAULT NULL,
  `article_ean` varchar(1024) DEFAULT NULL,
  `catalog` varchar(2048) DEFAULT NULL,
  `file_type` varchar(45) DEFAULT NULL,
  `asset_type` varchar(45) DEFAULT NULL,
  `product_status_new_product` varchar(45) DEFAULT NULL,
  `product_status_new_product_date` datetime DEFAULT NULL,
  `image_orientation` varchar(45) DEFAULT NULL,
  `image_type` varchar(45) DEFAULT NULL,
  `video_type` varchar(45) DEFAULT NULL,
  `original_web_path` varchar(2048) DEFAULT NULL,
  `preview_web_path` varchar(2048) DEFAULT NULL,
  `absolute_file_path` varchar(2048) DEFAULT NULL,
  `salesdoc_type` varchar(2048) DEFAULT NULL,
  `search_text` text,
  `description_long` text,
  `description_short` text,
  `name` text,
  `locale` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`object_id`),
  FULLTEXT KEY `description_short` (`description_short`),
  FULLTEXT KEY `search_text` (`search_text`),
  FULLTEXT KEY `description_short_2` (`description_short`,`search_text`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");   
        
        $this->addSql("CREATE TABLE `true_romance_asset_facett` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `object_path` varchar(1024) DEFAULT NULL,
  `facet_id` varchar(255) DEFAULT NULL,
  `facet_title` varchar(255) DEFAULT NULL,
  `facet_value` varchar(255) DEFAULT NULL,
  `facet_value_display` varchar(2048) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_TRFacett_object_id` (`object_id`),
  KEY `idx_TRFacett_locale` (`locale`),
  KEY `idx_TRFacett_facet_id` (`facet_id`),
  KEY `idx_true_romance_asset_facetts_facet_id` (`facet_id`),
  KEY `idx_true_romance_asset_facetts_facet_value` (`facet_value`),
  KEY `idx_true_romance_asset_facett_facet_value` (`facet_value`),
  KEY `idx_true_romance_asset_facett_facet_id` (`facet_id`),
  KEY `idx_true_romance_asset_facett_facet_title` (`facet_title`)
) ENGINE=InnoDB AUTO_INCREMENT=8762 DEFAULT CHARSET=latin1;
");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
