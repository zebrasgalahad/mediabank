<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190424074238 extends AbstractPimcoreMigration
{

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $query = "ALTER TABLE `true_romance_asset_index` 
ADD COLUMN `container_id` INT NULL AFTER `countries`";

        $this->addSql($query);

        $query = "ALTER TABLE `true_romance_asset_index` 
CHANGE COLUMN `container_id` `container_id` INT(11) NULL DEFAULT NULL AFTER `object_id`;
";
        $this->addSql($query);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        
    }
}
