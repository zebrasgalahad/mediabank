<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190408101744 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $query = "ALTER TABLE `true_romance_album` 
                  ADD COLUMN `created` INT(11) NULL AFTER `owner_user_id`;";
      
        $this->addSql($query);
        
        $query = "ALTER TABLE `true_romance_album` 
                  ADD COLUMN `updated` INT(11) NULL AFTER `created`;";        
        
        $this->addSql($query);

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
