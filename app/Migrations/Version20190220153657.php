<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190220153657 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE `true_romance_asset_facett` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `object_path` varchar(1024) DEFAULT NULL,
  `facet_id` varchar(255) DEFAULT NULL,
  `facet_title` varchar(255) DEFAULT NULL,
  `facet_value` varchar(255) DEFAULT NULL,
  `locale` varchar(255) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_TRFacett_object_id` (`object_id`),
  KEY `idx_TRFacett_locale` (`locale`),
  KEY `idx_TRFacett_facet_id` (`facet_id`),
  KEY `idx_true_romance_asset_facetts_facet_id` (`facet_id`),
  KEY `idx_true_romance_asset_facetts_facet_value` (`facet_value`)
) ENGINE=InnoDB AUTO_INCREMENT=5676 DEFAULT CHARSET=latin1;");
        
        $this->addSql("CREATE TABLE `true_romance_asset_index` (
  `object_id` int(11) NOT NULL,
  `file_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `asset_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `image_orientation` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `image_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `video_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `original_web_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `preview_web_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `absolute_file_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `search_text` text COLLATE utf8mb4_bin,
  `description_long` text COLLATE utf8mb4_bin,
  `description_short` text COLLATE utf8mb4_bin,
  PRIMARY KEY (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
");        
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("DROP TABLE IF EXISTS true_romance_asset_facetts");
    }
}
