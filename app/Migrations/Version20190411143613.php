<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190411143613 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $query = "ALTER TABLE `true_romance_asset_index` 
                  ADD COLUMN `preview_web_width` INT(11) NULL AFTER `preview_web_path`;";
        
        $this->addSql($query);
        
        $query = "ALTER TABLE `true_romance_asset_index` 
                  ADD COLUMN `preview_web_height` INT(11) NULL AFTER `preview_web_width`;";
        
        $this->addSql($query);
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
