<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190403092414 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("CREATE TABLE `true_romance_album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `public` int(11) DEFAULT NULL,
  `owner_user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
");     
        
        $this->addSql("CREATE TABLE `true_romance_album_asset` (
  `asset_id` int(11) NOT NULL,
  `album_id` int(11) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`asset_id`,`album_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
");
        
        $this->addSql("CREATE TABLE `true_romance_album_user` (
  `album_id` varchar(45) COLLATE utf8mb4_bin NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`album_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;
");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        

    }
}
