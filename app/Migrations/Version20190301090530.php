<?php

namespace App\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Pimcore\Migrations\Migration\AbstractPimcoreMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190301090530 extends AbstractPimcoreMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("DROP TABLE IF EXISTS true_romance_asset_index");
        
        $this->addSql("CREATE TABLE `true_romance_asset_index` (
  `object_id` int(11) NOT NULL,
  `object_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `catalog` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `file_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `asset_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `product_status_new_product` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `image_orientation` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `image_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `video_type` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
  `original_web_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `preview_web_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `absolute_file_path` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `salesdoc_type` varchar(2048) COLLATE utf8mb4_bin DEFAULT NULL,
  `search_text` text COLLATE utf8mb4_bin,
  `description_long` text COLLATE utf8mb4_bin,
  `description_short` text COLLATE utf8mb4_bin,
  `name` text COLLATE utf8mb4_bin,
  `locale` varchar(45) COLLATE utf8mb4_bin DEFAULT NULL,
   FULLTEXT description_short(description_short),
   FULLTEXT search_text(search_text),
   FULLTEXT(description_short,search_text),  
  PRIMARY KEY (`object_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
"); 
        
        $this->addSql("ALTER TABLE true_romance_asset_index CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
