<?php 

return [
    "content" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => "870"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 95,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "content"
    ],
    "galleryCarousel" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => "1140",
                    "height" => "400",
                    "positioning" => "center",
                    "doNotScaleUp" => "1"
                ]
            ]
        ],
        "medias" => [
            "(max-width: 940px)" => [
                [
                    "method" => "cover",
                    "arguments" => [
                        "width" => "940",
                        "height" => "350",
                        "positioning" => "center"
                    ]
                ]
            ],
            "(max-width: 720px)" => [
                [
                    "method" => "cover",
                    "arguments" => [
                        "width" => "720",
                        "height" => "300",
                        "positioning" => "center"
                    ]
                ]
            ],
            "(max-width: 320px)" => [
                [
                    "method" => "cover",
                    "arguments" => [
                        "width" => "320",
                        "height" => "100",
                        "positioning" => "center"
                    ]
                ]
            ]
        ],
        "name" => "galleryCarousel",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1610630886,
        "creationDate" => 1610630886,
        "forcePictureTag" => FALSE,
        "id" => "galleryCarousel"
    ],
    "galleryThumbnail" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => "260",
                    "height" => "180",
                    "positioning" => "center",
                    "doNotScaleUp" => "1"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryThumbnail"
    ],
    "galleryCarouselPreview" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => "100",
                    "height" => "54",
                    "positioning" => "center",
                    "doNotScaleUp" => "1"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryCarouselPreview"
    ],
    "galleryLightbox" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => "900"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "SOURCE",
        "quality" => 75,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "galleryLightbox"
    ],
    "productDetail" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 600,
                    "height" => 600
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productDetail",
        "description" => "",
        "format" => "SOURCE",
        "quality" => 100,
        "highResolution" => 0,
        "modificationDate" => 1460099856,
        "creationDate" => 1460099856,
        "id" => "productDetail"
    ],
    "productList" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 310,
                    "height" => 310
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "productList",
        "description" => "",
        "format" => "SOURCE",
        "quality" => 75,
        "highResolution" => 0,
        "modificationDate" => 1459954835,
        "creationDate" => 1459936048,
        "id" => "productList"
    ],
    "shopDetail" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => "260",
                    "height" => "260"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "PNG",
        "quality" => 80,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "shopDetail"
    ],
    "shopDetailTechnology" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => "56",
                    "height" => "56"
                ]
            ]
        ],
        "medias" => [

        ],
        "description" => "",
        "format" => "PNG",
        "quality" => 90,
        "highResolution" => 0,
        "filenameSuffix" => NULL,
        "id" => "shopDetailTechnology"
    ],
    "shopDetailThumb" => [
        "items" => [
            [
                "method" => "cover",
                "arguments" => [
                    "width" => 56,
                    "height" => 56,
                    "positioning" => "center",
                    "doNotScaleUp" => TRUE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "shopDetailThumb",
        "description" => "",
        "format" => "SOURCE",
        "quality" => 80,
        "highResolution" => 0,
        "modificationDate" => 1460196959,
        "creationDate" => 1460196959,
        "id" => "shopDetailThumb"
    ],
    "print_keyvisual" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 2000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_keyvisual",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481124999,
        "creationDate" => 1481124976,
        "id" => "print_keyvisual"
    ],
    "print_productcell" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 600,
                    "height" => 600
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_productcell",
        "description" => "",
        "format" => "PRINT",
        "quality" => 99,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481729573,
        "creationDate" => 1481278050,
        "id" => "print_productcell"
    ],
    "print_productcell_small" => [
        "items" => [
            [
                "method" => "frame",
                "arguments" => [
                    "width" => 200,
                    "height" => 200
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_productcell_small",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481278789,
        "creationDate" => 1481278780,
        "id" => "print_productcell_small"
    ],
    "print_titleimage" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 1000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_titleimage",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1482149676,
        "creationDate" => 1481298467,
        "id" => "print_titleimage"
    ],
    "print_backgroundimage" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 3000
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "print_backgroundimage",
        "description" => "",
        "format" => "PRINT",
        "quality" => 90,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "modificationDate" => 1481530257,
        "creationDate" => 1481530238,
        "id" => "print_backgroundimage"
    ],
    "officeImage" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 300,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "officeImage",
        "description" => "",
        "group" => "",
        "format" => "PRINT",
        "quality" => 60,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1552380995,
        "creationDate" => 1552379730,
        "id" => "officeImage"
    ],
    "assetDetailThumbnail" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 840,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "assetDetailThumbnail",
        "description" => "",
        "group" => "",
        "format" => "JPEG",
        "quality" => 100,
        "highResolution" => 0.0,
        "preserveColor" => TRUE,
        "preserveMetaData" => TRUE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1555329336,
        "creationDate" => 1555057389,
        "id" => "assetDetailThumbnail"
    ],
    "assetDetailFull" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 1920,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "assetDetailFull",
        "description" => "",
        "group" => "",
        "format" => "JPEG",
        "quality" => 100,
        "highResolution" => 0.0,
        "preserveColor" => TRUE,
        "preserveMetaData" => TRUE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1555329343,
        "creationDate" => 1555057529,
        "id" => "assetDetailFull"
    ],
    "catalogListView" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 480,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "catalogListView",
        "description" => "",
        "group" => "",
        "format" => "JPEG",
        "quality" => 100,
        "highResolution" => 0.0,
        "preserveColor" => TRUE,
        "preserveMetaData" => TRUE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1555329329,
        "creationDate" => 1555060679,
        "id" => "catalogListView"
    ],
    "listView" => [
        "items" => [
            [
                "method" => "scaleByHeight",
                "arguments" => [
                    "height" => 220,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "listView",
        "description" => "",
        "group" => "",
        "format" => "JPEG",
        "quality" => 100,
        "highResolution" => 0.0,
        "preserveColor" => TRUE,
        "preserveMetaData" => TRUE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1555329319,
        "creationDate" => 1555060879,
        "id" => "listView"
    ],
    "hires_cmyk" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 3500,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "hires_cmyk",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558089115,
        "creationDate" => 1558089080,
        "id" => "hires_cmyk"
    ],
    "hires_rgb" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 3500,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "hires_rgb",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558089550,
        "creationDate" => 1558089131,
        "id" => "hires_rgb"
    ],
    "office" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 1190,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "office",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558093433,
        "creationDate" => 1558092473,
        "id" => "office"
    ],
    "web" => [
        "items" => [
            [
                "method" => "scaleByWidth",
                "arguments" => [
                    "width" => 3500,
                    "forceResize" => FALSE
                ]
            ]
        ],
        "medias" => [

        ],
        "name" => "web",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558093309,
        "creationDate" => 1558092521,
        "id" => "web"
    ],
    "original" => [
        "items" => [

        ],
        "medias" => [

        ],
        "name" => "original",
        "description" => "",
        "group" => "",
        "format" => "SOURCE",
        "quality" => 85,
        "highResolution" => 0.0,
        "preserveColor" => FALSE,
        "preserveMetaData" => FALSE,
        "rasterizeSVG" => FALSE,
        "downloadable" => FALSE,
        "modificationDate" => 1558095915,
        "creationDate" => 1558095898,
        "id" => "original"
    ]
];
