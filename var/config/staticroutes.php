<?php 

return [
    1 => [
        "id" => 1,
        "name" => "app_login",
        "pattern" => "#(\\w+)/login#",
        "reverse" => "/%prefix/login",
        "module" => NULL,
        "controller" => "@UserAuthenticationBundle\\Controller\\UserAuthenticationController",
        "action" => "login",
        "variables" => "prefix",
        "defaults" => "prefix=/de",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1550587826,
        "modificationDate" => 1550752725
    ],
    2 => [
        "id" => 2,
        "name" => "action",
        "pattern" => "#/(.*?)/action/([a-zA-Z\\-\\_]+)/([a-z\\-]+)#",
        "reverse" => "/%prefix/action/%controller/%action",
        "module" => NULL,
        "controller" => "%controller",
        "action" => "%action",
        "variables" => "prefix,controller,action",
        "defaults" => NULL,
        "siteId" => [

        ],
        "priority" => 1,
        "legacy" => FALSE,
        "creationDate" => 1550587827,
        "modificationDate" => 1550588599
    ],
    3 => [
        "id" => 3,
        "name" => "app_logout",
        "pattern" => "#(\\w+)/logout#",
        "reverse" => "/%prefix/logout",
        "module" => NULL,
        "controller" => "@UserAuthenticationBundle\\Controller\\UserAuthenticationController",
        "action" => "logout",
        "variables" => "prefix",
        "defaults" => "prefix=de",
        "siteId" => [

        ],
        "priority" => 0,
        "legacy" => FALSE,
        "creationDate" => 1550587950,
        "modificationDate" => 1550752678
    ]
];
