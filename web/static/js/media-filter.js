var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.mediaFilter = function () {
    var mediaBlock = $(".media-block.mosaic");

    var mediaFilter = $(".media-filter");
    var mediaFilterToggle = $(".media-filter .toggle");
    var mediaFilterDropdown = $(".media-filter .dropdown");
    var mediaFilterGroup = $(".media-filter .filter-group");
    var mediaFilterGroupTitle = $(".media-filter .filter-group > .title");
    var mediaFilterSubGroup = $(".media-filter .filter-group .filter-group-dropdown .categories > ul");
    var mediaFilterSubGroupTitle = $(".media-filter .filter-group .filter-group-dropdown .categories > ul > li:not(.sub-category)");
    var mediaFilterSubGroupDropdown = $(".media-filter .filter-group .filter-group-dropdown .categories > ul > li > ul > li");
    var mediaFilterFilter = $(".media-filter .filter");
    var mediaFilterCategory = $(".media-filter .categories li").not(".sub-category");

    var filterCookies = "";
    var filterReset = $(".media-filter .filter-reset");
    var filterResetButton = $(".media-filter .filter-reset button");

    if (Cookies.get("mediaFilter")) {
        filterCookies = (Cookies.get("mediaFilter")).split(",");
    }

    mediaFilterToggle.off("click touch");

    mediaFilterToggle.on("click touch", function () {
        var filterId = mediaFilter.attr("filter-id");

        if (mediaFilter.hasClass("open")) {
            mediaFilter.removeClass("open");
            mediaBlock.removeClass("filter-open");
            mediaFilterDropdown.slideUp(100);
            removeFilterCookie(filterId);
        } else {
            mediaFilter.addClass("open");
            mediaBlock.addClass("filter-open");
            mediaFilterDropdown.slideDown(100);
            setFilterCookie(filterId);
        }

        sbd.static.helper.refreshMosaic(true);
    });

    mediaFilterCategory.on("click touch", function () {
        var thisList = $(this).closest("ul");
        var subCategory = thisList.children(".sub-category");

        if ($(this).hasClass("open")) {
            $(this).removeClass("open");
            subCategory.slideUp(300);
        } else {
            $(this).addClass("open");
            subCategory.slideDown(300);
        }
    });

    mediaFilterGroupTitle.off("click touch");

    mediaFilterGroupTitle.on("click touch", function () {
        var thisFilterGroup = $(this).closest(".filter-group");
        var thisFilterGroupDropdown = thisFilterGroup.find(".filter-group-dropdown");

        if (thisFilterGroup.hasClass("open")) {
            thisFilterGroup.removeClass("open");
            thisFilterGroupDropdown.slideUp("fast");
        } else {
            thisFilterGroup.addClass("open");
            thisFilterGroupDropdown.slideDown("fast");
        }
    });

    mediaFilterSubGroupTitle.on("click touch", function () {
        var thisFilterSubGroup = $(this).closest(".filter-sub-group");
        var thisFilterSubGroupDropdown = thisFilterSubGroup.find(".filter-sub-group-dropdown");

        if (thisFilterSubGroup.hasClass("open")) {
            thisFilterSubGroup.removeClass("open");
            thisFilterSubGroupDropdown.slideUp("fast");
        } else {
            thisFilterSubGroup.addClass("open");
            thisFilterSubGroupDropdown.slideDown("fast");
        }

        markCheckedFilterSubGroups();
    });

    mediaFilterFilter.on("click touch", function () {
        var inputElement = $(this).find("input");

        if ($(this).hasClass("checked")) {
            $(this).removeClass("checked");
            inputElement.attr("checked", false);
        } else {
            $(this).addClass("checked");
            inputElement.attr("checked", true);
        }

        inputElement.trigger("change");

        showFilterReset();
    });

    filterResetButton.on("click touch", function (e) {
        e.preventDefault();
        resetFilter();
    });

    var showFilterReset = function () {
        if (mediaFilterFilter.hasClass("checked")) {
            filterReset.slideDown("fast");
        } else {
            filterReset.slideUp("fast");
        }
    };

    var markCheckedFilterSubGroups = function () {
//        mediaFilterSubGroup.each(function () {
//            if ($(this).find(".filter.checked").length > 0) {
//                $(this).addClass("has-checked-filters");
//            } else {
//                $(this).removeClass("has-checked-filters");
//            }
//        });
    };

    var resetFilter = function () {
        mediaFilterFilter.each(function () {
            var filterId = $(this).attr("filter-id");
            removeFilterCookie(filterId);
        });

        mediaFilterFilter.removeClass("checked");
        markCheckedFilterSubGroups();
        showFilterReset();
        mediaFilterSubGroup.removeClass("open");
        mediaFilterSubGroupDropdown.slideUp("fast");

        var inputElements = mediaFilter.find("input");

        inputElements.removeAttr("checked");
    };

    var setFilterCookie = function (filterId) {
        var currentFilterCookie = "";

        if (Cookies.get("mediaFilter")) {
            currentFilterCookie = (Cookies.get("mediaFilter")).split(",");
        }

        if (currentFilterCookie) {
            if ($.inArray(filterId, currentFilterCookie) == -1) {
                currentFilterCookie.push(filterId);

                Cookies.set("mediaFilter", currentFilterCookie.join(","), {path: '/'});
            }
        } else {
            Cookies.set("mediaFilter", filterId, {path: '/'});
        }
    }

    function removeFilterCookie(filterId) {
        var currentFilterCookie = "";

        if (Cookies.get("mediaFilter")) {
            currentFilterCookie = (Cookies.get("mediaFilter")).split(",");
        }

        var newFilterCookie = $.grep(currentFilterCookie, function (value) {
            return value != filterId;
        });

        Cookies.set("mediaFilter", newFilterCookie.join(","), {path: '/'});
    }

    $(".media-filter, .filter-group, .filter-sub-group").each(function () {
        var filterId = $(this).attr("filter-id");

        if ($.inArray(filterId, filterCookies) > -1) {
            $(this).addClass("open");
        }
    });

    mediaFilterFilter.each(function () {
        var filterId = $(this).attr("filter-id");

        if ($.inArray(filterId, filterCookies) > -1) {
            $(this).addClass("checked");
            $(this).find("input").attr("checked", true);
        }
    });

    mediaFilterGroup.each(function () {
        if ($(this).hasClass("init-open")) {
            $(this).slideDown(300);
        }
    });

    if (mediaFilter.hasClass("open")) {
        mediaBlock.addClass("filter-open");
        mediaFilterDropdown.addClass("initial-open");
        showFilterReset();
        markCheckedFilterSubGroups();
    }

    var windowResizing = false;

    $(window).resize(function () {
        if (!windowResizing) {
            windowResizing = true;

            setTimeout(function () {
                sbd.static.helper.refreshMosaic(true);
                windowResizing = false;
            }, 1000);
        }
    });
};