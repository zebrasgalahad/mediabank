$(document).ready(function () {
    var galleryProducts = $(".gallery-products");
    var productThumbnail = $(".gallery-products .thumbnails .product");

    var equalizeGallery = function () {
        galleryProducts.each(function () {
            var product = $(this).find(".full .product");
            var minHeight = 400;

            product.each(function () {
                var imageHeight = $(this).find("img").outerHeight();
                var titleHeight = $(this).find(".title").outerHeight();
                var linkHeight = $(this).find(".button").outerHeight();

                var thisHeight = imageHeight + titleHeight + linkHeight;


                if (thisHeight > minHeight) {
                    minHeight = thisHeight;
                }
            });

            $(this).find(".full").css("min-height", minHeight);
        });
    }

    productThumbnail.on("click touch", function () {
        var productId = $(this).attr("product-id");

        var thisGallery = $(this).closest(".gallery-products");

        thisGallery.find(".product").removeClass("active");
        thisGallery.find(".product[product-id='" + productId + "']").addClass("active");
    });

    equalizeGallery();

    $(window).load(function () {
        equalizeGallery();
    });

    $(window).resize(function () {
        equalizeGallery();
    });
});