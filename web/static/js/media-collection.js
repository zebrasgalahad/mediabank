var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

var countElement = $(".my-collection-inactive .count");

sbd.class.mediaCollection = function (public) {
    var myCollection = $(".my-collection-inactive");
    var mediaDashboard = $(".horizontal-media-dashboard");
    var mediaDashboardContainer = $(".horizontal-media-dashboard .container");
    var mediaDashboardAddedMessage = $(".horizontal-media-dashboard #added-message");
    var mediaDashboardToggle = $(".horizontal-media-dashboard .toggle");
    var openScreenSelector = $(".collection");
    var storeTime = sbd.data.storeTimeHours * 3600000;
    var locale = sbd.i18n.locale;

    var isPublic = public;

    var count = 0;

    var _initStart = function () {
        // so that old collection are also shown, set new key '{locale}-asset-ids' and delete old asset-ids key
        if (typeof store.get('asset-ids') !== 'undefined') {
            store.set(locale + '-asset-ids', store.get('asset-ids'), new Date().getTime() + storeTime);
            store.set('asset-ids', [], new Date().getTime() + storeTime);
        }

        openScreenSelector.on("click", function () {
            if (getAssetIds().length > 0) {
                openOverlay($("main > .media-collection-container"), true);
            }
        });

        updateCounter(count);

        mediaDashboardToggle.on("click", function () {

            if (mediaDashboardContainer.hasClass("visible")) {
                sbd.static.helper.hideDashboardContainer();
            } else {
                openOverlay($(".horizontal-media-dashboard .container"));
                sbd.static.helper.showDashboardContainer();
            }
        });
    };

    var _initElement = function (targetElement) {
        updateCounter(count);
        updateExpiration(count);
        addNewAssetToMediaCollection(targetElement);

        toggleElementStatus(targetElement);
    };

    var toggleElementStatus = function (targetElement) {
        if (hasAssetIds()) {
            var assetId = targetElement.attr("data-asset-id");

            if ($.inArray(assetId, getAssetIds()) !== -1) {
                if (targetElement.hasClass("added") === false) {
                    targetElement.addClass("added");
                }
            }
        }
    };

    var deleteAssetFromCollectionList = function (element) {
        var id = element.attr("data-asset-id");

        deleteAssetId(id);

        element.remove();

        updateCounter(count);

        $(".media[data-asset-id='" + id + "']").removeClass("added");
    };

    var addAssetIdToMediaCollection = function (assetId) {
        var idArray = store.get(locale + '-asset-ids');

        if (!idArray) {
            idArray = [];
        }

        if ($.inArray(assetId, idArray) === -1) {
            idArray.push(assetId);
        }

        store.set(locale + '-asset-ids', idArray, new Date().getTime() + storeTime);

        updateCounter();
    };

    var addNewAssetToMediaCollection = function (targetElement) {
        var idArray = new Array();
        var idArrayAvailable = new Array();
        var id = targetElement.attr("data-asset-id");

        if (typeof store.get(locale + '-asset-ids') !== 'undefined') {
            idArrayAvailable = (store.get(locale + '-asset-ids'));
        }

        if (targetElement.hasClass("added")) {
            idArrayAvailable.forEach(function (e) {
                if (e !== id) {
                    idArray.push(e);
                }
            });
            store.set(locale + '-asset-ids', idArray, new Date().getTime() + storeTime);
            targetElement.removeClass("added");
            count = idArray.length;
        } else {
            targetElement.addClass("added");
            idArrayAvailable.forEach(function (e) {
                idArray.push(e);
            });

            if ($.inArray(id, idArray) === -1) {
                idArray.push(id);
            }

            store.set(locale + '-asset-ids', idArray, new Date().getTime() + storeTime);
            count = idArray.length;

            mediaDashboardToggle.addClass("added");
            setTimeout(function () {
                mediaDashboardToggle.removeClass("added");
            }, 600);
        }

        updateCounter(count);
        updateExpiration(count);

        if (mediaDashboardContainer.is(":visible") && targetElement.parents(".horizontal-media-dashboard").length == 0) {
            sbd.static.helper.hideDashboardContainer();
        }
    };

    var updateCounter = function (count) {
        if (typeof store.get(locale + '-asset-ids') !== 'undefined' && store.get(locale + '-asset-ids')) {
            count = store.get(locale + '-asset-ids').length;
        }

        if (count > 0) {
            myCollection.addClass("my-collection-active");
            myCollection.removeClass("my-collection-inactive");

            if (!$("main > .media-collection-container > .media-collection").is(":visible")) {
                sbd.static.helper.showDashboard();
            }

            countElement.html(count);
        }
        if (count === 0) {
            myCollection.removeClass("my-collection-active");
            myCollection.addClass("my-collection-inactive");
            sbd.static.helper.hideDashboard();
            countElement.html(count);
        }
    };

    var updateExpiration = function () {
        if (getAssetIds() < new Date().getTime()) {
            store.removeExpiredKeys();
        }
    };

    var getAssetIds = function () {
        if (typeof store.get(locale + '-asset-ids') !== 'undefined') {
            return store.get(locale + '-asset-ids');
        }

        return [];
    };

    var deleteAssetId = function (assetId) {
        var assetIds = getAssetIds();

        assetIds.splice(assetIds.indexOf(assetId), 1);

        store.set(locale + '-asset-ids', assetIds, new Date().getTime() + storeTime);
    };

    var deleteAllAssetIds = function (assetId) {
        store.set(locale + '-asset-ids', [], new Date().getTime() + storeTime);
    };

    var hasAssetIds = function () {
        return typeof getAssetIds() !== 'undefined';
    };

    var openOverlay = function (container, hideContent) {
        sbd.static.helper.removeOverlay();

        sbd.static.helper.getTemplate("collection/list").done(function (content) {
            if (hideContent) {
                sbd.static.helper.hideContent();
                sbd.static.helper.hideDashboard();
            }

            sbd.static.helper.showLoading();

            var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/collection/asset/list");

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(getAssetIds())
            }).done(function (response) {
                sbd.static.helper.hideLoading();

                var html = $.tmpl(content, response);

                if (sbd.static.userPermissions.isAllowed("invite_users") === false) {
                    html.find(".selection-share").remove();
                }

                sbd.static.helper.appendOverlay(container, html);

                html.find("span.action-delete").on("click touch", function () {
                    deleteAssetFromCollectionList($(this).closest("div.media"));
                    sbd.static.helper.refreshMosaic(true);
                });

                sbd.static.helper.refreshMosaic(true);

                sbd.static.helper.calculateMaxDashboardItems();

                html.find("div.action-delete-all").on("click touch", function () {
                    $("div.media-collection .media").remove();

                    deleteAllAssetIds();

                    updateCounter();

                    sbd.static.helper.removeOverlay();

                    sbd.static.helper.showContent();

                    sbd.static.helper.hideDashboardContainer();
                });

                $(".download-all").on("click touch", function () {
                    openDownloadAllModal(response);
                });

                $(".add-to-album").on("click touch", function () {
                    openAddToAlbumModal();
                });

                $(".selection-share").on("click touch", function () {
                    var data = {
                        name: __("collection.share.album.name"),
                        asset_ids: getAssetIds(),
                        public: false
                    };

                    _addNewAlbum(data).done(function (response) {
                        var albumList = new sbd.class.albumList();

                        albumList.openShareAlbumModal(response.album.album_id);
                    });
                });
            }).fail(function (response) {

            });
        }).fail(function (content) {

        });
    };

    var openDownloadAllModal = function (data) {
        sbd.static.helper.showLoading();

        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/collection/asset/list");

        var assetIds = [];
        if ($(".media-collection .media-block .media[data-album-id]").length > 0) {
            $(".media-collection .media-block .media[data-album-id]").each(function (index, element) {
                assetIds.push($(element).attr("data-asset-id"));
            });
        } else {
            assetIds = getAssetIds();
        }

        sbd.static.helper.getTemplate("collection/download-all").done(function (content) {
            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(assetIds)
            }).done(function (response) {
                sbd.static.helper.hideContent();

                sbd.static.helper.hideLoading();

                var data = [];
                if (response.data && response.data.length > 0) {
                    var data = response.data;
                }

                var html = $.tmpl(content, {data: data});

                if (response.length === 0) {
                    html.find(".button.yellow.download").remove();
                }

                var downloadAllSelector = html.find(".collection-download .button.yellow.download");

                var loaderSelector = html.find(".modal-content");

                createZipDownloadFile(downloadAllSelector, html, loaderSelector);

                html.insertAfter($(".header"));

                html.modal("show");
                html.modal("show").on("shown.bs.modal", function (e) {
                    $("select").select2({
                        minimumResultsForSearch: -1
                    });
                });
            }).fail(function (response) {

            });
        }).fail(function (content) {

        });
    };

    var createZipDownloadFile = function (selector, html, loaderSelector) {
        selector.on("click touch", function (e) {
            e.preventDefault();

            var loaderText = __("my-collection.download-is-created");

            sbd.static.helper.showLoading(loaderSelector, loaderText);

            var ids = html.find("input[name='asset_ids[]']").map(function () {
                return $(this).val();
            }).get();
            var types = html.find("select[name='single_download_types[]']").map(function () {
                return $(this).val();
            }).get();

            var url = sbd.static.helper.createUrlWithLocale("/{_locale}/download/multiple-assets");

            var data = {
                asset_ids: ids,
                single_download_types: types
            };

            $.ajax({
                type: "POST",
                dataType: "json",
                url: url,
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(data)
            }).done(function (response) {
                sbd.static.helper.hideLoading(loaderSelector);

                window.location = response;

                $.confirm({
                    title: __("my-collection.download-was-created"),
                    content: __("my-collection.download-was-created-success-text"),
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        download: function () {
                            text: __("my-collection.download-zip-file");
                            window.location = response;
                            return false;
                        },
                        close: function () {
                            text: __("button.close");
                        }
                    }
                });
            }).fail(function (xhr, textStatus, errorThrown) {
                sbd.static.helper.hideLoading(loaderSelector);
            });
        });
    };

    var openAddToAlbumModal = function (singleAsset, callback) {
        sbd.static.helper.showLoading();

        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/list");

        var assetIds = new Array();

        if (singleAsset) {
            assetIds.push(singleAsset);
        } else {
            assetIds = getAssetIds();
        }

        $.when(
                sbd.static.helper.getTemplate("collection/add-to-album"),
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(assetIds)
                })
                )
                .done(function (content, response) {
                    sbd.static.helper.hideLoading();

                    var html = $.tmpl(content, {albums: response[0]});

                    html.insertAfter($(".header"));

                    html.modal("show");

                    html.find(".add-to-album").on("click touch", function () {
                        sbd.static.helper.deleteModal();

                        addAssetsToAlbum(html, $(this), assetIds);
                    });

                    html.find(".save-as-new-album").on("click touch", function (e) {
                        e.preventDefault();

                        sbd.static.helper.deleteModal();

                        if (callback) {
                            openAddNewAlbumModal(null, callback, false, assetIds);
                        } else {
                            openAddNewAlbumModal(null, null, false, assetIds);
                        }

                    });
                })
                .fail(function () {
                    //handle errors
                });
        ;
    };

    var createNewAlbum = function (container, callback, fromAlbumView, assetIds) {
        var albumName = container.find("input").val();

        if (albumName === "" || albumName.length < 3) {
            container.find("span.error").show();
        } else {
            container.find("span.error").hide();

            if (fromAlbumView == true) {
                assetIds = null;
            }

            var data = {
                name: albumName,
                public: false,
                asset_ids: assetIds
            };

            _addNewAlbum(data).done(function (response) {
                sbd.static.helper.deleteModal();

                $.confirm({
                    title: __("album.new.added.title"),
                    content: __("album.new.added.text"),
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        close: function () {
                            text: __("button.close")
                        }
                    }
                });
                var albumList = new sbd.class.albumList();

                albumList.updateAlbumCountAction();

                if (typeof callback === 'function') {
                    callback();
                }
            }).fail(function () {
                sbd.static.helper.hideLoading();
            });
        }
    };

    var _addNewAlbum = function (data) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/collection/album/add");

        return $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data)
        });
    }

    /**
     * 
     * @param {type} showSelectAlbum
     * @param {type} callback
     * @param {type} fromAlbumView
     * @param {type} assetIds
     * @returns {undefined}
     */
    var openAddNewAlbumModal = function (showSelectAlbum, callback, fromAlbumView, assetIds) {
        sbd.static.helper.showLoading();

        sbd.static.helper.getTemplate("collection/new-album").done(function (content) {
            sbd.static.helper.hideContent();

            sbd.static.helper.hideLoading();

            var html = $.tmpl(content, {});

            html.insertAfter($(".header"));

            html.modal("show");

            html.find(".select-album").on("click touch", function () {
                sbd.static.helper.deleteModal();

                openAddToAlbumModal();
            });

            if (showSelectAlbum === false) {
                html.find(".select-album").remove();
            }

            html.find(".save-as-new-album").on("click touch", function (e) {
                e.preventDefault();

                createNewAlbum(html, callback, fromAlbumView, assetIds);
            });
        }).fail(function (content) {
        });
    };

    var addAssetsToAlbum = function (modal, buttonElement, assetIds) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/collection/album/asset/add");

        var data = {
            album_id: buttonElement.attr("data-album-id"),
            asset_ids: assetIds
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data)
        }).done(function (response) {
            sbd.static.helper.deleteModal();

            $.confirm({
                title: __("album.assets.added.title"),
                content: __("album.assets.added.text"),
                type: 'green',
                typeAnimated: true,
                buttons: {
                    close: function () {
                        text: __("button.close")
                    }
                }
            });
        }).fail(function (response) {

        });
    };

    var getDurrentDate = function () {
        var date = new Date();

        var day = date.getDate();

        if (day.length === 1) {
            day = "0" + day;
        }

        var dateString = day + "." + date.getMonth() + "." + date.getFullYear();

        return dateString;
    };

    $(window).resize(function () {
        sbd.static.helper.calculateMaxDashboardItems();
    });

    return {
        initStart: function () {
            _initStart();
        },
        initElement: function (element) {
            _initElement(element);
        },
        toggle: function (element) {
            toggleElementStatus(element);
        },
        /**
         * 
         * @param {type} showSelectAlbum
         * @param {type} callback
         * @param {type} fromAlbumView
         * @param {type} assetIds
         * @returns {undefined}
         */
        openAddNewAlbumModal: function (showSelectAlbum, callback, fromAlbumView, assetIds) {
            openAddNewAlbumModal(showSelectAlbum, callback, fromAlbumView, assetIds);
        },
        openDownloadAllModal: function (data) {
            openDownloadAllModal(data);
        },
        addAssetIdToMediaCollection: function ($assetId) {
            addAssetIdToMediaCollection($assetId);
        },
        addNewAssetToMediaCollection: function (targetElemnt) {
            addNewAssetToMediaCollection(targetElemnt);
        },
        showDownloadLoaderMessage: function (selector) {
            showDownloadLoaderMessage(selector);
        },
        addNewAlbum: function (data) {
            return _addNewAlbum(data);
        },
        openAddToAlbumModal: function (singleAsset, callback) {
            openAddToAlbumModal(singleAsset, callback);
        }

    };
};

$(function () {
    var mediaCollection = new sbd.class.mediaCollection();

    mediaCollection.initStart();

});