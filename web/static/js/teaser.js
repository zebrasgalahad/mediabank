$(document).ready(function() {
    var equalizeTeaser = function () {
        if($(window).width() > 992) {
            $(".teaser-block").equalize({
                    equalize: 'outerHeight',
                    reset: true,
                    children: '.teaser.2-rows'
                }
            );
            
            $(".teaser-block").equalize({
                    equalize: 'outerHeight',
                    reset: true,
                    children: '.teaser.3-rows'
                }
            );

            $(".teaser-block .teaser").each(function() {
                var teaserHeight = $(this).outerHeight();
                var contentHeight = $(this).find(".content").outerHeight();
                
                if(teaserHeight + 20 < contentHeight) {
                    $(this).addClass("short");
                } else {
                    $(this).removeClass("short");
                }
            });
            
            $(".teaser.pos-x-center").each(function() {
                var content = $(this).find(".content");
                
                if($(this).outerWidth() > content.outerWidth() + 80) {
                    var leftPx = (content.outerWidth() / 2) + 40;
                    content.css("left", "calc(50% - " + leftPx + "px");
                }
            });
        }
    }
    
    setTimeout(function () {
        equalizeTeaser();
    }, 300);

    $(window).load(function () {
        equalizeTeaser();
    });

    $(window).resize(function () {
        equalizeTeaser();
    });
   
});