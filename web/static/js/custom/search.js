var sbd = sbd ? sbd : {};

sbd.data.assetlist = {
    currentPage: null,
    pages: null
};

$(function () {
    var catalogNavigationContainer = $(".container.horizontal-catalog-bar");

    var catalogNavigationContainerContent = catalogNavigationContainer.find(".catalog-list");

    var searchContainer = $('.horizontal-search-bar .search-container');

    var catalogLinkContainer = $('.container.horizontal-catalog-bar');

    var searchInput = searchContainer.find('input[name="search"]');

    var searchForm = searchContainer.find('form');

    var facetFilter = searchContainer.find(".media-filter");

    var facetFilterFacetContainer = searchContainer.find(".facets");

    var facetFilterCategory = facetFilter.find(".categories");

    var mainFacetsSelect = searchContainer.find('select[name="top-facets"]');

    var mainContentContainer = $(".container.content.media");

    var mainContentListContainer = mainContentContainer.find(".container.horizontal-asset-list");

    var mainContentListLoading = mainContentListContainer.find(".loading");

    var newestAssetTeaserList = $(".container.horizontal-newest-teaser-list");

    var collectionTeaserList = $(".container.horizontal-collection-teaser-list");

    var contentContainer = $(".container.content.media");

    var contentContainers = contentContainer.find(".container");

    var archives = $("div.archives div.archive");

    var mediaDashboardContainer = $(".horizontal-media-dashboard .container");

    var addToMyCollectionSelector = $(".media-block .row .media .title .add");

    var mediaContainer = $(".media-block .row .media");

    var mediaCollection = new sbd.class.mediaCollection();

    mainFacetsSelect.on("change", function (e) {
        updateFrontendByFilter(true);
    });

    searchForm.on("submit", function (e) {
        e.preventDefault();

        if ($(".product-detail").is(":visible") === true) {
            return;
        }

        updateFrontendByFilter(true);
    });

    facetFilter.find("input").on("change", function (e) {
        updateFrontendByFilter(true);
    });

    facetFilter.find(".filter-reset").on("click", function (e) {
        updateFrontendByFilter(true);
    });

    addToMyCollectionSelector.on("click touch", function (e) {
        e.stopPropagation();

        var mediaContainer = $(this).closest(".media");

        mediaCollection.addNewAssetToMediaCollection(mediaContainer);
    });

    $.each(mediaContainer, function (index, container) {
        mediaCollection.toggle($(container));
    });

    archives.on("click", function (e) {
        e.preventDefault();

        var catalog = $(this).find("a").attr("data-catalog");

        var params = {f: {catalog: [catalog]}, start: 1};

        updateFrontendByFilter(true, params);
    });

    var updateFrontendByFilter = function (reset, requestData) {
        if (!requestData) {
            var requestData = getRequestData();
        }

        sbd.static.request.setRequestData(requestData);

        if (isCatalogStartpage(requestData) === true) {
            contentContainers.hide();

            renderCatalogStartPage(requestData);
        } else {
            renderAssetList(requestData, reset);

            facetFilter.show();
            searchContainer.addClass("with-filter");
        }

        var search = $(".search");
        var searchForm = $(".search-form");
        var searchInput = $(".search input");
        var searchButton = $(".search button");

        if (searchInput.val() != "") {
            search.addClass("active");
        } else {
            search.removeClass("active");
        }

        searchButton.off("click touch");
        searchButton.on("click touch", function (e) {
            if (search.hasClass("active")) {
                e.preventDefault();
                searchInput.val("");
                searchForm.submit();
            }
        });

        searchInput.off("keypress");
        searchInput.keypress(function (e) {
            if (e.which == 13) {
                searchForm.submit();
                return false;
            } else {
                search.removeClass("active");
            }
        });
    };

    var renderCatalogStartPage = function (requestData) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/asset/list");

        sbd.static.helper.showLoading(contentContainer);

        $.ajax({
            url: url,
            data: requestData
        }).done(function (data) {
            History.pushState(null, null, "?" + $.param(requestData));

            sbd.static.helper.getTemplate("list/single-asset").done(function (template) {
                newestAssetTeaserList.show();
                renderNewestAssetList(data.newestAssets, template, newestAssetTeaserList.find(".media-block"));

                renderCatalogBar(data.catalogs).done(function () {
                    setTeaserListLinks();
                });

                sbd.static.helper.hideLoading(contentContainer);

                sbd.static.helper.refreshMosaic(true);

                renderMainFacets(data);
            }).fail(function (template) {

            });

            sbd.static.helper.getTemplate("list/single-collection").done(function (template) {
                renderNewestAssetList(data.collections, template, collectionTeaserList.find(".media-block .row"));

                collectionTeaserList.show();

                collectionTeaserList.find(".count").html("(" + data.collections.length + ")");

                sbd.static.helper.hideLoading(contentContainer);

                sbd.static.helper.refreshMosaic(true);
            }).fail(function (template) {

            });
        });
    }

    var setTeaserListLinks = function () {
        var requestData = getRequestData();

        var newestAssetsTeaserData = jQuery.extend(true, {}, requestData);

        if (!newestAssetsTeaserData["f"]) {
            newestAssetsTeaserData["f"] = {};
        }

        newestAssetsTeaserData["f"]["product_status_new_product"] = ["all"];

        var collectionsTeaserData = jQuery.extend(true, {}, requestData);

        if (!collectionsTeaserData["f"]) {
            collectionsTeaserData["f"] = {};
        }

        collectionsTeaserData["f"]["asset_type"] = ["collection"];

        $(".all.newest-assets-teaser").on("click", function (e) {
            e.preventDefault();

            updateFrontendByFilter(true, newestAssetsTeaserData);
        });

        $(".all.collections-teaser").on("click", function (e) {
            e.preventDefault();

            updateFrontendByFilter(true, collectionsTeaserData);
        });
    };

    var renderNewestAssetList = function (data, template, listElement) {
        listElement.html("");

        var i = 0;
        var mediaCollection = new sbd.class.mediaCollection();
        for (i in data) {
            var counter = parseInt(i) + 1;

            data[i].counter = counter;

            var html = $.tmpl(template, data[i]);

            html.on("click touch", function (e) {
                e.preventDefault();

                handleListAssetEvents(event, this, mediaCollection);
            });

            mediaCollection.toggle(html);

            html.appendTo(listElement);
        }
    }

    /**
     * 
     * @param {type} requestData
     * @param {type} reset
     * @returns {undefined}
     */
    var renderAssetList = function (requestData, reset) {
        if (reset) {
            sbd.data.assetlist.currentPage = null;
            sbd.data.assetlist.pages = null;
        } else {
            if (sbd.data.assetlist.currentPage + 1 > sbd.data.assetlist.pages && sbd.data.assetlist.currentPage !== null && sbd.data.assetlist.pages !== null) {
                return;
            }
        }

        requestData.page = sbd.data.assetlist.currentPage + 1;

        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/asset/list");

        sbd.static.helper.getTemplate("list/single-asset").done(function (template) {
            sbd.static.helper.showLoading(mainContentListLoading);

            $.ajax({
                url: url,
                data: requestData
            }).done(function (data) {
                renderMainFacets(data);

                sbd.data.assetlist.pages = data.meta.pages;
                ++sbd.data.assetlist.currentPage;

                History.pushState(null, null, "?" + $.param(requestData));

                sbd.data.assetlist.pages = data.meta.pages;

                mainContentContainer.find(".container").hide();

                mainContentListContainer.show();

                var listElement = mainContentListContainer.find("div.list");

                if (reset === true) {
                    listElement.html("");
                }

                var i = 0;
                for (i in data.assets) {
                    var counter = parseInt(i) + 1;

                    data.assets[i].counter = counter;

                    var html = $.tmpl(template, data.assets[i]);

                    var mediaCollection = new sbd.class.mediaCollection();

                    html.on("click touch", function (e) {
                        e.preventDefault();

                        handleListAssetEvents(event, this, mediaCollection);
                    });

                    mediaCollection.toggle(html);

                    html.appendTo(listElement);
                }

                if (data.assets.length === 0) {
                    listElement.html('<div class="no-results">' + __("asset.list.no.results") + '</div>');
                }

                renderCatalogBar(data.catalogs);

                renderFacetCategories(data, requestData);

                renderFacets(data, requestData);

                sbd.static.helper.hideLoading(mainContentListLoading);

                sbd.static.helper.refreshMosaic(true);
            });
        }).fail(function (template) {

        });
    };

    var renderMainFacets = function (response) {
        var currentRequestData = getRequestData();

        var mainFacets = response.mainFacets;

        var filters = currentRequestData.f ? currentRequestData.f : {};

        var html = '<select name="top-facets" class="top-facets tr-select">';
        $.each(mainFacets, function (index, facet) {
            var key = facet.facet_id;
            var value = facet.facet_value;
            var displayValue = facet.facet_value_display;

            var selected = filters[key] && $.inArray(value, filters[key]) !== -1 ? ' selected ' : '';

            html += '<option value="' + key + ':' + value + '" ' + selected + '>' + displayValue + '</option>';
        });

        var htmlElement = $(html);

        htmlElement.on("change", function (e) {
            updateFrontendByFilter(true);
        });

        $("select.top-facets.tr-select").replaceWith(htmlElement);
        $("div.tr-select").remove();

        new sbd.class.trSelect();
    };

    var handleListAssetEvents = function (event, context, mediaCollection) {
        if ($(event.target).is(".add")) {
            mediaCollection.initElement($(context));

            if (mediaDashboardContainer.hasClass("visible")) {
                sbd.static.helper.hideDashboard();
            }

            return;
        }

        var assetDetail = sbd.class.assetDetail();

        assetDetail.initProductListMode(context);
    };

    var renderFacets = function (data, requestData) {
        sbd.static.helper.getTemplate("filter/group").done(function (template) {
            var resultHtml = "";
            $.each(data.facets, function (facetIndex, facet) {
                var facetId = facet[0].facet_id;
                var facetTitle = facet[0].facet_title;

                var sectionChecked = facet[0]["section"];

                var templateData = {
                    filterId: facetId,
                    filterTitle: facetTitle,
                    filters: facet,
                    sectionChecked: facet[0]["section"]
                };

                var templateHtml = $.tmpl(template, templateData);

                if (sectionChecked === true) {
                    templateHtml.addClass("open").addClass("init-open");
                }

                resultHtml += templateHtml[0].outerHTML;
            })

            facetFilterFacetContainer.html(resultHtml);

            new sbd.class.mediaFilter();

            facetFilterFacetContainer.find("input").on("change", function () {
                updateFrontendByFilter(true);
            });
        }).fail(function (template) {

        });
    };

    var renderFacetCategories = function (data, requestData) {
        var requestCategories = [];
        if (requestData["f"] && requestData["f"]["category"]) {
            requestCategories = requestData["f"]["category"];
        }

        if (data.categories) {
            var html = $(createFacetsHtml(data.categories, requestCategories));

            html.each(function () {
                if ($(this).find("input:checked").length > 0) {
                    $(this).addClass("has-checked-filters");
                } else {
                    $(this).removeClass("has-checked-filters");
                }
            });

            $(html).find("input:checked").parent("li").addClass("checked");

            $.each($(html).find("input:checked"), function (index, element) {
                var parents = $(element).parents("li").not(".filter.checked");

                parents.each(function (index, element) {
                    var targetElement = $(element).prev();

                    targetElement.addClass("open").show();

                    $(element).show();
                });
            });

            if (requestCategories.length > 0) {
                $(".filter-group.category .title").closest(".filter-group").find(".filter-group-dropdown").addClass("open").slideDown("fast");
            }

            html.find('input, li.filter').on('click touch', function (e) {
                e.stopPropagation();

                if ($(this).find("input:checked").length > 0) {
                    $(this).find("input:checked").prop("checked", false);
                } else {
                    $(this).find("input").prop("checked", true);
                }

                renderAssetList(getRequestData(), true);
            });

            facetFilterCategory.html(html);
        }
    };

    var createFacetsHtml = function (categories, requestCategories) {
        var html = '';
        $.each(categories, function (index, category) {
            html += '<ul>';

            var checkedAttribute = '';

            if ($.inArray(category.category_id, requestCategories) > -1) {
                checkedAttribute = ' checked ';
            }

            if (category.sub && category.sub.length > 0) {
                html += '<li class="selection" >' + category.facet_value_display + '</li>';
                html += '<li class="sub-category">';
                html += createFacetsHtml(category.sub, requestCategories);
                html += '</li>';
            } else {
                html += '<li class="filter"><input ' + checkedAttribute + ' type = "checkbox" name = "category[]" value = "' + category.category_id + '" / > ' + category.facet_value_display + ' </li>';
            }

            html += '</ul>';
        });

        return html;
    };

    var isCatalogStartpage = function (requestData) {
        return requestData["start"] && requestData["start"] == 1;
    };

    var renderCatalogBar = function (catalogs) {
        return sbd.static.helper.getTemplate("list/catalog").done(function (template) {
            catalogNavigationContainerContent.html("");
            var i = 0;
            for (i in catalogs) {
                var html = $.tmpl(template, catalogs[i]);

                if (catalogs[i]["count"] == 0) {
                    html.find("a").addClass("inactive");
                }

                if (catalogs[i]["active"] === true) {
                    html.find("a").addClass("active");
                }

                html.on("click", function (e) {
                    e.preventDefault();

                    if ($(this).find("a").hasClass("inactive")) {
                        return;
                    }

                    var catalogElements = $(".media-navigation.catalog-list li a");

                    catalogElements.removeClass("active");

                    $(this).find("a").addClass("active");

                    updateFrontendByFilter(true);

                    var assetDetail = new sbd.class.assetDetail();

                    assetDetail.hideOverlay();

                    sbd.static.helper.refreshMosaic(true);
                });

                html.appendTo(catalogNavigationContainerContent);
            }
        }).fail(function (template) {

        });
    };

    var getRequestData = function () {
        return sbd.static.request.getRequestData();
    };

    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == sbd.static.helper.getDocHeight() && sbd.static.helper.pageIsScrollable() === true) {
            updateFrontendByFilter(false);
        }
    });

    if ($.isEmptyObject(sbd.custom.requestData) === false) {
        updateFrontendByFilter(true, sbd.custom.requestData);
    }
});




