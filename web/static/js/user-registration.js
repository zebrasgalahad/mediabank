$(document).ready(function () {
    if(!HTMLFormElement.prototype.reportValidity) {
        HTMLFormElement.prototype.reportValidity = function() {
          if (this.checkValidity()) return true;
          var btn = document.createElement('button');
          this.appendChild(btn);
          btn.click();
          this.removeChild(btn);
          return false;
        }
    }
    
    var registerData = $(".register .register-data");
    var termsOfUse = $(".register .terms-of-use");
    var backToForm = $(".register .back-to-form");
    var form = document.querySelector("#invite");
    var reportButton = $(".show-terms-of-use");

    backToForm.on("click", function () {
        registerData.slideDown("slow");
        termsOfUse.slideUp("slow");
    });

    reportButton.on("click", function () {
        var reportVal = form.reportValidity();
        if (reportVal === true) {
            registerData.slideUp("slow");
            termsOfUse.slideDown("slow");
        }
    });

    $("select.select2").select2();
});