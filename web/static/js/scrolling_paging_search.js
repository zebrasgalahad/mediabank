if ($(".search-results").length >= 1) {
    $(function () {
        var productListLoading = false;

        var renderProductEntries = function (entries, appendEntries) {
            $.each(entries, function (index, entry) {
                var $entry = loadTemplate($("#list-search-result-entry-template"), {
                    'link': entry.link,
                    'image': entry.image,
                    'headline': entry.description_text,
                    'description': entry.search_text,
                    'type': herpa.search.types[entry.type]
                }).hide();

                $entry.appendTo(".search-results");
                $entry.fadeIn("slow", function () {});
            });
        };

        var loadPageEndHandler = function () {
            if (herpa.search.pages >= herpa.search.currentPage) {
                ++herpa.search.currentPage;

                var url = herpa.search.pimcoreUrl + "?limit=" + herpa.search.page_limit + "&page=" + herpa.search.currentPage + "&json=true&key=" + herpa.search.searchTerm;

                $.ajax({
                    url: url,
                }).done(function (response) {
                    herpa.search.pages = response.meta.pages;

                    renderProductEntries(response.data, true);

                    productListLoading = false;
                }).fail(function (response) {

                });
            }
        };

        var triggerPageEndHandler = function () {
            window.onscroll = function (ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
                    loadPageEndHandler();

                    productListLoading = true;
                }
            };
        };

        $(window).scroll(function () {
            triggerPageEndHandler();
        });

        $(window).resize(function () {
            triggerPageEndHandler();
        })
    });
}
