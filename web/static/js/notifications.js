$(document).ready(function() {
    var addNotificationModal = $(".notification-modal.add"); 
    var savedNotificationsModal = $(".notification-modal.saved");
    
    var notificationButtonShowSaved = $(".notification-button.show-saved");
    var notificationButtonAddNew = $(".notification-button.add-new");
    
    notificationButtonShowSaved.on("click", function() {
        addNotificationModal.modal("hide");
        savedNotificationsModal.modal("show");
    });
    
    notificationButtonAddNew.on("click", function() {
        savedNotificationsModal.modal("hide");
        addNotificationModal.modal("show");
    });
});