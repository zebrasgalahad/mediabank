$(function () {
    var galleryModal = $(".gallery-modal");
    
    var galleryModalPreviewImage = $(".gallery-full .preview-image");
    
    galleryModalPreviewImage.on("click", function () {
        var imageIndex = $(this).attr("image-index");
        
        var modal = $("#gallery-modal-" + $(this).attr("modal-id"));

        modal.modal("show");

        modal.on('shown.bs.modal', function () {
            var swiper = new Swiper($(this).find('.swiper-container'), {
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                centeredSlides: true,
                initialSlide: imageIndex
            });
            
            $(".pagination .current").text(swiper.activeIndex + 1);

            swiper.on('slideChange', function () {
                $(".pagination .current").text(swiper.activeIndex + 1);
            });

            setTimeout(function () {
                $(".swiper-container .swiper-wrapper").removeClass("load");
            }, 100);
        });

        modal.on('hidden.bs.modal', function () {
            $(".swiper-container .swiper-wrapper").addClass("load");
        });
    });
    
    galleryModal.on("shown.bs.modal", function() {
        styleModal(); 
    });
    
    var styleModal = function() {
        var modalImage = $(".gallery-modal .modal-dialog .swiper-slide .image img");
        
        var windowHeight = $(window).outerHeight();
        
        var modalHeaderHeight = $(".gallery-modal .modal-header").outerHeight();
        
        var modalImageMaxHeight = Math.max(windowHeight - modalHeaderHeight - 100, 400);
        
        modalImage.css("max-height", modalImageMaxHeight);
    }

    $(window).resize(function () {
        styleModal();
    });
});