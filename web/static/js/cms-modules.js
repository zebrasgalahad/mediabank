$(document).ready(function() {
    var cmsModules = $(".cms-modules > div");
    var gallery3ImagesClassName = "gallery-3-images";
    
    cmsModules.each(function() {
        if($(this).hasClass(gallery3ImagesClassName) && $(this).prev().attr("class") == gallery3ImagesClassName) {
            var prevGallery = $(this).prev().find(".row");

            $(this).find(".row > div").each(function() {
                prevGallery.append($(this));
            });
            
            $(this).remove();
        }
    });
});