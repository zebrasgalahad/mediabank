sbd.static = sbd.static ? sbd.static : {};
sbd.static.request = {
    requestData: {},
    setRequestData: function (requestData) {
        this.requestData = requestData;
    },
    getRequestData: function () {
        var catalogLinkContainer = $('.container.horizontal-catalog-bar');

        var searchContainer = $('.horizontal-search-bar .search-container');

        var mainFacetsSelect = searchContainer.find('select[name="top-facets"]');

        var catalog = catalogLinkContainer.find("a.active").attr("data-catalog");

        var facetFilter = searchContainer.find(".media-filter");

        var searchInput = searchContainer.find('input[name="search"]');

        var requestData = {};

        if (catalog) {
            requestData = {
                f: {
                    catalog: [catalog]
                }
            };
        }

        var selectValue = mainFacetsSelect.find("option:selected").val().split(":");

        if (selectValue.length === 2 && selectValue[1] !== "") {
            if (!requestData["f"]) {
                requestData["f"] = {};
            }

            requestData.f[selectValue[0]] = [selectValue[1]];
        }

        if (searchInput.val() !== "") {
            requestData["search"] = searchInput.val();
        }

        var facetFilterData = facetFilter.find('input:checked,input[checked="checked"]').serializeArray();

        if (facetFilterData.length > 0) {
            $.each(facetFilterData, function (index, element) {
                var cleanedKey = element.name.replace("[]", "");

                if (typeof requestData['f'] === 'undefined') {
                    requestData['f'] = {};

                }

                if (typeof requestData['f'][cleanedKey] === 'undefined') {
                    requestData['f'][cleanedKey] = [];
                }

                requestData.f[cleanedKey].push(element.value);
            });
        }

        return requestData;
    }
};

