sbd.static = sbd.static ? sbd.static : {};

sbd.static.userPermissions = {
    isAllowed: function (key) {
        if (typeof sbd.data.userPermissions === 'undefined') {
            return false;
        }

        return sbd.data.userPermissions[key];
    }
};