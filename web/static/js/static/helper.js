sbd.static = sbd.static ? sbd.static : {};
sbd.static.helper = {
    _getContentContainer: function () {
        return $(".container.content.media");
    },
    _getContainerTop: function () {
        return $(".container.top");
    },
    _getContainerHorizontalCatalog: function () {
        return $(".container.horizontal-catalog-bar");
    },
    _getContainerHorizontalSearch: function () {
        return $(".container.horizontal-search-bar");
    },
    _getHorizontalMediaDashboard: function () {
        return $(".horizontal-media-dashboard");
    },
    _getHorizontalMediaDashboardContainer: function () {
        return $(".horizontal-media-dashboard .container");
    },
    _getContainerContentPage: function () {
        return $(".container.content-page.overlay");
    },

    /**
     * 
     * @returns {Boolean}
     */
    isFilteredByCatalog: function () {
        return $(".horizontal-catalog-bar").length > 0;
    },
    createUrlWithLocale: function (url, params) {
        var url = url.replace("{_locale}", sbd.i18n.locale);

        if (params) {
            var queryString = $.param(params);

            url = url + "?" + queryString;
        }

        return url;
    },
    templateStorage: {},
    /**
     * 
     * @param {string} template - "asset-detail" or "/sub/asset-detail" with folder
     * @returns {Promise}
     */
    getTemplate: function (template) {
        var url = "/html/" + template + ".html.php"

        var deferred = new $.Deferred();

        var that = this;
        if (this.templateStorage[template]) {
            deferred.resolve(this.templateStorage[template]);
        } else {
            $.get(url, function () {

            }).done(function (response) {
                var regex = /\[\[.*?\]\]/g;

                var matches = response.match(regex);

                if (matches && matches.length > 0) {
                    $.each(matches, function (index, match) {
                        var cleanedMatch = match.replace("[[", "");

                        cleanedMatch = cleanedMatch.replace("]]", "");

                        var translation = __(cleanedMatch);

                        response = response.replace(match, translation);
                    });
                }

                that.templateStorage[template] = $(response);

                deferred.resolve(that.templateStorage[template]);
            }).fail(function (response) {
                deferred.fail(response);
            });
        }

        return deferred.promise();
    },
    getDocHeight: function () {
        return Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
                );
    },
    catalogIsFiltered: function () {
        return window.location.search !== "";
    },
    /**
     * 
     * @param {object} element
     * @param {string} text
     * @returns void
     */
    showLoading: function (element, text) {
        if (!element) {
            element = $("body");
        }

        var config = {};

        if (text) {
            config.text = text;
        }

        $(element).LoadingOverlay('show', config);
    },
    /**
     * 
     * @param {object} element
     * @returns void
     */
    hideLoading: function (element) {
        if (!element) {
            element = $("body");
        }

        $(element).LoadingOverlay('hide');
    },
    deleteModal: function () {
        $('.modal').remove();
        $('.modal-backdrop').remove();
        $('body').removeClass("modal-open");
    },
    refreshMosaic: function (forceRefresh) {
        var mediaBlock = $(".media-block.mosaic");

        if (!mediaBlock.hasClass("refreshing") && mediaBlock.parents(".horizontal-media-dashboard").length == 0 || forceRefresh) {
            if (!forceRefresh && $(window).width() < 1440 || forceRefresh) {
                mediaBlock.addClass("refreshing");

                setTimeout(function () {
                    mediaBlock.find(".media").attr("style", "");
                    mediaBlock.removeClass("jQueryMosaic");

                    mediaBlock.Mosaic({
                        innerGap: 10,
                        maxRowHeight: 252,
                        maxRowHeightPolicy: 'tail',
                        defaultAspectRatio: 1
                    });
                    
                    mediaBlock.find(".image img").attr("style", "");
                }, 300);

                setTimeout(function () {
                    mediaBlock.removeClass("refreshing");
                    mediaBlock.find(".media").addClass("visible");
                }, 600);
            }
        }
    },
    appendOverlay: function (container, element) {
        container.append(element);

        var that = this;
        element.find(".close-screen").on("click touch", function () {
            that.removeOverlay();
            that.showContent();
            that._getHorizontalMediaDashboard().addClass("visible");
        });
    },
    removeOverlay: function (container) {
        $("main > .media-collection-container").html("");
        $(".horizontal-media-dashboard .container").html("");
    },
    showContent: function () {
        this._getContentContainer().show();
        this._getContainerTop().show();
        this._getContainerHorizontalCatalog().show();
        this._getContainerHorizontalSearch().show();

        this.refreshMosaic(true);
    },
    hideContent: function () {
        this._getContentContainer().hide();
        this._getContainerTop().hide();
        this._getContainerHorizontalCatalog().hide();
        this._getContainerHorizontalSearch().hide();
        this._getContainerContentPage().hide();
    },
    showContentPage: function (container, element) {
        container.append(element);
        this._getContainerContentPage().show();
    },
    hideContentPage: function () {
        this._getContainerContentPage().hide();
        this._getContainerContentPage().html("");
        sbd.static.helper.showContent();
    },
    showDashboard: function () {
        this._getHorizontalMediaDashboard().addClass("visible");
    },
    hideDashboard: function () {
        this._getHorizontalMediaDashboard().removeClass("visible");
    },
    showDashboardContainer: function () {
        var mediaDashboardContainer = this._getHorizontalMediaDashboardContainer();

        mediaDashboardContainer.slideDown(300);
        mediaDashboardContainer.addClass("visible");
    },
    hideDashboardContainer: function () {
        var mediaDashboardContainer = this._getHorizontalMediaDashboardContainer();

        mediaDashboardContainer.slideUp(300);
        mediaDashboardContainer.removeClass("visible");
        sbd.static.helper.removeOverlay();
    },
    showAssetDetailFullOverlay: function (data) {
        sbd.static.helper.showLoading();

        sbd.static.helper.getTemplate("asset-detail/asset-detail-full-modal").done(function (content) {
            sbd.static.helper.hideLoading();

            var html = $.tmpl(content, data);

            html.insertAfter($(".header"));

            html.modal("show").on('hidden.bs.modal', function (e) {
                this.remove();
            })
        }).fail(function (content) {

        });
    },
    hideAssetDetailFullOverlay: function() {
        this._getAssetDetailFullContainer().html("");
    },
    calculateMaxDashboardItems: function () {
        var dashboardContainerWidth = $(".horizontal-media-dashboard .container").outerWidth();

        var mediaItemsWidth = 0;

        setTimeout(function () {
            $(".horizontal-media-dashboard .container .media").each(function () {
                mediaItemsWidth += $(this).outerWidth() + 10;
                if (mediaItemsWidth < dashboardContainerWidth - 200) {
                    $(this).addClass("visible");
                } else {
                    $(this).remove();
                    $(".horizontal-media-dashboard .container .more").addClass("visible");
                }
            });
        }, 100);
    },
    pageIsScrollable: function () {
        return $(".scrollable").is(":visible") && !$(".horizontal-media-dashboard > .container").is(":visible");
    }
};

