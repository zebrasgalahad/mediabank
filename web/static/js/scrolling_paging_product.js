if ($(".product-list-container").length >= 1) {

    $(function () {
        var productListLoading = false;
        var loadingIcon = $(".product-list .loading");

        var renderProductEntries = function (entries, appendEntries) {
            $.each(entries, function (index, entry) {
                var $entry = loadTemplate($("#list-product-result-entry-template"), {
                    'link': entry.url,
                    'image': entry.image,
                    'headline': entry.name,
                    'price': entry.price,
                    'status': entry.stock_status
                }).hide();

                $entry.appendTo(".product-list-container");
                $entry.fadeIn("slow", function () {});
            });
        };

        var loadPageEndHandler = function () {
            if (herpa.productlist.pages >= herpa.productlist.currentPage) {
                ++herpa.productlist.currentPage;

                var serializedData = $(".product-filter-form").serialize();

                var url = herpa.productlist.pimcoreUrl + "?limit=" + herpa.productlist.page_limit + "&page=" + herpa.productlist.currentPage + "&json=true&key=" + herpa.productlist.searchTerm + "&" + serializedData;

                $.ajax({
                    url: url,
                }).done(function (response) {
                    herpa.productlist.pages = response.meta.pages;

                    renderProductEntries(response.data, true);

                    productListLoading = false;
                    loadingIcon.slideUp("fast");
                }).fail(function (response) {

                });
            } 
        };

        var triggerPageEndHandler = function () {
            window.onscroll = function (ev) {
                if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && !productListLoading && herpa.productlist.pages >= herpa.productlist.currentPage) {
                    loadPageEndHandler();

                    productListLoading = true;
                    loadingIcon.slideDown("fast");
                }
            };
        };

        $(window).scroll(function () {
            triggerPageEndHandler();
        });

        $(window).resize(function () {
            triggerPageEndHandler();
        })
    });
}
