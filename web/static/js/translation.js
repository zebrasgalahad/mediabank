/**
 * Translates a sring in proper languages
 * @param {type} translationString - the string to translate
 * @returns {string}
 */
function __() {
    if (arguments.length === 0) {
        return "";
    }

    var translationString = arguments[0];

    if (typeof sbd.i18n === "object") {
        $.each(sbd.i18n.translations, function (key, value) {
            if (key === translationString) {
                translationString = value;

                if (value === "") {
                    translationString = key;
                }
            }
        });
    }

    if (arguments.length > 1) {
        for (i = 1; i < arguments.length; ++i) {
            translationString = translationString.replace("%s", arguments[i]);
        }
    }

    return translationString;
}