var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.albumList = function (public) {
    var albumLink = $("a.my-albums");

    var albumCountElement = albumLink.find("span.count");

    var overlayContainer = $(".container.media-collection-container");

    var albumsTeaserList = $(".horizontal-albums-teaser-list");

    var mediaCollection = new sbd.class.mediaCollection();

    var isPublic = false;
    if (typeof public !== "undefined") {
        isPublic = public;
    }

    var init = function () {
        if (isPublic === false) {
            updateAlbumCountAction();
        }

        albumLink.off("click touch");

        albumLink.on("click touch", function (e) {
            e.preventDefault();

            showAlbumListOverlay(overlayContainer);
        });
    };

    $(window).resize(function () {
        var mediaCollectionContainer = $(".media-collection-container .media-collection");

        if (albumsTeaserList.is(":visible") || mediaCollectionContainer.is(":visible")) {
            sbd.static.helper.refreshMosaic(true);
        }
    });

    $(window).trigger("resize");

    var showAlbumListOverlay = function () {
        sbd.static.helper.removeOverlay();

        sbd.static.helper.getTemplate("album/list").done(function (content) {
            sbd.static.helper.showLoading();

            var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/list");

            $.ajax({
                type: "GET",
                dataType: "json",
                url: url,
            }).done(function (response) {
                sbd.static.helper.hideContent();
                sbd.static.helper.hideLoading();

                var html = $.tmpl(content, {albums: response}).appendTo(overlayContainer);

                html.find(".action-delete").on("click touch", function (e) {
                    e.stopPropagation();

                    var albumElement = $(this).closest(".media");

                    deleteAlbumAction(albumElement.attr("data-album-id")).done(function () {
                        albumElement.fadeOut(700, function () {
                            albumElement.remove();

                            sbd.static.helper.refreshMosaic(true);
                        });

                        updateAlbumCountAction();
                    });
                });

                html.find(".save-new-album").on("click touch", function (e) {
                    e.preventDefault();

                    mediaCollection.openAddNewAlbumModal(false, showAlbumListOverlay, true);
                });

                html.find("div.media").on("click touch", function (e) {
                    e.preventDefault();

                    var albumId = $(this).attr("data-album-id");

                    showAlbumDetailOverlay(albumId);
                });

                sbd.static.helper.appendOverlay(overlayContainer, html);

                sbd.static.helper.refreshMosaic(true);
            }).fail(function (response) {

            });
        }).fail(function (content) {

        });
    };

    var openShareAlbumModal = function (albumId) {
        sbd.static.helper.getTemplate("album/share").done(function (content) {
            var html = $.tmpl(content, {});

            html.insertAfter($(".header"));

            var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/user/suggest");

            html.find('.external-user').select2({
                tags: true,
                "language": {
                    "noResults": function () {
                        return __("album.modal.add.external.mail");
                    }
                }
            });

            html.find('.ajax-existing-user').select2({
                ajax: {
                    url: url,
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data.items
                        };
                    }
                }
            });

            html.find('.share-album').on("click touch", function (e) {
                e.preventDefault();

                var form = $(this).closest("form");

                var formData = form.serializeArray();

                html.find(".error").hide();

                if (validateShareAlbum(formData) === true) {
                    shareAlbumAction(formData, albumId);
                } else {
                    html.find(".error").show();
                }
            });

            html.modal("show");
        });

    };

    var validateEmail = function (email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    var validateShareAlbum = function (formData) {
        if (formData.length === 0) {
            return false;
        }

        $.each(formData, function (index, element) {
            if (element.name === "new") {
                if (validateEmail(element.value) === false) {
                    return false;
                }
            }
        });

        return true;
    };

    var shareAlbumAction = function (formData, albumId) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/share");

        sbd.static.helper.showLoading();

        var data = {
            albumId: albumId,
            formData: formData
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data)
        }).done(function (response) {
            sbd.static.helper.hideLoading();
            sbd.static.helper.deleteModal();

            $.confirm({
                title: __("album.shared.title"),
                content: __("album.shared.text"),
                type: 'green',
                typeAnimated: true,
                buttons: {
                    close: function () {
                        text: __("button.close")
                    }
                }
            });

        }).fail(function (response) {

        });
    };

    var showAlbumDetailOverlay = function (albumId) {
        sbd.static.helper.removeOverlay();

        sbd.static.helper.getTemplate("album/detail").done(function (content) {
            sbd.static.helper.showLoading();

            var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/get");

            $.ajax({
                type: "GET",
                url: url,
                data: {id: albumId}
            }).done(function (response) {
                sbd.static.helper.hideContent();
                sbd.static.helper.hideLoading();

                var html = $.tmpl(content, response).appendTo(overlayContainer);

                if (isPublic) {
                    html.find(".event-share-album, .nav, .event-delete-album, .title-block").remove();

                    html.find(".event-album-name").removeAttr("contenteditable");
                }

                if (sbd.static.userPermissions.isAllowed("invite_users") === false) {
                    html.find(".event-share-album").remove();
                }

                html.find(".media-block .media span.action-delete").on("click touch", function (e) {
                    e.stopPropagation();

                    var mediaElement = $(this).closest(".media-block .media");
                    var albumId = mediaElement.attr("data-album-id");
                    var assetId = mediaElement.attr("data-asset-id");

                    deleteAlbumAssetAction(albumId, assetId);

                    mediaElement.remove();

                    sbd.static.helper.refreshMosaic(true);
                });

                html.find(".event-download-all").on("click touch", function (e) {
                    var mediaCollection = new sbd.class.mediaCollection(isPublic);

                    mediaCollection.openDownloadAllModal({data: response.assets, locale: sbd.i18n.locale});
                });

                html.find(".event-album-name").on("focusout", function (e) {
                    e.preventDefault();

                    updateAlbumAction($(this).attr("data-album-id"), {name: $(this).text()}, $(this));
                });

                html.find(".event-share-album").on("click touch", function (e) {
                    e.preventDefault();

                    openShareAlbumModal(albumId);
                });

                html.find(".event-delete-album").on("click touch", function (e) {
                    e.preventDefault();

                    var albumId = $(this).attr("data-album-id");

                    deleteAlbumAction(albumId).done(function () {
                        sbd.static.helper.removeOverlay();

                        showAlbumListOverlay(overlayContainer);
                    });
                });

                html.find(".event-show-all").on("click touch", function (e) {
                    e.preventDefault();

                    showAlbumListOverlay(overlayContainer);
                });

                if (!public) {
                    html.find(".event-album-name").keypress(function (e) {
                        var key = e.which;
                        if (key === 13) {
                            e.preventDefault();

                            $(this).trigger("blur");
                        }
                    });

                    html.find("#event-album-description").on("focusout", function (e) {
                        e.preventDefault();

                        updateAlbumAction($(this).attr("data-album-id"), {description: $(this).val()}, $(this));
                    });

                    html.find("#event-album-description").keypress(function (e) {
                        var key = e.which;
                        if (key === 13) {
                            e.preventDefault();

                            $(this).trigger("blur");
                        }
                    });
                }

                html.find(".media").on("click touch", function (e) {
                    e.preventDefault();

                    var element = $(this);

                    var assetDetail = new sbd.class.assetDetail(isPublic);

                    assetDetail.initAlbumListMode(element.attr("data-asset-id"), element.attr("data-album-id"));
                });

                if (response.navigation) {
                    var navigation = response.navigation;

                    var prevElement = html.find("span.prev")
                    if (navigation.prev) {
                        prevElement.on("click touch", function (e) {
                            e.preventDefault();

                            showAlbumDetailOverlay(navigation.prev);
                        });
                    } else {
                        prevElement.addClass("disabled");
                    }

                    var nextElement = html.find("span.next");
                    if (navigation.next) {
                        nextElement.on("click touch", function (e) {
                            e.preventDefault();

                            showAlbumDetailOverlay(navigation.next);
                        });
                    } else {
                        nextElement.addClass("disabled");
                    }
                }

                sbd.static.helper.appendOverlay(overlayContainer, html);

                sbd.static.helper.refreshMosaic(true);
            }).fail(function (response) {

            });
        }).fail(function (content) {

        });
    };

    var deleteAlbumAssetAction = function (albumId, assetId, listElement) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/asset/delete", {album_id: albumId, asset_id: assetId});

        if (listElement) {
            sbd.static.helper.showLoading(listElement);
        }

        return $.ajax({
            type: "GET",
            url: url,
        }).done(function (response) {
            if (listElement) {
                sbd.static.helper.hideLoading(listElement);

                listElement.fadeOut(750);
            }
        }).fail(function (response) {

        });
    };

    var updateAlbumAction = function (albumId, data, targetElement) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/update", {id: albumId});

        sbd.static.helper.showLoading(targetElement);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: url,
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(data)
        }).done(function (response) {
            sbd.static.helper.hideLoading(targetElement);
        }).fail(function (response) {

        });
    };

    var deleteAlbumAction = function (albumId) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/delete");

        return $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
            data: {id: albumId}
        });
    };

    var updateAlbumCountAction = function () {
        if (isPublic) {
            return;
        }

        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/album/user/count");

        $.ajax({
            type: "GET",
            dataType: "json",
            url: url,
        }).done(function (response) {
            albumCountElement.html(response.count);
        }).fail(function (response) {

        });
    };

    init();

    return {
        updateAlbumCountAction: function () {
            return updateAlbumCountAction();
        },
        showAlbumDetailOverlay: function (albumId) {
            return showAlbumDetailOverlay(albumId);
        },
        deleteAlbumAssetAction: function (albumId, assetId, listElement) {
            return deleteAlbumAssetAction(albumId, assetId, listElement);
        },
        openShareAlbumModal: function (albumId) {
            return openShareAlbumModal(albumId);
        },
        showAlbumListOverlay: function () {
            showAlbumListOverlay();
        }
    };
};

$(function () {
    if ($(".my-albums.albums-inactive").length > 0) {
        var albumList = new sbd.class.albumList(false);
    }

    if (sbd.custom.requestData["album"] && sbd.custom.requestData["id"]) {
        var assetDetail = new sbd.class.assetDetail();

        assetDetail.loadAlbumListData(sbd.custom.requestData["id"], sbd.custom.requestData["album"]);

        return;
    }

    if (sbd.custom.requestData["album"]) {
        var albumList = new sbd.class.albumList(false);

        albumList.showAlbumDetailOverlay(sbd.custom.requestData["album"]);
    }
});

