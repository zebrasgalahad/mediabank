$(document).ready(function() {
    var equalizeProductDetail = function() {
        var productDetailImage = $(".product-detail .image");
        var productDetailData = $(".product-detail .data");
        
        productDetailData.css("min-height", productDetailImage.height());
    }
    
    equalizeProductDetail();
    
    $(window).resize(function() {
        equalizeProductDetail(); 
    });
});