var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.mosaic = function () {
    var mediaBlock = $(".media-block.mosaic");
    var mediaBlockLoading = $(".loading[for='media-block']");
    var media = $(".media-block .media");
    var mediaDashboard = $(".media-dashboard");
    var mediaDashboardToggle = $(".media-dashboard .toggle");
    var mediaDashboardContainer = $(".media-dashboard .container");
    var mediaDashboardItemRemove = $(".media-dashboard .item .remove");




    var initMosaic = function () {
        media.each(function () {
            var initHeight = $(this).outerHeight();
            var initWidth = $(this).outerWidth();

            $(this).attr("init-height", initHeight);
            $(this).attr("init-width", initWidth)
        });

        mediaBlock.Mosaic({
            innerGap: 10,
            maxRowHeight: 320,
            maxRowHeightPolicy: 'tail',
            defaultAspectRatio: 1
        });

        mediaBlock.addClass("visible");
        mediaBlockLoading.hide();
    };

    var showDashboard = function () {
        mediaDashboard.addClass("visible");
    }

    var hideDashboard = function () {
        mediaDashboard.removeClass("visible");
        mediaDashboard.removeClass("open");
        mediaDashboardContainer.hide();
    }

    var checkDashboard = function () {
        if (media.hasClass("added")) {
            showDashboard();
        } else {
            hideDashboard();
        }
    }

    mediaDashboardToggle.on("click touch", function () {
        mediaDashboard.toggleClass("open");
        mediaDashboardContainer.slideToggle("fast");
    });

    mediaDashboardItemRemove.on("click touch", function () {
        var thisItem = $(this).closest(".item");

        thisItem.remove();

        if ($(".media-dashboard .item").length == 0) {
            hideDashboard();
        }
    });

    setTimeout(function () {
        initMosaic();
    }, 500);

    checkDashboard();
};

$(function () {
    new sbd.class.mosaic();
});
