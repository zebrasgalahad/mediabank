var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.homepage = function () {

    var albumList = new sbd.class.albumList();
    var assetDetail = new sbd.class.assetDetail();
    var contentPage = new sbd.class.contentPage();

    var openAlbumDetailOverlay = function (selector) {
        selector.on("click touch", function (e) {
            e.preventDefault();

            var albumId = $(this).attr("data-album-id");

            albumList.showAlbumDetailOverlay(albumId);

        });
    };

    var openCollectionDetailOverlay = function (selector) {
        selector.on("click touch", function (e) {
            e.preventDefault();

            assetDetail.initProductListMode($(this));
        });
    };

    return {
        openAlbumDetailOverlay: function (selector) {
            openAlbumDetailOverlay(selector);
        },
        openCollectionDetailOverlay: function (selector) {
            openCollectionDetailOverlay(selector);
        }
    };
};

$(function () {
    var albumSelector = $(".album.homepage-list");

    var collectionSelector = $(".media-block .row .media.file");

    var homepage = new sbd.class.homepage();

    homepage.openAlbumDetailOverlay(albumSelector);

    homepage.openCollectionDetailOverlay(collectionSelector);

    sbd.static.helper.refreshMosaic(true);
});

