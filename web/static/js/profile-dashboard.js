$(document).ready(function () {
    var profileDashboard = $("header .profile-dashboard");
    var profileDashboardToggle = $("header .profile-dashboard .toggle");
    var profileDashboardMenu = $("header .profile-dashboard .profile-menu");

    profileDashboardToggle.on("click touch", function (e) {
        e.preventDefault();

        if (profileDashboard.hasClass("open")) {
            profileDashboard.removeClass("open");
//             profileDashboardMenu.slideUp("fast");
        } else {
            profileDashboard.addClass("open");
        }
    });
});