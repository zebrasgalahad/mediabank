var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.contentPage = function (public) {
    var contentPageLink = $("a.content-page");
    
    var contentPageContainer = $(".container.content-page.overlay");
    
    var init = function () {
        var contentPageCloseScreen = $(".container.content-page.overlay .close-screen");
        
        contentPageCloseScreen.on("click touch", function() {
            sbd.static.helper.hideContentPage();
        });
        
        
        contentPageLink.off("click touch");

        contentPageLink.on("click touch", function (e) {
            e.preventDefault();
        
            var url = $(this).attr("href");

            showContentPageOverlay(contentPageContainer, url);
        });
        
        initFaq();
    };
    
    var initFaq = function() {
        var faqQuestion = $(".faq-block .question");
        
        faqQuestion.on("click touch", function() {
            var thisEntry = $(this).closest(".entry");
            
            thisEntry.toggleClass("open");
            thisEntry.find(".answer").slideToggle(300); 
        });
    }
    
    var showContentPageOverlay = function (container, url) {
        sbd.static.helper.showLoading();
        sbd.static.helper.removeOverlay();   
        sbd.static.helper.hideContentPage();

        $.ajax({
            type: "GET",
            dataType: "html",
            url: url,
        }).done(function (response) {
            sbd.static.helper.hideContent();
            sbd.static.helper.hideLoading();

            sbd.static.helper.showContentPage(contentPageContainer, response);
            
            init();
        }).fail(function (response) {

        });
    };
    
    init();
};