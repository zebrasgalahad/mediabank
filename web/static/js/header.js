$(document).ready(function() {
    var header = $("header");
    var headerNavbarToggle = $("header .navbar-toggle");
    var headerActions = $("header .header-actions");
    var headerheaderActionsLink = $("header .header-actions a");
    
    headerNavbarToggle.on("click touch", function() {
        if($(window).width() <= 720) {
            headerActions.slideToggle(300);
        }
    });
    
    headerActions.on("click touch", function() {
        if($(window).width() <= 720) {
            headerActions.slideUp(300);
        }
    });
    
    $(window).scroll(function() {
        var scrollTop = $(this).scrollTop();
        
        if(scrollTop < 10) {
            header.removeClass("scroll");
        } else {
            header.addClass("scroll");
        }
    });
});