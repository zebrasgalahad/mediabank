var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.trSelect = function () {
    $("select.tr-select").each(function () {
        var select = $(this);
        var selectClass = $(this).attr("class");
        var selectName = $(this).attr("name");
        var selectOptions = $(this).find("option");
        var selectedOptionText = $(this).find("option:selected").text();

        var trSelectHtml = "<div class='tr-select " + selectClass + "' for='" + selectName + "'>";

        trSelectHtml += "<div class='select-title'>" + selectedOptionText + "</div>";

        trSelectHtml += "<div class='options'>";

        selectOptions.each(function (index) {
            var optionValue = $(this).attr("value");
            var optionText = $(this).text();
            var optionSelected = select.val() == optionValue ? "selected" : "";

            trSelectHtml += "<div class='option " + optionSelected + "' index='" + (index + 1) + "'>" + optionText + "</div>";

        });

        trSelectHtml += "</div></div>";

        $(trSelectHtml).insertAfter($(this));
    });

    $(".tr-select").each(function () {
        var minWidth = $(this).find(".options").outerWidth();

        $(this).css("min-width", minWidth);
    });

    $(".tr-select .select-title").on("click touch", function () {
        var trSelect = $(this).closest(".tr-select");
        var options = trSelect.find(".options");

        if (trSelect.hasClass("open")) {
            trSelect.removeClass("open");
            options.slideUp("fast");
        } else {
            trSelect.addClass("open");
            options.slideDown("fast");
        }
    });

    $(".tr-select .option").on("click touch", function () {
        var trSelect = $(this).closest(".tr-select");
        var trSelectTitle = trSelect.find(".select-title");
        var trSelectName = trSelect.attr("for");

        var options = trSelect.find(".options");
        var optionText = $(this).text();
        var optionIndex = $(this).attr("index");

        var targetSelectElement = $("select[name='" + trSelectName + "']");

        targetSelectElement.find("option").removeAttr("selected");

        targetSelectElement.removeAttr("selected");

        targetSelectElement.find("option:nth-child(" + optionIndex + ")").attr("selected", true);

        trSelectTitle.text(optionText);

        trSelect.find(".option").removeClass("selected");

        $(this).addClass("selected");

        trSelect.removeClass("open");
        options.slideUp("fast");

        targetSelectElement.trigger("change");
    });

    $(window).on("click touch", function (e) {
        if ($(".tr-select").has(e.target).length == 0 && !$(".tr-select").is(e.target)) {
            $(".tr-select .options").slideUp("fast");
        }
    });
};

$(document).ready(function () {
    new sbd.class.trSelect();
});