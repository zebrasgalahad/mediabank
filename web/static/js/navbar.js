$(document).ready(function() {
    var navbarWrapper = $(".navbar-wrapper");
    
    var navbarToggle = $(".navbar .toggle-menu");
    
    var pushWrapper = $(".push-wrapper");
    
    var pushOverlay = $(".push-overlay");
    
    var pushMenu = $(".push-menu");
    
    var pushMenuLink = $(".push-menu .element a");
    
    var pushMenuDropdownMenu = $(".push-menu .element .dropdown-menu");
    
    var pushMenuDropdownMenuBackButton = $(".push-menu .element .dropdown-menu .back");
    
    var deepestLevelOpen = 0;
    
    navbarToggle.on("click touch", function() {
        if($(this).hasClass("active")) {
            clearPushMenu();
        } else {
            initPushMenu();
            $(this).addClass("active"); 
            pushWrapper.addClass("push-menu-open");
        }
    });
    
    pushMenuLink.on("click touch", function(e) { 
        if(!$(this).hasClass("clicked") && $(this).parent(".element").hasClass("with-dropdown")) {
            e.preventDefault();
            pushMenuLink.removeClass("clicked");
            $(this).addClass("clicked");
            
            var level = parseInt($(this).parent(".element").attr("data-level")) + 1;
            
            var dropdown = $(this).parent(".element").find(".dropdown-menu[data-level="+level+"]");

            dropdown.addClass("open");
            
            deepestLevelOpen = level;
            
            $("html, body").animate({ scrollTop: 0 }, 500);
        }
    });
    
    pushMenuDropdownMenuBackButton.on("click", function() {
        closeDropdownMenu();
    });
    
    var clearPushMenu = function() {
        navbarToggle.removeClass("active");
        pushWrapper.removeClass("push-menu-open");
        pushMenuLink.removeClass("clicked");
        pushMenuDropdownMenu.removeClass("open");
        deepestLevelOpen = 0;
    }
    
    var initPushMenu = function() {
        pushMenuLink.each(function() {
            if($(this).hasClass("active")) {
                var level = parseInt($(this).closest(".element").attr("data-level"));
                
                deepestLevelOpen = level + 1;
                
                $(this).parent(".dropdown-menu").addClass("open");

                for(level; level > 0; level--) {
                    var dropdown = $(this).closest(".dropdown-menu[data-level="+level+"]");
                    
                    dropdown.addClass("open");
                }
            } 
        });
    }
    
    var closeDropdownMenu = function() {
        pushMenuLink.removeClass("clicked");
        $(".push-menu .element .dropdown-menu[data-level='" + deepestLevelOpen + "']").removeClass("open");
        deepestLevelOpen--;
    }
    
    $(window).on("click touchstart", function (e) {
        if(pushOverlay.is(e.target)) {
            clearPushMenu();
        }
        
        if(pushMenuDropdownMenu.hasClass("open") && !pushMenuDropdownMenu.is(e.target) && !pushMenuLink.is(e.target) && !pushMenuDropdownMenuBackButton.is(e.target)) {
            closeDropdownMenu();
        }
    });
    
    $(window).load(function() {
        initPushMenu(); 
    });
});