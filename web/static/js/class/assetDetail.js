var sbd = sbd ? sbd : {};

sbd.class = sbd.class ? sbd.class : {};

sbd.class.assetDetail = function (public) {
    var overlayContainer = $(".container.media-collection-container");

    var isPublic = public;

    var initProductListMode = function (element) {
        var mediaFilter = $(".media-filter");

        var mediaBlock = $(".media-block.mosaic");

        var mediaFilterDropdown = $(".media-filter .dropdown");

        var mediaFilter = $(".media-filter");

        var filterId = mediaFilter.attr("filter-id");

        mediaFilter.removeClass("open");
        mediaBlock.removeClass("filter-open");
        mediaFilterDropdown.slideUp(100);
        removeFilterCookie(filterId);

        var id = $(element).attr("data-asset-id");

        loadProductListData(id);
    };

    var removeFilterCookie = function (filterId) {
        var currentFilterCookie = "";

        if (Cookies.get("mediaFilter")) {
            currentFilterCookie = (Cookies.get("mediaFilter")).split(",");
        }

        var newFilterCookie = $.grep(currentFilterCookie, function (value) {
            return value != filterId;
        });

        Cookies.set("mediaFilter", newFilterCookie.join(","), {path: '/'});
    };

    var initAlbumListMode = function (assetId, albumId) {
        loadAlbumListData(assetId, albumId);
    };

    var loadProductListData = function (id) {
        sbd.class.detailElementId = id;

        var requestData = sbd.static.request.getRequestData();

        requestData.id = id;

        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/asset/detail", requestData);

        var containerOverlaySelector = $(".container.overlay.media");

        var template = "asset-detail/asset-detail";

        sbd.static.helper.getTemplate(template).done(function (content) {

            sbd.static.helper.showLoading();

            $.ajax({
                dataType: "json",
                url: url
            }).done(function (response) {
                $(".product-detail").remove();

                hideOtherProducts();

                sbd.static.helper.hideLoading();

                var html = $.tmpl(content, response).appendTo(containerOverlaySelector);

                var counter = 0;
                $.each(["author", "copyright", "source"], function (index, key) {
                    if (!response[key]) {
                        html.find("." + key).remove();

                        ++counter;
                    }
                });

                if (counter === 3) {
                    html.find(".meta-data-container").remove();
                }

                if (!response["categories"]) {
                    html.find(".categories-data-container").remove();
                }

                if (response.has_image_data === false) {
                    html.find(".image-only").remove();
                }

                html.insertAfter($(".container.horizontal-search-bar .search-container"));

                handleDownloadOptions(response);

                showOtherProducts();

                $(".product-detail .image.has-full-image").on("click touch", function () {
                    sbd.static.helper.showAssetDetailFullOverlay(response);
                });

                html.find(".add-to-selection").on("click touch", function (e) {
                    var assetId = $(this).attr("data-asset-id");

                    var mediaCollection = new sbd.class.mediaCollection();

                    mediaCollection.addAssetIdToMediaCollection(assetId);
                });

                html.find(".add-to-album").on("click touch", function (e) {
                    var assetId = $(this).attr("data-asset-id");

                    var callback = function () {
                        var albumList = new sbd.class.albumList();

                        albumList.showAlbumListOverlay(callback);
                    };

                    var mediaCollection = new sbd.class.mediaCollection();

                    mediaCollection.openAddToAlbumModal(assetId, callback);
                });

                if (response.navigation) {
                    var navigation = response.navigation;

                    var prevElement = html.find(".prev")
                    if (navigation.prev) {
                        prevElement.on("click touch", function (e) {
                            e.preventDefault();

                            hideOverlay(true);

                            loadProductListData(navigation.prev);
                        });
                    } else {
                        prevElement.addClass("disabled");
                    }

                    var nextElement = html.find(".next");
                    if (navigation.next) {
                        nextElement.on("click touch", function (e) {
                            e.preventDefault();

                            hideOverlay(true);

                            loadProductListData(navigation.next);
                        });
                    } else {
                        nextElement.addClass("disabled");
                    }
                }
            }).fail(function (response) {

            });
        }).fail(function (content) {

        });
    };

    var loadAlbumListData = function (assetId, albumId) {
        sbd.static.helper.removeOverlay();

        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/asset/detail", {id: assetId, albumId: albumId});

        sbd.static.helper.getTemplate("asset-detail/asset-detail").done(function (content) {

            sbd.static.helper.showLoading();

            $.ajax({
                dataType: "json",
                url: url
            }).done(function (response) {
                sbd.static.helper.hideLoading();

                if (isPublic) {
                    content.find(".actions").remove();
                    content.find(".title-block").remove();
                }

                var html = $.tmpl(content, response);

                var navigation = response.navigation;

                var prevElement = html.find("p.prev")
                if (navigation.prev) {
                    prevElement.on("click touch", function (e) {
                        e.preventDefault();

                        loadAlbumListData(navigation.prev, albumId);
                    });
                } else {
                    prevElement.addClass("disabled");
                }

                var nextElement = html.find("p.next");
                if (navigation.next) {
                    nextElement.on("click touch", function (e) {
                        e.preventDefault();

                        loadAlbumListData(navigation.next, albumId);
                    });
                } else {
                    nextElement.addClass("disabled");
                }

                html.find(".show-all").on("click touch", function (e) {
                    var albumList = new sbd.class.albumList(isPublic);

                    showOtherProducts();

                    albumList.showAlbumDetailOverlay(response.album.album_id);
                });

                html.find(".delete-from-album").on("click touch", function (e) {
                    var assetId = $(this).attr("data-asset-id");

                    var albumList = new sbd.class.albumList();

                    albumList.deleteAlbumAssetAction(albumId, assetId).done(function () {
                        albumList.showAlbumDetailOverlay(albumId);
                    });
                });

                html.find(".add-to-selection").on("click touch", function (e) {
                    var assetId = $(this).attr("data-asset-id");

                    var mediaCollection = new sbd.class.mediaCollection();

                    mediaCollection.addAssetIdToMediaCollection(assetId);
                });

                sbd.static.helper.appendOverlay(overlayContainer, html);

                handleDownloadOptions(response);
            }).fail(function (response) {
            });
        }).fail(function (content) {

        });
    };

    var handleDownloadOptions = function (response) {
        var additionalDownloadOptions = $(".additional-download-options");
        var epsFileBlock = $(".additional-download-options .option.eps-download-file");

        if (response.show_all_download_options === false) {
            additionalDownloadOptions.remove();
        }

        if (response.has_eps_file === false) {
            epsFileBlock.remove();
        }
    };

    var hideOtherProducts = function () {
        var contentContainer = $(".container.content.media");

        sbd.static.helper.hideLoading(contentContainer);

        contentContainer.hide();
    };

    var hideOverlay = function (noAnimate) {
        var contentContainer = $(".container.content.media");

        var productDetailContainer = $(".product-detail");
        productDetailContainer.remove();
        contentContainer.show();
        sbd.static.helper.hideLoading();

        if (!noAnimate) {
            var element = $("div[data-asset-id='" + sbd.class.detailElementId + "']");
            if (element.length > 0) {
                $([document.documentElement, document.body]).animate({
                    scrollTop: element.offset().top - 90
                }, 600);
            }
        }
    };

    var showOtherProducts = function () {
        var showAllSelector = $(".product-detail .nav .show-all");

        showAllSelector.on("click", function () {
            hideOverlay();
        });
    };

    return {
        initProductListMode: function (element) {
            initProductListMode(element)
        },
        initAlbumListMode: function (assetId, albumId) {
            initAlbumListMode(assetId, albumId);
        },
        loadAlbumListData: function (assetId, albumId) {
            loadAlbumListData(assetId, albumId);
        },
        hideOverlay: function () {
            hideOverlay();
        }
    };
};