$(document).ready(function () {

    var bodyElement = $('body');
    var userInvite = $(".invite-user");

    userInvite.on("click touch", function (e) {
        var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/user-invitation/form");

        sbd.static.helper.showLoading(bodyElement);

        $.ajax({
            url: url
        }).done(function (data) {
            sbd.static.helper.getTemplate("userInvite").done(function (template) {

                renderInviteData(data, template);

                sbd.static.helper.hideLoading(bodyElement);
            }).fail(function (template) {
                sbd.static.helper.hideLoading(bodyElement);
            });
        });

        var renderInviteData = function (data, template) {
            var html = $.tmpl(template, data);

            html.insertAfter($("header"));

            makeModalFunctions();

            html.modal("show");
        };
    });


    var makeModalFunctions = function () {

        var userInviteModal = $(".user-invite-modal");
        var emailUserInvite = $(".user-invite-modal .user-invite-email");
        var permissionUserInvite = $(".user-invite-modal .user-invite-permission");
        var userInviteEmailError = $(".user-invite-email .error");
        var userInviteButtonEmail = $(".user-invite-email .continue");
        var userInviteButtonPermission = $(".back");
        var userInviteSave = $(".user-invite-permission .save");
        var userInviteOutput = $(".user-invite-modal .output");
        var output = $(".user-invite-modal .output .error");
        var message = $('[name="user-invite-form-add"] [name="message"]');
        var userGroupConsumer = $('[name="user_group_consumer"]');
        var userGroupProfessional = $('[name="user_group_professional"]');
        var userGroupFlexvolt = $('[name="user_group_flexvolt"]');
        var catalogConsumerInputs = $('input[name="group_consumer"]');
        var catalogProfessionalInputs = $('input[name="group_professional"]');
        var catalogFlexvoltInputs = $('input[name="group_flexvolt"]');
        var loaderContainer = $(".loader-container");

        if (message.length >= 1) {
            message.richText({
                bold: true,
                italic: true,
                underline: false,
                leftAlign: false,
                centerAlign: false,
                rightAlign: false,
                ol: false,
                ul: false,
                heading: false,
                fonts: false,
                fontColor: false,
                imageUpload: false,
                fileUpload: false,
                videoEmbed: false,
                urls: false,
                table: false,
                removeStyles: false,
                code: false,
                useSingleQuotes: false,
                height: 0,
                heightPercentage: 0,
                useParagraph: false
            });
        }

        userGroupConsumer.click(function () {
            if ($(this).is(':checked')) {
                catalogConsumerInputs.prop('checked', true);
            } else {
                catalogConsumerInputs.prop('checked', false);
            }
        });

        userGroupProfessional.click(function () {
            if ($(this).is(':checked')) {
                catalogProfessionalInputs.prop('checked', true);
            } else {
                catalogProfessionalInputs.prop('checked', false);
            }
        });

        userGroupFlexvolt.click(function () {
            if ($(this).is(':checked')) {
                catalogFlexvoltInputs.prop('checked', true);
            } else {
                catalogFlexvoltInputs.prop('checked', false);
            }
        });

        catalogConsumerInputs.click(function () {
            if ($(this).is(':checked')) {
            } else {
                userGroupConsumer.prop('checked', false);
            }
        });

        catalogProfessionalInputs.click(function () {
            if ($(this).is(':checked')) {
            } else {
                userGroupProfessional.prop('checked', false);
            }
        });

        catalogFlexvoltInputs.click(function () {
            if ($(this).is(':checked')) {
            } else {
                userGroupFlexvolt.prop('checked', false);
            }
        });

        userInviteModal.insertAfter("header");
        permissionUserInvite.slideUp("fast");
        userInviteOutput.slideUp("fast");

        var result = "";

        var validateEmails = function () {
            var emails = $('[name="user-invite-form-add"] [name="email[]"]').val();

            result = "";

            if (emails) {
                var arrayOfEmails = emails.split(";");

                var notValidatedEmails = new Array();

                arrayOfEmails.forEach(function (email) {
                    var valid = validateEmail(email);

                    if (valid === false) {
                        notValidatedEmails.push(email);
                    }
                });

                if (notValidatedEmails.length === 1) {
                    result = "<p>" + __("user.invite.email.not.valid") + "</p><p>&raquo; <b>" + notValidatedEmails[0] + "</b></p><p>" + __("user.invite.please.correct") + "</p>";
                } else if (notValidatedEmails.length > 1) {
                    var emails = "";

                    arrayOfEmails.forEach(function (email) {
                        emails += "&raquo; <b> " + email + "</b><br>";
                    });

                    result = "<p>" + __("user.invite.emails.not.valid") + "</p><p>" + emails + "</p><p>" + __("user.invite.please.correct") + "</p>";
                }
            } else {
                result = "<p>" + __("user.invite.no.emails.entered") + "</p>";
            }
        }

        userInviteModal.on('hidden.bs.modal', function (e) {
            userInviteModal.remove();
        });

        userInviteButtonEmail.on("click", function () {
            validateEmails();
            userInviteEmailError.hide();

            if (result) {
                userInviteEmailError.html(result);
                userInviteEmailError.slideDown("fast");
            } else {
                emailUserInvite.slideUp("fast");
                permissionUserInvite.slideDown("fast");
                userInviteOutput.slideUp("fast");
            }
        });

        userInviteButtonPermission.on("click", function () {

            permissionUserInvite.slideUp("slow");
            emailUserInvite.slideDown("slow");
            userInviteOutput.slideUp("fast");
        });

        userInviteSave.on("click", function (e) {

            e.preventDefault();

            var message = $('[name="user-invite-form-add"] [name="message"]').val();
            var country = new Array();
            var catalog = new Array();
            var userGroup = new Array();

            $("input:checkbox[name=country]:checked").each(function () {
                country.push($(this).val());
            });

            if ($("input:checkbox[name=user_group_consumer]:checked").length > 0) {
                $("input:checkbox[name=user_group_consumer]:checked").each(function () {
                    userGroup.push($(this).val());
                });
            } else {
                $("input:checkbox[name=group_consumer]:checked").each(function () {
                    catalog.push($(this).val());
                });
            }

            if ($("input:checkbox[name=user_group_professional]:checked").length > 0) {
                $("input:checkbox[name=user_group_professional]:checked").each(function () {
                    userGroup.push($(this).val());
                });
            } else {
                $("input:checkbox[name=group_professional]:checked").each(function () {
                    catalog.push($(this).val());
                });
            }

            if ($("input:checkbox[name=user_group_flexvolt]:checked").length > 0) {
                $("input:checkbox[name=user_group_flexvolt]:checked").each(function () {
                    userGroup.push($(this).val());
                });
            } else {
                $("input:checkbox[name=group_flexvolt]:checked").each(function () {
                    catalog.push($(this).val());
                });
            }

            var emails = $('[name="user-invite-form-add"] [name="email[]"]').val();
            var arrayOfEmails = emails.split(";");

            validateEmails();

            if (result === "") {
                output.hide();
                sbd.static.helper.showLoading(loaderContainer);

                var url = sbd.static.helper.createUrlWithLocale("/{_locale}/ajax/user-authentication/user-invite");

                $.ajax({
                    url: url,
                    data: {emails: arrayOfEmails,
                        message: message,
                        country: country,
                        catalog: catalog,
                        userGroup: userGroup}
                }).done(function (data) {
                    sbd.static.helper.hideLoading(loaderContainer);
                    var emails = data.emails.join(", ");

                    var outputText = __(data.result) + " " + emails;
                    output.show();
                    output.removeClass("error");
                    output.addClass("success");
                    output.html(outputText);
                }).fail(function ($xhr) {
                    var data = $xhr.responseJSON;

                    var emails = data.emails.join(", ");

                    output.removeClass("success");
                    output.addClass("error");

                    var outputText = __(data.result) + " (" + emails + ")";
                    sbd.static.helper.hideLoading(loaderContainer);
                    output.show();
                    output.html(outputText);
                });
            } else {
                output.html(result);
            }

            permissionUserInvite.slideUp(300);
            userInviteOutput.slideDown(300);

        });

        function validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
        }

    };

});