$(document).ready(function() {
    var productFilterForm = $(".product-filter-form");
    var productFilterSelect = $(".product-filter .bootstrap-select");
    var productFilterSelectColorOptions = $(".product-filter .bootstrap-select select[title='Grundfarbe'] option");
    var priceFilter = $(".product-filter .price-filter");
    var priceFilterDropdownToggle = $(".product-filter .price-filter .dropdown-toggle");
    var priceFilterButton = $(".product-filter .price-filter .filter-button");
    var filterActiveButton = $(".filter-active .button");
    var selectMobileSelectTitle = $(".select-mobile .select-title");
    var selectMobileOption = $(".select-mobile .options .option");
    var selectMobileSubmit = $(".product-filter-form button.mobile");
    var selectMobileReset = $(".mobile-actions .button.reset-all");
    var minPrice = $(".price-filter #min-price");
    var maxPrice = $(".price-filter #max-price");
    var sliderPriceHandle = $("#slider-range .ui-slider-handle");
    var sliderRange = $("#slider-range");
    var moreFiltersButton = $(".product-filter-form .more-filters");
    var allFiltersInput = $(".product-filter-form input#all-filters");
    
    var url = window.location.href;

    if(url.indexOf("?") > 0 && productFilterForm.length) {
        $("html, body").animate({ 
            scrollTop: productFilterForm.offset().top - 120 
        }, 0);
    }
    
    filterActiveButton.on("click touch", function() {
        if($(this).hasClass("reset-all")) {
            $(".product-filter-form select option").attr("selected", false);
            productFilterForm.trigger("reset");
        } else if($(this).hasClass("reset-price")) {
            minPrice.val("");
            maxPrice.val("");  
        } else {
            $(".product-filter-form select[name='" + $(this).attr("filter-key") + "'] option[name='" + $(this).attr("option-key") +"']").attr("selected", false);
        }

        $("select").selectpicker("refresh");
        productFilterForm.submit();
    });
    
    selectMobileReset.on("click touch", function() {
        $(".product-filter-form select option").attr("selected", false); 
        $("select").selectpicker("refresh");
        productFilterForm.submit();
    });
    
    selectMobileSubmit.on("click touch", function() {
        productFilterForm.submit();
    });
    
    productFilterSelect.each(function() {
        if($(this).find(".bs-searchbox").length > 0) {
            $(this).addClass("with-searchbox");
            $(this).next(".select-mobile").addClass("with-searchbox");
        } 
        
        if($(this).find("select option:selected").length > 0) {
            $(this).addClass("filter-checked");
            $(this).next(".select-mobile").addClass("filter-checked");
        }
        
        if($(this).find("select option:not(:disabled)").length == 0) {
            $(this).addClass("empty");
            $(this).next(".select-mobile").addClass("empty");
        }
        
        $(this).find(".dropdown-menu.open").append("<div class='filter-button'><p>" + herpa.productlist.cancel + "</p></div>");
    });

    $("select[title='Grundfarbe']").on("shown.bs.select", function() {
        if(!$(this).hasClass("colors-styled")) {
            var text = $(".select-group.Grundfarbe ul.dropdown-menu.inner > li > a > span.text");
            
            text.each(function(index) {
                var colorTitle = $("select[title='Grundfarbe'] option:nth-child(" + (index+1) + ")").attr("color-title");
                $(this).html(colorTitle);
            });
            
            $(this).addClass("colors-styled");
        }
    });
    
    productFilterSelect.change(function() {
        $(this).addClass("change");
        $(this).find(".filter-button").addClass("save");
        $(this).find(".filter-button p").text(herpa.productlist.save);
    });
    
    productFilterSelect.on('hide.bs.dropdown', function () {
        if($(this).hasClass("change")) {
            productFilterForm.submit();
        }
    });
    
    priceFilterDropdownToggle.on("click touch", function() {
        priceFilter.toggleClass("open"); 
    });
    
    priceFilterButton.on("click touch", function() {
        priceFilter.removeClass("open");
        if(priceFilter.hasClass("change")) {
            productFilterForm.submit();
        }
    });
    
    moreFiltersButton.on("click touch", function() {
        if($(this).hasClass("open")) {
            allFiltersInput.val("");
            $(this).removeClass("open");
            $(".select-group.inactive").removeClass("active");
            $(this).html(herpa.productlist.show_more);
        } else {
            allFiltersInput.val(1);
            $(this).addClass("open");
            $(".select-group.inactive").addClass("active");
            $(this).html(herpa.productlist.show_less);
        }
    });
    
    selectMobileSelectTitle.on("click touch", function() {
        if($(this).hasClass("open")) {
            selectMobileSelectTitle.removeClass("open");
            selectMobileSelectTitle.closest(".select-mobile").find(".options").slideUp("slow");
        } else {
            $(this).addClass("open");
            $(this).closest(".select-mobile").find(".options").slideDown("slow");
        }
    });
    
    selectMobileOption.on("click touch", function() {
        var optionIndex = $(this).index()+1;
        var matchingSelectOption = $(this).closest(".select-group").find("select option:nth-child(" + optionIndex + ")");
        
        if($(this).hasClass("selected")) {
            matchingSelectOption.attr("selected", false);
            $(this).removeClass("selected");
        } else {
            matchingSelectOption.attr("selected", true);
            $(this).addClass("selected");
        }
    });
    
    var equalizeProductList = function () {
        if($(window).width() > 992) {
            $(".product-list").equalize({
                    equalize: 'outerHeight',
                    reset: true,
                    children: '.description-block .title'
                }
            );
        }
    }
    
    var triggerPriceFilter = function() {
        var minPriceRange = parseInt(minPrice.val());
        var maxPriceRange = parseInt(maxPrice.val());

        if (minPriceRange > maxPriceRange) {
		    maxPrice.val(minPriceRange);
	    } else if(minPriceRange == maxPriceRange){
			maxPriceRange = minPriceRange + 10;
			minPrice.val(minPriceRange);		
			maxPrice.val(maxPriceRange);
	    }
        
        priceFilter.addClass("change");
        priceFilterButton.addClass("save");
        priceFilterButton.find("p").text(herpa.productlist.save);
    }
    
    minPrice.on("change paste keyup", function () {
        triggerPriceFilter();
	});
    
    maxPrice.on("change paste keyup", function () {
        triggerPriceFilter();
	});

	$(function () {
    	var minPriceValue = 0;
    	var maxPriceValue = Math.max(maxPrice.attr("max"), maxPrice.attr("get-value"));
    	
    	var getPriceValueMin = minPrice.attr("get-value") ? minPrice.attr("get-value") : minPriceValue;
    	var getPriceValueMax = maxPrice.attr("get-value") ? maxPrice.attr("get-value") : maxPriceValue;
    	
        sliderRange.slider({
    		range: true,
    		orientation: "horizontal",
    		min: minPriceValue,
    		max: maxPriceValue,
    		values: [getPriceValueMin, getPriceValueMax],
    		step: 1,
    
    		slide: function (event, ui) {
    		    if (ui.values[0] == ui.values[1]) {
    			    return false;
    		    }

                minPrice.val(ui.values[0]);
                maxPrice.val(ui.values[1]);
    		},
    		
    		change: function( event, ui ) {
        		triggerPriceFilter();
    		}
        });
    
        minPrice.val(sliderRange.slider("values", 0));
        maxPrice.val(sliderRange.slider("values", 1));
	});
    
    $(window).load(function() {
        equalizeProductList(); 
    });
    
    $(window).resize(function() {
        equalizeProductList(); 
    });
});