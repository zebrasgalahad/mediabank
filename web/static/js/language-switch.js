$(document).ready(function() {
    var languageSwitch = $("header .header-actions select.language-switch");
    
    languageSwitch.on("change", function() {
        var optionLink = languageSwitch.find("option:selected").attr("data-link");
        
        if(optionLink) {
            window.location.href = optionLink;
        }
    });
});