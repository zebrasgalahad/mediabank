<div>
    <div class="media" data-asset-id="${object_id}">
        <div class="image">
            <img src="${preview_web_path}" style="width: ${preview_web_width}px; height: ${preview_web_height}px"/>
        </div>
        <div class="info">
            <p>${description_short}</p>
            {{if article_ean}}
            <p class="ean">[[site.list.assets.ean]]: ${article_ean}</p>
            {{/if}}
        </div>
        <div class="title">
            <p>${article_number}</p>
            <div class="add"></div>
        </div>
    </div>
</div>