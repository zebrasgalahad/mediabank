<div>
    <div class="media file visible" data-asset-id="${object_id}">
        <p class="overlay-title yellow">${article_number}></p>
        <div class="title">
            <p>[[collection.teaser.type.text]]</p>
            <a href="" class="add"></a>
        </div>
        <div class="info collection">
            <p>${description_short}</p>
        </div>        
    </div>    
</div>