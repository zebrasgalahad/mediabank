<div>
    <div class="asset-detail-full-modal add modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"></button>
                <img src="${asset_detail_full_path}"/>
            </div>
        </div>
    </div>
</div>
