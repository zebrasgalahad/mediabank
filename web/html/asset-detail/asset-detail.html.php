<div class="container content-overlay">
    <div class="title-block album-only">
        <h2 class="title">[[album.my-albums]]</h2>
        <div class="close-screen"></div>
    </div>
    <div class="row album-only">
        <div class="col-sm-12 col-md-9">
            {{if album}}
                <h2 class="album-title">${album.name}</h2>
                <p class="album-description">${album.description}</p>
            {{/if}}
        </div>
    </div>    

    <div class="product-detail">
        <div class="row">
            <div class="col-sm-12 col-md-8 col-lg-9">
                <h2 class="title">${description_short}</h2>
            </div>
            <div class="col-sm-12 col-md-4 col-lg-3">
                <div class="nav">
                    <p class="prev"><span>[[product.detail.nav.previous]]</span></p>
                    <p class="show-all">[[product.detail.nav.show-all]]</p>
                    <p class="next"><span>[[product.detail.nav.next]]</span></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                <div class="data">
                    <div class="data-block">
                        <h3 class="sub-title">${description_long}</h3>
                    </div>
                    <div class="data-block">
                        <table>
                            <tr class="product-article-number">
                                <th>[[product.detail.article-number]]</th>
                                <td><div class="arrow-target">${article_number}</div></td>
                            </tr>
                            <tr class="product-ean-number">
                                <th>[[product.detail.ean]]:</th>
                                <td>${ean_number}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="data-block categories-data-container">
                        {{if is_article_asset}}
                        <h4>[[product.detail.informations]]</h4>
                        <table class="small">
                            <tr>
                                <th>[[product.detail.innovations]] ${$catalog_name}:</th>
                                <td>${new_from}</td>
                            </tr>
                            <tr>
                                <th>[[product.detail.brand]]:</th>
                                <td>${marke}</td>
                            </tr>
                        </table>
                        {{/if}}
                        <div class="catalog-categories">
                            {{if catalog_name}}
                            <p>[[product.detail.catalog-name]] ${catalog_name}</p>
                            {{/if}}
                            {{if categories}}
                                {{each categories}}
                                    <div class="category"><p class="arrow-target">${$value}</p></div>
                                {{/each}}
                            {{/if}}
                        </div>
                    </div>
                    
                    <div class="data-block meta-data-container">
                        <h4>[[product.detail.copyrights]]</h4>
                        <table class="small">
                            <tr class="author">
                                <th>[[product.detail.creator]]:</th>
                                <td>${author}</td>
                            </tr>
                            <tr class="source">
                                <th>[[product.detail.copyright-source]]:</th>
                                <td>${source}</td>
                            </tr>
                            <tr class="copyright">
                                <th>[[product.detail.copyright]]:</th>
                                <td>${copyright}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="data-block asset-info">
                        <h4>[[product.detail.file-info]]</h4>
                        <table class="small">
                            <tr>
                                <th>[[product.detail.file-size]]:</th>
                                <td>${filesize}</td>
                            </tr>
                            <tr class="image-only">
                                <th>[[product.detail.file-format]]:</th>
                                <td>${image_width_px} x ${image_height_px} px</td>
                            </tr>
                            <tr class="image-only">
                                <th>[[product.detail.file-resolution]]:</th>
                                <td>${dpi} dpi</td>
                            </tr>
                            <tr class="image-only">
                                <th>[[product.detail.size]]:</th>
                                <td>${image_width_cm} cm x ${image_height_cm} cm</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-9 col-lg-6">
                <div class="image {{if has_full_image}}has-full-image{{/if}}">
                    <img src="${asset_path}"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
                <div class="options-block">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                            <div class="options downloads">
                                <div class="headline">[[product.detail.downloads-options]]</div>
                                <a class="option original-download-file" href="${original_file_download_url}?id=${asset_id}&type=original">[[product.detail.original.file]]</a>
                                <div class="additional-download-options">
                                    <a class="option eps-download-file" href="${eps_file_download_url}?id=${asset_id}&type=eps">[[product.detail.eps.file]]</a>
                                    <a class="option hires-cmyk-download-file" href="${hires_cmyk_file_download_url}?id=${asset_id}&type=hires_cmyk">[[product.detail.hires.cmyk.file]]</a>
                                    <a class="option hires-rgb-download-file" href="${hires_rgb_file_download_url}?id=${asset_id}&type=hires_rgb">[[product.detail.hires.rgb.file]]</a>
                                    <a class="option office-download-file" href="${office_file_download_url}?id=${asset_id}&type=office">[[product.detail.office.file]]</a>
                                    <a class="option web-download-file" href="${web_file_download_url}?id=${asset_id}&type=web">[[product.detail.web.file]]</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-12">
                            <div class="options actions">
                                <div class="headline">[[product.detail.actions]]</div>
                                <div class="option add-to-album list-only" data-asset-id="${asset_id}">[[product.detail.actions.add-to-album]]</div>
                                <div class="option delete-from-album album-only" data-asset-id="${asset_id}">[[product.detail.actions.delete-from-album]]</div>
                                <div class="option add-to-selection" data-asset-id="${asset_id}" >[[product.detail.actions.add-to-selection]]</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>