<div>
    <div class="user-invite-modal add modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"></button>
                <div class="modal-header">
                    <p class="title">[[user-invite.modal.add.title]]</p>
                </div>
                <div class="modal-body">
                    <form name="user-invite-form-add">
                        <div class="user-invite-email">
                            <p class="title">[[user-invite.modal.email.address]]</p>
                            <p class="grey email-to">[[user-invite.modal.email.send.to]]</p>
                            <input name="email[]"></input>
                            <div class="error"></div>
                            <textarea name="message">[[user-invite.modal.email.text]] ${customer["firstName"]} ${customer["lastName"]}</textarea>
                            
                            <div class="grey step">
                                [[user-invite.modal.step.1]]
                            </div>
                            <div class="right">
                                <button type="button" class="button yellow continue">[[user-invite.modal.continue]]</button>
                            </div>
                        </div>
                        <div class="user-invite-permission">
                            <p class="title">[[user-invite.modal.choose-country.name]]</p>
                            <div class="row">
                                <div class="col-xs-12 col-md-4">
                                    <p class="sub-title">[[user-invite.modal.choose-country]]</p>
                                    <ul class="country">
                                        {{each config.country}}
                                            <li><input type="checkbox" name="country" value="${$value.value}">${$value.name}</li>
                                        {{/each}}
                                    </ul>
                                </div>
                            </div>
                            <p class="title">[[user-invite.modal.choose-user-group.name]]</p>
                            <div class="row">
                                {{if config.consumer}}
                                <div class="col-xs-12 col-md-4">
                                    <p class="sub-title"><input type="checkbox" name="user_group_consumer" value="Consumer">[[user-invite.modal.consumer]]</p>
                                    <ul>
                                    {{each config.consumer}}
                                        <li><input type="checkbox" name="group_consumer" value="${$value.value}">${$value.name}</li>
                                    {{/each}}
                                    </ul>
                                </div>
                                {{/if}}
                                {{if config.professional}}
                                <div class="col-xs-12 col-md-4">
                                    <p class="sub-title"><input type="checkbox" name="user_group_professional" value="Professional">[[user-invite.modal.professional]]</p>
                                    <ul>
                                        {{each config.professional}}
                                            <li><input type="checkbox" name="group_professional" value="${$value.value}">${$value.name}</li>
                                        {{/each}}
                                    </ul>
                                </div>
                                {{/if}}
                                {{if config.flexvolt}}
                                <div class="col-xs-12 col-md-4">
                                    <p class="sub-title"><input type="checkbox" name="user_group_flexvolt" value="Flexvolt">[[user-invite.modal.flexvolt]]</p>
                                    <ul>
                                        {{each config.flexvolt}}
                                            <li><input type="checkbox" name="group_flexvolt" value="${$value.value}">${$value.name}</li>
                                        {{/each}}
                                    </ul>
                                </div>
                                {{/if}}
                            </div>
                            <hr>
                            <div class="grey step">
                                [[user-invite.modal.step.2]]
                            </div>
                            <div class="right">
                                <button type="button" class="button back">[[user-invite.modal.back]]</button>
                                <button type="submit" class="button yellow save">[[user-invite.modal.save]]</button>
                            </div>    
                        </div>
                        <div class="loader-container"></div>
                        <div class="output">
                            <div class="error"></div>
                            <button type="button" class="button back">[[user-invite.modal.back]]</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

