<div>
    <div filter-id="${filterId}" class="filter-group">
        <div class="title">${filterTitle}</div>
        <div class="filter-group-dropdown" style="">
            {{each filters}}
                {{if $value.checked === true}}
                    <div class="filter checked ${$value.facet_value}" filter-id="${$value.facet_id}">
                        <input name="${$value.facet_id}[]" value="${$value.facet_value}" type="hidden" checked="checked">
                        <label>${$value.facet_value_display} (${$value.count})</label>
                    </div>
                {{else}}
                    <div class="filter ${$value.facet_value}" filter-id="${$value.facet_id}">
                       <input name="${$value.facet_id}[]" value="${$value.facet_value}" type="hidden" >
                       <label>${$value.facet_value_display} (${$value.count})</label>
                    </div>               
                {{/if}}
            {{/each}}            
        </div>
    </div>
</div>