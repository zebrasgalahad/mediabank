<div>
    <div class="media-collection">
        <div class="title-block">
            <h2 class="title">[[collection.my-collection]]</h2>
            <div class="close-screen"></div>
        </div>
        <div class="collection-navigation">
            <div class="button download-all">[[collection.selection-download]]</div>
            <div class="button add-to-album">[[collection.add-to-album]]</div>
            <div class="button selection-share">[[collection.selection-share]]</div>
            <div class="button selection-delete action-delete-all">[[collection.selection-delete]]</div>
        </div>
        
        <div class="media-block mosaic">
            {{each data}}
                <div class="media" data-asset-id="${asset_id}">
                    <span class="delete action-delete" title="[[collection.delete-object]]"></span>
                    <div class="image">
                        <img src="${preview_web_path}" style="width: ${preview_web_width}px; height: ${preview_web_height}px;"/>
                    </div>
                    <div class="title">
                        <p class="ean">${article_number}</p>
                    </div>
                </div>
            {{/each}}
        </div>
        <div class="more">[[collection.dashboard.more]]</div>
    </div>
</div>