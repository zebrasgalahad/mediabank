<div>
    <div class="collection-save-as-new-album-modal add modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"></button>
                <form name="collection-download" action="/${locale}/download/multiple-assets" method="post">
                    <div class="new-album">
                        <div class="modal-header">
                            <p class="title" style="color:white;">[[collection.save-as-new-album]]</p>
                        </div>
                        <div class="modal-body">                            
                            <div class="row">
                                <form>
                                    <input type="text" placeholder="[[collection.type-in-album-name]]"/>
                                    <button type="submit" class="button yellow save-as-new-album">[[collection.save]]</button>
                                    <span class="error" style="display: none;">[[collection.album-name.error]]</span>
                                </form>
                            </div>
                            <div class="button-panel">
                                <button type="submit" class="button select-album">[[collection.select-album]]</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
