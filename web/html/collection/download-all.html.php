<div>
    <div class="collection-download-modal add modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"></button>
                <form id="collection-download" name="collection-download" action="" method="post">
                    <div class="modal-header">
                        <p class="title" style="color:white;">[[collection.download-select]]</p>
                    </div>
                    <div class="modal-body">
                        {{each data}}
                            <div class="download-file">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <div class="asset-image">
                                            <img src="${preview_web_path}"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-6">
                                        <div class="infos">
                                            <div class="article-number">
                                                <p class="article-number-text">${article_number}</p>
                                            </div>
                                            <div class="select-download-type">
                                                <input hidden="" name="asset_ids[]" value="${asset_id}" />
                                                <select name="single_download_types[]" id="${$value.asset_id}" style="display: block;">
                                                    {{each $value.options}}
                                                    <option value="${$index}">${$value}</option>
                                                    {{/each}}
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {{/each}}
                        {{if data.length == 0}}
                            <p>[[collection.no-items-for-download]]</p>
                        {{/if}}                        
                    </div>
                    <div class="collection-download">
                        <button type="submit" class="button yellow download">[[collection.download-start]]</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
