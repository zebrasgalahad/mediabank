<div>
    <div class="collection-add-to-album-modal add modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"></button>
                <form name="collection-download" action="/${locale}/download/multiple-assets" method="post">
                    <div class="album-selection">
                        <div class="modal-header">
                            <p class="title">[[collection.choose-album]]:</p>
                        </div>
                        <div class="modal-body">                            
                            {{each albums}}
                                <div class="album">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <div class="asset-image">
                                                {{if $value.assets[0]}}
                                                    <img src="${$value.assets[0].preview_web_path}"/>
                                                {{/if}}
                                            </div>
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="info">
                                                <p class="album-title">${$value.album.name}</p>
                                                <div class="button add add-to-album" data-album-id="${$value.album.album_id}">[[collection.add-to-album]]</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button add add-to-album mobile-only" data-album-id="${$value.album.album_id}">[[collection.add-to-album]]</div>
                                </div>
                            {{/each}}
                            <div class="button-panel">
                                <button type="submit" class="button yellow save-as-new-album">[[collection.save-as-new-album]]</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
