<div>
    <div class="row">

    </div>    
    <div class="media-collection">
        <div class="title-block">
            <h2 class="title">[[album.my-albums]]</h2>
            <div class="close-screen"></div>
        </div>
        <div class="nav">
            <span class="prev <?php //echo $prevProduct ? "" : "disabled";  ?>">[[album.detail.nav.previous]]</span>
            <span class="event-show-all">[[album.detail.nav.show-all]]</span>
            <span class="next <?php //echo $nextProduct ? "" : "disabled";  ?>">[[album.detail.nav.next]]</span>
        </div>
        <div class="collection-navigation">
            <div class="button event-download-all">[[album.detail.download]]</div>
            <div class="button event-share-album">[[album.detail.share]]</div>
            <div class="button selection-delete event-delete-album" data-album-id="${album.album_id}">[[album.detail.delete]]</div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-2">
                <div class="album-info">
                    {{if assets.length != 0}}
                        <div class="album-image">
                            <img src="${assets[0].preview_web_path}"/>
                        </div>
                    {{/if}}
                    <p class="creator">${album_user_view_data.customer_full_name}</p>
                    <p class="creation-date">${album_user_view_data.creation_date}</p>
                    {{if album_user_view_data.is_shared_album}}
                        <p class="shared-album">[[album.detail.shared-album]]</p>
                    {{/if}}
                </div>
            </div>
            <div class="col-xs-12 col-sm-9 col-md-10">
                <div>
                    <h2 contenteditable="true" data-album-id="${album.album_id}" class="event-album-name">${album.name}</h2>
                    <div class="album-description">
                        <input type="text" name="event-album-description" id="event-album-description" placeholder="[[album.detail.add-description]]" value="${album.description}" data-album-id="${album.album_id}"/>
                    </div>    
                </div>
                <div class="media-block mosaic">
                    {{if assets.length != 0}}
                        {{each assets}}
                        {{if $value.album_with_assets}}
                            <div class="media" data-asset-id="${$value.asset_id}" data-album-id="${album.album_id}">
                                <span class="delete action-delete" title="[[album.detail.delete-asset]]"></span>
                                <div class="image">
                                    <img src="${$value.preview_web_path}"  style="width: ${$value.preview_web_width}px; height: ${$value.preview_web_height}px"/>
                                </div>
                                <div class="title">
                                    <p class="ean">${$value.article_number}</p>
                                </div>
                            </div>
                        {{/if}}
                        {{/each}}            
                    {{else}}
                        <div>[[album.detail.no-assets]]</div>
                    {{/if}}
                </div>
            </div>
        </div>
    </div>
</div>