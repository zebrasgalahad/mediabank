<div>
    <div class="media-collection">
        <div class="title-block">
            <h2 class="title">[[album.my-albums]]</h2>
            <div class="close-screen"></div>
        </div>
        <div class="collection-navigation">
            <div class="button save-new-album">[[album.add-album]]</div>
        </div>
        <div class="media-block mosaic">
            {{if albums.length != 0}}
                {{each albums}}
                    <div class="media" data-album-id="${$value.album.album_id}">
                        <span class="delete action-delete" title="[[album.delete-object]]"></span>
                        <div class="image">
                            {{if $value.assets[0]}}
                                <img src="${$value.assets[0].preview_web_path}" style="width: ${$value.assets[0].preview_web_width}px; height: ${$value.assets[0].preview_web_height}px;"/>
                            {{/if}}
                        </div>
                        <div class="title">
                            <p class="ean">${$value.album.name}</p>
                        </div>
                    </div>
                {{/each}}            
            {{else}}
                <div>[[album.list.no.albums]]</div>
            {{/if}}
        </div>
    </div>
</div>