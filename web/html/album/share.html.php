<div>
    <div class="collection-share-modal add modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <button type="button" class="close" data-dismiss="modal"></button>
                <form name="collection-download" action="" method="post">
                    <div class="modal-header">
                        <p class="title" style="color:white;">[[album.share.modal.title]]</p>
                    </div>
                    <div class="modal-body">
                        <label>[[album.share.modal.internal.user]]</label>
                        <select class="ajax-existing-user" multiple="multiple" name="existing"></select>
                        <label>[[album.share.modal.external.user]]</label>
                        <select class="form-control external-user" multiple="multiple" name="new"></select>
                        <span class="error" style="display: none;">[[album.share.modal.error]]</span>
                    </div>
                    <div class="collection-download">
                        <button type="submit" class="button yellow share-album">[[album.share.modal.button]]</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
