<?php

require_once __DIR__ . '/../../startup.php';

ini_set("memory_limit", "8192M");

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use Pimcore\Model\DataObject;
use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;
use Pimcore\Model\Asset;

$db = Database::get();

$query = "SELECT id FROM assets WHERE type != 'folder'";

$results = $db->fetchAll($query);

foreach ($results as $index => $result) {
    ++$index;

    $assetInstance = Asset::getById($result['id']);

    if ($assetInstance && $assetInstance->getFileSize() == 0) {
        CliHelper::success("Delete $index from " . count($results));
        $assetInstance->delete();
    }
}

function dd($term)
{
    \Symfony\Component\VarDumper\VarDumper::dump($term);
    die;
}

