<?php

/**
 * Hinweise:
 * - Store muss angelegt sein und dem Skript mitgegeben werden!
 * - Nach dem  Import enthält die Datei group_ids.csv die erzeigten Gruppen-Ids des jeweiligen Stores - diese müssen dann noch der entsprechenden Eigenschaft hinzugefügt werden
 */

require_once '../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\Tools\ClassificationStore;

$errorMessage = "Bitte Datei und Classification-Store-Id für Import angeben: [Dateipfad] [Classification-Store-Id]  \nMit --delete [Classification-Store-Id] können alle Gruppen und Eigenschaften eines Stores gelöscht werden \nOptional: --silent";

if (isset($argv[3]) === true && $argv[3] === "--silent") {
    CliHelper::$echoOutput = false;
}

if(isset($argv[1]) === true && isset($argv[2]) === true && $argv[1] === "--delete") {
    CliHelper::info("Lösche Gruppen und Eigenschaften des Stores mit ID {$argv[2]}");
    
    ClassificationStore::deleteGroupsAndKeyDefinitions((int) $argv[2]);
    
    CliHelper::info("Aktion erfolgreich");
    die;
}

if (isset($argv[1]) === false) {
    CliHelper::error($errorMessage);
    die;
}

if (isset($argv[2]) === false) {
    CliHelper::error($errorMessage);
    die;
}

$filepath = __DIR__ . "/" . $argv[1];

if (is_file($filepath) === false) {
    CliHelper::error("$filepath ist keine gültige Datei!");
    die;
}

$classificationStoreId = (int) $argv[2];

if (isset($argv[3]) === true && $argv[3] === "--silent") {
    CliHelper::$echoOutput = false;
}

$csvInstance = new \TrueRomanceBundle\Library\CSV($filepath);

$groupedData = [];
foreach ($csvInstance->data as $lineData) {
    if (isset($groupedData[$lineData["name"]]) === false) {
        $groupedData[$lineData["name"]] = [];
    }

    $groupedData[$lineData["name"]][] = $lineData;
}

$groupIds = [];
foreach ($groupedData as $groupName => $groupData) {
    $groupConfig = ClassificationStore::createGroupIfNotExists($groupName, $classificationStoreId);
    
    $groupIds[] = $groupConfig->getId();
    
    CliHelper::success("Gruppe $groupName wurde erstellt");
    
    foreach ($groupData as $index => $attribute) {
        $keyConfig = ClassificationStore::createAttributeIfNotExists($attribute, $classificationStoreId);
        
        ClassificationStore::assignKeyToGroup($groupConfig, $keyConfig, $index);
        
        CliHelper::success("Key {$attribute["name"]} wurde erstellt und Gruppe $groupName zugeordnet");
    }
}

$groupIdsFile = "group_ids.csv";

file_put_contents($groupIdsFile, implode(",", $groupIds));

CliHelper::success("Datei $groupIdsFile wurde erstellt und enthält alle für den aktuellen Store erstellten Gruppen-Ids");

