<?php

/**
 * Update all product features by existing xml files
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\CSV;
use TrueRomanceBundle\Library\Import\Features;

ini_set("memory_limit", "8192M");

$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

if (isset($argv[1]) === false) {
    CliHelper::error("Bitte Datei für Import angeben! Optional: --silent");
    die;
}

$filepath = __DIR__ . "/" . $argv[1];

if (is_file($filepath) === false) {
    CliHelper::error("$filepath ist keine gültige Datei!");
    die;
}

if (isset($argv[2]) === false) {
    CliHelper::error("Bitte Classification Store Id angeben!");
    die;
}

$classificationStoreId = (int) $argv[2];

if (isset($argv[3]) === true && $argv[3] === "--silent") {
    CliHelper::$echoOutput = false;
}

try {
    $xmlNormalizer = new \TrueRomanceBundle\Library\Import\Xml\Normalizer();

    $xmlReader = new \TrueRomanceBundle\Library\Import\File\Xml\Reader($filepath, $xmlNormalizer);

    $xmlData = new TrueRomanceBundle\Library\Import\Xml\Data($xmlReader);

    $productData = $xmlData->get(["T_NEW_CATALOG", "PRODUCT"]);
    
    $propertiesFiles = "../../../data/import/proficlass/Merkmale.csv";
    
    $csvInstance = new CSV();

    $csvInstance->auto($propertiesFiles);   

    \Pimcore\Model\Version::disable();
    
    Features::import($productData, $csvInstance->data, $classificationStoreId);
} catch (\Exception $ex) {
    CliHelper::error($ex->getMessage());
    if (method_exists($ex, "getSubItems") === true) {
        foreach ($ex->getSubItems() as $subItem) {
            /** @var $subItem \Pimcore\Model\Element\ValidationException * */
            CliHelper::error($subItem->getMessage());
        }
    }
    
    CliHelper::error("");
    CliHelper::info($ex->getTraceAsString());
}

$filename = strtolower(str_replace(".xml", "", basename($filepath)));

CliHelper::saveFile("import_log_{$filename}_" . date("Y-m-d_H-i-s") .".log");
