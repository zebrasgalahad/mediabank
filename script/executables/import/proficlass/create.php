<?php

/**
 * Imports complete proficlass entities as classification store groups and key definitions
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\CSV;
use TrueRomanceBundle\Library\Import\Proficlass;

function format_time($t, $f = ':') // t = seconds, f = separator 
{
    return sprintf("%02d%s%02d%s%02d", floor($t / 3600), $f, ($t / 60) % 60, $f, $t % 60);
}

if (isset($argv[1]) === false) {
    CliHelper::error("Bitte Classification Store Id angeben!");
    die;
}

$classificationStoreId = (int) $argv[1];

$filepaths = [
    "einheiten" => "../../../data/import/proficlass/Einheiten.csv",
    "klassen" => "../../../data/import/proficlass/Klassen.csv",
    "klassenmerkmalewerte" => "../../../data/import/proficlass/KlassenMerkmaleWerte_rel.csv",
    "klassenmerkmale" => "../../../data/import/proficlass/KlassenMerkmale_rel.csv",
    "klassenschlagworte" => "../../../data/import/proficlass/KlassenSchlagworte_rel.csv",
    "merkmale" => "../../../data/import/proficlass/Merkmale.csv",
    "schlagworte" => "../../../data/import/proficlass/Schlagworte.csv",
    "werte" => "../../../data/import/proficlass/Werte.csv",
    
];

$data = [];
if(file_exists('cache_csv') === true) {
    $data = unserialize(file_get_contents('cache_csv'));
} else {
    foreach ($filepaths as $key => $filepath) {
        $csvInstance = new CSV();

        $csvInstance->auto($filepath);   

        $data[$key] = $csvInstance->data;
    }    
    
    file_put_contents('cache_csv', serialize($data));
}

\Pimcore\Model\Version::disable();

Proficlass::importUnits($data["einheiten"]);

Proficlass::importClasses($data, $classificationStoreId);