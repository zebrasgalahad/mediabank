<?php

/**
 * Deletes all items from a specific classification store (groups and key definitions) by store id
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

if (isset($argv[1]) === false) {
    CliHelper::error("Bitte Classification Store Id angeben!");
    die;
}

$classificationStoreId = (int) $argv[1];

$groupListing = new \Pimcore\Model\DataObject\Classificationstore\GroupConfig\Listing();

$groupListing->addConditionParam("storeId = $classificationStoreId");

foreach ($groupListing->load() as $group) {
    CliHelper::success("Delete group with id " . $group->getId());
    
    $group->delete();
}

$keyConfigListing = new \Pimcore\Model\DataObject\Classificationstore\KeyConfig\Listing();

$keyConfigListing->addConditionParam("storeId = $classificationStoreId");

foreach ($keyConfigListing->load() as $keyConfig) {
    CliHelper::success("Delete key with id " . $keyConfig->getId());
    
    $keyConfig->delete();
}