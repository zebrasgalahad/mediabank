<?php

require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "8192M");

if (isset($argv[1]) === false) {
    CliHelper::error("Bitte Datei für Import angeben! Optional: --silent");
    die;
}

$filepath = __DIR__ . "/" . $argv[1];

if (is_file($filepath) === false) {
    CliHelper::error("$filepath ist keine gültige Datei!");
    die;
}

if (isset($argv[2]) === true && $argv[2] === "--silent") {
    CliHelper::$echoOutput = false;
}

try {
    $xmlNormalizer = new \TrueRomanceBundle\Library\Import\Xml\Normalizer();

    $xmlReader = new \TrueRomanceBundle\Library\Import\File\Xml\Reader($filepath, $xmlNormalizer);

    $xmlData = new TrueRomanceBundle\Library\Import\Xml\Data($xmlReader);

    $xml = new \TrueRomanceBundle\Library\Import\Xml($xmlData);

    $xml->import();
} catch (\Exception $ex) {
    CliHelper::error($ex->getMessage());
    if (method_exists($ex, "getSubItems") === true) {
        foreach ($ex->getSubItems() as $subItem) {
            /** @var $subItem \Pimcore\Model\Element\ValidationException * */
            CliHelper::error($subItem->getMessage());
        }
    }
    
    CliHelper::error("");
    CliHelper::info($ex->getTraceAsString());
}

$filename = strtolower(str_replace(".xml", "", basename($filepath)));

CliHelper::saveFile("import_log_{$filename}_" . date("Y-m-d_H-i-s") .".log");
