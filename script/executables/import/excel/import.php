<?php

require_once '../../startup.php';

ini_set("memory_limit", "8192M");

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\CSV;
use TrueRomanceBundle\Library\Tools\ObjectOption;

$directory = 'files';
$files = array_diff(scandir($directory), array('..', '.'));

$files = array_values($files);

if ($argv[1]) {
    $files = [$argv[1]];
}

function format_time($t, $f = ':') // t = seconds, f = separator 
{
    return sprintf("%02d%s%02d%s%02d", floor($t / 3600), $f, ($t / 60) % 60, $f, $t % 60);
}

foreach ($files as $index => $file) {
    $csvInstance = new CSV($file);

    $csvEntry = [
        "data" => $csvInstance->data,
        "header" => array_keys($csvInstance->data[0])
    ];

    $count = count($csvInstance->data);

    CliHelper::success("Entry count: " . $count);

    $packs = [];
    $addCounter = 0;
    $steps = 10;
    $startTime = time();
    $csvData = [];
    foreach ($csvInstance->data as $indexData => $data) {
        $packs[] = $data;
        
        if(count($packs) < $steps) {
            continue;
        }
        
        $commands = [];
        foreach ($packs as $index => $pack) {
            $serialized = json_encode($pack);

            file_put_contents("pack_data_{$index}.txt", $serialized);

            $commands[] = "php inc/import_fork.php $index";
        }

        $executableCommand = implode(" ; ", $commands);

        exec($executableCommand, $output, $return);

        $packs = [];

        $addCounter += $steps;

        $relapseTime = (time() - $startTime);

        $estimatedTime = ($relapseTime / $addCounter) * ($count - $addCounter);

        CliHelper::success("$addCounter of $count entries...");

        CliHelper::info("Estimated: " . format_time($estimatedTime) . " / Passed: " . format_time($relapseTime));

        $outputs = [];
        foreach ($output as $outputLine) {
            echo $outputLine . PHP_EOL;

            $outputs[] = $outputLine;
        }
    }
}

if(count($packs) > 0) {
        $commands = [];
        foreach ($packs as $index => $pack) {
            $serialized = json_encode($pack);

            file_put_contents("pack_data_{$index}.txt", $serialized);

            $commands[] = "php inc/import_fork.php $index";
        }

        $executableCommand = implode(" ; ", $commands);

        exec($executableCommand, $output, $return);

        $packs = [];

        $addCounter += $steps;

        $relapseTime = (time() - $startTime);

        $estimatedTime = ($relapseTime / $addCounter) * ($count - $addCounter);

        CliHelper::success("$addCounter of $count entries...");

        CliHelper::info("Estimated: " . format_time($estimatedTime) . " / Passed: " . format_time($relapseTime));
}