<?php

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\Tools\ObjectOption;


require_once '../../startup.php';

function convertNumber($value)
{
    $value = (float) str_replace(",", ".", $value);

    if (preg_match('/^\d+$/', $value) === 1) {
        $value = (int) $value;

        if ($value === 0) {
            return "";
        }

        return $value;
    }

    return $value;
}
$mapping = [
    "article_status" => function ($article, $attribute, $value) {
        return ObjectOption::keyToValue($article, $attribute, $value);
    },
    "Discontinued_from" => function ($article, $attribute, $value) {
        $xd = explode(".", $value);

        $dt = new DateTime();
        $dt->setTimestamp(mktime(0, 0, 0, $xd[1], $xd[0], $xd[2]));

        return $dt;
    },
    "Discontinued_product" => function ($article, $attribute, $value) {
        return $value === "NEIN" ? false : true;
    },
    "followup_article" => function ($article, $attribute, $value) {
        return Pimcore\Model\DataObject\Article::getBySUPPLIER_PID($value)->current();
    },
    "data_export_share" => function ($article, $attribute, $value) {
        $getter = "get" . ucfirst($attribute);

        $data = $article->$getter();

        if (!is_array($data)) {
            $data = [];
        }

        $data[] = $value;

        return $data;
    },
    "MANUFACTURER_TYPE_DESCR" => function ($article, $attribute, $value) {
        return $value;
    },
    "Brand" => function ($article, $attribute, $value) {
        return $value;
    },
    "new_from" => function ($article, $attribute, $value) {
        $xd = explode(".", $value);

        $dt = new DateTime();
        $dt->setTimestamp(mktime(0, 0, 0, $xd[1], $xd[0], $xd[2]));

        return $dt;
    },
    "PRODUCT_STATUS_NEW_PRODUCT" => function ($article, $attribute, $value) {
        return $value === "NEIN" ? false : true;
    },
    "PRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "customer_special_price_amount" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "PRODUCT_PRICE_NRP_PRICE_AMOUNT" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "price_nrp_quantity" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price2_amount" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price2_quantity" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price3_amount" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price3_quantity" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price4_amount" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price4_quantity" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price5_amount" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price5_quantity" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price6_amount" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "list_price6_quantity" => function ($article, $attribute, $value) {
        return convertNumber($value);
    },
    "DISCOUNT_GROUP_MANUFACTURER" => function ($article, $attribute, $value) {
        return $value;
    },
    "description_short2" => function ($article, $attribute, $value) {
        return $value;
    },
];

$index = $argv[1];

$data = json_decode(file_get_contents("pack_data_{$index}.txt"), true);

\Pimcore\Cache\Runtime::getInstance()->clear();

$article = Pimcore\Model\DataObject\Article::getBySUPPLIER_PID($data["SUPPLIER_PID"])->current();

if (!$article) {
    file_put_contents("errors.log", PHP_EOL . "Article not found: {$data["SUPPLIER_PID"]}", FILE_APPEND);
} else {
    $arrayKeys = array_keys($data);

    unset($arrayKeys[0]);

    foreach ($arrayKeys as $arrayKey) {
        try {
            $value = $mapping[$arrayKey]($article, $arrayKey, $data[$arrayKey]);

            $setter = "set" . ucfirst($arrayKey);

            $article->$setter($value);
        } catch (\Exception $ex) {
            CliHelper::error($ex->getMessage());

            $error = [
                "value" => $value,
                "key" => $key
            ];

            CliHelper::error(print_r($error, true));
        }
    }
    
    $article->save();
}

      