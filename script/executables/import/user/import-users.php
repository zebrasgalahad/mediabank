<?php

require_once '../../startup.php';

define("SCRIPT_DIR_ROOT", PIMCORE_PROJECT_ROOT . "/script/");

define("PRODUCT_FILES_DIR_ROOT", PIMCORE_PROJECT_ROOT . "/files/products/");

require_once PIMCORE_PROJECT_ROOT . '/src/TrueRomanceBundle/Library/CSV.php';

use Pimcore\Model\DataObject\Customer;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

$dataCsv = SCRIPT_DIR_ROOT . '/data/import/customers/190418_customer_import.csv';

$csvObject = new \TrueRomanceBundle\Library\CSV();

$csvObject->auto($dataCsv);

$csvData = $csvObject->data;

####################################Importiere Customers#############################################

foreach ($csvData as $index => $csvEntry) {
    
      $customer = new Customer();
      /* @var $customer Customer */
      
      $userGroups = [];
      
      $userGroupNames = explode(",", $csvEntry["UserGroup"]);
      
      foreach ($userGroupNames as $group) {
          $userGroupInstance = \Pimcore\Model\DataObject\UserGroup::getByName($group)->current();
          
          if ($userGroupInstance) {
              $userGroups[] = $userGroupInstance;
          }
      }
      
      $userCountries = explode(",", $csvEntry["Permission/Countries"]);
      
      $key = Pimcore\File::getValidFilename($csvEntry["username"]);
      
      $customer->setKey($key);
      
      $customer->setCustomerNumber($csvEntry["customerNumber"]);
      
      $customer->setFirstname($csvEntry["firstname"]);
      
      $customer->setLastname($csvEntry["lastname"]);
      
      $customer->setEmail("test_" . $csvEntry["email"]);
      
      $departmentValue = \TrueRomanceBundle\Library\Tools\ObjectOption::keyToValue($customer, "department", $csvEntry["department"]);
      
      $customer->setDepartment($departmentValue);
      
      $customer->setCompany($csvEntry["company"]);
      
      $customer->setCountries($userCountries);
      
      $customer->setUserGroup($userGroups);
      
      $customer->setRegistered_Language("de_DE");
      
      $customer->setTerms_Accepted(true);
      
      $customer->setActive(false);
      
      $customer->setPublished(false);
      
      CliHelper::success("Creating user {$csvEntry["firstname"]} {$csvEntry["lastname"]}");
      
      $customer->save();
}

CliHelper::success("Import finished.");

