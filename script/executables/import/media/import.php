<?php

require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\CSV;
use \TrueRomanceBundle\Library\Import\Asset\Folder as AssetFolder;
use TrueRomanceBundle\Library\Import\Asset;
use Pimcore\Model\WebsiteSetting;

ini_set("memory_limit", "8096M");

$asyncJobHelperService = \Pimcore::getContainer()->get('trueromance.import.async_job_helper');
/* @var $asyncJobHelperService \TrueRomanceBundle\Services\AsyncJobHelper */

$dbBackupService = \Pimcore::getContainer()->get('trueromance.tools.database.backup');
/* @var $dbBackupService \TrueRomanceBundle\Services\Backup\Database */

$mailService = \Pimcore::getContainer()->get('trueromance.tools.mail');
/* @var $mailService TrueRomanceBundle\Services\Mail */

$mailHelper = \Pimcore::getContainer()->get('trueromance.tools.mail.helper');
/* @var $mailHelper TrueRomanceBundle\Services\MailHelper */

$mailSubject = $mailHelper->getAssetImportEmailSubject();

$mailText = $mailHelper->getAssetImportEmailText();

$mailErrorSubject = $mailHelper->getAssetImportEmailErrorSubject();

$mailErrorText = $mailHelper->getAssetImportEmailErrorText();

$mailBccs = $mailHelper->getAssetImportBccEmails();

$explodedBcc = explode(",", $mailBccs);

$cleanBccs = [];

foreach ($explodedBcc as $bcc) {
    if (!filter_var(trim($bcc), FILTER_VALIDATE_EMAIL)) {
        continue;
    }

    $cleanBccs[trim($bcc)] = trim($bcc);
}

$dbTable = $asyncJobHelperService::DB_TABLE;

$type = $asyncJobHelperService::TYPE_ASSET_IMPORT;

$currentJob = $asyncJobHelperService->getCurrentJob($type);

$locale = $currentJob['Locale'];

if(!WebsiteSetting::getByName("php_executable")) {
    CliHelper::error("No PHP executable path found! Please check for path 'php_executable' in Website Settings!");
    die;
}

$phpExecutable = WebsiteSetting::getByName("php_executable")->getData();

if(!is_file($phpExecutable)) {
    CliHelper::error("PHP executable is no file! Please check for path 'php_executable' in Website Settings!");
    die;
}

if ($currentJob) {

    $currentJobId = $currentJob["id"];

    $userData = $asyncJobHelperService->getUserDataByJobId($currentJobId, $type);

    $serializedData = unserialize($currentJob["SerializedData"]);

    $unserializedData = $serializedData["data"];

    $uploadedFilename = $serializedData["filename"];

    if ($dbBackupService->backup() === false) {
        $asyncJobHelperService->setFinishedStatus((int) $currentJobId, $type);
        $asyncJobHelperService->setJobTableStartDate((int) $currentJobId, $type);
        $asyncJobHelperService->setJobTableEndDate((int) $currentJobId, $type);
        $asyncJobHelperService->insertErrorMessageInJobTable("Import asset task kann nicht fortgesetzt werden, "
                . "da das Datenbankdump nicht erstellt werden konnte.", $currentJobId, $type);

        $mailErrorText .= "\nDatei: $uploadedFilename";

        $mailErrorText .= "\nErstellungsdatum: {$currentJob["CreationDate"]}";

        $mailService->send($userData["email"], $cleanBccs, $mailErrorSubject, $mailErrorText);

        die;
    }

    Asset::moveAssetsFromFtpToServer();

    $folders = [];
    foreach ($unserializedData as $singleLine) {
        if (in_array($singleLine["Mime_Purpose"], $folders) === false) {
            $folders[] = $singleLine["Mime_Purpose"];
        }
    }

    asort($folders);

    CliHelper::info("Check target folders...");
    foreach ($folders as $folder) {
        AssetFolder::createIfNotExists($folder);
    }

    CliHelper::info("Check done, now importing assets...");

    $filename = strtolower(str_replace(".xml", "", basename($filepath)));

    $filepath = "import_log_{$filename}_" . date("Y-m-d_H-i-s") . ".log";

    if (is_file($filepath)) {
        unlink($filepath);
    }

    $outputs = [];

    function format_time($t, $f = ':') // t = seconds, f = separator
    {
        return sprintf("%02d%s%02d%s%02d", floor($t / 3600), $f, ($t / 60) % 60, $f, $t % 60);
    }

    $addCounter = 0;
    $steps = 10;
    $startTime = time();
    $entityCounter = 0;
    $count = count($unserializedData);

    $asyncJobHelperService->setInProgressStatus((int) $currentJobId, $type, $locale);

    $asyncJobHelperService->setJobTableStartDate((int) $currentJobId, $type, $locale);

    foreach ($unserializedData as $index => $singleLine) {
        $singleLine['locale'] = $locale;

        $serialized = json_encode($singleLine);

        file_put_contents(__DIR__ . "/pack_data.txt", $serialized);

        ++$entityCounter;

        $execPath = __DIR__ . "/inc/import_fork.php";

        $command = "$phpExecutable $execPath";

        exec($command, $output, $return);

        $relapseTime = (time() - $startTime);

        $estimatedTime = ($relapseTime / $entityCounter) * ($count - $entityCounter);

        CliHelper::success("$entityCounter of $count media files...");

        CliHelper::info("Estimated: " . format_time($estimatedTime) . " / Passed: " . format_time($relapseTime));

        $outputs = [];
        if ($output) {
            foreach ($output as $outputLine) {
                //echo $outputLine . PHP_EOL;

                $outputs[] = $outputLine;
            }
        }

        file_put_contents($filepath, implode("\n\r", $outputs), FILE_APPEND);
    }

    $mailText .= "\nDatei: $uploadedFilename";

    $mailText .= "\nErstellungsdatum: {$currentJob["CreationDate"]}";

    $asyncJobHelperService->setFinishedStatus((int) $currentJobId, $type, $locale);

    $asyncJobHelperService->setJobTableEndDate((int) $currentJobId, $type, $locale);

    $mailService->send($userData["email"], $cleanBccs, $mailSubject, $mailText);

    Asset::deleteFilesFromBaseFolder();
}
