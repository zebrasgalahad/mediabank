<?php

require_once '../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "4096M");

$classes = [
    Pimcore\Model\DataObject\ArticleAssets\Listing::class,
];

foreach ($classes as $class) {
    $listingObject = new $class();
    
    CliHelper::info("Lösche alle Element von Klasse {$class}:");
    
    foreach ($listingObject as $counter => $element) {
        $element->delete();
    }
}