<?php

class ArticleAssetsImport
{

    public function import(\Doctrine\DBAL\Connection $dbConnection, $incPath)
    {
        $articleAssets = $dbConnection->fetchAll("SELECT * FROM object_query_1000");

        $tables = [
            'object_collection_AssetImage_1000' => 'object_collection_AssetImage_localized_1000',
            'object_collection_AssetFile_1000' => 'object_collection_AssetFile_localized_1000',
            'object_collection_AssetVideo_1000' => 'object_collection_AssetVideo_localized_1000',
        ];

        $targetConnection = \Pimcore\Db::get();

        $count = count($articleAssets);
        foreach ($articleAssets as $index => $articleAsset) {
            foreach ($tables as $sourceTable => $targetTable) {
                $counter = $index + 1;

                $query = "SELECT * FROM $sourceTable WHERE o_id = :id";

                $entries = $dbConnection->fetchAll($query, ["id" => $articleAsset["oo_id"]]);

                if ($entries && count($entries) >= 0) {
                    foreach ($entries as $entry) {
                        if ($sourceTable === "object_collection_AssetImage_1000") {
                            $data = [
                                'asset_type' => $entry['asset_type'],
                                'image_type' => $entry['image_type'],
                                'image_orientation' => $entry['image_orientation'],
                                'allowed_countries' => $entry['allowed_countries'],
                            ];
                        }

                        if ($sourceTable === "object_collection_AssetFile_1000") {
                            $data = [
                                'asset_type' => $entry['asset_type'],
                                'salesdoc_type' => $entry['salesdoc_type'],
                                'allowed_countries' => $entry['allowed_countries'],
                            ];
                        }

                        if ($sourceTable === "object_collection_AssetVideo_1000") {
                            $data = [
                                'asset_type' => $entry['asset_type'],
                                'video_type' => $entry['video_type'],
                                'allowed_countries' => $entry['allowed_countries'],
                            ];
                        }

                        try {
                            $targetConnection->update($targetTable, $data, ['ooo_id' => $articleAsset["oo_id"], "language" => "de_DE"]);
                        } catch (\Exception $e) {
                            Symfony\Component\VarDumper\VarDumper::dump($targetTable);
                            Symfony\Component\VarDumper\VarDumper::dump($data);
                            Symfony\Component\VarDumper\VarDumper::dump($e->getMessage());
                            die;
                        }

                    }
                }

                $message = "{$counter} of {$count} article assets";

                \TrClassUpdateBundle\Library\Cli\Helper::success($message);
                error_log($message);
            }
        }
    }
}
