<?php

class CatalogImport
{
    private function getMappings()
    {
        return [
            'SUPPLIER_SUPPLIER_ID_SUPPLIER_SPECIFIC' => null,
            'SUPPLIER_SUPPLIER_NAME' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_NAME' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_STREET' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_ZIP' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_CITY' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_STATE' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_COUNTRY' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_PHONE' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_FAX' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_EMAIL' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_URL' => null,
            'SUPPLIER_ADDRESS_SUPPLIER_ADDRESS_REMARKS' => null,
        ];
    }

    public function import(\Doctrine\DBAL\Connection $dbConnection, $incPath)
    {
        $catalogs = $dbConnection->fetchAll("SELECT * FROM object_query_400");

        $count = count($catalogs);
        foreach ($catalogs as $index => $catalog) {
            $counter = $index + 1;

            $message = "{$counter} of {$count} catalogs";

            \TrClassUpdateBundle\Library\Cli\Helper::success($message);
            error_log($message);

            $this->importSingleCatalog($catalog);
        }
    }

    public function importSingleCatalog($catalog)
    {
        $catalogInstance = \Pimcore\Model\DataObject\Catalog::getById($catalog['oo_id']);

        if ($catalogInstance) {
            foreach ($this->getMappings() as $mappingKey => $mappingValue) {
                $setterKey = str_replace('__id', '', $mappingKey);

                $setter = "set" . ucfirst($setterKey);

                $value = is_callable($mappingValue) ? $mappingValue($catalog[$mappingKey]) : $catalog[$mappingKey];

                $catalogInstance->$setter($value);
            }

            $catalogInstance->save();
        }
    }
}
