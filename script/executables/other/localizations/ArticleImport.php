<?php

class ArticleImport
{
    private function getMappings()
    {
        return [
            'PRODUCT_STATUS_NEW_PRODUCT' => null,
            'article_status' => null,
            'new_from' => function ($value) {
                $carbon = new \Carbon\Carbon();

                $carbon->setTimestamp($value);

                return $carbon;
            },
            'new_until' => function ($value) {
                $carbon = new \Carbon\Carbon();

                $carbon->setTimestamp($value);

                return $carbon;
            },
            'Discontinued_product' => null,
            'Discontinued_from' => function ($value) {
                $carbon = new \Carbon\Carbon();

                $carbon->setTimestamp($value);

                return $carbon;
            },
            'followup_article__id' => function ($value) {
                if ($value) {
                    return \Pimcore\Model\DataObject::getById($value);
                }

                return $value;
            },
            'Predecessor_article__id' => function ($value) {
                if ($value) {
                    return \Pimcore\Model\DataObject::getById($value);
                }

                return $value;
            },
            'data_export_share' => null,
            'SAP_EXTRACT' => null,
            'DATETIME_VALID_START_DATE' => function ($value) {
                $carbon = new \Carbon\Carbon();

                $carbon->setTimestamp($value);

                return $carbon;
            },
            'DATETIME_VALID_END_DATE' => function ($value) {
                $carbon = new \Carbon\Carbon();

                $carbon->setTimestamp($value);

                return $carbon;
            },
            'DISCOUNT_GROUP_MANUFACTURER' => null,
            'master_service' => null
        ];
    }

    public function import(\Doctrine\DBAL\Connection $dbConnection, $incPath)
    {
        $mappings = $this->getMappings();

        $articles = $dbConnection->fetchAll("SELECT * FROM object_query_100");

        $phpExcecutable = \Pimcore\Model\WebsiteSetting::getByName("php_executable")->getData();

        $count = count($articles);
        foreach ($articles as $index => $article) {
//            dd($article);

            file_put_contents($incPath . "/article.serialized", serialize($article));

            $counter = $index + 1;

            $message = "{$counter} of {$count} articles";

            \TrClassUpdateBundle\Library\Cli\Helper::success($message);
            error_log($message);

            $command = "{$phpExcecutable} {$incPath}/articles.php";

            exec($command);
        }
    }

    public function importSingleArticle($article)
    {
        $articleInstance = \Pimcore\Model\DataObject\Article::getById($article['oo_id']);

        foreach ($this->getMappings() as $mappingKey => $mappingValue) {
            $setterKey = str_replace('__id', '', $mappingKey);

            $setter = "set" . ucfirst($setterKey);

            $value = is_callable($mappingValue) ? $mappingValue($article[$mappingKey]) : $article[$mappingKey];

            $articleInstance->$setter($value);
        }

        $articleInstance->save();
    }
}
