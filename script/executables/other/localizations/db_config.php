<?php

require_once __DIR__ . '/../../startup.php';

$connectionParams = array(
    'dbname' => 'sbd_pimcore',
    'user' => 'root',
    'password' => 'root',
    'host' => '127.0.0.1',
    'port' => '3306',
    'driver' => 'pdo_mysql',
);

$connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
