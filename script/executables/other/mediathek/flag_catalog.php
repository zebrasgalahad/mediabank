<?php

/**
 * Script for setting 'item_mediathek' flag in fieldcollections for artice asset objects
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

//error_reporting(E_ALL);

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

$assetIds = TrueRomanceBundle\Library\Database\AbstractDatabase::get()->fetchAll("SELECT oo_id FROM object_query_500");

$targetProperties = [
    "img_logo",
    "img_icon",
    "video_asset",
    "file_certificate",
    "data_bundle",
    "catalog_download",
    "pricelist_download",
];

$assetCount = count($assetIds);
foreach ($assetIds as $index => $articleIdData) {
    $currentCount = $index + 1;
    
    $assetInstance = \Pimcore\Model\DataObject\CatalogAssets::getById($articleIdData["oo_id"]);
    
    CliHelper::success("{$currentCount} von {$assetCount} - ID: " . $assetInstance->getId());
    
    foreach ($targetProperties as $targetProperty) {
        $getter = "get" . ucfirst($targetProperty);
        
        $fieldCollectionInstance = $assetInstance->$getter();
        
        if($fieldCollectionInstance !== null && is_object($fieldCollectionInstance) === true) {
            /* @var $fieldCollectionInstance Pimcore\Model\DataObject\Fieldcollection */
            $setter = "set" . ucfirst($targetProperty);
            
            if( $fieldCollectionInstance->getCount() === 0) {
                continue;
            }
            
            foreach ($fieldCollectionInstance->getItems() as &$fieldCollectionItem) {
                $fieldCollectionItem->setItem_mediathek(true);
            }
            
            $assetInstance->$setter($fieldCollectionInstance);
        }
    }
    
    $assetInstance->save();
}




