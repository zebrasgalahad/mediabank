<?php

/**
 * Script for setting 'item_bmecat' flag in fieldcollections for article assets property 'file_productdatasheet'
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

//error_reporting(E_ALL);

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

\Pimcore\Model\Version::disable();

$articleAssetIds = TrueRomanceBundle\Library\Database\AbstractDatabase::get()->fetchAll("SELECT oq.oo_id FROM object_query_100 oq, objects o
WHERE oq.oo_id = o.o_id
AND o.o_published = 1");

$targetProperties = ["file_productdatasheet"];

$assetCount = count($articleAssetIds);
foreach ($articleAssetIds as $index => $articleIdData) {
    $assetInstanceId = TrueRomanceBundle\Library\Database\AbstractDatabase::get()->fetchOne("SELECT o_id FROM objects WHERE o_parentId = ?", [$articleIdData["oo_id"]]);
    
    if(!$assetInstanceId) {
        CliHelper::error("Article with id {$articleIdData["oo_id"]} has no assets!");
        
        continue;
    }
    
    $currentCount = $index + 1;
    
    $assetInstance = \Pimcore\Model\DataObject\ArticleAssets::getById($assetInstanceId);
    
    CliHelper::success("{$currentCount} von {$assetCount} - ID: " . $assetInstance->getId());
    
    foreach ($targetProperties as $targetProperty) {
        $getter = "get" . ucfirst($targetProperty);
        
        $fieldCollectionInstance = $assetInstance->$getter();
        
        if($fieldCollectionInstance !== null && is_object($fieldCollectionInstance) === true) {
            /* @var $fieldCollectionInstance Pimcore\Model\DataObject\Fieldcollection */
            if( $fieldCollectionInstance->getCount() === 0) {
                continue;
            }
            
            foreach ($fieldCollectionInstance->getItems() as &$fieldCollectionItem) {
                $fieldCollectionItem->setItem_bmecat(true);
            }
            
            $setter = "set" . ucfirst($targetProperty);
            
            $assetInstance->$setter($fieldCollectionInstance);
        } else {
            CliHelper::info("No productinformations found for ArticleAssets ID: " . $assetInstance->getId());
        }
    }
    
    $assetInstance->save();
    
    \Pimcore::collectGarbage();
}

CliHelper::saveFile();


