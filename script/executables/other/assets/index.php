<?php

/**
 * Converts new localized fields from old schema to new one (reuqires old database configured in db_config.php)
 */
require_once __DIR__ . '/../../startup.php';
require_once  'AssetConvert.php';
require_once 'db_config.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

\Pimcore\Model\Version::disable();

$incPath = __DIR__ . "/inc/";

$task = new \TrueRomanceBundle\Library\Index\Task(\Pimcore::getContainer());

$catalog = \Pimcore\Model\DataObject\Catalog::getById(18191);

$task->updateCatalogAssets($catalog);

$task->updateCatalogEntry($catalog);

$task->updateSingleAsset(31966); // Article

$task->updateSingleAsset(18191); // Catalog

$task->updateSingleAsset(82197); // AssetFile

$task->updateSingleAsset(82198); // AssetImage

$task->updateSingleAsset(82368); // AssetVideo
