<?php

require_once __DIR__ . '/../../../startup.php';
require_once  __DIR__ . '/../AssetConvert.php';
require_once __DIR__ . '/../db_config.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

\Pimcore\Model\Version::disable();

extract(unserialize(file_get_contents(__DIR__ . '/convert.serialized')));

$assetConvert = new AssetConvert($connection);

$assetConvert->convert($articleAsset, $supportedLocale);
