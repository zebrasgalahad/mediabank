<?php

/**
 * Converts new localized fields from old schema to new one (reuqires old database configured in db_config.php)
 */
require_once __DIR__ . '/../../startup.php';
require_once  'AssetConvert.php';
require_once 'db_config.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

\Pimcore\Model\Version::disable();

$incPath = __DIR__ . "/inc/";

$supportedLocales = \Pimcore\Tool::getValidLanguages();

$articleAssets = $connection->fetchAll("SELECT oq.*, o.* FROM object_query_1000 oq, objects o
WHERE oq.oo_id = o.o_id");

$tempFile = __DIR__ . "/inc/convert.serialized";

$articleAssetsCount = count($articleAssets);
foreach ($articleAssets as $index => $articleAsset) {
    $articleAssetInstance = \Pimcore\Model\DataObject\ArticleAssets::getById($articleAsset['oo_id']);

    if (!$articleAssetInstance) {
        continue;
    }

    $currentCount = $index + 1;

    echo PHP_EOL . "$currentCount of $articleAssetsCount";
    error_log($returnLine . PHP_EOL);

    foreach ($supportedLocales as $supportedLocale) {

        file_put_contents($tempFile, serialize([
            'articleAsset' => $articleAsset,
            'supportedLocale' => $supportedLocale
        ]));

        $command = $phpExecutable . " " .  __DIR__ . "/inc/convert.php";

        $return = [];
        exec($command, $return);

//        foreach ($return as $returnLine) {
//            echo $returnLine . PHP_EOL;
//            error_log($returnLine . PHP_EOL);
//        }
//
//        $assetConvert = new AssetConvert($connection);
//        $assetConvert->convert($articleAsset, $supportedLocale);
    }
}
