<?php

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

class AssetConvert
{
    /**
     * @var \Doctrine\DBAL\Connection
     */
    private $dbConnection;

    public function __construct(\Doctrine\DBAL\Connection $dbConnection)
    {
        $this->dbConnection = $dbConnection;
    }

    public function convert ($assetArticle, $locale) {
        $this->createAssets($assetArticle, $locale);
    }

    public function convertCatalogAssets ($catalogAsset, $supportedLocale) {
        $types = [
            "File",
            "Image",
            "Video"
        ];

        foreach ($types as $assetType) {
            CliHelper::success("{$assetType} for ID {$catalogAsset['oo_id']}");

            $this->createCatalogAssetsByType($catalogAsset, $supportedLocale, $assetType);
        }

        $catalogAssetArticleInstance = \Pimcore\Model\DataObject\CatalogAssets::getById($catalogAsset['oo_id']);

        if($catalogAssetArticleInstance) {
            $catalogAssetArticleInstance->delete();
        }
    }

    private function createCatalogAssetsByType($assetArticle, $locale, $type) {
        if($type === "File") {
            $query = "SELECT oca.*, ocal.*, orel.dest_id as asset_relation FROM object_collection_AssetFile_500 as oca, object_collection_AssetFile_localized_500 ocal, object_relations_500 as orel
WHERE oca.o_id = ocal.ooo_id
AND oca.o_id = orel.src_id
AND oca.fieldname = ocal.fieldname
AND oca.index = ocal.index
AND ocal.language = ?
AND oca.o_id = ?
GROUP BY oca.fieldname, oca.index";
        } elseif($type === "Image") {
            $query = "SELECT oca.*, ocal.* FROM object_collection_Asset{$type}_500 as oca, object_collection_Asset{$type}_localized_500 ocal
WHERE oca.o_id = ocal.ooo_id
AND ocal.language = ?
AND oca.fieldname = ocal.fieldname
AND oca.index = ocal.index
AND oca.o_id = ?
group by oca.fieldname, oca.index";
        } elseif($type === "Video") {
            $query = "SELECT oca.*, ocal.* FROM object_collection_Asset{$type}_500 as oca, object_collection_Asset{$type}_localized_500 ocal
WHERE oca.o_id = ocal.ooo_id
AND ocal.language = ?
AND oca.fieldname = ocal.fieldname
AND oca.index = ocal.index
AND oca.o_id = ?
group by oca.fieldname, oca.index";
        }

        $fileAssets = $this->dbConnection->fetchAll($query, [$locale, $assetArticle['oo_id']]);

        if(!$fileAssets && count($fileAssets) === 0) {
            return;
        }

        $fileAssetsParent = \Pimcore\Model\DataObject::getById($assetArticle['o_parentId']);

        foreach ($fileAssets as $index => $fileAsset) {
            $fileAsset = $this->prepareArticleAssetData($fileAsset);

            $keyCount = $index + 1;

            $assetObjectKey = ucfirst($type) . "_{$keyCount}";

            $targetPath = $fileAssetsParent->getRealFullPath() . "/" . $assetObjectKey;

            $assetObject = \Pimcore\Model\DataObject::getByPath($targetPath);

            if(!$assetObject) {
                if($type === "File") {
                    $assetObject = new Pimcore\Model\DataObject\AssetFile();
                } elseif($type === "Image") {
                    $assetObject = new Pimcore\Model\DataObject\AssetImage();
                } elseif($type === "Video") {
                    $assetObject = new Pimcore\Model\DataObject\AssetVideo();
                }

                $assetObject->setKey($assetObjectKey );
                $assetObject->setParent($fileAssetsParent);
                $assetObject->setPublished(true);
            }

            if ($type !== "Video") {
                $asset = \Pimcore\Model\Asset::getById($fileAsset['asset_relation']);
            } else {
                $assetRelationData = unserialize($fileAsset['asset_relation']);

                $assetInstance = \Pimcore\Model\Asset::getById($assetRelationData['data']);

                $videoAssetData = new \Pimcore\Model\DataObject\Data\Video();
                $videoAssetData->setData($assetInstance);
                $videoAssetData->setType($assetRelationData['data']);
                $videoAssetData->setTitle($assetRelationData['title']);
                $videoAssetData->setDescription($assetRelationData['title']);

                $asset = $videoAssetData;
            }

            if($fileAsset['allowed_countries']) {
                $fileAsset['allowed_countries'] = explode(",", $fileAsset['allowed_countries']);
            }

            if($asset) {
                $fileAsset['asset_relation'] = $asset;
            } else {
                CliHelper::error("No asset found for {$fileAsset['asset_relation']} - adding sample image");

                $fileAsset['asset_relation'] = \Pimcore\Model\Asset::getById(4937);
            }

            $assetObject = $this->setArticleAssetData($assetObject, $fileAsset, $locale);

            $assetObject->save();

            CliHelper::info("Asset object saved ({$assetObject->getId()})");
        }
    }

    public function createAssets($assetArticle, $locale) {
        $types = [
            "File",
            "Image",
            "Video"
        ];

        foreach ($types as $assetType) {
            CliHelper::success("{$assetType} for ID {$assetArticle['oo_id']}");

            $this->createAssetsByType($assetArticle, $locale, $assetType);
        }

        $assetArticleInstance = \Pimcore\Model\DataObject\ArticleAssets::getById($assetArticle['oo_id']);

        if($assetArticleInstance) {
            $assetArticleInstance->delete();
        }
    }

    private function createAssetsByType($assetArticle, $locale, $type) {
        if($type === "File") {
            $query = "SELECT oca.*, ocal.*, orel.dest_id as asset_relation FROM object_collection_AssetFile_1000 as oca, object_collection_AssetFile_localized_1000 ocal, object_relations_1000 as orel
WHERE oca.o_id = ocal.ooo_id
AND oca.o_id = orel.src_id
AND oca.fieldname = ocal.fieldname
AND oca.index = ocal.index
AND ocal.language = ?
AND oca.o_id = ?
GROUP BY oca.fieldname, oca.index";
        } elseif($type === "Image") {
            $query = "SELECT oca.*, ocal.* FROM object_collection_Asset{$type}_1000 as oca, object_collection_Asset{$type}_localized_1000 ocal
WHERE oca.o_id = ocal.ooo_id
AND ocal.language = ?
AND oca.fieldname = ocal.fieldname
AND oca.index = ocal.index
AND oca.o_id = ?
group by oca.fieldname, oca.index";
        } elseif($type === "Video") {
            $query = "SELECT oca.*, ocal.* FROM object_collection_Asset{$type}_1000 as oca, object_collection_Asset{$type}_localized_1000 ocal
WHERE oca.o_id = ocal.ooo_id
AND ocal.language = ?
AND oca.fieldname = ocal.fieldname
AND oca.index = ocal.index
AND oca.o_id = ?
group by oca.fieldname, oca.index";
        }

        $fileAssets = $this->dbConnection->fetchAll($query, [$locale, $assetArticle['oo_id']]);

        if(!$fileAssets && count($fileAssets) === 0) {
            return;
        }

        $fileAssetsParent = \Pimcore\Model\DataObject::getById($assetArticle['o_parentId']);

        foreach ($fileAssets as $index => $fileAsset) {
            $fileAsset = $this->prepareArticleAssetData($fileAsset);

            $keyCount = $index + 1;

            $assetObjectKey = ucfirst($type) . "_{$keyCount}";

            $targetPath = $fileAssetsParent->getRealFullPath() . "/" . $assetObjectKey;

            $assetObject = \Pimcore\Model\DataObject::getByPath($targetPath);

            if(!$assetObject) {
                if($type === "File") {
                    $assetObject = new Pimcore\Model\DataObject\AssetFile();
                } elseif($type === "Image") {
                    $assetObject = new Pimcore\Model\DataObject\AssetImage();
                } elseif($type === "Video") {
                    $assetObject = new Pimcore\Model\DataObject\AssetVideo();
                }

                $assetObject->setKey($assetObjectKey );
                $assetObject->setParent($fileAssetsParent);
                $assetObject->setPublished(true);
            }

            if ($type !== "Video") {
                $asset = \Pimcore\Model\Asset::getById($fileAsset['asset_relation']);
            } else {
                $assetRelationData = unserialize($fileAsset['asset_relation']);

                $assetInstance = \Pimcore\Model\Asset::getById($assetRelationData['data']);

                $videoAssetData = new \Pimcore\Model\DataObject\Data\Video();
                $videoAssetData->setData($assetInstance);
                $videoAssetData->setType($assetRelationData['data']);
                $videoAssetData->setTitle($assetRelationData['title']);
                $videoAssetData->setDescription($assetRelationData['title']);

                $asset = $videoAssetData;
            }

            if($fileAsset['allowed_countries']) {
                $fileAsset['allowed_countries'] = explode(",", $fileAsset['allowed_countries']);
            }

            if($asset) {
                $fileAsset['asset_relation'] = $asset;
            } else {
                CliHelper::error("No asset found for {$fileAsset['asset_relation']} - adding sample image");

                $fileAsset['asset_relation'] = \Pimcore\Model\Asset::getById(4937);
           }

            $assetObject = $this->setArticleAssetData($assetObject, $fileAsset, $locale);

            $assetObject->save();

            CliHelper::info("Asset object saved ({$assetObject->getId()})");
        }
    }

    private function prepareArticleAssetData ($assetData) {
        unset($assetData['ooo_id']);
        unset($assetData['o_id']);
        unset($assetData['index']);
        unset($assetData['fieldname']);

        return $assetData;
    }

    private function setArticleAssetData (\Pimcore\Model\DataObject $articleAssetObject, array $data, string $locale) {
        foreach ($data as $key => $value) {
            $setter = "set" . ucfirst($key);

            if(!method_exists($articleAssetObject, $setter)) {
                continue;
            }

            try {
                $articleAssetObject->$setter($value, $locale);
            } catch (\Excpetion $e) {
                $articleAssetObject->$setter($value);
            }
        }

        return $articleAssetObject;
    }

}
