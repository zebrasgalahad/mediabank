<?php

/**
 * Converts new localized fields from old schema to new one (reuqires old database configured in db_config.php)
 */
require_once __DIR__ . '/../../startup.php';
require_once  'AssetConvert.php';
require_once 'db_config.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

\Pimcore\Model\Version::disable();

$incPath = __DIR__ . "/inc/";

$supportedLocales = \Pimcore\Tool::getValidLanguages();

$catalogAssets = $connection->fetchAll("SELECT oq.*, o.* FROM object_query_500 oq, objects o
WHERE oq.oo_id = o.o_id");

$catalogsAssetsCount = count($catalogAssets);
foreach ($catalogAssets as $index => $catalogAsset) {
    $currentCount = $index + 1;

    CliHelper::success("$currentCount of $catalogsAssetsCount ({$catalogAsset['o_key']})");

    foreach ($supportedLocales as $supportedLocale) {
        $assetConvert = new AssetConvert($connection);
        $assetConvert->convertCatalogAssets($catalogAsset, $supportedLocale);
    }
}
