<?php

/**
 * Script for setting 'item_mediathek' flag in fieldcollections for artice asset objects
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\Index\Task as IndexTask;

//error_reporting(E_ALL);

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

$assetProperties = \TrueRomanceBundle\Library\Import\Asset::$mapping;
$targetProperties = [];
foreach ($assetProperties as $assetProperty) {
    if(in_array($assetProperty["field"], $targetProperties) === false) {
        $targetProperties[] = $assetProperty["field"];
    }
}

$indexService = new IndexTask(\Pimcore::getContainer());

$indexService->skipImageCreating = true;

### ArticleAssets
$assetIds = TrueRomanceBundle\Library\Database\AbstractDatabase::get()->fetchAll("SELECT oo_id FROM object_query_1000");

$assetCount = count($assetIds);
foreach ($assetIds as $index => $articleIdData) {
    $currentCount = $index + 1;
    
    $assetInstance = \Pimcore\Model\DataObject\ArticleAssets::getById($articleIdData["oo_id"]);
    
    CliHelper::success("ARTICLEASSET - {$currentCount} von {$assetCount} - ID: " . $assetInstance->getId());
    foreach ($targetProperties as $targetProperty) {
        $getter = "get" . ucfirst($targetProperty);
        
        $fieldCollectionInstance = $assetInstance->$getter();
        
        if($fieldCollectionInstance !== null && is_object($fieldCollectionInstance) === true) {
            /* @var $fieldCollectionInstance Pimcore\Model\DataObject\Fieldcollection */
            $setter = "set" . ucfirst($targetProperty);
            
            if( $fieldCollectionInstance->getCount() === 0) {
                continue;
            }
                
            foreach ($fieldCollectionInstance->getItems() as &$fieldCollectionItem) {
                if(in_array($targetProperty, ["img_award", "img_Heroshot" , "img_application", "img_product_other"])) {
                    $fieldCollectionItem->setItem_mediathek(true);
                    $fieldCollectionItem->setItem_bmecat(true);
                }   
                
                if(in_array($targetProperty, ["file_pr_text", "file_productdatasheet", "video_asset"])) {
                    $fieldCollectionItem->setItem_mediathek(true);
                    $fieldCollectionItem->setItem_bmecat(false);
                }                         
            }
            
            $assetInstance->$setter($fieldCollectionInstance);
        }
    }
    
    $assetInstance->save();
    
    $indexService->updateSingleAsset($assetInstance->getId());
}

$catalogTargetProperties = [
    "img_logo",
    "img_icon",
    "video_asset",
    "file_certificate",
    "data_bundle",
    "catalog_download",
    "pricelist_download"
];

### CatalogAssets
$catalogAssetIds = TrueRomanceBundle\Library\Database\AbstractDatabase::get()->fetchAll("SELECT oo_id FROM object_query_500");

$assetCount = count($catalogAssetIds);
foreach ($catalogAssetIds as $index => $articleAssetIdData) {
    $currentCount = $index + 1;
    
    $catalogAssetInstance = \Pimcore\Model\DataObject\CatalogAssets::getById($articleAssetIdData["oo_id"]);
    
    CliHelper::success("CATALOGASSET - {$currentCount} von {$assetCount} - ID: " . $catalogAssetInstance->getId());
    
    foreach ($catalogTargetProperties as $targetProperty) {
        $getter = "get" . ucfirst($targetProperty);
        
        $fieldCollectionInstance = $catalogAssetInstance->$getter();
        
        if($fieldCollectionInstance !== null && is_object($fieldCollectionInstance) === true) {
            /* @var $fieldCollectionInstance Pimcore\Model\DataObject\Fieldcollection */
            $setter = "set" . ucfirst($targetProperty);
            
            if( $fieldCollectionInstance->getCount() === 0) {
                continue;
            }
                
            foreach ($fieldCollectionInstance->getItems() as &$fieldCollectionItem) {
                $fieldCollectionItem->setItem_mediathek(true);
                $fieldCollectionItem->setItem_bmecat(false);
            }
            
            $catalogAssetInstance->$setter($fieldCollectionInstance);
        }
    }
    
    $catalogAssetInstance->save();
    
    $indexService->updateSingleCatalogAsset($catalogAssetInstance, "de_DE");
}

CliHelper::saveFile("SBD-64-Activity.log");



