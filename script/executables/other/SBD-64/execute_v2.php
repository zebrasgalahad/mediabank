<?php

/**
 * Script for setting 'item_mediathek' flag in fieldcollections for artice asset objects
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\Index\Task as IndexTask;
use TrueRomanceBundle\Library\Database\AbstractDatabase as Database;

//error_reporting(E_ALL);

ini_set("memory_limit", "8192M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

$assetProperties = \TrueRomanceBundle\Library\Import\Asset::$mapping;
$targetProperties = [];
foreach ($assetProperties as $assetProperty) {
    if(in_array($assetProperty["field"], $targetProperties) === false) {
        $targetProperties[] = $assetProperty["field"];
    }
}

$indexService = new IndexTask(\Pimcore::getContainer());

$catalogPaths = [
   "catalogs/katalog-stanley-fatmax",
   "catalogs/katalog-dewalt-flexvolt",
];

foreach ($catalogPaths as $catalogPath) {
    $sql = "SELECT o_id as id FROM objects WHERE o_className = 'ArticleAssets' AND o_published = 1 AND o_path LIKE '%$catalogPath%'";

    $ids = Database::getDatabaseObject()->fetchAll($sql);

    CliHelper::success("Katalog-Pfad: $catalogPath");
    
    $idsCount = count($ids);
    foreach ($ids as $index => $id) {
        $indexService->updateSingleAsset($id["id"]);
        
        $currentCount = $index + 1;
        
        CliHelper::success("$currentCount von $idsCount wurden aktualisiert (ID: {$id["id"]})");
    }
}

CliHelper::saveFile("SBD-64-Activity_v2.log");



