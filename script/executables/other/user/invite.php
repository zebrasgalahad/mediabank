<?php

require_once '../../startup.php';

use UserAuthenticationBundle\Library\Security\UserToken as UserTokenUtility;

ini_set("memory_limit", "8096M");

if(!$argv[1]) {
    die("Parent-ID für Ordner muss angegeben werden!" . PHP_EOL);
}

$parentId = $argv[1];

$folder = \Pimcore\Model\DataObject\Folder::getById($parentId);

$folderPath = $folder->getRealFullPath();

$customerListing = new \Pimcore\Model\DataObject\Customer\Listing();

$customerListing->setCondition("o_path LIKE '%$folderPath%'");

foreach ($customerListing as $userIndex => $user) {
    if(substr($user->getEmail(), 0, 5) === "test_") {
        $user->setEmail(substr($user->getEmail(), 5));
    }
    
    $user->setActive(true);
    
    $user->save();
    
    $count = $userIndex + 1;
    
    error_log("({$count}) SEND INVITATION-MAIL TO: " . $user->getEmail());
    
    $tokenInstance = UserTokenUtility::create($user);
    
    $kernel->getContainer()->get("userauthentication.mailer")->sendInitialPasswordMail($tokenInstance, "http://mediathek.sbdinc.de/de/login/passwort-vergessen");    
}