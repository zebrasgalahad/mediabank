<?php

require_once __DIR__. '/../../../startup.php';

use Pimcore\Db;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CLiHelper;

$assetId = $argv[1];

$asset = Pimcore\Model\Asset::getById($assetId);

if($asset->getType() === "folder") {
    CLiHelper::error("Asset is of type folder - skipping...");
    
    exit;
}

if(!$asset) {
    CLiHelper::error("No asset with id {$assetId} found - not deleting asset...");
    
    exit;
}

$message = "Asset with id {$assetId} found - now deleting asset...";
CLiHelper::error($message);

$db = Db::get();

$query = 'select * from dependencies where targetid = ' . $assetId;

$dependencies = $db->fetchAll($query);

CLiHelper::success("Dependencies count: " . count($dependencies) . ", memory usage: " . formatBytes(memory_get_usage()));

if (count($dependencies) === 0) {
    CLiHelper::success($asset->getId() . ": No dependencies found - deleting asset...");

    $asset->delete();
    
    exit;
}

CLiHelper::success($asset->getId() . ": Dependencies found - not deleting asset...");
