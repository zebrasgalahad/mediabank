<?php

require_once '../../startup.php';

ini_set("memory_limit", "10240M");

use Pimcore\Db;
use TrueRomanceBundle\Library\Tools\Cli\Helper as CLiHelper;

$assetListing = new Pimcore\Model\Asset\Listing();

$assets = Db::get()->fetchAll("SELECT * FROM assets");

foreach ($assets as $asset) {
    $assetId = $asset["id"];
    
    if ($assetId == 1) {
        continue;
    }
    
    $command = "php inc/fork.php $assetId";

    exec($command, $output, $return);
    
    CLiHelper::success("Main process memory usage: " . formatBytes(memory_get_usage()));
    
    $outputs = [];
    if ($output) {
        foreach ($output as $outputLine) {
            echo $outputLine . PHP_EOL;

            $outputs[] = $outputLine;
        }
    }
}