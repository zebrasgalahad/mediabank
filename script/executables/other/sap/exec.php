<?php

require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\Import\Pimcore\Folder;
use TrueRomanceBundle\Services\AsyncJobHelper;
use Pimcore\Model\WebsiteSetting;
use Pimcore\Log\ApplicationLogger;

ini_set("memory_limit", "8096M");

// sap => pimcore
$fieldMappings = [
    "SKU" => "SUPPLIER_PID",
    "MINORDERQUANTITY" => "QUANTITY_MIN",
    "STEPQUANTITY" => "STEPQUANTITY",
    "QUANTITYUNIT" => "ORDER_UNIT",
    "SHORTDESCRIPTION" => "DESCRIPTION_SHORT",
    "CATEGORYLINKN" => "Code",
    "AASTANDARDEAN" => "EAN",
    "AASTANDARDVK" => "PRODUCT_PRICE_NRP_PRICE_AMOUNT",
    "HEK_EURO" => "PRODUCT_PRICE_NET_CUSTOMER_PRICE_AMOUNT",
    "HEK_WAERUNG" => "PRODUCT_PRICE_nrp_PRICE_CURRENCY",
    "STATWANR" => "PRODUCT_LOGISTIC_DETAILS_CUSTOMS_NUMBER",
    "MFGSKU" => "MANUFACTURER_TYPE_DESCR",
    "PACKINGLENGTH" => "PACKING_UNITS_LENGTH",
    "PACKINGLENGTHUNIT" => "PACKING_UNITS_LENGTH_UNIT",
    "PACKINGWIDTH" => "PACKING_UNITS_WIDTH",
    "PACKINGWIDTHUNIT" => "PACKING_UNITS_WIDTH_UNIT",
    "PACKINGHEIGHT" => "PACKING_UNITS_DEPTH",
    "PACKINGHEIGHTUNIT" => "PACKING_UNITS_DEPTH_UNIT",
    "GROSSWEIGHT" => "PACKING_UNITS_WEIGHT",
    "GROSSWEIGHTUNIT" => "PACKING_UNITS_WEIGHT_UNIT"
];

$asyncJobHelperService = \Pimcore::getContainer()->get('trueromance.import.async_job_helper');

$mailHelper = \Pimcore::getContainer()->get('trueromance.tools.mail.helper');
/* var $mailHelper TrueRomanceBundle\Services\MailHelper */

$mailSubject = $mailHelper->getSapImportEmailSubject();

$mailText = $mailHelper->getSapImportEmailText();

$mailErrorSubject = $mailHelper->getSapImportEmailErrorSubject();

$mailErrorText = $mailHelper->getSapImportEmailErrorText();

$mailBccs = $mailHelper->getSapImportBccEmails();

$explodedBcc = explode(",", $mailBccs);

$cleanBccs = [];

foreach ($explodedBcc as $bcc) {
    if (!filter_var(trim($bcc), FILTER_VALIDATE_EMAIL)) {
        continue;
    }

    $cleanBccs[trim($bcc)] = trim($bcc);
}
/* @var $asyncJobHelperService AsyncJobHelper */

$currentJob = $asyncJobHelperService->getCurrentJob(AsyncJobHelper::TYPE_SAP_IMPORT);

if(!$currentJob) {
    return;
}

$currentJobId = $currentJob["id"];

$userData = $asyncJobHelperService->getUserDataByJobId($currentJobId, AsyncJobHelper::TYPE_SAP_IMPORT);

$databaseBackup = \Pimcore::getContainer()->get("trueromance.tools.database.backup");

$mailService = \Pimcore::getContainer()->get('trueromance.tools.mail');
if($databaseBackup->backup() === false) {
    $asyncJobHelperService->setFinishedStatus((int) $currentJobId, $type);
    $asyncJobHelperService->setJobTableStartDate((int) $currentJobId, $type);
    $asyncJobHelperService->setJobTableEndDate((int) $currentJobId, $type);
    $asyncJobHelperService->insertErrorMessageInJobTable("Datenbank-Backup konnte nciht erstellt werden", $currentJobId, $type);
    
    $mailService->send($userData["email"], $cleanBccs, "Datenbank Backup fehler!", "Fehler: Datenbank-Backup konnte nicht erstellt werden!");
    exit;    
}

$dataCollection = unserialize($currentJob["SerializedData"]);

$folderHelper = new Folder();

$parentFolder = $folderHelper->createFolderIfNotExists("00-NEW-SKU", "/");

$asyncJobHelperService->setInProgressStatus((int) $currentJobId, AsyncJobHelper::TYPE_SAP_IMPORT);

$asyncJobHelperService->setJobTableStartDate((int) $currentJobId, AsyncJobHelper::TYPE_SAP_IMPORT);

$phpExecutable = WebsiteSetting::getByName("php_executable")->getData();

$directory = __DIR__;
$forkCommand = "$phpExecutable {$directory}/fork.php";

$dataCount = count($dataCollection["data"]);
foreach ($dataCollection["data"] as $csvEntryIndex => $csvEntry) {
    $locale = str_replace("-", "_", $csvEntry["LOCALE"]);    
    
    foreach ($csvEntry as $key => $value) {
        if(is_numeric($key)) {
            unset($csvEntry[$key]);
        }
    }

    $resultItem = [];
    foreach ($fieldMappings as $sapField => $pimcoreField) {
        $resultItem[$pimcoreField] = $csvEntry[$sapField];
    }

    $resultItem["LOCALE"] = $csvEntry["IDLOCAL"];
    
    $logCount = $csvEntryIndex + 1;
    
    file_put_contents("temp.json", serialize($resultItem));

    exec($forkCommand, $output, $return);

    $logMessage = "SAP-IMPORT-TASK || {$logCount} of {$dataCount}: {$resultItem["SUPPLIER_PID"]}";
    
    ApplicationLogger::getInstance()->info($logMessage, ["component" => "SAP-IMPORT-TASK"]);
    
    $outputs = [];
    foreach ($output as $outputLine) {
        $singleOutput = $outputLine . PHP_EOL;
    }  
}

$asyncJobHelperService->setFinishedStatus((int) $currentJobId, AsyncJobHelper::TYPE_SAP_IMPORT);

$asyncJobHelperService->setJobTableEndDate((int) $currentJobId, AsyncJobHelper::TYPE_SAP_IMPORT);

$mailText .= "\nDateiname: " . $dataCollection["filename"];
    
$mailText .= "\nErstellungsdatum: {$currentJob["CreationDate"]}";

$mailService->send($userData["email"], $cleanBccs, $mailSubject, $mailText);

