<?php

require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\CSV;
use TrueRomanceBundle\Library\Import\Pimcore\Folder;

$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

$folderHelper = new Folder();

$parentFolder = $folderHelper->createFolderIfNotExists("00-NEW-SKU", "/");

$csvEntry = unserialize(file_get_contents("temp.json"));

$locale = str_replace("-", "_", $csvEntry["LOCALE"]);

unset($csvEntry["LOCALE"]);

$csvEntry["DESCRIPTION_SHORT"] = utf8_encode($csvEntry["DESCRIPTION_SHORT"]);

TrueRomanceBundle\Library\Import\CSV::import($csvEntry, $locale, $parentFolder);

