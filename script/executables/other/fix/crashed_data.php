<?php

/**
 * Script for setting 'item_mediathek' flag in fieldcollections for artice asset objects
 */
require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;
use TrueRomanceBundle\Library\CSV;

//error_reporting(E_ALL);

ini_set("memory_limit", "12000M");

// remove event listener because of performance reasons
$listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

$removeListener = null;
foreach ($listeners as $listener) {
    if($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
        $removeListener = $listener[0];
    }
}

if($removeListener) {
    \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
}

$csvInstance = new CSV("object_store_100.csv");

$articleAmount = count($csvInstance->data);

foreach ($csvInstance->data as $index => $entry) {
    $count = $index + 1;
    
    CliHelper::success("$count von $articleAmount: Aktualisiere Artikel {$entry["SUPPLIER_PID"]} (ID: {$entry["oo_id"]})");
    
    $article = Pimcore\Model\DataObject\Article::getById($entry["oo_id"]);
    
    if(is_numeric($entry["new_from"])) {
        $dt = new DateTime();
        $dt->setTimestamp((int)$entry["new_from"]);
        $article->setNew_from($dt);        
    } else {
        $article->setNew_from(null); 
    }
    
    if(is_numeric($entry["new_until"])) {
        $dt = new DateTime();
        $dt->setTimestamp((int)$entry["new_until"]);    
        $article->setNew_until($dt);        
    } else {
        $article->setNew_until(null); 
    }

    if(is_numeric($entry["Discontinued_from"])) {
        $dt = new DateTime();
        $dt->setTimestamp((int)$entry["Discontinued_from"]);        
        $article->setDiscontinued_from($dt);     
    } else {
        $article->setDiscontinued_from(null); 
    }
    
    $article->save();
}