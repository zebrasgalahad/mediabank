<?php

require_once __DIR__ . '/../../../startup.php';

use TrClassUpdateBundle\Library\Cli\Helper as CliHelper;

$filepath = __DIR__ . "/../pack_data.txt";

$outputBufferLogfile = __DIR__ . "/../output_buffer.log";

$data = json_decode(file_get_contents($filepath), true);

TrueRomanceBundle\Library\Cleanup\Assets::cleanup($data);

CliHelper::saveFile($outputBufferLogfile, "all", true);
