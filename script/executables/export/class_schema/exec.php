<?php

require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

$pimcoreSchemaDocumentRoot = PIMCORE_PROJECT_ROOT . "/schema/pimcore/";

$classesList = new \Pimcore\Model\DataObject\ClassDefinition\Listing();
$classesList->setOrderKey('name');
$classesList->setOrder('asc');

$classes = $classesList->load();

foreach ($classes as $class) {
    /* @var $class Pimcore\Model\DataObject\ClassDefinition */
    $name = $class->getName();
    
    $filename = "class_" . $name . "_export.json";
    
    $json = \Pimcore\Model\DataObject\ClassDefinition\Service::generateClassDefinitionJson($class);
    
    $targetFilepath = $pimcoreSchemaDocumentRoot . $filename;
    
    file_put_contents($targetFilepath, $json);
    
    CliHelper::success("Klasse {$name} wurde exportiert nach {$targetFilepath}.");
}

CliHelper::success("

███████╗███████╗██████╗ ██████╗ ██╗███████╗ ██████╗██╗  ██╗██╗
██╔════╝██╔════╝██╔══██╗██╔══██╗██║██╔════╝██╔════╝██║  ██║██║
█████╗  █████╗  ██║  ██║██║  ██║██║███████╗██║     ███████║██║
██╔══╝  ██╔══╝  ██║  ██║██║  ██║██║╚════██║██║     ██╔══██║╚═╝
██║     ███████╗██████╔╝██████╔╝██║███████║╚██████╗██║  ██║██╗
╚═╝     ╚══════╝╚═════╝ ╚═════╝ ╚═╝╚══════╝ ╚═════╝╚═╝  ╚═╝╚═╝
                                                              
");