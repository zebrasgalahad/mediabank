<?php

require_once __DIR__ . '/../../startup.php';

use TrueRomanceBundle\Library\Tools\Cli\Helper as CliHelper;

ini_set("memory_limit", "8192M");

$db = \TrueRomanceBundle\Library\Database\AbstractDatabase::get();

$query = "SELECT ooo_id FROM object_localized_query_AssetImage_de_DE WHERE asset_title = 'Bild 2 Heroshot 45'";

$results = $db->fetchAll($query);

if (!empty($results)) {
    $count = count($results);

    foreach ($results as $index => $result) {
        ++$index;

        $articleAssetInstance = \Pimcore\Model\DataObject::getById($result['ooo_id']);

        if ($articleAssetInstance) {
            $assetTitle = "Bild 2 Heroshot 45° (WEB1)";
            $articleAssetInstance->setAsset_title($assetTitle, 'de_DE')->save();
            CliHelper::success("Asset Title changed for ArticleAsset with ID: {$articleAssetInstance->getId()} ($index of $count)");
        }
    }
}
