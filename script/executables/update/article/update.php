<?php

require_once __DIR__ . '/../../startup.php';

ini_set("memory_limit", "8192M");

use Carbon\Carbon;
use TrueRomanceBundle\Library\Database\AbstractDatabase;
use Pimcore\Model\DataObject\Article;

$asyncJobHelperService = \Pimcore::getContainer()->get('trueromance.import.async_job_helper');
/* var $asyncJobHelperService \TrueRomanceBundle\Services\AsyncJobHelper */

$dbBackupService = \Pimcore::getContainer()->get('trueromance.tools.database.backup');
/* var $dbBackupService \TrueRomanceBundle\Services\Backup\Database */

$mailService = \Pimcore::getContainer()->get('trueromance.tools.mail');
/* var $mailService TrueRomanceBundle\Services\Mail */

$mailHelper = \Pimcore::getContainer()->get('trueromance.tools.mail.helper');
/* var $mailHelper TrueRomanceBundle\Services\MailHelper */

$mailSubject = $mailHelper->getDateChangeEmailSubject();

$mailText = $mailHelper->getDateChangeEmailText();

$mailErrorSubject = $mailHelper->getDateChangeEmailErrorSubject();

$mailErrorText = $mailHelper->getDateChangeEmailErrorText();

$mailBccs = $mailHelper->getDateChangeBccEmails();

$explodedBcc = explode(",", $mailBccs);

$cleanBccs = [];

foreach ($explodedBcc as $bcc) {
    if (!filter_var(trim($bcc), FILTER_VALIDATE_EMAIL)) {
        continue;
    }

    $cleanBccs[trim($bcc)] = trim($bcc);
}

$dbTable = $asyncJobHelperService::DB_TABLE;

$type = $asyncJobHelperService::TYPE_DATE_CHANGE;

$currentJob = $asyncJobHelperService->getCurrentJob($type);

if ($currentJob) {

    $currentJobId = $currentJob["id"];

    $userData = $asyncJobHelperService->getUserDataByJobId($currentJobId, $type);

    if ($dbBackupService->backup() === false) {
        $asyncJobHelperService->setFinishedStatus((int) $currentJobId, $type);
        $asyncJobHelperService->setJobTableStartDate((int) $currentJobId, $type);
        $asyncJobHelperService->setJobTableEndDate((int) $currentJobId, $type);
        $asyncJobHelperService->insertErrorMessageInJobTable("Artikel Preis Gültigkeit Prozess kann nicht fortgesetzt werden, "
                . "da das Datenbankdump nicht erstellt konnte.", $currentJobId, $type);

        $mailService->send($userData["email"], $cleanBccs, $mailErrorSubject, $mailErrorText);

        die;
    }

    if (is_array($currentJob["SerializedData"])) {
        $unserializedData = $currentJob["SerializedData"];
    } else {
        $unserializedData = unserialize($currentJob["SerializedData"]);
    }

    $dateFromSingleValues = explode("/", $unserializedData["dateFrom"]);
    $dateToSingleValues = explode("/", $unserializedData["dateTo"]);

    $locale = $unserializedData["locale"];

    $dates = [
        "startDateDay" => (int) $dateFromSingleValues[1],
        "startDateMonth" => (int) $dateFromSingleValues[0],
        "startDateYear" => (int) $dateFromSingleValues[2],
        "endDateDay" => (int) $dateToSingleValues[1],
        "endDateMonth" => (int) $dateToSingleValues[0],
        "endDateYear" => (int) $dateToSingleValues[2]
    ];

    \Pimcore\Model\Version::disable();

    $listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.preUpdate");

    $removeListener = null;
    foreach ($listeners as $listener) {
        if ($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
            $removeListener = $listener[0];
        }
    }

    if ($removeListener) {
        \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.preUpdate", $listeners[0]);
    }

    $listeners = \Pimcore::getEventDispatcher()->getListeners("pimcore.dataobject.postUpdate");

    $removeListener = null;
    foreach ($listeners as $listener) {
        if ($listener[0] instanceof TrueRomanceBundle\Library\EventListener) {
            $removeListener = $listener[0];
        }
    }

    if ($removeListener) {
        \Pimcore::getEventDispatcher()->removeListener("pimcore.dataobject.postUpdate", $listeners[0]);
    }

    $query = "SELECT oo_id FROM object_100";

    $articleListing = AbstractDatabase::get()->fetchAll($query);

    $start = Carbon::create($dates["startDateYear"], $dates["startDateMonth"], $dates["startDateDay"]);
    $end = Carbon::create($dates["endDateYear"], $dates["endDateMonth"], $dates["endDateDay"]);

    $count = count($articleListing);

    error_log("Starting process");

    $asyncJobHelperService->setInProgressStatus((int) $currentJobId, $type);

    $asyncJobHelperService->setJobTableStartDate((int) $currentJobId, $type);

    foreach ($articleListing as $index => $articleData) {
        ++$index;

        $article = Article::getById($articleData["oo_id"]);

        if (!$article) {
            continue;
        }

        $article->setDATETIME_VALID_START_DATE($start, $locale);
        $article->setDATETIME_VALID_END_DATE($end, $locale);
        $article->save();

        error_log("Time changed for article $index from $count (Article Id: {$article->getId()})");

        \Pimcore::collectGarbage();
    }

    $asyncJobHelperService->setFinishedStatus((int) $currentJobId, $type);

    $asyncJobHelperService->setJobTableEndDate((int) $currentJobId, $type);

    $mailService->send($userData["email"], $cleanBccs, $mailSubject, $mailText);
}
