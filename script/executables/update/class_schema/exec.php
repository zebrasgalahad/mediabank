<?php

require_once '../../startup.php';

if(isset($argv[1]) === false) {
	die("\033[31mUser-ID fÃ¼r das Erstellen von neuen Klassen fehlt (im Normalfall die trdev-User-Id)!\033[0m" . PHP_EOL);
}

$userId = $argv[1];

$pimcoreSchemaDocumentRoot = PIMCORE_PROJECT_ROOT . "/schema/pimcore/";

$singleFiles = preg_grep('/^([^.])/', scandir($pimcoreSchemaDocumentRoot));

$classJsonDefinition = [];
foreach ($singleFiles as $singleFile) {
    $data = file_get_contents($pimcoreSchemaDocumentRoot . $singleFile);
    
    $className = explode("_", $singleFile)[1];
    
    $classJsonDefinition[$className] = $data;
}

foreach ($classJsonDefinition as $className => $json) {
    $classDefiniton = \Pimcore\Model\DataObject\ClassDefinition::getByName($className);
    
    $className = preg_replace('/[^a-zA-Z0-9]+/', '', $className);
    $className = preg_replace("/^[0-9]+/", "", $className);
    
    if($classDefiniton === null) {
    $jsonData = json_decode($json, true);
        
        $classDefiniton = \Pimcore\Model\DataObject\ClassDefinition::create(['name' => $className, 'userOwner' => $userId, 'id' => $jsonData['id']]);

        $classDefiniton->save();
    }    
    
    $class = Pimcore\Model\DataObject\ClassDefinition::getByName($className);

    echo PHP_EOL . "Importiere/aktualisiere Pimcore-Klasse " . $className . PHP_EOL;

    $importClassFromJson = \Pimcore\Model\DataObject\ClassDefinition\Service::importClassDefinitionFromJson($class,
                    $json);
}

echo PHP_EOL . "Import/Aktualisierung abgeschlossen." . PHP_EOL;
